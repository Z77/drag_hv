package com.draghv.com;


import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

/**
 * Created by karthik on 06/02/16.
 */
@IgnoreExtraProperties
public class GetBusiness {
    String title;
    String about;
    String category;
    String sub_category;
    long createdAt;
    boolean is_verified;
    String objectId;
    //RatingValue rating;
    String status;
    long updatedAt;
    List<String> imageList;
    List<String> tags;
    List<String> payments;
    List<String> facility;
    //List<TitleValue> capacity;
    List<String> suitableevents;
    //List<TitleValue> seating;
    List<String> imagetextImageList;
    List<String> imagetextTextList;
    List<String> imagetextQualiList;
    List<String> toTiming;
    List<String> fromTiming;
    List<String> holiList;
    String infoHeading;
    List<String[]> infoData;
    String type;
    String userId;
    String picture;
    String businessType;
    String email,website,facebook_page,google_page,twitter;
    double latitude,longitude;
    boolean isfav;
    String template;
    int count;
    //BusinessAddress address;
    //List<TimingValue>timing;
    List<String> service;
    boolean isprivate;
    long followerscount;
    String phone;
    String exp;

    public GetBusiness() {
    }

//    public GetBusiness(String title, RatingValue rating, String picture, double latitude, double longitude) {
//        this.title = title;
//        this.rating = rating;
//        this.picture = picture;
//        this.latitude = latitude;
//        this.longitude = longitude;
//    }

    public int getCount() {
        return count;
    }

    public String getTemplate() {
        return template;
    }

    public double getLongitude() {
        return longitude;
    }

    public boolean getIsfav() {
        return isfav;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getGoogle_page() {
        return google_page;
    }

    public String getFacebook_page() {
        return facebook_page;
    }

    public String getWebsite() {
        return website;
    }

    public String getEmail() {
        return email;
    }

    public String getBusinessType() {
        return businessType;
    }

    public String getPicture() {
        return picture;
    }

    public String getUserId() {
        return userId;
    }

    public String getType() {
        return type;
    }
    public String getInfoHeading() {
        return infoHeading;
    }

//    public List<TitleValue> getSeating() {
//        return seating;
//    }

    public List<String> getSuitableevents() {
        return suitableevents;
    }

//    public List<TitleValue> getCapacity() {
//        return capacity;
//    }

    public List<String> getFacility() {
        return facility;
    }

    public List<String> getPayments() {
        return payments;
    }
    public List<String[]> getInfoData() {
        return infoData;
    }

    public List<String> getTags() {
        return tags;
    }

    public List<String> getImageList() {
        return imageList;
    }
    public List<String> getImagetextImageList() {
        return imagetextImageList;
    }
    public List<String> getImagetextTextList() {
        return imagetextTextList;
    }
    public List<String> getImagetextQualiList() {
        return imagetextQualiList;
    }
    public List<String> getToTiming() {
        return toTiming;
    }
    public List<String> getFromTiming() {
        return fromTiming;
    }
    public List<String> getHoliList() {
        return holiList;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public String getStatus() {
        return status;
    }

//    public RatingValue getRating() {
//        return rating;
//    }

    public String getObjectId() {
        return objectId;
    }

    public boolean getIs_verified() {
        return is_verified;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public String getSub_category() {
        return sub_category;
    }

    public String getAbout() {
        return about;
    }

    public String getCategory() {
        return category;
    }

    public String getTitle() {
        return title;
    }

//    public BusinessAddress getAddress() {
//        return address;
//    }

//    public List<TimingValue> getTiming() {
//        return timing;
//    }

    public List<String> getService() {
        return service;
    }

    public boolean getIsprivate() {
        return isprivate;
    }

    public long getFollowerscount() {
        return followerscount;
    }

    public String getPhone() {
        return phone;
    }

    public String getExp() {
        return exp;
    }
}
