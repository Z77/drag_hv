package com.draghv.com.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.draghv.com.Adapter.MemberAdapter;
import com.draghv.com.Adapter.SharedUserAdapter;
import com.draghv.com.MainActivity;
import com.draghv.com.Models.MemberPOJO;
import com.draghv.com.Models.MyItem;
import com.draghv.com.Models.SharedBoardUserPOJO;
import com.draghv.com.R;
import com.draghv.com.ZAuth;
import com.draghv.com.Zboard;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 28-Feb-17.
 */
public class FragmentShareBoard extends Fragment {

    View rootView;
    ListView userlist;//adapter useradapter
    FirebaseDatabase database;
    List<SharedBoardUserPOJO> userdatalist = new ArrayList<>();//we have shared board with this for show listing
    SharedUserAdapter useradapter;//userlis adapter

    List<MemberPOJO> alluserlist = new ArrayList<>();//all app user list for prompt
    List<HashMap<String, Object>> alluserdata; //app's user whole data

    static List<String> alreadySharedUser = new ArrayList<>();//already shared pepol user id list

    static HashMap<String, Object> sharedUserData;

    FloatingActionButton addmember;
    TextView nodata;
    Zboard zboard;
    ZAuth zAuth;

    public static Fragment instantiate(List<String> SharedUserList, HashMap<String, Object> shareduser_Data) {
        FragmentShareBoard fragmentShareBoard = new FragmentShareBoard();
        alreadySharedUser = SharedUserList;
        sharedUserData = shareduser_Data;
        return fragmentShareBoard;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_share_board, container, false);
        zAuth = new ZAuth();
        userlist = (ListView) rootView.findViewById(R.id.list_user);
        addmember = (FloatingActionButton) rootView.findViewById(R.id.fabAddshare_member);
        nodata = (TextView) rootView.findViewById(R.id.nodata);

        database = FirebaseDatabase.getInstance();
        alluserdata = new ArrayList<>();
        zboard = new Zboard();

        useradapter = new SharedUserAdapter(getContext(), userdatalist);
        userlist.setAdapter(useradapter);
        loadAppUser();
        loadData();

        Log.i("TAG", "already shared user id:" + alreadySharedUser);
        Log.i("TAG", "already shared user data:" + sharedUserData);
        if (((MainActivity) getActivity()).is_write)
            addmember.setVisibility(View.VISIBLE);
        else
            addmember.setVisibility(View.GONE);
        addmember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowUserList(alreadySharedUser);
                //Toast.makeText(getContext(), "Show user dialog", Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.setGroupVisible(R.id.main_menu_group, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void loadData() {
        //show user whose are already in shared board data
        if (alreadySharedUser != null && alreadySharedUser.size() > 0) {

            nodata.setVisibility(View.GONE);
            for (int i = 0; i < alreadySharedUser.size(); i++) {

                final String userid = alreadySharedUser.get(i);
                database.getReference().child("_user").child(userid).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                        Log.i("TAG", "user data:" + userdata);

                        HashMap<String, Object> map = (HashMap<String, Object>) sharedUserData.get(userid);
                        Log.i("TAG", "user data:" + userdata);

                        SharedBoardUserPOJO data = new SharedBoardUserPOJO();
                        data.setRead((Boolean) (map.get("read")));
                        data.setWrite((Boolean) map.get("write"));
                        data.setUserid((String) userdata.get("objectId"));

                        if (userdata.get("image") != null && userdata.get("name") != null) {
                            data.setUsername((String) userdata.get("name"));
                            data.setUserprofile(userdata.get("image").toString());
                        } else { //if (userdata.get("name") != null)

                            //memberPOJO = new MemberPOJO(userdata.get("name").toString(), "", false);

                            data.setUsername((String) userdata.get("name"));
                            data.setUserprofile("");
                        }
//                        else {
//                            Toast.makeText(getContext(), "No data of shared user", Toast.LENGTH_SHORT).show();
//                        }


                        userdatalist.add(data);
                        useradapter.notifyDataSetChanged();
                        Log.i("TAG", "shared user showing list:" + userdatalist.size());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

        } else {
            nodata.setVisibility(View.VISIBLE);
        }
    }

    public void loadAppUser() {
        database.getReference().child("_user").orderByChild("name").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("TAG", "data e:" + dataSnapshot.getKey());

                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    HashMap<String, Object> userdata = (HashMap<String, Object>) data.getValue();

                    if (!data.getKey().equals(zAuth.get())) {
                        alluserdata.add(userdata);
                        Log.i("TAG", "user data:" + userdata);
                        MemberPOJO memberPOJO = null;
                        if (userdata.get("image") != null && userdata.get("name") != null) {
                            memberPOJO = new MemberPOJO(userdata.get("name").toString(), userdata.get("image").toString(), false);
//                        username.add(userdata.get("name").toString());
//                        usernamedata.add(userdata);
                        } else {
                            //if (userdata.get("name") != null) {
//                        username.add(userdata.get("name").toString());
//                        usernamedata.add(userdata);
                            memberPOJO = new MemberPOJO(userdata.get("name").toString(), "", false);
                        }
                        alluserlist.add(memberPOJO);

                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void ShowUserList(final List<String> shareduser) {

        View prompt = LayoutInflater.from(getContext()).inflate(R.layout.prompt_assign, null, false);
        ListView userlist = (ListView) prompt.findViewById(R.id.assign_parent);
        final MemberAdapter adaper;
//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) userlist.getLayoutParams();
//        params.height = 500;
//        params.weight = 1;
//        userlist.setLayoutParams(params);
        int totalItemsHeight = 0;

        MaterialDialog users = new MaterialDialog.Builder(getContext())
                .title("Add user").titleColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .positiveText("Add").positiveColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .customView(prompt, true)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        ArrayList<String> added = new ArrayList<>();
                        ArrayList<String> removed = new ArrayList<>();
                        //userdatalist.clear();
                        Boolean found = false;
                        List<String> tempUserlist = new ArrayList<>();
                        for (int i = 0; i < alluserlist.size(); i++) {
                            if (alluserlist.get(i).getIsMember()) {
                                Log.i("TAG", "in all user data id=>name : " + alluserdata.get(i).get("objectId") + "=>" + alluserdata.get(i).get("name").toString());
                                Log.i("TAG", "user id=> name:" + alluserlist.get(i).getUsername());
                                tempUserlist.add(alluserdata.get(i).get("objectId").toString()); //to add in firebase;
                                alluserlist.get(i).setIsMember(false);//bcoz if pass true it show tick icon so conver to false
                                SharedBoardUserPOJO data = new SharedBoardUserPOJO();
                                data.setUserid((String) alluserdata.get(i).get("objectId"));
                                data.setUsername(alluserlist.get(i).getUsername());
                                data.setUserprofile(alluserlist.get(i).getUserprofile());
                                data.setRead(true);
                                data.setWrite(false);
                                if (!shareduser.contains(alluserdata.get(i).get("objectId"))) {
                                    userdatalist.add(data);//to show in list
                                    added.add(alluserdata.get(i).get("objectId").toString());
                                }
                                found = true;
                            }
                        }

                        Log.i("TAG", "ticked user:" + tempUserlist);
                        Log.i("TAG", "already shared user:" + alreadySharedUser);

                        //for removing that board from b_shared_board->userid->boardid
                        if (alreadySharedUser != null && alreadySharedUser.size() > 0) {
                            for (int i = 0; i < alreadySharedUser.size(); i++) {
                                final String userid = alreadySharedUser.get(i);
                                if (!tempUserlist.contains(userid)) {
                                    removed.add(userid);
                                    Log.i("TAG", "user remove from sharing" + userid);
                                    zboard.getShareBoardRoot().child(userid).child(MainActivity.BOARD_ID).removeValue();
                                    zboard.removeSharedWith(((MainActivity) getActivity()).ownerId, MainActivity.BOARD_ID, userid, null);
//                                    zboard.getShareBoardRoot().child(userid).addListenerForSingleValueEvent(new ValueEventListener() {
//                                        @Override
//                                        public void onDataChange(DataSnapshot dataSnapshot) {
//                                            for(DataSnapshot boardata:dataSnapshot.getChildren()){
//                                                Log.i("TAG","data++"+boardata);
//                                                HashMap<String,Object> map= (HashMap<String, Object>) boardata.getValue();
//                                                if(map.get("boardid").equals(MainActivity.BOARD_ID)){
//                                                    zboard.getShareBoardRoot().child(userid).child(boardata.getKey()).removeValue();
//                                                }
//                                            }
//                                        }
//                                        @Override
//                                        public void onCancelled(DatabaseError databaseError) {
//
//                                        }
//                                    });
                                }
                            }
                        }
                        for(int i=0;i<userdatalist.size();i++){
                            if(removed.contains(userdatalist.get(i).getUserid()))
                                userdatalist.remove(userdatalist.get(i));
                        }

                         alreadySharedUser = tempUserlist;//to show imidiate chages in list
                        useradapter.notifyDataSetChanged();

                        if (found)
                            nodata.setVisibility(View.GONE);
                        else
                            nodata.setVisibility(View.VISIBLE);

                        Log.i("TAG", "newly added user is:" + added);
                        Log.i("TAG", "newly removed user is:" + removed);

                        HashMap<String, Object> shared = new HashMap<>();
                        for (String id : added) {
                            HashMap<String, Object> bdata = new HashMap<>();
                            HashMap<String, Object> boarddata = new HashMap<>();
                            boarddata.put("createdAt", ServerValue.TIMESTAMP);
                            boarddata.put("updatedAt", ServerValue.TIMESTAMP);
                            boarddata.put("read", true);
                            boarddata.put("write", false);
                            shared.put(id, boarddata);
                            bdata.put(MainActivity.BOARD_ID, zAuth.get());
                            zboard.getShareBoardRoot().child(id).updateChildren(bdata, null);
                            zboard.addSharedWith(((MainActivity) getActivity()).ownerId, MainActivity.BOARD_ID, shared, null);

                            /*HashMap<String, Object> bdata = new HashMap<>();

                            HashMap<String, Object> boarddata = new HashMap<>();
                            boarddata.put("createdAt", ServerValue.TIMESTAMP);
                            boarddata.put("updatedAt", ServerValue.TIMESTAMP);
                            boarddata.put("read", true);
                            boarddata.put("write", false);
                            shared.put(id,boarddata);

//                            bdata.put("ownerid",zAuth.get());
//                            bdata.put("boardid",MainActivity.BOARD_ID);
//                            bdata.put("read",false);
//                            bdata.put("write",false);

                            bdata.put(MainActivity.BOARD_ID, zAuth.get());
                            //zboard.getShareBoardRoot().child(id).push().setValue(bdata);
                            //updateChildren(bdata, null);
                            zboard.getShareBoardRoot().child(id).updateChildren(bdata, null);*/
                        }

                        /*((MainActivity) getActivity()).addSharedUser(tempUserlist, shared);*/
                        //zboard.updateSharedWith(MainActivity.BOARD_ID, shared, null);
                        Log.i("TAG", "selected user list:" + tempUserlist);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {

                    }
                })
                .show();

        adaper = new MemberAdapter(alluserlist, getContext());
        userlist.setAdapter(adaper);
        for (int i = 0; i < alluserlist.size(); i++) {
            View item = adaper.getView(i, null, userlist);
            item.measure(0, 0);
            totalItemsHeight += item.getMeasuredHeight();
            ViewGroup.LayoutParams params1 = userlist.getLayoutParams();
            params1.height = totalItemsHeight;
            userlist.setLayoutParams(params1);
            userlist.requestLayout();
        }
        for (int i = 0; i < alluserdata.size(); i++) {

            if (shareduser != null && shareduser.contains(alluserdata.get(i).get("objectId"))) {
                alluserlist.get(i).setIsMember(true);
            }
        }

        userlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (alluserlist.get(position).getIsMember()) {
                    alluserlist.get(position).setIsMember(false);
                } else {
                    alluserlist.get(position).setIsMember(true);
                }
                adaper.notifyDataSetChanged();
            }
        });

    }
}
