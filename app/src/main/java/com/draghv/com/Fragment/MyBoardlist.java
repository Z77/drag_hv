package com.draghv.com.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.draghv.com.Adapter.BoardListAdapter;
import com.draghv.com.GetBoardList;
import com.draghv.com.Helper.RecyclerItemClickListener;
import com.draghv.com.MainActivity;
import com.draghv.com.Models.BoardListPOJO;
import com.draghv.com.R;
import com.draghv.com.ZAuth;
import com.draghv.com.Zboard;
import com.draghv.com.Zboard_archive;
import com.draghv.com.Zboard_history;
import com.draghv.com.ZobazeUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 28-Feb-17.
 */
public class MyBoardlist extends Fragment {
    View rootView;
    List<BoardListPOJO> boardList;
    List<GetBoardList> FB_boardList;
    BoardListAdapter boardListAdapter;
    RecyclerView lv_boardnames;
    FloatingActionButton addBoardfab;
    TextView nodata;
    Zboard zboardList;
    Zboard_history history;
    ZAuth zAuth;

    ArrayList<HashMap<String, Object>> boardColsData;
    ArrayList<HashMap<String, Object>> boardLableData;
    ArrayList<HashMap<String, Object>> colorLabelsList;

    public static String android_id;
    Boolean isDataChanged = false;
    ValueEventListener valueEventListener;

    public MyBoardlist() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_myboardlist, container, false);
        lv_boardnames = (RecyclerView) rootView.findViewById(R.id.boardList);
        RecyclerView.LayoutManager llm = new LinearLayoutManager(getContext());//default it will use vertical orientation
        lv_boardnames.setLayoutManager(llm);
        addBoardfab = (FloatingActionButton) rootView.findViewById(R.id.fabAddBoard);
        nodata = (TextView) rootView.findViewById(R.id.nodata);
        boardList = new ArrayList<>();
        boardColsData = new ArrayList<>();
        boardLableData = new ArrayList<>();
        colorLabelsList = new ArrayList<>();
        zboardList = new Zboard();
        history = new Zboard_history();
        zAuth = new ZAuth();
        Zboard_archive archive = new Zboard_archive();
        FB_boardList = new ArrayList<>();

        android_id = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.i("TAG", "device id:" + android_id);
        boardListAdapter = new BoardListAdapter(getContext(), boardList, this);
        lv_boardnames.setAdapter(boardListAdapter);


//        zboardList.getBOARD_ROOT("-KcR_w1EpYqZGMg4NJ0I").removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(Task<Void> task) {
//                Log.i("TAG", "Board deleted");
//                Toast.makeText(getContext(), "Deleted success", Toast.LENGTH_SHORT).show();
//            }
//        });
//        history.getBoardHistory("-KcR_w1EpYqZGMg4NJ0I").removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.i("TAG","Board history deleted");
//
//            }
//        });
//        zboardList.getBOARD_DATA_ROOT().child("-KcR_w1EpYqZGMg4NJ0I").removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.i("TAG","Board cards deleted");
//            }
//        });
//        archive.getBOARD_ARCHIVEDATA("-KcR_w1EpYqZGMg4NJ0I").removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.i("TAG","Board archive cards removed");
//            }
//        });

        loadData();
//        lv_boardnames.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), lv_boardnames, new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//
////                if(view.getId()==R.id.boardlist_parent) {
//                Intent i = new Intent(getContext(), MainActivity.class);
//                i.putExtra("type", FB_boardList.get(position).getTitle());
//                i.putExtra("bid", FB_boardList.get(position).getObjectId());
//                i.putExtra("collist", FB_boardList.get(position).getTotalcol());// for not to load again firebase data for columns data
//                i.putExtra("otherdata", boardColsData);//other board cols data for move list,copy,move all,move
//                i.putExtra("othercolor", boardLableData);//other board color label data
//                i.putExtra("clrlbl", FB_boardList.get(position).getLabel());//default color lable which we have gave
//                i.putExtra("member", FB_boardList.get(position).getMember());
//                i.putExtra("sharewith", FB_boardList.get(position).getSharedwith());
//                i.putExtra("fromshare", false);
//                i.putExtra("edit", true);
//                i.putExtra("write", true);
//                startActivity(i);
////                }
////                else if(view.getId()==R.id.iv_favorite){
////                    Toast.makeText(getContext(),"Star",Toast.LENGTH_SHORT).show();
////                }
//                //zboardList.getROOT().child("e204681a69ff2d54").removeEventListener(valueEventListener);
//            }
//
//            @Override
//            public void onLongItemClick(View view, final int position) {
//                TextView tv_delete;
//                final MaterialDialog delete_dialog = new MaterialDialog.Builder(getContext())
//                        .customView(R.layout.prompt_board_option, true)
//                        .title("Board Actions").titleColorRes(R.color.colorPrimary)
//                        .show();
//                tv_delete = (TextView) delete_dialog.findViewById(R.id.delete_board);
//                tv_delete.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        showDeleteBoardPrompt(position);
//                        delete_dialog.dismiss();
//                    }
//                });
//                Toast.makeText(getContext(), "pos is:" + position, Toast.LENGTH_SHORT).show();
//            }
//        }));

        lv_boardnames.setLongClickable(true);
//        lv_boardnames.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
//                TextView tv_delete;
//                final MaterialDialog delete_dialog = new MaterialDialog.Builder(getContext())
//                        .customView(R.layout.prompt_board_option, true)
//                        .title("Board Actions").titleColorRes(R.color.colorPrimary)
//                        .show();
//                tv_delete = (TextView) delete_dialog.findViewById(R.id.delete_board);
//                tv_delete.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        showDeleteBoardPrompt(position);
//                        delete_dialog.dismiss();
//                    }
//                });
//                Toast.makeText(getContext(), "pos is:" + position, Toast.LENGTH_SHORT).show();
//                return true;
//            }
//        });
        addBoardfab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final MaterialDialog createBoard = new MaterialDialog.Builder(getContext())
                        .title("Add board").titleColorRes(R.color.colorPrimary)
                        .customView(R.layout.prompt_rename_header, true)
                        .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                        .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                        .positiveText("Create").positiveColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                        .negativeText("Cancel").negativeColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                //TextInputEditText promptTitle = (TextInputEditText) dialog.findViewById(R.id.tv_list_title);
                                EditText promptTitle = (EditText) dialog.findViewById(R.id.tv_list_title);
                                final String name = promptTitle.getText().toString().trim();
                                final String boardid = zboardList.save();
                                zboardList.setTitle(name);
                                //zboardList.setUserid(getString(R.string.userid));
                                zboardList.setUserid(zAuth.get());
                                zboardList.setcolorLabel(createColorLabelList());
                                final ArrayList<HashMap<String, Object>> headernames = (ArrayList<HashMap<String, Object>>) createHeaderlist();
                                zboardList.setTotalCol(headernames);
                                zboardList.saveboardWithListener(new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                        Toast.makeText(getContext(), "Data saved", Toast.LENGTH_SHORT).show();
                                        Intent i = new Intent(getContext(), MainActivity.class);
                                        i.putExtra("type", name);
                                        i.putExtra("bid", boardid);
                                        i.putExtra("collist", headernames);
                                        i.putExtra("otherdata", boardColsData);
                                        i.putExtra("othercolor", boardLableData);
                                        i.putExtra("clrlbl", colorLabelsList);
                                        i.putExtra("fromshare", false);
                                        i.putExtra("write", true);
                                        startActivity(i);
                                    }
                                });
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                EditText promptTitle = (EditText) createBoard.findViewById(R.id.tv_list_title);
                promptTitle.setHint("Board name");

            }
        });
        return rootView;
    }

    //called from @boardListAdapter
    public void RecyclerViewClick(int position) {
        Intent i = new Intent(getContext(), MainActivity.class);
        i.putExtra("type", FB_boardList.get(position).getTitle());
        i.putExtra("bid", FB_boardList.get(position).getObjectId());//using this get cards data
        i.putExtra("collist", FB_boardList.get(position).getTotalcol());// for not to load again firebase data for columns data
        i.putExtra("otherdata", boardColsData);//other board cols data for move list,copy,move all,move
        i.putExtra("othercolor", boardLableData);//other board color label data
        i.putExtra("clrlbl", FB_boardList.get(position).getLabel());//default color label which we have gave
        i.putExtra("member", FB_boardList.get(position).getMember());
        i.putExtra("sharewith", FB_boardList.get(position).getSharedwith());
        i.putExtra("fromshare", false);
        i.putExtra("edit", true);
        i.putExtra("write", true);
        startActivity(i);
    }

    public void loadData() {

        //zboardList.getROOT().child(getString(R.string.userid)).addValueEventListener(new ValueEventListener() {
        //valueEventListener = zboardList.getROOT().child("e204681a69ff2d54").addValueEventListener(new ValueEventListener() {
        valueEventListener = zboardList.getROOT().child(zAuth.get()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("TAG", "board data:" + dataSnapshot);
                isDataChanged = true;
                if (boardList.size() > 0) {
                    boardList.clear();
                }
                if (boardColsData.size() > 0) {
                    boardColsData.clear();
                }
                if (FB_boardList.size() > 0) {
                    FB_boardList.clear();
                }
                if (dataSnapshot.getChildrenCount() > 0) {
                    nodata.setVisibility(View.GONE);
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                        GetBoardList board_List = dataSnapshot1.getValue(GetBoardList.class);
                        if (board_List.getSharedwith() != null) {
                            boardList.add(new BoardListPOJO(board_List.getTitle(), false, board_List.getSharedwith().size()));
                        } else {
                            boardList.add(new BoardListPOJO(board_List.getTitle(), false, 0));
                        }
                        boardListAdapter.notifyDataSetChanged();
                        FB_boardList.add(board_List);

                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("bid", board_List.getObjectId());
                        hashMap.put("name", board_List.getTitle());
                        hashMap.put("coldata", board_List.getTotalcol());
                        boardColsData.add(hashMap);

                        HashMap<String, Object> lablemap = new HashMap<>();
                        lablemap.put("bid", board_List.getObjectId());
                        lablemap.put("name", board_List.getTitle());
                        lablemap.put("labeldata", board_List.getLabel());
                        boardLableData.add(lablemap);
                    }
                } else {
                    nodata.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public HashMap<String, Object> getTitleHashmap(String title) {
        HashMap<String, Object> newHashmap = new HashMap<>();
        newHashmap.put("title", title);
        newHashmap.put("id", createUniqueid());
        newHashmap.put("autoduedate", false);
        return newHashmap;
    }

    public List<HashMap<String, Object>> createHeaderlist() {
        List<HashMap<String, Object>> parent = new ArrayList<>();
        parent.add(getTitleHashmap("To do"));
        parent.add(getTitleHashmap("Doing"));
        parent.add(getTitleHashmap("Done"));
        return parent;
    }

    public HashMap<String, Object> getColorMap(String colorcode, String title) {
        HashMap<String, Object> newHashmap = new HashMap<>();
        newHashmap.put("color", colorcode);
        newHashmap.put("title", title);
        return newHashmap;
    }

    public List<HashMap<String, Object>> createColorLabelList() {
        colorLabelsList.add(getColorMap(getString(R.string.red_500), ""));
        colorLabelsList.add(getColorMap(getString(R.string.yellow_500), ""));
        colorLabelsList.add(getColorMap(getString(R.string.light_purple), ""));
        colorLabelsList.add(getColorMap(getString(R.string.sky_500), ""));
        return colorLabelsList;
    }

    public HashMap<String, Object> getCardData(String title, int itemAttcnt, int imageAttcnt, int commentcnt, int shareTeam, String dueDate, String color, Boolean isAssign) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("card_title", title);
        data.put("card_item_attachcnt", itemAttcnt);
        data.put("card_img_attachcnt", imageAttcnt);
        data.put("card_comment", commentcnt);
        data.put("card_team", shareTeam);
        data.put("card_dueDate", dueDate);
        data.put("card_color", color);
        data.put("card_assign", isAssign);
        return data;
    }

    //for crateing the boardid
    public static String createUniqueid() {
        String mainString = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String randomString;
        int len = 10;
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < 10; i++) {
            sb.append(mainString.charAt(rnd.nextInt(mainString.length())));
        }
        Log.i("TAG", "Created_unique_id::" + sb);
        return sb.toString();
    }

    public void showDeleteBoardPrompt(final int pos) {
        MaterialDialog md = new MaterialDialog.Builder(getContext())
                .title("Delete board")
                .content("Are you sure you want to delete this board there is no undo for this action?")
                .positiveText("Delete")
                .negativeText("Cancel")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        zboardList.deleteboardWithListener(FB_boardList.get(pos).getObjectId(), new OnSuccessListener() {
                            @Override
                            public void onSuccess(Object o) {
                                boardListAdapter.notifyDataSetChanged();
                            }
                        });
//                        zboardList.getBOARD_DATA(FB_boardList.get(pos).getObjectId()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
//                            @Override
//                            public void onSuccess(Void aVoid) {
//                                boardListAdapter.notifyDataSetChanged();
//                            }
//                        });
//                        zboardList.getBOARD_ROOT(FB_boardList.get(pos).getObjectId()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
//                            @Override
//                            public void onSuccess(Void aVoid) {
//                                boardListAdapter.notifyDataSetChanged();
//                            }
//                        });
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {

                    }
                })
                .show();
    }

}
