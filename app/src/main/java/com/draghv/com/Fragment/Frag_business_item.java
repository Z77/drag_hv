package com.draghv.com.Fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.draghv.com.Adapter.CDetail_RVItemAdapter;
import com.draghv.com.CardDetailActivity;
import com.draghv.com.MainActivity;
import com.draghv.com.R;
import com.draghv.com.ZAuth;
import com.draghv.com.Zboard_history;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.*;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 09-Feb-17.
 */
public class Frag_business_item extends Fragment {
    View rootview;
    Spinner business;
    LinearLayout itemlist;
    List<String> businessTitleList = new ArrayList<>();
    static List<HashMap<String, Object>> businessList;
    static List<HashMap<String, Object>> seletctedItemList;
    List<String> businessIdList, categoryIdList, itemIdList, itemImagePathlist, itemTitlelist;
    String lastBsiness_Id;
    List<String> added = new ArrayList<>();
    List<String> removed = new ArrayList<>();
    List<String> updated = new ArrayList<>();
    String lastbid4hisory;
    Zboard_history history;
    static List<HashMap<String, Object>> preselected;
    ZAuth zAuth;
    String userid;


    public static Fragment instantiate(List<HashMap<String, Object>> businesslist, List<HashMap<String, Object>> selecteditemlist) {
        Frag_business_item fragment = new Frag_business_item();
        businessList = businesslist;
        seletctedItemList = selecteditemlist;
        preselected = selecteditemlist;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Toast.makeText(getContext(), "Item frag", Toast.LENGTH_SHORT).show();
        zAuth=new ZAuth();
        userid=zAuth.get();
        Log.i("TAG", "business list:" + businessList);
        rootview = inflater.inflate(R.layout.fragment_add_item, container, false);
        business = (Spinner) rootview.findViewById(R.id.spinner_business);
        itemlist = (LinearLayout) rootview.findViewById(R.id.parent_items);
        history = new Zboard_history();
        businessTitleList.add("---Select Business---");
        for (int i = 0; i < businessList.size(); i++) {
            HashMap<String, Object> datalist = (HashMap<String, Object>) businessList.get(i);
            businessTitleList.add((String) datalist.get("title"));
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, businessTitleList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        business.setAdapter(spinnerArrayAdapter);
        if (seletctedItemList.size() > 0) {
            lastBsiness_Id = (String) seletctedItemList.get(0).get("businessId");
            lastbid4hisory=lastBsiness_Id;
        }

        if (lastBsiness_Id != null) {
            int i = 0;
            for (HashMap<String, Object> businessData : businessList) {
                if (businessData.get("objectId").equals(lastBsiness_Id)) {
                    Log.i("TAG", "last selected id:" + lastBsiness_Id);
                    business.setSelection(i + 1);
                    break;
                }
                i++;
            }
        }
        business.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    itemlist.setVisibility(View.VISIBLE);
                    String businessId = (String) businessList.get(business.getSelectedItemPosition() - 1).get("objectId");
                    addItems(businessId);
                } else {
                    itemlist.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return rootview;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.setGroupVisible(R.id.detail_menu_group, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            getActivity().onBackPressed();
        }
        if (itemId == R.id.done_check) {
            if (seletctedItemList.size() > 0)
                seletctedItemList.clear();
            ArrayList<CDetail_RVItemAdapter.ItemData> itemdatalist = new ArrayList<>();
            if (business.getSelectedItemPosition() > 0) {
                lastBsiness_Id = (String) businessList.get(business.getSelectedItemPosition() - 1).get("objectId");
            }
            for (int i = 0; i < itemlist.getChildCount(); i++) {
                View rowat = itemlist.getChildAt(i);
                CheckBox chkat = (CheckBox) rowat.findViewById(R.id.chk_item);
                Log.i("TAG", "item " + i + ":" + chkat.getText().toString() + "and is" + chkat.isChecked());
                if (chkat.isChecked()) {
                    CDetail_RVItemAdapter.ItemData item_Data = new CDetail_RVItemAdapter.ItemData(itemImagePathlist.get(i), itemTitlelist.get(i));
                    itemdatalist.add(item_Data);
                    HashMap<String, Object> tempItemList = new HashMap<>();
                    tempItemList.put("businessId", businessIdList.get(i));
                    tempItemList.put("categoryId", categoryIdList.get(i));
                    tempItemList.put("itemId", itemIdList.get(i));
                    seletctedItemList.add(tempItemList);
                    if (!added.contains(itemIdList.get(i)))
                        updated.add(itemIdList.get(i));

                } else {
                    if (added.contains(itemIdList.get(i))) {
                        added.remove(itemIdList.get(i));
                        removed.add(itemIdList.get(i));
                    }
                }
            }

            ((CardDetailActivity) getActivity()).card_items.setVisibility(View.VISIBLE);
            ((CardDetailActivity) getActivity()).items_rvAdapter = new CDetail_RVItemAdapter(itemdatalist, getContext());
            ((CardDetailActivity) getActivity()).items_rv.setAdapter(((CardDetailActivity) getActivity()).items_rvAdapter);
            CardDetailActivity.itemdata.setItemAttach(seletctedItemList);
            CardDetailActivity.passDataToAdapterAndSet(CardDetailActivity.itemdata);
            getActivity().onBackPressed();

            Log.i("TAG","current seleted item:"+business.getSelectedItem());
            //history
            if (preselected == null || preselected.size() == 0) {
                HashMap<String, Object> mapdata = history.getJSON_CardItem(String.valueOf(seletctedItemList.size()), business.getSelectedItem().toString(), CardDetailActivity.itemdata.getObjectId(), CardDetailActivity.itemdata.getData(), userid,"add");
                history.addHistory(MainActivity.BOARD_ID, mapdata, null);
            } else if (lastbid4hisory.equals(lastBsiness_Id)) {
                if (updated.size() > 0) {
                    HashMap<String, Object> mapdata = history.getJSON_CardItem(String.valueOf(updated.size()), business.getSelectedItem().toString(), CardDetailActivity.itemdata.getObjectId(), CardDetailActivity.itemdata.getData(), userid,"update");
                    history.addHistory(MainActivity.BOARD_ID, mapdata, null);
                }
                if (removed.size() > 0) {
                    HashMap<String, Object> mapdata = history.getJSON_CardItem(String.valueOf(removed.size()), business.getSelectedItem().toString(), CardDetailActivity.itemdata.getObjectId(), CardDetailActivity.itemdata.getData(), userid,"delete");
                    history.addHistory(MainActivity.BOARD_ID, mapdata, null);
                }
            } else {
                HashMap<String, Object> mapdata = history.getJSON_CardItem(String.valueOf(seletctedItemList.size()), business.getSelectedItem().toString(), CardDetailActivity.itemdata.getObjectId(), CardDetailActivity.itemdata.getData(), userid,"add");
                history.addHistory(MainActivity.BOARD_ID, mapdata, null);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void addItems(String BusinessId) {
        itemlist.removeAllViews();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("b_items").child(BusinessId.trim());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getContext()).build();
        final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        loader.init(config);
        final DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .build();
        myRef.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            HashMap<String, Object> itemdetail = (HashMap<String, Object>) dataSnapshot.getValue();
                                            businessIdList = new ArrayList<>();
                                            categoryIdList = new ArrayList<>();
                                            itemIdList = new ArrayList<>();
                                            itemImagePathlist = new ArrayList<>();
                                            itemTitlelist = new ArrayList<>();
                                            if (dataSnapshot.getValue() != null) {
                                                for (Object objlist : itemdetail.values()) {
                                                    HashMap<String, Object> datamap = (HashMap<String, Object>) objlist;
                                                    View row = LayoutInflater.from(getContext()).inflate(R.layout.ticket_builder_items_select_row, null, false);
                                                    final ImageView thumb = (ImageView) row.findViewById(R.id.iv_imgthumbnail);
                                                    final ProgressBar progress = (ProgressBar) row.findViewById(R.id.pbar);
                                                    TextView title = (TextView) row.findViewById(R.id.item_name);
                                                    CheckBox chk = (CheckBox) row.findViewById(R.id.chk_item);
                                                    String itemname = datamap.get("title").toString();
                                                    final String img_url = datamap.get("image").toString();//.replace("image", "thumb");
                                                    title.setText(itemname);
                                                    businessIdList.add(datamap.get("businessId").toString());
                                                    itemImagePathlist.add(img_url);
                                                    itemTitlelist.add(itemname);
                                                    if (seletctedItemList.size() > 0) {
                                                        for (int ii = 0; ii < seletctedItemList.size(); ii++) {
                                                            if (seletctedItemList.get(ii).get("itemId").toString().equals(datamap.get("objectId"))) {
                                                                chk.setChecked(true);
                                                                if (!added.contains(seletctedItemList.get(ii).get("itemId").toString()))
                                                                    added.add(seletctedItemList.get(ii).get("itemId").toString());
                                                            }

                                                        }
                                                    }
                                                    if (datamap.containsKey("categoryId")) {
                                                        categoryIdList.add(datamap.get("categoryId").toString());
                                                    } else {
                                                        categoryIdList.add("");
                                                    }
                                                    itemIdList.add(datamap.get("objectId").toString());

                                                    loader.displayImage(img_url, thumb, options, new ImageLoadingListener() {
                                                        @Override
                                                        public void onLoadingStarted(String imageUri, View view) {
                                                            Log.i("TAG", "image loading started");
                                                        }

                                                        @Override
                                                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                                            Log.i("TAG", "image loading failed");
                                                        }

                                                        @Override
                                                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                                            progress.setVisibility(View.GONE);
                                                        }

                                                        @Override
                                                        public void onLoadingCancelled(String imageUri, View view) {
                                                            Log.i("TAG", "image loading candelled");
                                                        }
                                                    });
                                                    itemlist.addView(row);
                                                }
                                            } else

                                            {
                                                Toast.makeText(getContext(), "No items,Please select another business", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    }

        );
    }
}
