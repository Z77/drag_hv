package com.draghv.com.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.draghv.com.Adapter.BoardListAdapter;
import com.draghv.com.Adapter.SharedBoardListAdapter;
import com.draghv.com.GetBoardList;
import com.draghv.com.GetSharedBoard;
import com.draghv.com.MainActivity;
import com.draghv.com.Models.BoardListPOJO;
import com.draghv.com.R;
import com.draghv.com.ZAuth;
import com.draghv.com.Zboard;
import com.draghv.com.Zboard_history;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 28-Feb-17.
 */
public class SharedBoardList extends Fragment {

    View rootView;
   // List<GetSharedBoard> shareddataList; //for store shared board maindata i.e. read,write,boardid,ownerid
    List<GetBoardList> boarddatalist; //for data of that board


    SharedBoardListAdapter boardListAdapter;
    RecyclerView lv_boardnames;
    TextView nodata;
    Zboard zboardList;
    Zboard_history history;
    ZAuth zAuth;

    ArrayList<HashMap<String, Object>> boardColsData;
    ArrayList<HashMap<String, Object>> boardLableData;
    ArrayList<HashMap<String, Object>> colorLabelsList;

    ValueEventListener valueEventListener;

    public SharedBoardList() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_sharedboardlist, container, false);
        lv_boardnames = (RecyclerView) rootView.findViewById(R.id.boardList);
        RecyclerView.LayoutManager llm = new LinearLayoutManager(getContext());
        lv_boardnames.setLayoutManager(llm);
        nodata = (TextView) rootView.findViewById(R.id.nodata);
        zAuth = new ZAuth();
        zboardList = new Zboard();
        //boardList = new ArrayList<>();
        boarddatalist = new ArrayList<>();
        boardColsData = new ArrayList<>();
        boardLableData = new ArrayList<>();
        //shareddataList = new ArrayList<>();
        boardListAdapter = new SharedBoardListAdapter(boarddatalist, getContext(), this);
        lv_boardnames.setAdapter(boardListAdapter);

        loadData();

//        lv_boardnames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                int sharedcount = 0;
//                if (boardColsData.size() > 0)
//                    boardColsData.clear();
//                if (boardLableData.size() > 0)
//                    boardLableData.clear();
//
//                for (GetBoardList sharedBoard : boarddatalist) {
//                    if (sharedBoard.getUserId().equals(boarddatalist.get(position).getUserId())) {
//                        HashMap<String, Object> hashMap = new HashMap<>();
//                        hashMap.put("bid", sharedBoard.getObjectId());
//                        hashMap.put("name", sharedBoard.getTitle());
//                        hashMap.put("coldata", sharedBoard.getTotalcol());
//                        boardColsData.add(hashMap);
//
//                        HashMap<String, Object> lablemap = new HashMap<>();
//                        lablemap.put("bid", sharedBoard.getObjectId());
//                        lablemap.put("name", sharedBoard.getTitle());
//                        lablemap.put("labeldata", sharedBoard.getLabel());
//                        boardLableData.add(lablemap);
//                        Log.i("TAG", "the user:" + sharedBoard.getUserId() + "has shared" + (sharedcount++) + " board with u");
//                    }
//                }
//
//                Intent i = new Intent(getContext(), MainActivity.class);
//                i.putExtra("type", boarddatalist.get(position).getTitle());
//                i.putExtra("bid", boarddatalist.get(position).getObjectId());
//                //i.putExtra("tcol", String.valueOf(boarddatalist.get(position).getTotalcol().size()));
//                i.putExtra("collist", boarddatalist.get(position).getTotalcol());
//                i.putExtra("otherdata", boardColsData);
//                i.putExtra("othercolor", boardLableData);
//                i.putExtra("clrlbl", boarddatalist.get(position).getLabel());
//                //i.putExtra("trow", String.valueOf(boarddatalist.get(position).getTotalrow()));
//                //i.putExtra("uid", boarddatalist.get(position).getUserId());
//                i.putExtra("member", boarddatalist.get(position).getMember());
//                i.putExtra("sharewith", boarddatalist.get(position).getSharedwith());
//                i.putExtra("fromshare", true);
//                i.putExtra("owner", boarddatalist.get(position).getUserId());
//                i.putExtra("edit", true);
//
//                Boolean write=shareddataList.get(position).getWrite();
//                Log.i("TAG","is this board is writable:"+write);
//                i.putExtra("write",write);
//                startActivity(i);
//
//            }
//        });

        return rootView;
    }

    public void RecyclerViewClick(int position) {
        int sharedcount = 0;
        if (boardColsData.size() > 0)
            boardColsData.clear();
        if (boardLableData.size() > 0)
            boardLableData.clear();
        //getting this users other board data if available in list
        for (GetBoardList sharedBoard : boarddatalist) {
            if (sharedBoard.getUserId().equals(boarddatalist.get(position).getUserId())) {
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("bid", sharedBoard.getObjectId());
                hashMap.put("name", sharedBoard.getTitle());
                hashMap.put("coldata", sharedBoard.getTotalcol());
                boardColsData.add(hashMap);

                HashMap<String, Object> lablemap = new HashMap<>();
                lablemap.put("bid", sharedBoard.getObjectId());
                lablemap.put("name", sharedBoard.getTitle());
                lablemap.put("labeldata", sharedBoard.getLabel());
                boardLableData.add(lablemap);
                Log.i("TAG", "the user:" + sharedBoard.getUserId() + "has shared" + (sharedcount++) + " board with u");
            }
        }

        Intent i = new Intent(getContext(), MainActivity.class);
        i.putExtra("type", boarddatalist.get(position).getTitle());
        i.putExtra("bid", boarddatalist.get(position).getObjectId());
        i.putExtra("collist", boarddatalist.get(position).getTotalcol());
        i.putExtra("otherdata", boardColsData);
        i.putExtra("othercolor", boardLableData);
        i.putExtra("clrlbl", boarddatalist.get(position).getLabel());
        i.putExtra("member", boarddatalist.get(position).getMember());
        i.putExtra("sharewith", boarddatalist.get(position).getSharedwith());
        i.putExtra("fromshare", true);
        i.putExtra("owner", boarddatalist.get(position).getUserId());
        i.putExtra("edit", true);

        HashMap<String,Object> permission= (HashMap<String, Object>) boarddatalist.get(position).getSharedwith().get(zAuth.get());//permission;
        //HashMap<String,Object> permi_data= (HashMap<String, Object>) permission.get("permission");
        Boolean write = (Boolean)permission.get("write") ;
        Log.i("TAG", "is this board is writable:" + write);
        i.putExtra("write", write);
        startActivity(i);
    }

    public void loadData() {
        zboardList.getUserSharedBoard(zAuth.get()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (shareddataList.size() > 0)
//                    shareddataList.clear();

                if (dataSnapshot.getChildrenCount() > 0) {

                    for (final DataSnapshot boarddata : dataSnapshot.getChildren()) {
//                        GetSharedBoard sharedBoard = boarddata.getValue(GetSharedBoard.class);
//                        shareddataList.add(sharedBoard);
//                        Log.i("TAG", "board user id :" + boarddata.getKey());
                        getBoardData(String.valueOf(boarddata.getValue()),boarddata.getKey());
                        //boardList.add(sharedBoard);
                        //boardListAdapter.notifyDataSetChanged();
//                        lv_boardnames.setVisibility(View.VISIBLE);
//                        nodata.setVisibility(View.GONE);
//                    zboardList.getROOT().addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            for (DataSnapshot userdata : dataSnapshot.getChildren()) {
//                                Boolean check = userdata.hasChild(boarddata.getKey());
//                                Log.i("TAG", "try to check datasnap has child" + check);
//                                if (check) {
//                                    Log.i("TAG", "board found user is:" + userdata.getKey());
//                                    displayBoard(userdata.getKey(), boarddata.getKey());//userid,boardid
//                                    lv_boardnames.setVisibility(View.VISIBLE);
//                                    nodata.setVisibility(View.GONE);
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//
//                        }
//                    });
                    }
                } else {
                    lv_boardnames.setVisibility(View.GONE);
                    nodata.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

//    public void displayBoard(String userid, String boardid) {
//        zboardList.getROOT().child(userid).child(boardid).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.i("TAG", "datasnap of board" + dataSnapshot);
//                GetBoardList board_List = dataSnapshot.getValue(GetBoardList.class);
////                            if (board_List.getSharedwith() != null) {
////                                boardList.add(new BoardListPOJO(board_List.getTitle(), false, board_List.getSharedwith().size()));
////                            } else {
//                boardList.add(new BoardListPOJO(board_List.getTitle(), false, 0));
////                            }
//                boardListAdapter.notifyDataSetChanged();
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//    }

    private void  getBoardData(String userid, String boardid) {
        zboardList.getROOT().child(userid).child(boardid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("TAG","data snap of shared board:"+dataSnapshot);
                if (boarddatalist.size() > 0)
                    boarddatalist.clear();
//                for(DataSnapshot boardData:dataSnapshot.getChildren()){
                GetBoardList data = dataSnapshot.getValue(GetBoardList.class);
                boarddatalist.add(data);
                boardListAdapter.notifyDataSetChanged();
                lv_boardnames.setVisibility(View.VISIBLE);
                nodata.setVisibility(View.GONE);
//                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
