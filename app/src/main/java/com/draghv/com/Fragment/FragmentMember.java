package com.draghv.com.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.draghv.com.MainActivity;
import com.draghv.com.Adapter.MemberAdapter;
import com.draghv.com.Models.MemberPOJO;
import com.draghv.com.R;
import com.draghv.com.ZAuth;
import com.draghv.com.Zboard;
import com.draghv.com.Zboard_history;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 10-Feb-17.
 */
public class FragmentMember extends Fragment {
    View rootView;
    ListView listView;
    List<MemberPOJO> memberlist = new ArrayList<>();
    MemberAdapter memberAdapter;
    FirebaseDatabase database;
    List<String> useridlist = new ArrayList<>();
    static List<String> alreadyMemberList;
    Button addmember;
    Zboard board;
    Zboard_history zboard_history;
    ZAuth zAuth;
    List<String> added = new ArrayList<>();
    List<String> removed = new ArrayList<>();

    public static FragmentMember instantiate(List<String> members) {
        FragmentMember fragmentMember = new FragmentMember();
        alreadyMemberList = members;
        return fragmentMember;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_member, container, false);
        zAuth=new ZAuth();
        listView = (ListView) rootView.findViewById(R.id.lv_members);
        addmember = (Button) rootView.findViewById(R.id.btnaddmember);
        memberAdapter = new MemberAdapter(memberlist, getContext());
        board = new Zboard();
        zboard_history = new Zboard_history();
        listView.setAdapter(memberAdapter);
        if(((MainActivity)getActivity()).is_write) {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Boolean showadd = false;
                    if (memberlist.get(position).getIsMember()) {
                        memberlist.get(position).setIsMember(false);
                        removed.add(useridlist.get(position));
                        if (added.contains(useridlist.get(position)))
                            added.remove(useridlist.get(position));
                    } else {
                        if (!added.contains(useridlist.get(position))) {
                            added.add(useridlist.get(position));
                            if (removed.contains(useridlist.get(position)))
                                removed.remove(useridlist.get(position));
                        }


                        memberlist.get(position).setIsMember(true);
                    }
                    memberAdapter.notifyDataSetChanged();
                    for (int i = 0; i < memberlist.size(); i++) {
                        if (!memberlist.get(i).getIsMember())
                            showadd = true;
                    }
                    if (showadd)
                        chageMenu(true);
                    else
                        chageMenu(false);

                }
            });
        }
        addmember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> temp_userlist = new ArrayList<>();
                for (int i = 0; i < memberlist.size(); i++) {
                    if (memberlist.get(i).getIsMember()) {
                        temp_userlist.add(useridlist.get(i));
                    }
                }
                Log.i("TAG", "removed :" + removed);
                Log.i("TAG", "added :" + added);
                if(alreadyMemberList!=null&&alreadyMemberList.size()>0) {
                    if (added.size() > 0) {
                        HashMap<String, Object> memeberdata = zboard_history.getJSON_Addmember(added, zAuth.get(), MainActivity.BOARD_ID, MainActivity.type);
                        zboard_history.addHistory(MainActivity.BOARD_ID, memeberdata, null);
                    }
                    if (removed.size() > 0) {
                        HashMap<String, Object> memeberdata = zboard_history.getJSON_Removemember(removed, zAuth.get(), MainActivity.BOARD_ID, MainActivity.type);
                        zboard_history.addHistory(MainActivity.BOARD_ID, memeberdata, null);
                    }
                }else{
                    HashMap<String, Object> memeberdata = zboard_history.getJSON_Addmember(temp_userlist, zAuth.get(), MainActivity.BOARD_ID, MainActivity.type);
                    zboard_history.addHistory(MainActivity.BOARD_ID, memeberdata, null);
                }
                if(((MainActivity)getActivity()).fromShare) {
                    String ownerid = ((MainActivity) getActivity()).ownerId;
                    board.getROOT().child(ownerid).child(MainActivity.BOARD_ID).child("member").setValue(temp_userlist);
                }else{
                    board.getBOARD_ROOT(MainActivity.BOARD_ID).child("member").setValue(temp_userlist);
                }

                ((MainActivity)getActivity()).Addmember(temp_userlist);
                getActivity().onBackPressed();
            }
        });
        getMembers();
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.setGroupVisible(R.id.main_menu_group, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void getMembers() {
        database = FirebaseDatabase.getInstance();
        database.getReference().child("follow").child("users").child("uRPz0t7TWk").child("imfollowing").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    useridlist.add(data.getKey());
                }
                Log.i("TAG", "users:" + useridlist);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        database.getReference().child("follow").child("users").child("uRPz0t7TWk").child("myfollowers").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (!useridlist.contains(data.getKey()))
                        useridlist.add(data.getKey());
                }
                Log.i("TAG", "users:" + useridlist);
                for (int i = 0; i < useridlist.size(); i++) {
                    final String id = useridlist.get(i);
                    database.getReference().child("_user").child(id).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Log.i("TAG", "userdata snap:" + dataSnapshot);
                            HashMap<String, Object> map = (HashMap<String, Object>) dataSnapshot.getValue();
                            String image = map.get("image") != null ? (String) map.get("image") : "";
                            if (alreadyMemberList != null && alreadyMemberList.size() > 0 && alreadyMemberList.contains(id))
                                memberlist.add(new MemberPOJO(map.get("name").toString(), image, true));
                            else
                                memberlist.add(new MemberPOJO(map.get("name").toString(), image, false));
                            Log.i("TAG", "userdata:" + map);
                            memberAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void chageMenu(Boolean show) {
        if (show)
            addmember.setVisibility(View.VISIBLE);
        else
            addmember.setVisibility(View.GONE);
    }

}
