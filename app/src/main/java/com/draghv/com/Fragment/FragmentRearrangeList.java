package com.draghv.com.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.draghv.com.ActivityBoardList;
import com.draghv.com.Helper.CallbackItemTouch;
import com.draghv.com.MainActivity;
import com.draghv.com.Models.MyItem;
import com.draghv.com.Helper.MyItemTouchHelperCallback;
import com.draghv.com.R;
import com.draghv.com.Adapter.rearrangeAdapter;
import com.draghv.com.ZAuth;
import com.draghv.com.Zboard;
import com.draghv.com.Zboard_archive;
import com.draghv.com.Zboard_history;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 05-Jan-17.
 */
public class FragmentRearrangeList extends Fragment implements CallbackItemTouch {
    static RecyclerView listrv;
    static rearrangeAdapter list_adapter;
    static ArrayList<String> myHeaderList;
    static List<List<MyItem>> mygrandList;
    static ArrayList<HashMap<String, Object>> columnsData;
    static ArrayList<HashMap<String, Object>> otherBoardData;
    View rootView;
    FloatingActionButton newcol;
    Zboard_archive zboard_archive;
    ZAuth zAuth;
    List<HashMap<String, Object>> added = new ArrayList<>();
    List<HashMap<String, Object>> removed = new ArrayList<>();
    List<HashMap<String, Object>> edited = new ArrayList<>();


    public static Fragment instantiate(List<List<MyItem>> myGrandList, ArrayList<String> headerList, ArrayList<HashMap<String, Object>> colsData, ArrayList<HashMap<String, Object>> otherboarddata) {
        FragmentRearrangeList rearrangeList = new FragmentRearrangeList();
        myHeaderList = headerList;
        mygrandList = myGrandList;
        columnsData = colsData;
        otherBoardData = otherboarddata;
        return rearrangeList;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_rearrange_list, container, false);
        zAuth = new ZAuth();
        zboard_archive = new Zboard_archive();
        setHasOptionsMenu(true);
        listrv = (RecyclerView) rootView.findViewById(R.id.listname_RV);
        newcol = (FloatingActionButton) rootView.findViewById(R.id.addnewcol);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity().getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listrv.setLayoutManager(llm);

        if(((MainActivity)getActivity()).is_write) {
            ItemTouchHelper.Callback callback = new MyItemTouchHelperCallback(this);// create MyItemTouchHelperCallback
            ItemTouchHelper touchHelper = new ItemTouchHelper(callback); // Create ItemTouchHelper and pass with parameter the MyItemTouchHelperCallback
            touchHelper.attachToRecyclerView(listrv); // Attach ItemTouchHelper to RecyclerView
        }
        if(((MainActivity)getActivity()).is_write)
            newcol.setVisibility(View.VISIBLE);
        else
            newcol.setVisibility(View.GONE);

        newcol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialDialog addcolumn = new MaterialDialog.Builder(getActivity())
                        .title("Add new column")
                        .customView(R.layout.prompt_rename_header, true)
                        .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                        .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                        .titleColorRes(R.color.colorPrimary)
                        .positiveText("Add").positiveColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary))
                        .negativeText("Cancel").negativeColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                EditText promptTitle = (EditText) dialog.findViewById(R.id.tv_list_title);
                                String name = promptTitle.getText().toString().trim();
                                myHeaderList.add(name);
                                HashMap<String, Object> colsdata = new HashMap<String, Object>();
                                colsdata.put("autoduedate", false);
                                colsdata.put("title", name);
                                colsdata.put("id", MyBoardlist.createUniqueid());
                                columnsData.add(colsdata);
                                List<MyItem> newitemList = new ArrayList<MyItem>();
                                mygrandList.add(newitemList);
                                Log.i("TAG", "add column at mygrnlist:" + mygrandList);
                                list_adapter.notifyDataSetChanged();
                                added.add(colsdata);
                                Log.i("TAG", "added col data:" + added);
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                EditText promptTitle = (EditText) addcolumn.findViewById(R.id.tv_list_title);
                promptTitle.setHint("column name");
            }
        });
        displaylist(myHeaderList, mygrandList);
        return rootView;
    }

    private void displaylist(ArrayList<String> myHeaderList, List<List<MyItem>> mylist) {
        list_adapter = new rearrangeAdapter(myHeaderList, getContext(), FragmentRearrangeList.this,((MainActivity)getActivity()).is_write);
        listrv.setAdapter(list_adapter);
        Log.i("TAG", "grand listitems display list:" + mylist);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        //super.onPrepareOptionsMenu(menu);

        menu.setGroupVisible(R.id.main_menu_group, false);
        //item.setVisible(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        if(((MainActivity)getActivity()).is_write)
            inflater.inflate(R.menu.menu_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.done_check) {
            Toast.makeText(getActivity().getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
            try {
                Log.i("TAG", "column edit done time:" + myHeaderList);
                ((MainActivity) getActivity()).ClearBoardAddColumn(mygrandList, myHeaderList);//first add column
                ((MainActivity) getActivity()).changeDataInFirebase(columnsData);//after that put card

                if (added.size() > 0) {
                    for (int i = 0; i < added.size(); i++) {
                        addColumn(added.get(i).get("title").toString(), added.get(i).get("id").toString());
                    }
                }
                if (edited.size() > 0) {
                    for (int i = 0; i < edited.size(); i++) {
                        editColumn(edited.get(i).get("id").toString(), edited.get(i).get("oldtitle").toString(), edited.get(i).get("newtitle").toString());
                    }
                }

            } catch (ClassCastException e) {
                Toast.makeText(getContext(), "Exception:" + e, Toast.LENGTH_SHORT).show();
            }
            getActivity().onBackPressed();
            return true;
        }
        if (itemId == R.id.action_done_detail) {
            Toast.makeText(getActivity().getApplicationContext(), "Done detail", Toast.LENGTH_SHORT).show();
            return true;
        }
        if (itemId == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
        //return false;
    }

    @Override
    public void itemTouchOnMove(int oldPosition, int newPosition) {
        Collections.swap(myHeaderList, oldPosition, newPosition);
        Collections.swap(columnsData, oldPosition, newPosition);
        list_adapter.notifyItemMoved(oldPosition, newPosition); //notify changes in adapter
        Collections.swap(mygrandList, oldPosition, newPosition);
    }

    public void EditHeader(View view, final int pos) {
        if(((MainActivity)getActivity()).is_write) {
            ImageView ib = (ImageView) view;
            MaterialDialog edit = new MaterialDialog.Builder(view.getContext())
                    .title("Edit column name").titleColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary))
                    .positiveText("Rename").positiveColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary))
                    .negativeText("Cacel").negativeColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary))
                    .customView(R.layout.prompt_rename_header, true)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            final EditText tv_title = (EditText) dialog.findViewById(R.id.tv_list_title);
                            if (tv_title.getText().toString().length() > 0) {

                                HashMap<String, Object> editedmap = new HashMap<String, Object>();
                                editedmap.put("id", columnsData.get(pos).get("id"));
                                editedmap.put("oldtitle", columnsData.get(pos).get("title"));
                                editedmap.put("newtitle", tv_title.getText().toString().trim());
                                edited.add(editedmap);

                                myHeaderList.set(pos, tv_title.getText().toString().trim());
                                list_adapter.notifyDataSetChanged();
                                columnsData.get(pos).put("title", tv_title.getText().toString().trim());
                                Log.i("TAG", "column edit time:" + myHeaderList);
                            }
                            dialog.dismiss();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            dialog.dismiss();
                        }
                    }).show();
            final EditText tv_title = (EditText) edit.findViewById(R.id.tv_list_title);
            tv_title.setText(columnsData.get(pos).get("title").toString());
        }
//        else{
//            Toast.makeText(getContext(),"no write permission, contact board admin",Toast.LENGTH_SHORT).show();
//        }
    }

    public void DeleteHeader(final View view, final int pos) {
        if (((MainActivity) getActivity()).is_write) {
            String title_str = "";
            int array = 0;
            Log.i("TAG", "add column at mygrnlist delete time:" + mygrandList);
            Log.i("TAG", "add column at mygrnlist delete time:" + mygrandList.get(pos).size());
            if (mygrandList.get(pos).size() > 0) {
                title_str = "you can not delete this column?It has " + mygrandList.get(pos).size() + " cards";
                new MaterialDialog.Builder(view.getContext())
                        .title("Delete Column").titleColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary))
                        .content(title_str)
                        .items(R.array.col_delete_option)
                        .positiveText("Ok").positiveColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary))
                        .negativeText("Cancel").negativeColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary))
                        .itemsCallbackSingleChoice(1, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                return true;//allow selection
                            }
                        })
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                Log.i("selected is:", "" + dialog.getSelectedIndex());
                                int action = dialog.getSelectedIndex();


                                if (action == 0) {
                                    String boardid = MainActivity.BOARD_ID;
                                    List<MyItem> templist = mygrandList.get(pos);

                                    for (int i = 0; i < templist.size(); i++) {
                                        MainActivity.BOARD_.deleteCardInBoard(boardid, templist.get(i).getObjectId(), null);
                                    }
                                    deletecolumn(columnsData.get(pos).get("title").toString(), columnsData.get(pos).get("id").toString());
                                    FragmentRearrangeList.mygrandList.remove(pos); //remove col from grandlist
                                    myHeaderList.remove(pos);//remove col from curent string headerlist
                                    columnsData.remove(pos);//remove that col data from columns data where all cols data is save with id and title;
                                    list_adapter.notifyItemRemoved(pos);
                                    ((MainActivity) getActivity()).ClearBoardAddColumn(mygrandList, myHeaderList);
                                    ((MainActivity) getActivity()).changeDataInFirebase(columnsData);
                                    dialog.dismiss();
                                    Toast.makeText(getContext(), "column deleted successfully....", Toast.LENGTH_SHORT).show();

                                } else if (action == 1) {
                                    Toast.makeText(view.getContext(), "archive all card", Toast.LENGTH_SHORT).show();
                                    List<MyItem> mItemArrays = mygrandList.get(pos);
                                    for (int i = 0; i < mItemArrays.size(); i++) {
                                        MyItem item = mItemArrays.get(i);
                                        String colid = columnsData.get(pos).get("id").toString();
                                        zboard_archive.addCardInArchive(MainActivity.BOARD_ID, item.getObjectId(), item.getCardJSON(colid, i), null);
                                    }
                                    for (int k = 0; k < mItemArrays.size(); k++) {
                                        MyItem deleteItem = mItemArrays.get(k);
                                        Log.i("TAG", "archive id:" + deleteItem.getObjectId());
                                        MainActivity.BOARD_.deleteCardInBoard(MainActivity.BOARD_ID, deleteItem.getObjectId(), null);
                                    }
                                    deletecolumn(columnsData.get(pos).get("title").toString(), columnsData.get(pos).get("id").toString());
                                    FragmentRearrangeList.mygrandList.remove(pos); //remove col from grandlist
                                    myHeaderList.remove(pos);//remove col from curent string headerlist
                                    columnsData.remove(pos);//remove that col data from columns data where all cols data is save with id and title;
                                    list_adapter.notifyItemRemoved(pos);
                                    ((MainActivity) getActivity()).ClearBoardAddColumn(mygrandList, myHeaderList);
                                    ((MainActivity) getActivity()).changeDataInFirebase(columnsData);
                                    dialog.dismiss();
                                    Log.i("TAG", "add to archive all card");
                                    Toast.makeText(getContext(), "column deleted successfully....", Toast.LENGTH_SHORT).show();
                                } else if (action == 2) {
                                    Log.i("TAG", "Column is:" + pos);
                                    Log.i("TAG", "column title" + columnsData.get(pos).get("title") + "id=>" + columnsData.get(pos).get("id"));
                                    Log.i("TAG", "column first data" + " " + mygrandList.get(pos).get(0).getData());
                                    MoveAllCardDialog(pos);
                                    //Toast.makeText(view.getContext(), "move all card", Toast.LENGTH_SHORT).show();
                                    Log.i("TAG", "move all card to another");
                                }
//                        removeHeader(getAdapterPosition());
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();

            } else {
                title_str = "Are you sure to delete? There is no undo for this action";
                new MaterialDialog.Builder(view.getContext())
                        .title("Delete Column").titleColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary))
                        .content(title_str)
                        .positiveText("Ok").positiveColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary))
                        .negativeText("Cancel").negativeColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                //check if user has add new column after cmg in this screen and try to delete without save
                                Log.i("TAG", "added col data delete:" + added);
                                if (added.contains(columnsData.get(pos))) {
                                    added.remove(columnsData.get(pos));
                                    removed.add(columnsData.get(pos));
                                    list_adapter.notifyDataSetChanged();
                                    myHeaderList.remove(pos);
                                    columnsData.remove(pos);
                                    mygrandList.remove(pos);
                                } else {
                                    Log.i("TAG", "removed column before header:" + columnsData);
                                    //user hasn't added column after cmg in this screen so we just delete it from firebase.
                                    deletecolumn(columnsData.get(pos).get("title").toString(), columnsData.get(pos).get("id").toString());
                                    myHeaderList.remove(pos);
                                    columnsData.remove(pos);
                                    mygrandList.remove(pos);
                                    Log.i("TAG", "removed column:" + myHeaderList);
                                    Log.i("TAG", "removed column after header:" + columnsData);
                                    ((MainActivity) getActivity()).ClearBoardAddColumn(mygrandList, myHeaderList);
                                    ((MainActivity) getActivity()).changeDataInFirebase(columnsData);
                                }
                                Log.i("TAG", "added col data after delete:" + added);
                                list_adapter.notifyDataSetChanged();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {

                            }
                        })
                        .show();
            }
        }
//        else{
//            Toast.makeText(getContext(),"no write permission, contact board admin",Toast.LENGTH_SHORT).show();
//        }

    }

    public void MoveAllCardDialog(final int column) {
        Spinner boardlist;
        final Spinner collist;
        List<String> boardList = new ArrayList<>();
        MaterialDialog moveallcard = new MaterialDialog.Builder(getContext())
                .title("Move all cards")
                .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                .titleColorRes(R.color.colorPrimary)
                .positiveText("Move").positiveColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .customView(R.layout.promp_move_all_cards, true)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(final MaterialDialog dialog, DialogAction which) {
                        Spinner spinner_boardlist = (Spinner) dialog.findViewById(R.id.spn_boardlist);
                        Spinner spinner_list = (Spinner) dialog.findViewById(R.id.spn_list);
                        if (spinner_boardlist.getSelectedItem().equals(MainActivity.type)) {
                            //same board
                            String selected = spinner_list.getSelectedItem().toString();//current column
                            String now = (String) columnsData.get(column).get("title");
                            if (!selected.equals(now)) {
                                int newColumnPos = spinner_list.getSelectedItemPosition();
                                int viewCount = mygrandList.get(column).size();
                                int sourceindex = mygrandList.get(newColumnPos).size();
                                HashMap<String, Object> cardsmapdata = new HashMap<String, Object>();
                                String colid = columnsData.get(newColumnPos).get("id").toString();
                                for (int i = 0; i < viewCount; i++) {
                                    cardsmapdata.put(mygrandList.get(column).get(i).getObjectId(), mygrandList.get(column).get(i).getCardJSON(colid, sourceindex++));
                                }
                                myHeaderList.remove(column);
                                columnsData.remove(column);
                                mygrandList.remove(column);
                                list_adapter.notifyDataSetChanged();
                                MainActivity.BOARD_.updateAllCardData(MainActivity.BOARD_ID, cardsmapdata, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                        Log.i("TAG", "card data updated in firebase");
                                        ((MainActivity) getActivity()).ClearBoardAddColumn(mygrandList, myHeaderList);
                                        ((MainActivity) getActivity()).changeDataInFirebase(columnsData);
                                        dialog.dismiss();
                                    }
                                });
                            } else {
                                Toast.makeText(getContext(), "not moved", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            //board change
                            String newboardID = (String) otherBoardData.get(spinner_boardlist.getSelectedItemPosition()).get("bid");
                            int newColumnPos = spinner_list.getSelectedItemPosition();
                            List<HashMap<String, Object>> coldata = (List<HashMap<String, Object>>) otherBoardData.get(spinner_boardlist.getSelectedItemPosition()).get("coldata");
                            String newColumnId = (String) coldata.get(newColumnPos).get("id");

                            int viewCount = mygrandList.get(column).size();
                            List<String> cardOldid = new ArrayList<>();
                            HashMap<String, Object> cardData = new HashMap<>();

                            for (int i = 0; i < viewCount; i++) {
                                cardOldid.add(mygrandList.get(column).get(i).getObjectId());
                                cardData.put(mygrandList.get(column).get(i).getObjectId(), mygrandList.get(column).get(i).getCardJSON(newColumnId, 0));
                            }
                            Log.i("TAG", "Cards old id:" + cardOldid);
                            for (int i = 0; i < cardOldid.size(); i++) {
                                MainActivity.BOARD_.getBOARD_DATA(MainActivity.BOARD_ID).child(cardOldid.get(i)).removeValue();
                            }

                            myHeaderList.remove(column);
                            columnsData.remove(column);
                            mygrandList.remove(column);
                            list_adapter.notifyDataSetChanged();
                            MainActivity.BOARD_.updateAllCardData(newboardID, cardData, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    ((MainActivity) getActivity()).ClearBoardAddColumn(mygrandList, myHeaderList);
                                    ((MainActivity) getActivity()).changeDataInFirebase(columnsData);
                                    dialog.dismiss();
                                }
                            });
                            dialog.dismiss();

                        }
                        deletecolumn(columnsData.get(column).get("title").toString(), columnsData.get(column).get("id").toString());
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {

                    }
                })
                .show();
        boardlist = (Spinner) moveallcard.findViewById(R.id.spn_boardlist);
        collist = (Spinner) moveallcard.findViewById(R.id.spn_list);
        for (int i = 0; i < otherBoardData.size(); i++) {
            boardList.add((String) otherBoardData.get(i).get("name"));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, boardList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        boardlist.setAdapter(adapter);
        boardlist.setSelection(boardList.indexOf(MainActivity.type));

        List<String> newcoldata = new ArrayList<>();
        for (int i = 0; i < columnsData.size(); i++) {
            if (!columnsData.get(i).get("title").toString().equals(columnsData.get(column).get("title")))
                newcoldata.add(columnsData.get(i).get("title").toString());
        }

        ArrayAdapter<String> a = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, newcoldata);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        collist.setAdapter(a);

        boardlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                List<String> newcollist = new ArrayList<>();
                List<HashMap<String, Object>> coldata = (List<HashMap<String, Object>>) otherBoardData.get(position).get("coldata");
                for (int i = 0; i < coldata.size(); i++) {
                    if (!coldata.get(i).get("id").equals(columnsData.get(column).get("id")))
                        newcollist.add((String) coldata.get(i).get("title"));

                }
                ArrayAdapter<String> a = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, newcollist);
                a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                collist.setAdapter(a);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void deletecolumn(String colname, String colid) {
        Zboard_history zboard_history = new Zboard_history();
        HashMap<String, Object> datamap = zboard_history.getJSON_DeleteColumn(colname, colid, zAuth.get());
        zboard_history.addHistory(MainActivity.BOARD_ID, datamap, null);
    }

    public void addColumn(String colname, String colid) {
        Zboard_history zboard_history = new Zboard_history();
        HashMap<String, Object> datamap = zboard_history.getJSON_AddColumn(colname, colid, zAuth.get());
        zboard_history.addHistory(MainActivity.BOARD_ID, datamap, null);
    }

    public void editColumn(String colid, String oldname, String newname) {
        Zboard_history zboard_history = new Zboard_history();
        HashMap<String, Object> datamap = zboard_history.getJSON_EditColumn(colid, oldname, newname, zAuth.get());
        zboard_history.addHistory(MainActivity.BOARD_ID, datamap, null);
    }
}

