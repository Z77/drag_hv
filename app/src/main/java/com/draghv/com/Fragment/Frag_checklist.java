package com.draghv.com.Fragment;

import android.content.Context;
import android.icu.text.MessagePattern;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.draghv.com.CardDetailActivity;
import com.draghv.com.Adapter.MemberAdapter;
import com.draghv.com.MainActivity;
import com.draghv.com.R;
import com.draghv.com.ZAuth;
import com.draghv.com.Zboard_history;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 09-Feb-17.
 */
public class Frag_checklist extends Fragment {
    View rootView;
    EditText input;
    LinearLayout parent;
    ScrollView scroll;
    static List<HashMap<String, Object>> checkListData;
    //List<MemberPOJO> checklistdata;
    MemberAdapter checklistAdapter;
    Zboard_history zboard_history;
    List<HashMap<String, Object>> updated = new ArrayList<>();
    int deleteTag=0;
    ZAuth zAuth;
    String userid;
    FloatingActionButton add_Checklist;


    public static Fragment instantiate(List<HashMap<String, Object>> checkdatalist) {
        Frag_checklist fragment = new Frag_checklist();
        checkListData = checkdatalist;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        zAuth=new ZAuth();
        userid=zAuth.get();
        rootView = inflater.inflate(R.layout.fragment_checklist, container, false);
        add_Checklist=(FloatingActionButton)rootView.findViewById(R.id.add_checklist);
        add_Checklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getContext(),"Show dialog",Toast.LENGTH_SHORT).show();
                showAddDialog();
            }
        });
        input = (EditText) rootView.findViewById(R.id.input_editext);
        parent = (LinearLayout) rootView.findViewById(R.id.lv_chklist);
        scroll=(ScrollView)rootView.findViewById(R.id.scroll_chk);
        zboard_history = new Zboard_history();



//        checklistdata=new ArrayList<>();
//        checklistAdapter=new MemberAdapter(checklistdata,getContext());
//        parent.setAdapter(checklistAdapter);
        Log.i("TAG", "already added:" + checkListData);

        input.setImeActionLabel("Add", KeyEvent.KEYCODE_ENTER);
        if (checkListData != null) {
            for (int i = 0; i < checkListData.size(); i++) {
                View row = LayoutInflater.from(getContext()).inflate(R.layout.frag_checklist_row, container, false);
                CheckBox chk = (CheckBox) row.findViewById(R.id.chk_row);
                final ImageView delete=(ImageView)row.findViewById(R.id.delete_chk);
                delete.setTag(deleteTag++);
                final String title = (String) checkListData.get(i).get("title");
                Boolean select = (Boolean) checkListData.get(i).get("selected");
                chk.setText(title);
                chk.setChecked(select);
                parent.addView(row);
                chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked)
                            markCheck(title);
                        else
                            markUncheck(title);

                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        removeCheckbox((Integer) delete.getTag());
//                        Toast.makeText(getContext(),"Pos:"+delete.getTag(),Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        //showKeyboard();
        scroll.fullScroll(View.FOCUS_DOWN);//to scroll down scrollview

        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            boolean handled = false;

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (input.getText().length() > 0) {
                    if (actionId == KeyEvent.KEYCODE_ENTER || actionId == KeyEvent.KEYCODE_ENDCALL) {
                        View row = LayoutInflater.from(getContext()).inflate(R.layout.frag_checklist_row, container, false);
                        CheckBox chk = (CheckBox) row.findViewById(R.id.chk_row);
                        final ImageView delete=(ImageView)row.findViewById(R.id.delete_chk);
                        delete.setTag(deleteTag++);
                        final String title = input.getText().toString();
                        chk.setText(title);
                        parent.addView(row);
                        if (checkListData != null) {
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("title", chk.getText().toString());
                            map.put("selected", chk.isChecked());
                            updated.add(map);
                        }
                        delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                removeCheckbox((Integer) delete.getTag());
                                //Toast.makeText(getContext(),"Pos:"+delete.getTag(),Toast.LENGTH_SHORT).show();
                            }
                        });
//                        checklistdata.add(new MemberPOJO(title, false));
//                        checklistAdapter.notifyDataSetChanged();
                        input.setText("");
                        handled = true;
                        chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if(isChecked)
                                    markCheck(title);
                                else
                                    markUncheck(title);
                            }
                        });
                    }
                } else {
                    Toast.makeText(getContext(), "Text can not be blank !!!", Toast.LENGTH_SHORT).show();
                }
                return handled;
            }
        });
        return rootView;
    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void showAddDialog(){
        MaterialDialog add=new MaterialDialog.Builder(getContext())
                .title("Add Checklist").titleColor(ContextCompat.getColor(getContext(),R.color.colorPrimary))
                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME)
                .inputRange(1, 50, ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .positiveText("New")
                .negativeText("Add")
                .neutralText("Cancel")
                .autoDismiss(false)
                .widgetColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        //Toast.makeText(getContext(), "positive", Toast.LENGTH_SHORT).show();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        //Toast.makeText(getContext(), "negative", Toast.LENGTH_SHORT).show();
                        final String input=dialog.getInputEditText().getText().toString();
                        if (input.length() > 0) {
                            final View row = LayoutInflater.from(getContext()).inflate(R.layout.frag_checklist_row, null, false);
                            CheckBox chk = (CheckBox) row.findViewById(R.id.chk_row);
                            final ImageView delete = (ImageView) row.findViewById(R.id.delete_chk);
                            delete.setTag(deleteTag++);

                            chk.setText(input);
                            parent.addView(row);
                            if (checkListData != null) {
                                HashMap<String, Object> map = new HashMap<>();
                                map.put("title", chk.getText().toString());
                                map.put("selected", chk.isChecked());
                                updated.add(map);
                            }
                            delete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    removeCheckbox((Integer) delete.getTag());
                                    //Toast.makeText(getContext(),"Pos:"+delete.getTag(),Toast.LENGTH_SHORT).show();
                                }
                            });
//                        checklistdata.add(new MemberPOJO(title, false));
//                        checklistAdapter.notifyDataSetChanged();

                            //handled = true;
                            chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked)
                                        markCheck(input);
                                    else
                                        markUncheck(input);
                                }
                            });
                        }
                        dialog.dismiss();
                    }
                })
                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                     //   Toast.makeText(getContext(), "neutral", Toast.LENGTH_SHORT).show();
                    }
                })
                .input("Add item...", "", false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, final CharSequence input) {
                        View row = LayoutInflater.from(getContext()).inflate(R.layout.frag_checklist_row, null, false);
                        CheckBox chk = (CheckBox) row.findViewById(R.id.chk_row);
                        final ImageView delete=(ImageView)row.findViewById(R.id.delete_chk);
                        delete.setTag(deleteTag++);

                        chk.setText(input);
                        parent.addView(row);
                        if (checkListData != null) {
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("title", chk.getText().toString());
                            map.put("selected", chk.isChecked());
                            updated.add(map);
                        }
                        delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                removeCheckbox((Integer) delete.getTag());
                                //Toast.makeText(getContext(),"Pos:"+delete.getTag(),Toast.LENGTH_SHORT).show();
                            }
                        });
//                        checklistdata.add(new MemberPOJO(title, false));
//                        checklistAdapter.notifyDataSetChanged();

                        //handled = true;
                        chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if(isChecked)
                                    markCheck((String) input);
                                else
                                    markUncheck((String) input);
                            }
                        });
                        dialog.getInputEditText().setText("");
                        Toast.makeText(getContext(), "checkbox="+input, Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_done, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.setGroupVisible(R.id.detail_menu_group, false);
        menu.findItem(R.id.action_done_detail).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            getActivity().onBackPressed();
        }
        if (itemId == R.id.action_done) {

//            if(CardDetailActivity.checklist_datalist.size()>0)
//                CardDetailActivity.checklist_datalist.clear();
            List<HashMap<String, Object>> chklist = new ArrayList<>();
            for (int i = 0; i < parent.getChildCount(); i++) {
                View row = parent.getChildAt(i);
                CheckBox chk = (CheckBox) row.findViewById(R.id.chk_row);
                HashMap<String, Object> map = new HashMap<>();
                map.put("title", chk.getText().toString());
                map.put("selected", chk.isChecked());
                chklist.add(map);
                //CardDetailActivity.checklist_datalist.add(new MemberPOJO(checklistdata.get(i).getTitle(),checklistdata.get(i).getSelected()));
            }

            CardDetailActivity.itemdata.setChecklist(chklist);
            CardDetailActivity.card_chklist.setVisibility(View.VISIBLE);
            CardDetailActivity.passDataToAdapterAndSet(CardDetailActivity.itemdata);
            ((CardDetailActivity) getActivity()).setChecklist(chklist);
            getActivity().onBackPressed();

            if (checkListData == null) {
                HashMap<String, Object> data = zboard_history.getJSON_addChecklist(CardDetailActivity.itemdata,userid, "added", null);
                zboard_history.addHistory(MainActivity.BOARD_ID, data, null);
                Log.i("TAG", "Added checklist");
            } else {
                if (chklist.size() > checkListData.size()) {
                    HashMap<String, Object> data = zboard_history.getJSON_addChecklist(CardDetailActivity.itemdata, userid, "updated", updated);
                    zboard_history.addHistory(MainActivity.BOARD_ID, data, null);
                    Log.i("TAG", "updated checklist");
                }
                //if (!chklist.equals(checkListData)) {
//                    for (int i = 0; i < chklist.size(); i++) {
//                        if (!chklist.contains(checkListData.get(i))) {
//                            if(((Boolean)chklist.get(i).get("selected"))&&((Boolean)checkListData.get(i).get("selected")))
//                                Log.i("TAG", "checked checkbox of " + checkListData.get(i).get("title"));
//                        }
//                    }

                //}
            }
        }
        return super.onOptionsItemSelected(item);
    }
    public void markCheck(String title){
        HashMap<String,Object> mapdata=zboard_history.getJSON_checkedChecklist(title, userid, CardDetailActivity.itemdata.getData(), CardDetailActivity.itemdata.getObjectId(), true);
        zboard_history.addHistory(MainActivity.BOARD_ID,mapdata,null);
    }
    public void markUncheck(String title){
        HashMap<String,Object> mapdata=zboard_history.getJSON_checkedChecklist(title, userid, CardDetailActivity.itemdata.getData(),CardDetailActivity.itemdata.getObjectId(),false);
        zboard_history.addHistory(MainActivity.BOARD_ID,mapdata,null);
    }

    public void removeCheckbox(int pos){
        parent.removeViewAt(pos);
    }

}
