package com.draghv.com.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.draghv.com.CardDetailActivity;
import com.draghv.com.MainActivity;
import com.draghv.com.R;
import com.draghv.com.ZAuth;
import com.draghv.com.Zboard_history;

import java.util.HashMap;

/**
 * Created by Payal on 02-Feb-17.
 */
public class Frag_Description extends Fragment implements TextWatcher {
    private static String desc;
    private View rootView;
    private EditText desc_text;
    private Button btnSetDesc;
    TextView count;
    int pos,row;
    Zboard_history history;
    ZAuth zAuth;

    public static Fragment instantiate(String description) {
        Frag_Description frag_itemDesc=new Frag_Description();
        desc=description;
        return frag_itemDesc;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_item_desc, container, false);
        desc_text = (EditText) rootView.findViewById(R.id.description_text);
        btnSetDesc=(Button)rootView.findViewById(R.id.btn_setDescription);
        count=(TextView)rootView.findViewById(R.id.count_desc);
        desc_text.addTextChangedListener(this);
        zAuth=new ZAuth();
        desc_text.setText(desc);
        pos= CardDetailActivity.pos;
        row= CardDetailActivity.row;
//        btnSetDesc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//            }
//        });
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        //super.onPrepareOptionsMenu(menu);
        menu.setGroupVisible(R.id.detail_menu_group, false);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if(itemId==R.id.done_check){
            InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.RESULT_UNCHANGED_HIDDEN, 0);

            if(desc_text.getText().length()>0) {
                CardDetailActivity.itemdata.setDesc(desc_text.getText().toString().trim());
                CardDetailActivity.description = desc_text.getText().toString().trim();
                CardDetailActivity.tv_card_desc.setText(desc_text.getText().toString().trim());

                ((CardDetailActivity) getActivity()).passDataToAdapterAndSet(CardDetailActivity.itemdata);
//                MainActivity.adapterList.get(pos).changeData(CardDetailActivity.itemdata, row);
//                MainActivity.updateDataInFirebase(CardDetailActivity.itemdata, pos, row);
                getActivity().onBackPressed();
                if (desc == null || desc.equals("")) {
                    history = new Zboard_history();
                    HashMap<String, Object> mapdata = history.getJSON_CardDesc(CardDetailActivity.itemdata.getObjectId(), CardDetailActivity.itemdata.getData(), zAuth.get());
                    history.addHistory(MainActivity.BOARD_ID, mapdata, null);
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count_) {
        count.setText(String.valueOf(count_));
    }

    @Override
    public void afterTextChanged(Editable s) {
        count.setText(String.valueOf(s.length()));
    }
}
