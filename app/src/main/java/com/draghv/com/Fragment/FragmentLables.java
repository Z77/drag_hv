package com.draghv.com.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.draghv.com.Adapter.ColorAdapter;
import com.draghv.com.Models.ColorPOJO;
import com.draghv.com.Adapter.ColorlabelAdapter;
import com.draghv.com.Models.ColorlabelListPOJO;
import com.draghv.com.MainActivity;
import com.draghv.com.R;
import com.draghv.com.ZAuth;
import com.draghv.com.Zboard;
import com.draghv.com.Zboard_history;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Payal on 04-Feb-17.
 */
public class FragmentLables extends Fragment {

    View rootview;
    public static List<HashMap<String, Object>> colorlabelList;
    static List<ColorlabelListPOJO> colorlabelListPOJOList = new ArrayList<>();
    List<ColorPOJO> colorlist;

    ColorlabelAdapter colorlabelAdapter;
    ListView colorlabellist_LV;
    FloatingActionButton fab_addmoreColor;
    String color;
    Zboard board;
    Zboard_history zboard_history;
    ZAuth zAuth;

    List<String> newaddedColor = new ArrayList<>();//for add new color in history
    List<String> deletedColor = new ArrayList<>();//for delete entry in history
    List<String> colorcode = new ArrayList<>();//for edit entry in history
    List<String> oldtitle = new ArrayList<>();//for edit entry in history
    List<String> newtitle = new ArrayList<>();//for edit entry in history
    Boolean is_write;

    public static Fragment instantiate(List<HashMap<String, Object>> colorlabellist) {
        FragmentLables lables = new FragmentLables();
        colorlabelList = colorlabellist;
        return lables;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_lables, container, false);
        zAuth = new ZAuth();
        fab_addmoreColor = (FloatingActionButton) rootview.findViewById(R.id.fabAddColor);
        color = "";
        colorlist = new ArrayList<>();
        board = new Zboard();
        zboard_history = new Zboard_history();
        addColors();
        if(((MainActivity)getActivity()).is_write){
            fab_addmoreColor.setVisibility(View.VISIBLE);
        }else {
            fab_addmoreColor.setVisibility(View.GONE);
        }
        fab_addmoreColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showColorDialog();
            }
        });
        colorlabellist_LV = (ListView) rootview.findViewById(R.id.list_lable);
        if (colorlabelListPOJOList.size() > 0)
            colorlabelListPOJOList.clear();
        for (int i = 0; i < colorlabelList.size(); i++) {
            String color = (String) colorlabelList.get(i).get("color");
            String title = (String) colorlabelList.get(i).get("title");
            colorlabelListPOJOList.add(new ColorlabelListPOJO(title, color));
        }
        is_write=((MainActivity)getActivity()).is_write;
        colorlabelAdapter = new ColorlabelAdapter(getContext(), colorlabelListPOJOList, this,is_write);
        colorlabellist_LV.setAdapter(colorlabelAdapter);
        Toast.makeText(getContext(), "in frag labels", Toast.LENGTH_SHORT).show();
        return rootview;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.setGroupVisible(R.id.main_menu_group, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if(is_write)
            inflater.inflate(R.menu.menu_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        Log.i("TAG", "item option id int" + itemId);
        Log.i("TAG", "itme option done_check" + R.id.done_check);
        if (itemId == android.R.id.home) {
            getActivity().onBackPressed();
        }
        if (itemId == R.id.done_check) {
            colorlabelList.clear();
            for (int i = 0; i < colorlabelListPOJOList.size(); i++) {
                String color = colorlabelListPOJOList.get(i).getHexColor();
                String title = colorlabelListPOJOList.get(i).getTitle();
                colorlabelList.add(getHasMap(title, color));
            }
            if (newaddedColor.size() > 0) {
                addColorHistory(newaddedColor);
            }
            if (deletedColor.size() > 0) {
                deleteColorHistory(deletedColor);
            }
            if (colorcode.size() > 0) {
                editColorHistory(colorcode, oldtitle, newtitle);
            }

            if (((MainActivity) getActivity()).fromShare) {
                String ownerid = ((MainActivity) getActivity()).ownerId;
                board.getROOT().child(ownerid).child(MainActivity.BOARD_ID).child("label").setValue(colorlabelList);
            } else {
                board.getBOARD_ROOT(MainActivity.BOARD_ID).child("label").setValue(colorlabelList);
            }
            getActivity().onBackPressed();
            //Toast.makeText(getActivity().getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void showColorDialog() {
        GridView gridView;
        final ColorAdapter colorAdapter;
        final MaterialDialog addColor = new MaterialDialog.Builder(getContext())
                .title("Color Palette")
                .customView(R.layout.prompt_color_add, true)
                .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                .titleColorRes(R.color.colorPrimary)
                .positiveText("Add").positiveColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        Boolean found = false;
                        for (int i = 0; i < colorlabelListPOJOList.size(); i++) {
                            if (colorlabelListPOJOList.get(i).getHexColor().equals(color))
                                found = true;
                        }
                        if (found) {
                            Toast.makeText(getContext(), "This color is already added,choose other color.", Toast.LENGTH_LONG).show();
                        } else {
                            colorlabelListPOJOList.add(new ColorlabelListPOJO("", color)); //4 adapter to show user
                            colorlabelList.add(getHasMap("", color));//4 firebase
                            if (deletedColor.contains(getColorName(color)))
                                deletedColor.remove(getColorName(color));
                            newaddedColor.add(getColorName(color));//4 history
                            colorlabelAdapter.notifyDataSetChanged();
                            dialog.dismiss();
                            Toast.makeText(getContext(), "select color", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .canceledOnTouchOutside(false)
                .show();
        gridView = (GridView) addColor.findViewById(R.id.color_grid);
        colorAdapter = new ColorAdapter(colorlist, getContext());
        gridView.setAdapter(colorAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < colorlist.size(); i++) {
                    Boolean selected = colorlist.get(i).getSelected();
                    if (selected) {
                        colorlist.get(i).setSelected(false);
                        colorlist.get(position).setSelected(true);
                        colorAdapter.notifyDataSetChanged();
                        break;
                    }
                }
                color = colorlist.get(position).getColor();
            }
        });
    }

    private void addColors() {
        //colorlist.add(new ColorPOJO(getString(R.string.red_500), true));
        colorlist.add(new ColorPOJO(getString(R.string.pink_500), true));
        colorlist.add(new ColorPOJO(getString(R.string.purple_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.deep_purple_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.indigo_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.blue_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.light_blue_500), false));
        //colorlist.add(new ColorPOJO(getString(R.string.cyan_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.teal_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.light_green_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.lime_500), false));
        //colorlist.add(new ColorPOJO(getString(R.string.yellow_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.amber_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.orange_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.deep_orange_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.grey_500), false));
        colorlist.add(new ColorPOJO(getString(R.string.blue_500), false));
    }

    private String getColorName(String colorCode) {
        String name = "";
        if (colorCode.equals(getString(R.string.red_500))) {
            name = "Red";
            return name;
        } else if (colorCode.equals(getString(R.string.purple_500))) {
            name = "Purple";
            return name;
        } else if (colorCode.equals(getString(R.string.light_purple))) {
            name = "Light purple";
            return name;
        } else if (colorCode.equals(getString(R.string.sky_500))) {
            name = "Sky";
            return name;
        } else if (colorCode.equals(getString(R.string.deep_purple_500))) {
            name = "Dark Purple";
            return name;
        } else if (colorCode.equals(getString(R.string.indigo_500))) {
            name = "Indigo";
            return name;
        } else if (colorCode.equals(getString(R.string.blue_500))) {
            name = "Blue";
            return name;
        } else if (colorCode.equals(getString(R.string.light_blue_500))) {
            name = "LightBlue";
            return name;
        } else if (colorCode.equals(getString(R.string.cyan_500))) {
            name = "Cyan";
            return name;
        } else if (colorCode.equals(getString(R.string.teal_500))) {
            name = "Teal";
            return name;
        } else if (colorCode.equals(getString(R.string.green_500))) {
            name = "Green";
            return name;
        } else if (colorCode.equals(getString(R.string.light_green_500))) {
            name = "LightGreen";
            return name;
        } else if (colorCode.equals(getString(R.string.lime_500))) {
            name = "Lime";
            return name;
        } else if (colorCode.equals(getString(R.string.yellow_500))) {
            name = "Yellow";
            return name;
        } else if (colorCode.equals(getString(R.string.amber_500))) {
            name = "Amber";
            return name;
        } else if (colorCode.equals(getString(R.string.orange_500))) {
            name = "Orange";
            return name;
        } else if (colorCode.equals(getString(R.string.deep_orange_500))) {
            name = "DeepOrange";
            return name;
        } else if (colorCode.equals(getString(R.string.grey_500))) {
            name = "Grey";
            return name;
        } else if (colorCode.equals(getString(R.string.blue_grey_500))) {
            name = "BlueGrey";
            return name;
        }
        return name;
    }

    public void DeleteLabel(final int pos, final View v) {
        if(((MainActivity)getActivity()).is_write) {
            new MaterialDialog.Builder(v.getContext())
                    .title("Delete label")
                    .positiveText("Delete")
                    .negativeText("Cancel")
                    .content("Are you sure to delete?There is no undo for this action.")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            Toast.makeText(v.getContext(), "pos:" + pos, Toast.LENGTH_SHORT).show();
                            if (newaddedColor.contains(getColorName(colorlabelListPOJOList.get(pos).getHexColor())))
                                newaddedColor.remove(getColorName(colorlabelListPOJOList.get(pos).getHexColor()));
                            deletedColor.add(getColorName(colorlabelListPOJOList.get(pos).getHexColor()));
                            FragmentLables.colorlabelList.remove(pos);
                            colorlabelListPOJOList.remove(pos);
                            colorlabelAdapter.notifyDataSetChanged();

                            dialog.dismiss();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            dialog.dismiss();
                        }
                    }).show();
        }
//        else{
//            Toast.makeText(getContext(),"no write perimission,contact board admin",Toast.LENGTH_SHORT).show();
//        }
    }

    public void EditLabel(final int pos, final View v) {
        if (((MainActivity) getActivity()).is_write) {
            new MaterialDialog.Builder(v.getContext())
                    .title("Edit label name").titleColor(ContextCompat.getColor(getContext(),R.color.colorPrimary))
                    .positiveText("Ok")
                    .negativeText("Cancel")
                    .customView(R.layout.prompt_rename_header, true)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            Toast.makeText(v.getContext(), "pos:" + pos, Toast.LENGTH_SHORT).show();
                            final EditText tv_title = (EditText) dialog.findViewById(R.id.tv_list_title);
                            if (tv_title.getText().toString().length() > 0) {
                                //datalist.get(pos).setTitle(tv_title.getText().toString().trim());
                                Log.i("TAG", "hex:" + colorlabelListPOJOList.get(pos).getHexColor());
                                Log.i("TAG", "name:" + getColorName(colorlabelListPOJOList.get(pos).getHexColor()));

                                colorcode.add(getColorName(colorlabelListPOJOList.get(pos).getHexColor()));//4 history
                                if (colorlabelListPOJOList.get(pos).getTitle().length() > 0)
                                    oldtitle.add(colorlabelListPOJOList.get(pos).getTitle());
                                else
                                    oldtitle.add("");
                                newtitle.add(tv_title.getText().toString().trim());
                                colorlabelListPOJOList.get(pos).setTitle(tv_title.getText().toString().trim());
                                colorlabelAdapter.notifyDataSetChanged();
                            }
                            dialog.dismiss();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            dialog.dismiss();
                        }
                    }).show();
        }
//        else{
//            Toast.makeText(getContext(),"no write permission,Contact board admin",Toast.LENGTH_SHORT).show();
//        }
    }

    private HashMap<String, Object> getHasMap(String title, String color) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("title", title);
        data.put("color", color);
        return data;
    }

    public void addColorHistory(List<String> newaddedColor) {
        HashMap<String, Object> color = zboard_history.getJSON_Addlabel(newaddedColor, zAuth.get(), MainActivity.BOARD_ID, MainActivity.type);
        zboard_history.addHistory(MainActivity.BOARD_ID, color, null);
    }

    public void editColorHistory(List<String> colorcode, List<String> oldtitle, List<String> newtitle) {
        HashMap<String, Object> color = zboard_history.getJSON_Editlabel(colorcode, oldtitle, newtitle, zAuth.get(), MainActivity.BOARD_ID, MainActivity.type);
        zboard_history.addHistory(MainActivity.BOARD_ID, color, null);
    }

    public void deleteColorHistory(List<String> deletedColor) {
        HashMap<String, Object> color = zboard_history.getJSON_Removelabel(deletedColor, zAuth.get(), MainActivity.BOARD_ID, MainActivity.type);
        zboard_history.addHistory(MainActivity.BOARD_ID, color, null);
    }
}
