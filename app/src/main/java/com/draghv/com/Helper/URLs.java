package com.draghv.com.Helper;

import java.util.Date;
import java.util.Random;

/**
 * Created by karthik on 29/01/16.
 */
public class URLs {
    public static String MASTER_URL= "https://zobaze.firebaseio.com";
    public static String MASTER_URL_USER= "_user";
    public static String MASTER_URL_BUSINESS="business";
    public static String MASTER_URL_BCATEGORY="b_category";
    public static String MASTER_URL_BITEMS="b_items";
    public static String MASTER_URL_FOLLOW="follow";
    public static String FOLLOWING="imfollowing";
    public static String FOLLOWER="myfollowers";
    public static String COUNTS="counts";
    public static String USER="users";
    public static String BUSINESS="business";
    public static final String CONNECTION = "connection";
    public static final String ONLINE = "online";
    public static final String OFFLINE = "offline";
    public static final String FEEDS = "feeds";
    public static final String POSTS = "posts";
    public static final String LIKES = "likes";
    public static final String COMMENTS = "comments";
    public static final String RATING = "rating";
    public static String BUSINESSCOUNT="businesscount";
    public static String INVITE="invite";
    public static String SUBSCRIBE="subscribe";
    public static String FEECBACK="feedback";
    public static String CHAT="chat";
    public static String NOTI="notification";
    public static String ALLBUSINESS="total";
    public static String HISTORY="userhistory";
    public static String SEARCH="search";
    public static String RECENTITEMS="recentitems";
    public static String SERVICE="service";
    public static String PRODUCT="product";
    public static String ALLITEM="all";
    public static String PEOPLE="people";
    public static String SHORT="short";
    public static String BUSILOCATION="business-loc";
    public static String OFFER="offer";
    public static String FAVOURITE="favourite";
    public static String LIST="list";
    public static String COLLECTION="collection";
    public static String COLLECTIONLIST="collectionlist";
    public static String SHAREDWITH="sharedwith";
    public static String CATEGORY="category";
    public static String ITEMS="items";
    public static String CONFIG="config";
    public static String ORDERLIST="orderlist";
    public static String TICKETS="tickets";
    public static String TICKETSMETADATA="ticket_metadata";
    public static String HISTORYMETADATA="history";




        public static String generateRandomString(int length) {
        Random random = new Random((new Date()).getTime());
        char[] values = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3',
                '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

        String out = "";

        for (int i = 0; i < length; i++) {
            int idx = random.nextInt(values.length);
            out += values[idx];
        }
        return out;
    }
}
