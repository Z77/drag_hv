package com.draghv.com;

/**
 * Created by Payal on 12-Jan-17.
 */
public class BoardListPOJO {
    String boardname;
    Boolean favorite;

    public BoardListPOJO(String boardname, Boolean favorite) {
        this.boardname = boardname;
        this.favorite = favorite;
    }

    public String getBoardname() {
        return boardname;
    }

    public void setBoardname(String boardname) {
        this.boardname = boardname;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }
}
