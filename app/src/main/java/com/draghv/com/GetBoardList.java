package com.draghv.com;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Payal on 17-Jan-17.
 */
public class GetBoardList {
    Long createdAt;
    String objectId;
    String title;
    Long updatedAt;
    String userId;
    //int totalcol;
    int totalrow;
    ArrayList<HashMap<String,Object>> totalcol;
    ArrayList<HashMap<String,Object>> label;
    ArrayList<String> member;
    private Map<String,Object> boards;
    //ArrayList<String> sharedwith;
    HashMap<String,Object> sharedwith;


    public GetBoardList() {
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public String getObjectId() {
        return objectId;
    }

    public String getTitle() {
        return title;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public Map<String, Object> getBoards() {
        return boards;
    }

//    public int getTotalcol() {
//        return totalcol;
//    }

    public int getTotalrow() {
        return totalrow;
    }

    public ArrayList<HashMap<String,Object>> getTotalcol() {
        return totalcol;
    }

    public ArrayList<HashMap<String, Object>> getLabel() {
        return label;
    }

    public ArrayList<String> getMember() {
        return member;
    }

    public HashMap<String, Object> getSharedwith() {
        return sharedwith;
    }

    //when using arraylist
//    public ArrayList<String> getSharedwith() {
//        return sharedwith;
//    }
}
