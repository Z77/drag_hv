package com.draghv.com;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

/**
 * Created by Payal on 05-Dec-16.
 */
public class BusinessSelectionAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<BusinessData> listState;
    LayoutInflater mInflater;

//    public BusinessSelectionAdapter(Context context, int resource,ArrayList<BusinessData> listState) {
//        super(context, resource,listState);
//        this.listState = listState;
//        this.mContext=context;
//    }
     public BusinessSelectionAdapter(Context mContext, ArrayList<BusinessData> listState) {
        this.mContext = mContext;
        this.listState = listState;
        mInflater = (LayoutInflater)mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }
    class ViewHolder{
        ImageView img;
        TextView name;
        CheckBox checkBox;
        ProgressBar progressBar;
    }

    @Override
    public int getCount() {
        return listState.size();
    }

    @Override
    public Object getItem(int position) {
        return listState.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        BusinessData data= listState.get(position);
        LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.ticket_builder_items_select_row, parent, false);
            //convertView=LayoutInflater.from(mContext).inflate(R.layout.ticket_builder_items_select_row,null);
            holder = new ViewHolder();
            holder.img=(ImageView)convertView.findViewById(R.id.iv_imgthumbnail);
            holder.name=(TextView)convertView.findViewById(R.id.item_name);
            holder.checkBox=(CheckBox)convertView.findViewById(R.id.chk_item);
            holder.progressBar=(ProgressBar)convertView.findViewById(R.id.pbar);
            convertView.setTag(holder);
        }
        else{
            holder=(ViewHolder)convertView.getTag();
        }
        ImageLoaderConfiguration config=new ImageLoaderConfiguration.Builder(mContext).build();
        ImageLoader loader=ImageLoader.getInstance();
        loader.init(config);
        DisplayImageOptions options=new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .build();

            Log.i("TAG", "path is " + data.getImg_path());
            Log.i("TAG", "path is " + data.getName());


        loader.displayImage(data.getImg_path(), holder.img, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
//        }else{
//            holder.img.setBackgroundColor(Color.BLACK);
//        }
        holder.name.setText(data.getName());
        if(data.getIsSelectd()) {
            holder.checkBox.setChecked(true);
            holder.checkBox.setVisibility(View.VISIBLE);
        }else{
            holder.checkBox.setChecked(false);
            holder.checkBox.setVisibility(View.GONE);
        }


        return convertView;
    }

}
