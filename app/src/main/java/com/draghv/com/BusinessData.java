package com.draghv.com;

/**
 * Created by Payal on 05-Dec-16.
 */
public class BusinessData {
    String name;
    Boolean isSelectd;
    String img_path;

    public BusinessData(String name, String img_path, Boolean isSelectd) {
        this.name = name;
        this.isSelectd = isSelectd;
        this.img_path = img_path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsSelectd() {
        return isSelectd;
    }

    public void setIsSelectd(Boolean isSelectd) {
        this.isSelectd = isSelectd;
    }

    public String getImg_path() {
        return img_path;
    }

    public void setImg_path(String img_path) {
        this.img_path = img_path;
    }
}
