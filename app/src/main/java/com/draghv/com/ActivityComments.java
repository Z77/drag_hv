package com.draghv.com;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.draghv.com.Adapter.CommentAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Payal on 27-Feb-17.
 */
public class ActivityComments extends AppCompatActivity {
    Button send;
    EditText input;
    RecyclerView chat_list;
    GetBoardComment comment;
    List<GetBoardComment> commentList;
    CommentAdapter commentAdapter;
    String cardid;
    TextView nocomment;
    ChildEventListener listener;
    ValueEventListener valueEventListener;
    ZobazeUser user;
    Boolean is_write;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
        setContentView(R.layout.activity_comment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        send = (Button) findViewById(R.id.send_button);
        input = (EditText) findViewById(R.id.chat_edittext);
        chat_list = (RecyclerView) findViewById(R.id.chat_RV);
        nocomment = (TextView) findViewById(R.id.nocmnt);
        user = new ZobazeUser();

        RecyclerView.LayoutManager llm = new LinearLayoutManager(ActivityComments.this, LinearLayoutManager.VERTICAL, false);
        chat_list.setLayoutManager(llm);
        commentList = new ArrayList<>();
        commentAdapter = new CommentAdapter(commentList, ActivityComments.this);
        chat_list.setAdapter(commentAdapter);

        comment = new GetBoardComment();

        Bundle b = getIntent().getExtras();
        cardid = b.getString("cid");
        is_write = b.getBoolean("iswrite");
        Log.i("TAG", "card id is:" + cardid);

        loadData();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_write)
                    sendCommentToFirebase();
//                else
//                    Toast.makeText(getApplicationContext(),"no write permission,Contact board admin",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void sendCommentToFirebase() {
        if (!input.getText().toString().isEmpty()) {
            comment.setComment(input.getText().toString().trim());
            //comment.setUserId(getString(R.string.userid));
            comment.setUserId(user.userObjectId());
            input.setText("");
            comment.save(cardid, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    Toast.makeText(getApplicationContext(), "Comment sent", Toast.LENGTH_SHORT).show();
                    input.setText("");

                }
            });
        }
    }


    public void loadData() {
        valueEventListener = comment.getCardCommentRoot(cardid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    CardDetailActivity.itemdata.setCommentCnt(dataSnapshot.getChildrenCount());
                    CardDetailActivity.passDataToAdapterAndSet(CardDetailActivity.itemdata);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        listener = comment.getCardCommentRoot(cardid).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getChildrenCount() > 0) {
//                    CardDetailActivity.itemdata.setCommentCnt(dataSnapshot.getChildrenCount());
//                    CardDetailActivity.passDataToAdapterAndSet(CardDetailActivity.itemdata);
                    nocomment.setVisibility(View.GONE);
                    commentList.add(dataSnapshot.getValue(GetBoardComment.class));
                    commentAdapter.notifyDataSetChanged();
                    Log.i("TAG", "children added are:" + dataSnapshot);
                } else {
                    nocomment.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.i("TAG", "children changed are:" + dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.i("TAG", "children removed are:" + dataSnapshot);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.i("TAG", "children moved are:" + dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        comment.getCardCommentRoot(cardid).removeEventListener(listener);
        comment.getCardCommentRoot(cardid).removeEventListener(valueEventListener);
    }
}
