package com.draghv.com;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

/**
 * Created by Payal on 24-Nov-16.
 */
public class RVAdapterForImageList extends RecyclerView.Adapter<RVAdapterForImageList.myViewHolder> {
    public List<String> itemList;
    private Context context;

    public RVAdapterForImageList(List<String> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_builder_image_row, parent, false);
        myViewHolder mvh = new myViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, final int position) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
        final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        loader.init(config);
        final DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .build();
        loader.displayImage(itemList.get(position), holder.img_,options);

    }

    @Override
    public int getItemCount() {
//        Log.i("TAG", "" + itemList);
        if (itemList != null&&itemList.size()>0) {
            return itemList.size();
        } else {
            return 0;
        }
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_;
        private RelativeLayout close;

        public myViewHolder(View v) {
            super(v);
            img_ = (ImageView) v.findViewById(R.id.imageView_img);
            close = (RelativeLayout) v.findViewById(R.id.close);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeImage(getAdapterPosition());
                }
            });
        }
    }

    public void addImage(String obj) {
        itemList.add(obj);
        notifyDataSetChanged();
    }


    public void removeImage(int pos) {
        String url=itemList.remove(pos);
        CardDetailActivity.deleteImage(pos, url);
        notifyItemRemoved(pos);
        if (itemList.size() == 0)
            CardDetailActivity.removeImageFromList(pos);

    }
}


