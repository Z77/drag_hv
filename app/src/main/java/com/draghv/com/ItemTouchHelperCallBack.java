package com.draghv.com;

/**
 * Created by Payal on 13-Dec-16.
 */
public interface ItemTouchHelperCallBack {
    /**
     * Called when the {@link MyItemTouchHelperCallback} first registers an
     * item as being moved or swiped.
     * Implementations should update the item view to indicate
     * it's active state.
     */
    void onItemSelected();

    void onItemClear();
}
