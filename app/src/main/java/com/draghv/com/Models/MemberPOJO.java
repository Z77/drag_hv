package com.draghv.com.Models;

/**
 * Created by Payal on 09-Feb-17.
 */
public class MemberPOJO {
    String username;
    String userprofile;
    Boolean isMember;

    public MemberPOJO(String username, String userprofile, Boolean isMember) {
        this.username = username;
        this.userprofile = userprofile;
        this.isMember = isMember;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserprofile() {
        return userprofile;
    }

    public void setUserprofile(String userprofile) {
        this.userprofile = userprofile;
    }

    public Boolean getIsMember() {
        return isMember;
    }

    public void setIsMember(Boolean isMember) {
        this.isMember = isMember;
    }
}
