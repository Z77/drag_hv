package com.draghv.com.Models;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 28-Dec-16.
 */
public class MyItem {

    String data;
    String desc;
    long itemid;
    String objectId;
    HashMap<String,Object> duedate;
    Long commentCnt;
    int sharePersoncnt;
    List<String> colorcodes;
    List<String> imageAttach;
    List<HashMap<String,Object>> itemAttach;
    List<HashMap<String,Object>> checklist;
    Boolean comment;
    List<String> assign;
    Boolean auto_duedate;
    String columnid;

    public MyItem() {
    }

    public MyItem(long itemid,
                  String objectId,
                  String data,
                  String desc,
                  HashMap<String,Object> dueDate,
                  Long messagecnt,
                  List<String> colorcodes,
                  List<String> imageAttach,
                  List<HashMap<String,Object>> itemAttachCnt,
                  List<HashMap<String,Object>> chklist,
                  Boolean comment,
                  List<String> assign,
                  Boolean auto_duedate) {
        this.data = data;
        this.desc=desc;
        this.objectId=objectId;
        this.itemid=itemid;
        this.duedate=dueDate;
        this.commentCnt = messagecnt;
        this.colorcodes = colorcodes;
        this.imageAttach = imageAttach;
        this.itemAttach = itemAttachCnt;
        this.checklist=chklist;
        this.comment=comment;
        this.assign=assign;
        this.auto_duedate=auto_duedate;
    }

    public long getItemid() {
        return itemid;
    }

    public void setItemid(long itemid) {
        this.itemid = itemid;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public HashMap<String,Object> getdueDate() {
        return duedate;
    }

    public void setdueDate(HashMap<String,Object> dueDate) {
        this.duedate = dueDate;
    }

    public Long getCommentCnt() {
        return commentCnt;
    }

    public void setCommentCnt(Long commentCnt) {
        this.commentCnt = commentCnt;
    }

//    public int getSharePersoncnt() {
//        return sharePersoncnt;
//    }
//
//    public void setSharePersoncnt(int sharePersoncnt) {
//        this.sharePersoncnt = sharePersoncnt;
//    }

    public List<String> getColorcodes() {
        return colorcodes;
    }

    public void setColorcodes(List<String> colorcodes) {
        this.colorcodes = colorcodes;
    }

    public List<String> getImageAttach() {
        return imageAttach;
    }

    public void setImageAttach(List<String> imageAttach) {
        this.imageAttach = imageAttach;
    }

    public List<HashMap<String, Object>> getItemAttach() {
        return itemAttach;
    }

    public void setItemAttach(List<HashMap<String, Object>> itemAttachCnt) {
        this.itemAttach = itemAttachCnt;
    }

    public List<HashMap<String, Object>> getChecklist() {
        return checklist;
    }

    public void setChecklist(List<HashMap<String, Object>> checklist) {
        this.checklist = checklist;
    }

    public Boolean getComment() {
        return comment;
    }

    public void setComment(Boolean comment) {
        this.comment = comment;
    }

    public List<String> getAssign() {
        return assign;
    }

    public void setAssign(List<String> assign) {
        this.assign = assign;
    }

    public Boolean getAuto_duedate() {
        return auto_duedate;
    }

    public void setAuto_duedate(Boolean auto_duedate) {
        this.auto_duedate = auto_duedate;
    }

    public String getColumnid() {
        return columnid;
    }

    public void setColumnid(String columnid) {
        this.columnid = columnid;
    }

    public HashMap<String,Object> getCardJSON(String columnId,int row){
        HashMap<String,Object> objectHashMap=new HashMap<>();
        objectHashMap.put("objectId",this.objectId);
        objectHashMap.put("columnId",columnId);
        objectHashMap.put("row",row);
        objectHashMap.put("title",this.data);
        objectHashMap.put("description",this.desc);
        objectHashMap.put("arrItems",this.itemAttach);
        objectHashMap.put("attachment",this.imageAttach);
        objectHashMap.put("duedate",this.duedate);
        objectHashMap.put("label",this.colorcodes);
        objectHashMap.put("checklist",this.checklist);
        objectHashMap.put("comment",this.comment);
        objectHashMap.put("commentCnt",this.commentCnt);
        objectHashMap.put("assign",this.assign);
        return objectHashMap;
    }
}
