package com.draghv.com.Models;

/**
 * Created by Payal on 04-Feb-17.
 */
public class ColorlabelListPOJO {
    String title;
    String hexColor;

    public ColorlabelListPOJO(String title, String hexColor) {
        this.title = title;
        this.hexColor = hexColor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHexColor() {
        return hexColor;
    }

    public void setHexColor(String hexColor) {
        this.hexColor = hexColor;
    }
}
