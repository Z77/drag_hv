package com.draghv.com.Models;

/**
 * Created by Payal on 06-Feb-17.
 */
public class ColorPOJO {
    String color;
    Boolean selected;

    public ColorPOJO(String color, Boolean selected) {
        this.color = color;
        this.selected = selected;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
