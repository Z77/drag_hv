package com.draghv.com.Models;

/**
 * Created by Payal on 12-Jan-17.
 */
public class BoardListPOJO {
    String boardname;
    Boolean favorite;
    int sharedWithCount;

    public BoardListPOJO(String boardname, Boolean favorite,int sharedCount) {
        this.boardname = boardname;
        this.favorite = favorite;
        this.sharedWithCount=sharedCount;
    }

    public String getBoardname() {
        return boardname;
    }

    public void setBoardname(String boardname) {
        this.boardname = boardname;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public int getSharedWithCount() {
        return sharedWithCount;
    }

    public void setSharedWithCount(int sharedWithCount) {
        this.sharedWithCount = sharedWithCount;
    }
}
