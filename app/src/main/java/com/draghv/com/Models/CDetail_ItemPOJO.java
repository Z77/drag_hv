package com.draghv.com.Models;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Payal on 23-Nov-16.
 */
public class CDetail_ItemPOJO {
    ArrayList<HashMap<String, Object>> itemList;

    public ArrayList<HashMap<String, Object>> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<HashMap<String, Object>> itemList) {
        this.itemList = itemList;
    }
    public HashMap<String,Object> getItemJSON(){
        HashMap<String,Object> objectHashMap=new HashMap<>();
        objectHashMap.put("arrItems",this.itemList);
        objectHashMap.put("fieldName","Items");
        return objectHashMap;
    }

}
