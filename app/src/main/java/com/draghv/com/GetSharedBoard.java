package com.draghv.com;

/**
 * Created by Payal on 10-Mar-17.
 */
public class GetSharedBoard {
    String boardid;
    String ownerid;
    Boolean read;
    Boolean write;

    public String getBoardid() {
        return boardid;
    }

    public String getOwnerid() {
        return ownerid;
    }

    public Boolean getRead() {
        return read;
    }

    public Boolean getWrite() {
        return write;
    }
}
