package com.draghv.com;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.ControllableTask;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Payal on 17-Jan-17.
 */
public class Zboard {

    DatabaseReference firebase_board;
    DatabaseReference firebase_boarddata;
    DatabaseReference firebase_sharedboardData;
    DatabaseReference firebase_boardShort;

    Map<String, Object> child;

    private String title;
    private int msgcount;
    private String cardtitle;
    private String columnId;
    String cardid;

    private List<HashMap<String, Object>> colorLabel;
    private String userid;
    private String key;
    List<HashMap<String, Object>> collist;
    List<HashMap<String, Object>> cardlist;
    HashMap<String, Object> cardData;
    private int row;
    ArrayList<HashMap<String, Object>> totalCol;
    HashMap<String, Object> archivemap;
    ZAuth zAuth;


    public void setValue(String key, Object value) {
        child.put(key, value);
    }

    public Zboard() {
        firebase_board = FirebaseDatabase.getInstance().getReference().child("dashboard");
        firebase_boarddata = FirebaseDatabase.getInstance().getReference().child("boardcontent");
        firebase_sharedboardData=FirebaseDatabase.getInstance().getReference().child("b_shared_board");
        firebase_boardShort=FirebaseDatabase.getInstance().getReference().child("board_short");

        zAuth=new ZAuth();
        archivemap = new HashMap<>();
        child = new HashMap<>();
        colorLabel = new ArrayList<>();
        collist = new ArrayList<>();
        cardlist = new ArrayList<>();
        cardData = new HashMap<>();
//        userid = String.valueOf(R.string.userid);
        //userid = "1AsCJsa5MC";
        //userid = "e204681a69ff2d54";
        userid=zAuth.get() ;
    }

    public String save() {
        key = firebase_board.push().getKey();
        Log.i("TAG", "boardid:" + key);
        child.put("createdAt", ServerValue.TIMESTAMP);
        child.put("updatedAt", ServerValue.TIMESTAMP);
        child.put("objectId", key);
        return key;
    }
//    public String saveArchiveCard(){
//        String cardid=firebase_ArchiveData.push().getKey();
//        return cardid;
//    }

    //called when we adding column in this it will update totalcol key in firebase dashboard content
    public DatabaseReference.CompletionListener upadateCOL_Board(String user_id,String boardId, List<HashMap<String, Object>> newColData, DatabaseReference.CompletionListener completionListener) {
        child.put("totalcol", newColData);
        child.put("updatedAt", ServerValue.TIMESTAMP);
        firebase_board.child(user_id).child(boardId).updateChildren(child, completionListener);
        firebase_board.keepSynced(true);
        return completionListener;
    }
    //when we use list and in that we only stores user's id
//    public DatabaseReference.CompletionListener updateSharedWith(String boardId,List<String> userlist,DatabaseReference.CompletionListener listener){
//        child.put("sharedwith",userlist);
//        child.put("updatedAt", ServerValue.TIMESTAMP);
//        firebase_board.child(userid).child(boardId).updateChildren(child, listener);
//        return listener;
//    }

    /*this is new in this we will stores map and its key is userid */
    public DatabaseReference.CompletionListener addSharedWith(String userid,String boardId,HashMap<String,Object> userlist,DatabaseReference.CompletionListener listener){
        //child.put("sharedwith",userlist);
        child.put("updatedAt", ServerValue.TIMESTAMP);
        //firebase_board.child(userid).child(boardId).updateChildren(child, listener);
        firebase_board.child(userid).child(boardId).child("sharedwith").updateChildren(userlist, listener);
        return listener;
    }
    public DatabaseReference.CompletionListener removeSharedWith(String userid,String boardID,String sharedUserId,DatabaseReference.CompletionListener listener){
        firebase_board.child(userid).child(boardID).child("sharedwith").child(sharedUserId).removeValue();
        return listener;
    }

    /*public DatabaseReference.CompletionListener updateSharePermission(String boardId,String ownerid,String shareduserid,Boolean read,Boolean write,DatabaseReference.CompletionListener listener){
        HashMap<String,Object> map=new HashMap<>();
        map.put("updatedAt",ServerValue.TIMESTAMP);
        map.put("read",read);
        map.put("write",write);
        firebase_board.child(ownerid).child(boardId).child("sharedwith").child(shareduserid).updateChildren(map,listener);
        return listener;
    }*/
    public DatabaseReference.CompletionListener updateReadPermission(String boardId,String ownerid,String shareduserid,Boolean read,DatabaseReference.CompletionListener listener){
        HashMap<String,Object> map=new HashMap<>();
        map.put("updatedAt",ServerValue.TIMESTAMP);
        map.put("read",read);

        firebase_board.child(ownerid).child(boardId).child("sharedwith").child(shareduserid).updateChildren(map, listener);
        return listener;
    }
    public DatabaseReference.CompletionListener updateWritePermission(String boardId,String ownerid,String shareduserid,Boolean write,DatabaseReference.CompletionListener listener){
        HashMap<String,Object> map=new HashMap<>();
        map.put("updatedAt",ServerValue.TIMESTAMP);

        map.put("write",write);
        firebase_board.child(ownerid).child(boardId).child("sharedwith").child(shareduserid).updateChildren(map,listener);
        return listener;
    }


    //when we press create btn of create board prompt it will add dashboard content
    public DatabaseReference.CompletionListener saveboardWithListener(DatabaseReference.CompletionListener completionListener) {
        firebase_board.child(userid).child(key).setValue(child, completionListener);
        firebase_board.keepSynced(true);
        return completionListener;
    }
    public OnSuccessListener deleteboardWithListener(String boardId,OnSuccessListener successListener){
        getBOARD_DATA(boardId).removeValue().addOnSuccessListener(successListener);
        getBOARD_ROOT(boardId).removeValue().addOnSuccessListener(successListener);
        getShortRoot().child(userid).child(boardId).removeValue().addOnSuccessListener(successListener);
        return successListener;
    }

    //called when we add new card in board
    public DatabaseReference.CompletionListener addCardInBoard(String boardId, HashMap<String, Object> cardmap, DatabaseReference.CompletionListener completionListener) {
//        HashMap<String,Object> cardmeta=new HashMap<>();
//        cardmeta.put(cardid,cardData);
//        firebase_boarddata.child(key).setValue(cardmeta, completionListener);
        firebase_boarddata.child(boardId).child(cardid).setValue(cardmap, completionListener);
        Log.i("TAG", "short colid:" + cardmap.get("columnId"));
        Log.i("TAG", "short row:" + cardmap.get("row"));
        firebase_boardShort.child(MainActivity.ownerId).child(boardId).child(cardmap.get("columnId").toString()).child(cardid).setValue(getHashMap(cardid, cardmap.get("row").toString()));
        firebase_boarddata.keepSynced(true);
        firebase_boardShort.keepSynced(true);
        return completionListener;
    }
    private HashMap<String,Object> getHashMap(String objectId,String row){
        HashMap<String,Object> data=new HashMap<>();
        data.put("objectId", objectId);
        data.put("row", row);
        return data;
    }
    public DatabaseReference.CompletionListener deleteCardInBoard(String boardId, String cardId, DatabaseReference.CompletionListener completionListener) {
        firebase_boarddata.child(boardId).child(cardId).removeValue(completionListener);
        return completionListener;
    }

    //call when we drag end update all card data
    public DatabaseReference.CompletionListener updateAllCardData(String boardId, HashMap<String, Object> carddataMap, DatabaseReference.CompletionListener completionListener) {
        firebase_boarddata.child(boardId).updateChildren(carddataMap, completionListener);
        firebase_boarddata.keepSynced(true);
        return completionListener;
    }


    //call when we update any card data mean(when we go in detail screen of card then)
    public DatabaseReference.CompletionListener updateCard(String boardId, String cardId, HashMap<String, Object> cardData, DatabaseReference.CompletionListener completionListener) {
        firebase_boarddata.child(boardId).child(cardId).updateChildren(cardData, completionListener);

        firebase_boarddata.keepSynced(true);
        return completionListener;
    }

    //update shorttable
    public DatabaseReference.CompletionListener updateShort(String boardId,HashMap<String,Object> cardData,DatabaseReference.CompletionListener completionListener){
        firebase_boardShort.child(MainActivity.ownerId).child(boardId).child(cardData.get("columnId").toString()).child(cardData.get("objectId").toString()).updateChildren(getHashMap(cardData.get("objectId").toString(), cardData.get("row").toString()),completionListener);
        return completionListener;
    }


    public void setTitle(String title) {
        child.put(CONSTANTS.BOARD_TITLE, title);
        this.title = title;
    }

    public void Update(String boardId) {
        child.put(CONSTANTS.UPDATED_AT, ServerValue.TIMESTAMP);
        firebase_board.child(userid).child(boardId).updateChildren(child);
    }

    public void setUserid(String userid) {
        child.put(CONSTANTS.USER_ID, userid);
        this.userid = userid;
    }

    public void setCardtitle(String cardtitle) {
        cardData.put(CONSTANTS.CARD_TITLE, cardtitle);
        this.cardtitle = cardtitle;
    }

    public void setColumnId(String columnId) {
        cardData.put("columnId", columnId);
        this.columnId = columnId;
    }

    public void setCardid(String boardId) {
        Log.i("TAG", "boardid:" + boardId);
        this.cardid = firebase_boarddata.child(boardId).push().getKey();
        cardData.put(CONSTANTS.CARD_ID, cardid);
    }

    public void setcolorLabel(List<HashMap<String, Object>> colorLabel) {
        child.put(CONSTANTS.COLOR_LABELS, colorLabel);
        this.colorLabel = colorLabel;
    }

    public void setRow(int row) {
        cardData.put("row", row);
        this.row = row;
    }

    public void setTotalCol(ArrayList<HashMap<String, Object>> columnList) {
        child.put("totalcol", columnList);
        this.totalCol = columnList;
    }

    public DatabaseReference getROOT() {
        return firebase_board;
    }

    public DatabaseReference getBOARD_ROOT(String boardId) {
        return firebase_board.child(userid).child(boardId);
    }

    public DatabaseReference getBOARD_DATA_ROOT() {
        return firebase_boarddata;
    }

    public DatabaseReference getBOARD_DATA(String boardId) {
        return firebase_boarddata.child(boardId);
    }

    public DatabaseReference getShareBoardRoot(){
        return firebase_sharedboardData;
    }

    public DatabaseReference getUserSharedBoard(String userid){
        return firebase_sharedboardData.child(userid);
    }

    public DatabaseReference getShortRoot(){
        return firebase_boardShort;
    }
    public DatabaseReference getShortUserBoard(String userid,String boardid){
        return firebase_boardShort.child(userid).child(boardid);
    }
}
