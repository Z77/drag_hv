package com.draghv.com;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 23-Nov-16.
 */
public class CDetail_addItem extends AppCompatActivity {
    LinearLayout itemlist;
    String pos;
    ArrayList<HashMap<String,Object>> itemArrlist;
    ArrayList<String> pathlist;
    ArrayList<String> titlelist;
    ArrayList<String> businessIdList;
    ArrayList<String> categoryIdList;
    ArrayList<String> itemIdList;
    ArrayList<BusinessData>rowdata;
    List<HashMap<String,Object >> itemsmaplist=new ArrayList<>();
    List<HashMap<String,Object>> templist;
    Button btn_setitem;
    Button businessSpinner;
    String pre,current;
    HashMap<String,Object> datalist;
    ArrayList<HashMap<String,Object>> tempBdataList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_item_detail);
//        setSupportActionBar(toolbar);
//        toolbar.setTitle("Select Items");

        Bundle b=getIntent().getExtras();
        //pos=b.get("pos").toString();
//        if(b.get("iList")!=null){
//            Log.i("TAG","itemlist:"+b.get("iList"));
//        }

        templist=(List<HashMap<String, Object>>) b.get("iList");
        //itemlist=(LinearLayout)findViewById(R.id.ticket_parent);
        businessSpinner=(Button)findViewById(R.id.btn_business_spinner);
        btn_setitem =new Button(CDetail_addItem.this);
        btn_setitem.setText("Set Item");

        String lastBusiness;
        if(templist.size()>0) {
            lastBusiness =templist.get(0).get("businessId").toString();
            Log.i("TAG","last businessid"+lastBusiness);

        }else {
            lastBusiness = "1tT8NMdKBG";
        }

        addBusinee(lastBusiness);
        businessSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBusinessListPrompt();
            }
        });


        addItems(lastBusiness);//adding items of first business of user
        btn_setitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<CDetail_RVItemAdapter.ItemData> itemdatalist = new ArrayList<CDetail_RVItemAdapter.ItemData>();
                itemsmaplist.clear();
                for (int i = 0; i < itemlist.getChildCount() - 1; i++) {

                    View rowat = itemlist.getChildAt(i);
                    CheckBox chkat = (CheckBox) rowat.findViewById(R.id.chk_item);
                    Log.i("TAG", "item " + i + ":" + chkat.getText().toString() + "and is" + chkat.isChecked());
                    if (chkat.isChecked()) {

                        CDetail_RVItemAdapter.ItemData item_Data = new CDetail_RVItemAdapter.ItemData(pathlist.get(i), titlelist.get(i));
                        itemdatalist.add(item_Data);
                        HashMap<String, Object> tempItemList = new HashMap<>();
                        tempItemList.put("businessId", businessIdList.get(i));
                        tempItemList.put("categoryId", categoryIdList.get(i));
                        tempItemList.put("itemId", itemIdList.get(i));
                        itemsmaplist.add(tempItemList);
                        Log.i("TAG", "itemslist:" + itemdatalist.size());
                    }
                }
//                Intent i=new Intent();
//                i.putExtra("dlist",itemdatalist.toString());
//                //i.putExtra("pos",position);
//                setResult(Activity.RESULT_OK,i);
                CDetail_ItemPOJO TIP=new CDetail_ItemPOJO();
                TIP.setItemList((ArrayList<HashMap<String, Object>>) itemsmaplist);
                //TicketBuilder.refreshDataFromProperty(Integer.parseInt(pos), TIP);

                if (itemdatalist.size() != 0) {
                    Intent i = new Intent();
//                i.putExtra("dlist",itemdatalist.toString());
                    //i.putExtra("pos",position);
                    setResult(Activity.RESULT_OK, i);
                    CDetail_addItem.this.finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Please select any one item!!!", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(itemsmaplist.size()==0){
            Toast.makeText(getApplicationContext(), "Please select any one item!!! or\n press Set Item", Toast.LENGTH_LONG).show();
        }

    }
    public void showBusinessListPrompt(){
        final View popUp = LayoutInflater.from(CDetail_addItem.this).inflate(R.layout.business_list_promp, null);
        final PopupWindow PW = new PopupWindow(popUp,businessSpinner.getMeasuredWidth()+20, 550);
        PW.setBackgroundDrawable(new BitmapDrawable(getResources(),""));
        PW.setOutsideTouchable(true);

        ListView list=(ListView)popUp.findViewById(R.id.list_business);
        final BusinessSelectionAdapter adapter=new BusinessSelectionAdapter(CDetail_addItem.this,rowdata);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for(int i=0;i<rowdata.size();i++){
                    BusinessData data=rowdata.get(i);
                    if(data.getIsSelectd()){
                        rowdata.set(i,new BusinessData(tempBdataList.get(i).get("title").toString(),tempBdataList.get(i).get("picture").toString(),false));
                    }
                }
                rowdata.set(position, new BusinessData(tempBdataList.get(position).get("title").toString(), tempBdataList.get(position).get("picture").toString(), true));
                adapter.notifyDataSetChanged();
                addItems(tempBdataList.get(position).get("objectId").toString());

                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        PW.dismiss();
                    }
                },300);

            }
        });

        int[] originalPos = new int[2];
        businessSpinner.getLocationInWindow(originalPos);
//                Log.i("TAG","co-ordinate:"+originalPos[0]+" "+originalPos[1]);
        Log.i("TAG", "x:" + businessSpinner.getLeft() + " y:" + businessSpinner.getTop());
        PW.showAtLocation(popUp, Gravity.NO_GRAVITY, originalPos[0]-10, (originalPos[1] + 130));
//        PW.setFocusable(true);
//        PW.update();

    }
    public void addBusinee(final String lastBusiness){
        rowdata=new ArrayList<>();
        tempBdataList=new ArrayList<>();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("business").child("uRPz0t7TWk");//uRPz0t7TWk is demo static user id
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //ArrayList<String> businesslist=new ArrayList<String>();
                for(DataSnapshot data:dataSnapshot.getChildren()){
                    datalist= (HashMap<String, Object>) data.getValue();
                    tempBdataList.add(datalist);

                    Log.i("TAG","Title"+datalist.get("title").toString());
                    Log.i("TAG","Title"+datalist.get("picture").toString());
                    if(datalist.get("objectId").toString().equals(lastBusiness)) {
//                    if(datalist.get("objectId").toString().equals(pre)){
                        rowdata.add(new BusinessData(datalist.get("title").toString(), datalist.get("picture").toString(), true));
                    }else{
                        rowdata.add(new BusinessData(datalist.get("title").toString(), datalist.get("picture").toString(), false));
                    }
                    //rowdata.add(Bdata);
                    Log.i("TAG", "value is::" + data.getKey());
                   // businesslist.add(data.getKey());
                }
               // Log.i("TAG", "items detail:" + businesslist);
                //ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(TicketItemDetail.this, android.R.layout.simple_spinner_item, businesslist); //selected item will look like a spinner set from XML
                //spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                //businessSpinner.setAdapter(spinnerArrayAdapter);
                BusinessSelectionAdapter adapter=new BusinessSelectionAdapter(CDetail_addItem.this,rowdata);
//                adapter.setDropDownViewResource(R.layout.ticket_builder_items_select_row);
//                businessSpinner.setAdapter(adapter);
//                businessSpinner.setSelection(2);
//                pre=String.valueOf(businessSpinner.getSelectedItemPosition());
//                Log.i("TAG", "selected:" + pre + " pos=>" + businessSpinner.getSelectedItemPosition());
                //businessSpinner.setSelection(selectedItem);
 //               Log.i("TAG","items detail2:"+formdetail);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    public void addItems(final String businessId){
        itemlist.removeAllViews();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("b_items").child(businessId.trim());

        ImageLoaderConfiguration config=new ImageLoaderConfiguration.Builder(CDetail_addItem.this).build();
        final ImageLoader loader=ImageLoader.getInstance();
        loader.init(config);
        final DisplayImageOptions options=new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .build();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("TAG","item detail"+dataSnapshot);
                HashMap<String, Object> formdetail = (HashMap<String, Object>) dataSnapshot.getValue();
                itemArrlist=new ArrayList<>();
                pathlist=new ArrayList<String>();
                titlelist=new ArrayList<String>();
                businessIdList=new ArrayList<String>();
                categoryIdList=new ArrayList<String>();
                itemIdList=new ArrayList<String>();
                Log.i("TAG", "datasnap:" + formdetail);
                if(dataSnapshot.getValue()!=null) {
                    for (Object objlist : formdetail.values()) {
                        HashMap<String, Object> datamap = (HashMap<String, Object>) objlist;
                        itemArrlist.add(datamap);
                        Log.i("TAG", "datamap:" + datamap);
                        View row = LayoutInflater.from(CDetail_addItem.this).inflate(R.layout.ticket_builder_items_select_row, null, false);
                        final ImageView thumb = (ImageView) row.findViewById(R.id.iv_imgthumbnail);
                        final ProgressBar progress = (ProgressBar) row.findViewById(R.id.pbar);
                        TextView title = (TextView) row.findViewById(R.id.item_name);
                        CheckBox chk = (CheckBox) row.findViewById(R.id.chk_item);
                        final String img_url = datamap.get("image").toString();//.replace("image", "thumb");
                        pathlist.add(img_url);

                        title.setText(datamap.get("title").toString());
                        titlelist.add(datamap.get("title").toString());
                        businessIdList.add(datamap.get("businessId").toString());
                        if (datamap.containsKey("categoryId")) {
                            categoryIdList.add(datamap.get("categoryId").toString());
                        } else {
                            categoryIdList.add("");
                        }
                        itemIdList.add(datamap.get("objectId").toString());
                        try {
                            loader.displayImage(img_url, thumb, options, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    Log.i("TAG", "image loading started");
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    Log.i("TAG", "image loading failed");
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    progress.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                    Log.i("TAG", "image loading candelled");
                                }
                            });
                        } catch (Exception e) {
                            Log.i("TAG", "image loading catch");
                            Toast.makeText(getApplicationContext(), "TicketItemDetail image loading prob!", Toast.LENGTH_SHORT).show();
                        }
                        if (templist.size() > 0) {
                            for (int ii = 0; ii < templist.size(); ii++) {
                                if (templist.get(ii).get("itemId").toString().equals(datamap.get("objectId")))
                                    chk.setChecked(true);
                            }
                        }
                        itemlist.addView(row);

                    }
                    if (btn_setitem.getParent() == null)
                        itemlist.addView(btn_setitem);
                }else{
                 Toast.makeText(getApplicationContext(),"No items,Please select another business",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
