package com.draghv.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.draghv.com.Adapter.CDetail_RVItemAdapter;
import com.draghv.com.Adapter.MemberAdapter;
import com.draghv.com.Fragment.Frag_Description;
import com.draghv.com.Fragment.Frag_business_item;
import com.draghv.com.Fragment.Frag_checklist;
import com.draghv.com.Models.MemberPOJO;
import com.draghv.com.Models.MyItem;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.UploadTask;
import com.tangxiaolv.telegramgallery.GalleryActivity;
import com.tangxiaolv.telegramgallery.GalleryConfig;

import com.twitter.sdk.android.core.models.Card;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Payal on 16-Jan-17.
 */
public class CardDetailActivity extends AppCompatActivity implements View.OnClickListener {

    public static int pos;
    public static int row;
    public static MyItem itemdata;
    private static final int RC_GALLERY_PERM = 124;
    private static final int REQUEST_SELECT_PICTURE = 0x01;
    static final int FILEMANAGER_REQUEST = 101;
    public static String description = "";
    private int filesCnt = 0;

    List<CDetail_RVItemAdapter.ItemData> adapteritemslist;
    List<HashMap<String, Object>> businesslist = new ArrayList<>();
    List<HashMap<String, Object>> attach_itemdatalist = new ArrayList<>();
    List<HashMap<String, Object>> board_clrlabel = new ArrayList<>();
    List<String> photos, filesNameList, fileUrlList, board_memberlist;
    static List<String> destinationFileNamelist, imageUrlList, attachlist;

    FirebaseDatabase database;

    static Zimages zimages;
    static Zboard_history zboard_history;
    static Zboard_archive zboard_archive;
    ZAuth zAuth;

    static String USERID = "";
    Toolbar toolbar;
    FloatingActionButton addmore;

    static TextView toolbarTitle;
    static TextView tv_card_title;
    static TextView tv_card_duedate;
    static TextView tv_card_duetime;
    public static TextView tv_card_desc;

    CardView card_title;
    CardView card_desc;
    CardView card_assign;
    CardView card_lables;
    CardView card_duedate;
    CardView card_comment;
    static CardView card_attach;
    public CardView card_items;
    public static CardView card_chklist;

    LinearLayout attchFile, cardlist_parent, user_assign;
    RelativeLayout parent;
    BottomSheetLayout bottomSheetLayout;
    View attach_file_view;
    static private ProgressBar spin_kit;
    public RecyclerView items_rv;
    RecyclerView image_rv;
    static LinearLayout checklist_listview;
    public CDetail_RVItemAdapter items_rvAdapter;
    boolean disablecomment = true;
    RVAdapterForImageList imageRV_adapter;
    int i = 0;
    String duedatecolor = "";
    //SpringFloatingActionMenu Fabmenu;
    String[] MONTH = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    Boolean is_write;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
        setContentView(R.layout.activity_itemdetail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        bottomSheetLayout = (BottomSheetLayout) findViewById(R.id.bottomsheet);
        bottomSheetLayout.setPeekOnDismiss(true);

        database = FirebaseDatabase.getInstance();
        zimages = new Zimages();
        zAuth = new ZAuth();
        //USERID = getString(R.string.userid);
        USERID = zAuth.get();
        zboard_history = new Zboard_history();
        zboard_archive = new Zboard_archive();
        imageUrlList = new ArrayList<>();
        destinationFileNamelist = new ArrayList<>();
        filesNameList = new ArrayList<>();
        fileUrlList = new ArrayList<>();
        attachlist = new ArrayList<>();
        board_memberlist = new ArrayList<>();

        pos = Integer.parseInt(getIntent().getStringExtra("col"));
        row = Integer.parseInt(getIntent().getStringExtra("row"));
        Bundle b = getIntent().getExtras();
        if (b.get("clr") != null) {
            board_clrlabel = (List<HashMap<String, Object>>) b.get("clr");
            Log.i("TAG", "cle label" + board_clrlabel);
        } else {
            Toast.makeText(getApplicationContext(), "clr list null", Toast.LENGTH_SHORT).show();
        }
        if (b.get("iswrite") != null)
            is_write = b.getBoolean("iswrite");
        if (b.get("member") != null)
            board_memberlist = (List<String>) b.get("member");
        itemdata = MainActivity.clickeditem_data;
        toolbarTitle.setText(itemdata.getData());
        description = itemdata.getDesc();
        Log.i("TAG", "data:" + itemdata);

        card_title = (CardView) findViewById(R.id.card_title);
        card_desc = (CardView) findViewById(R.id.card_desc);
        card_assign = (CardView) findViewById(R.id.card_assign);
        card_items = (CardView) findViewById(R.id.card_item);
        card_attach = (CardView) findViewById(R.id.card_attach);
        card_lables = (CardView) findViewById(R.id.card_lable);
        card_duedate = (CardView) findViewById(R.id.card_dueDate);
        card_comment = (CardView) findViewById(R.id.card_comment);
        card_chklist = (CardView) findViewById(R.id.card_checklist);

        spin_kit = (ProgressBar) findViewById(R.id.spin_kit);
        tv_card_title = (TextView) findViewById(R.id.tv_card_title);
        tv_card_desc = (TextView) findViewById(R.id.tv_card_description);
        tv_card_duedate = (TextView) findViewById(R.id.duedate_tv);
        tv_card_duetime = (TextView) findViewById(R.id.duetime_tv);
        addmore = (FloatingActionButton) findViewById(R.id.fab_card_detail);
        parent = (RelativeLayout) findViewById(R.id.detail_parent);
        user_assign = (LinearLayout) findViewById(R.id.user_assign_parent);
        items_rv = (RecyclerView) findViewById(R.id.rv_items);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);
        items_rv.setLayoutManager(layoutManager);
        image_rv = (RecyclerView) findViewById(R.id.rv_item_image_list);
        RecyclerView.LayoutManager lm = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        image_rv.setLayoutManager(lm);
        attchFile = (LinearLayout) findViewById(R.id.attach_file_parent);
        cardlist_parent = (LinearLayout) findViewById(R.id.label_card_list_parent);
        checklist_listview = (LinearLayout) findViewById(R.id.chk_list);

        tv_card_title.setText(itemdata.getData());
        if (description != null)
            tv_card_desc.setText(description);
        if (itemdata.getItemAttach() != null) {
            adapteritemslist = new ArrayList<>();
            attach_itemdatalist = itemdata.getItemAttach();
            items_rvAdapter = new CDetail_RVItemAdapter(adapteritemslist, CardDetailActivity.this);
            items_rv.setAdapter(items_rvAdapter);
            card_items.setVisibility(View.VISIBLE);
            for (int x = 0; x < attach_itemdatalist.size(); x++) {
                String businessid = (String) attach_itemdatalist.get(x).get("businessId");
                String itemid = (String) attach_itemdatalist.get(x).get("itemId");
                DatabaseReference itemRef = database.getReference("b_items").child(businessid).child(itemid);
                itemRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        HashMap<String, Object> formdetail = (HashMap<String, Object>) dataSnapshot.getValue();
                        CDetail_RVItemAdapter.ItemData item_Data = new CDetail_RVItemAdapter.ItemData(formdetail.get("image").toString(), formdetail.get("title").toString());
                        items_rvAdapter.addItem(item_Data);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
        //attchment of card either it will be image or any file
        if (itemdata.getImageAttach() != null) {
            card_attach.setVisibility(View.VISIBLE);
//            imageUrlList.addAll(itemdata.getImageAttach());
            attachlist.addAll(itemdata.getImageAttach());
            imageRV_adapter = new RVAdapterForImageList(imageUrlList, getApplicationContext());
            image_rv.setAdapter(imageRV_adapter);
            for (int i = 0; i < attachlist.size(); i++) {

                String[] part1 = attachlist.get(i).split("\\?");
                String[] part2 = part1[0].split("%2F");
                if (part1[0].contains("jpg") | part1[0].contains("png") | part1[0].contains("jpeg")) {
                    imageUrlList.add(attachlist.get(i));
                    destinationFileNamelist.add(part2[part2.length - 1]);
                } else {
                    fileUrlList.add(attachlist.get(i));
                    String[] part3 = part2[part2.length - 1].split("_");
                    String filename = part3[part3.length - 1];
                    filename = filename.replace("+", " ");
                    filesNameList.add(part2[part2.length - 1]); //bcoz we set filename is userid+filename
                    attach_file_view = LayoutInflater.from(CardDetailActivity.this).inflate(R.layout.attach_file_row, null, false);
                    final TextView filenameView = (TextView) attach_file_view.findViewById(R.id.filename_preview);
                    final ImageView delete = (ImageView) attach_file_view.findViewById(R.id.file_preview_delete);
                    delete.setTag(filesCnt);
                    filesCnt++;
                    filenameView.setText(filename);

                    delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            spin_kit.setVisibility(View.VISIBLE);
                            String fileremoved = fileUrlList.get((Integer) v.getTag());
                            if (attachlist.contains(fileremoved))
                                attachlist.remove(fileremoved);
                            itemdata.setImageAttach(attachlist);
                            passDataToAdapterAndSet(itemdata);
                            HashMap<String, Object> data = zboard_history.getJSON_removeAttchment(fileremoved, USERID, itemdata.getObjectId(), itemdata.getData(), "file");
                            zboard_history.addHistory(MainActivity.BOARD_ID, data, null);
                            Toast.makeText(CardDetailActivity.this, "Title is " + filesNameList.get((Integer) v.getTag()), Toast.LENGTH_SHORT).show();
                            zimages.deleteFileFromBoard(filesNameList.get((Integer) v.getTag()).replace("+", " "), new OnSuccessListener() {
                                @Override
                                public void onSuccess(Object o) {
                                    spin_kit.setVisibility(View.GONE);
                                    attchFile.removeViewAt((Integer) v.getTag());
                                }
                            });
                        }
                    });
                    attchFile.addView(attach_file_view);
                }
                Log.i("TAG", "destination file name is:" + destinationFileNamelist);
            }
        }
        if (itemdata.getdueDate() != null) {
            card_duedate.setVisibility(View.VISIBLE);
            HashMap<String, Object> duedateMap = itemdata.getdueDate();
            if (duedateMap.containsKey("isOnlyDate")) {
                if ((Boolean) duedateMap.get("isOnlyDate")) {
                    String datetime = String.valueOf(getDateFromTimeStamp((Long) itemdata.getdueDate().get("duedate"), true));
                    tv_card_duedate.setText(datetime);
                    tv_card_duetime.setVisibility(View.GONE);
                } else {
                    String[] datetime = String.valueOf(getDateFromTimeStamp((Long) itemdata.getdueDate().get("duedate"), false)).split(" ");
                    tv_card_duedate.setText(datetime[0]);
                    tv_card_duetime.setText(datetime[1]);
                }
            }//else portion for temp becoz in some old data isOnlyDate is not contains in duedate map
            else {
                String[] datetime = String.valueOf(getDateFromTimeStamp((Long) itemdata.getdueDate().get("duedate"), false)).split(" ");
                tv_card_duedate.setText(datetime[0]);
                tv_card_duetime.setText(datetime[1]);
            }

        }
        Log.i("TAG", "item color is:" + itemdata.getColorcodes());
        if (itemdata.getColorcodes() != null) {
            for (int i = 0; i < itemdata.getColorcodes().size(); i++) {
                card_lables.setVisibility(View.VISIBLE);
                View view = new View(CardDetailActivity.this);
                LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(dpToPx(40), dpToPx(30));
                viewparams.setMargins(5, 0, 5, 0);
                view.setLayoutParams(viewparams);

                Drawable mDrawable = ContextCompat.getDrawable(CardDetailActivity.this, R.drawable.viewpink_clr);
                mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(itemdata.getColorcodes().get(i)), PorterDuff.Mode.SRC_IN));
                view.setBackground(mDrawable);
                cardlist_parent.addView(view);
            }
        }
        if (itemdata.getChecklist() != null) {
            setChecklist(itemdata.getChecklist());
        }
        if (itemdata.getComment() != null && itemdata.getComment()) {
            card_comment.setVisibility(View.VISIBLE);
            disablecomment = true;
            invalidateOptionsMenu();
        }
        if (itemdata.getAssign() != null) {
            Log.i("TAG", "assign" + itemdata.getAssign());
            if(itemdata.getAssign().size()>0)
                card_assign.setVisibility(View.VISIBLE);
            else
                card_assign.setVisibility(View.GONE);

            for (int i = 0; i < itemdata.getAssign().size(); i++) {
                final String id = itemdata.getAssign().get(i);
                database.getReference().child("_user").child(id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        HashMap<String, Object> map = (HashMap<String, Object>) dataSnapshot.getValue();
                        View row = LayoutInflater.from(CardDetailActivity.this).inflate(R.layout.frag_member_row, null, false);
                        TextView name_ = (TextView) row.findViewById(R.id.user_fullname);
                        ImageView profile = (ImageView) row.findViewById(R.id.user_avatar);
                        name_.setText((String) map.get("name"));
                        String image = map.get("image") != null ? (String) map.get("image") : "";
                        if (image.length() > 0)
                            com.draghv.com.ImageLoader.defaultLoad(profile, image, null);
                        user_assign.addView(row);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        }

        card_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_write) {
                    showTitleDialog(tv_card_title.getText().toString());
                }
            }
        });
        card_desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_write)
                    changeFragment(1);
            }
        });
        card_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_write)
                    changeFragment(2);
                //showItemDialog(lastBsinessId);
            }
        });
        card_attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_write)
                    showAttachDialog();
            }
        });
        card_duedate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_write) {
                    if (tv_card_duedate.getText().length() > 0) {
                        showDueDateDialog((String) itemdata.getdueDate().get("color"), true);
                    } else {
                        showDueDateDialog((String) itemdata.getdueDate().get("color"), false);
                    }
                }
            }
        });
        card_lables.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_write) {
                    if (board_clrlabel.size() > 0)
                        showLabelDialog(itemdata.getColorcodes());
                    else
                        Toast.makeText(getApplicationContext(), "No color lable set in setting", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "Label", Toast.LENGTH_SHORT).show();
                }
            }
        });
        card_chklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_write)
                    changeFragment(3);
            }
        });
        card_assign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_write)
                    showAssignDialog(itemdata.getAssign(), board_memberlist);
            }
        });
        card_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ActivityComments.class);
                i.putExtra("cid", itemdata.getObjectId());
                i.putExtra("iswrite", is_write);
                startActivity(i);
            }
        });
        //get all businesses data of user
        setUpBusinessData();
        Log.i("TAG", "activity oncreate");

//        final com.melnykov.fab.FloatingActionButton fab = new com.melnykov.fab.FloatingActionButton(this);
//        fab.setType(com.melnykov.fab.FloatingActionButton.TYPE_NORMAL);
//        fab.setImageResource(R.drawable.add);
//        fab.setColorPressedResId(R.color.colorPrimaryDark);
//        fab.setColorNormalResId(R.color.colorPrimary);
//        fab.setColorRippleResId(R.color.black_textColor);
//        fab.setShadow(true);
//        //fab.setImageResource(R.drawable.add);
//        Fabmenu=new SpringFloatingActionMenu.Builder(this)
//                .fab(fab)
//                .addMenuItem(R.color.red_500, R.drawable.add, "Test1", R.color.black_textColor, this)
//                .addMenuItem(R.color.green_500,R.drawable.add,"Test2",R.color.black_textColor,this)
//                .addMenuItem(R.color.amber_500, R.drawable.add, "Test3", R.color.black_textColor, this)
//                .addMenuItem(R.color.blue_500, R.drawable.add, "Test4", R.color.black_textColor, this)
//                .addMenuItem(R.color.light_blue_500,R.drawable.add,"Test5",R.color.black_textColor,this)
//                .addMenuItem(R.color.pink_500,R.drawable.add,"Test6",R.color.black_textColor,this)
//                .addMenuItem(R.color.purple_500, R.drawable.add, "Test7", R.color.black_textColor, this)
//                .addMenuItem(R.color.deep_purple_500,R.drawable.add,"Test8",R.color.black_textColor,this)
//                .addMenuItem(R.color.yellow_500,R.drawable.add,"Test9",R.color.black_textColor,this)
//                .animationType(SpringFloatingActionMenu.ANIMATION_TYPE_BLOOM)
//                .revealColor(R.color.transparant)
//                .enableFollowAnimation(true)
//                //.gravity(Gravity.RIGHT | Gravity.BOTTOM)
//                .onMenuActionListner(new OnMenuActionListener() {
//                    @Override
//                    public void onMenuOpen() {
//                        fab.setImageResource(R.drawable.cancel);
//                    }
//
//                    @Override
//                    public void onMenuClose() {
//                        fab.setImageResource(R.drawable.add);
//                    }
//                })
//                .build();

    }

    public void hidekeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.RESULT_UNCHANGED_HIDDEN, 0);
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        //    hidekeyboard();
        if (count > 0) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            toolbarTitle.setText(itemdata.getData());
            parent.setVisibility(View.VISIBLE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } else {
//            if (Fabmenu.isMenuOpen()) {
//                Fabmenu.hideMenu();
//            }else {
            super.onBackPressed();
//            }

            MainActivity.clickeditem_data = null;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//
//        if (!disablecomment) {
//            menu.findItem(R.id.action_removecomment).setVisible(true);
//            menu.findItem(R.id.action_addcomment).setVisible(false);
//        } else {
//            menu.findItem(R.id.action_removecomment).setVisible(false);
//            menu.findItem(R.id.action_addcomment).setVisible(true);
//        }
//        if (!is_write)
//            menu.setGroupVisible(R.id.detail_menu_group, false);
//        Log.i("TAG", "activity onPrepateOptionMenu");
//        return super.onPrepareOptionsMenu(menu);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_card_detail, menu);
        if (!is_write) {
            menu.setGroupVisible(R.id.detail_menu_group, false);
        }
        Log.i("TAG", "activity onCreateOptionMenu");
        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAG", "activity onResume");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            //    hidekeyboard();
            if (count > 0) {
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                toolbarTitle.setText(itemdata.getData());
                parent.setVisibility(View.VISIBLE);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            } else {
                Log.i("TAG", "pos col:" + pos);
                Log.i("TAG", "pos row:" + row);
                MainActivity.clickeditem_data = null;
                this.finish();
            }
        }
        if (id == R.id.menu_icon) {
            showDetailSheetMenu();
            return true;
        }
        //if (id == R.id.action_done_detail) {
//            passDataToAdapterAndSet(itemdata);
//            if(itemdata.getAssign()!=null) {
//                HashMap<String, Object> assign = zboard_history.getJSON_AssignCardInHistory(itemdata, zAuth.get());
//                zboard_history.addHistory(MainActivity.BOARD_ID, assign, null);
//            }
//            if(itemdata.getdueDate()!=null){
//                HashMap<String,Object> duedate=zboard_history.getJSON_DuedateInHistory(itemdata,zAuth.get());
//                zboard_history.addHistory(MainActivity.BOARD_ID,duedate,null);
//            }
//            MainActivity.clickeditem_data = null;
//            this.finish();
//            Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_LONG).show();
        //}
//        if (id == R.id.action_addAssign) {
//            Log.i("TAG", "item assign:" + itemdata.getAssign());
//            if (is_write) {
//                if (board_memberlist != null && board_memberlist.size() > 0)
//                    showAssignDialog(itemdata.getAssign(), board_memberlist);
//                else
//                    Toast.makeText(getApplicationContext(), "no members added in board", Toast.LENGTH_SHORT).show();
//            }
//        }
//        if (id == R.id.action_addItems) {
//            if (is_write)
//                changeFragment(2);
//            //showItemDialog(lastBsinessId);
//            Toast.makeText(getApplicationContext(), "Add Items", Toast.LENGTH_SHORT).show();
//        }
//        if (id == R.id.action_addAttach) {
//            if (is_write) {
//                showAttachDialog();
//                Toast.makeText(getApplicationContext(), "Add Attachments", Toast.LENGTH_SHORT).show();
//            }
//        }
//        if (id == R.id.action_addLables) {
//            if (is_write) {
//                if (board_clrlabel.size() > 0)
//                    showLabelDialog(itemdata.getColorcodes());
//                else
//                    Toast.makeText(getApplicationContext(), "No color lable set in setting", Toast.LENGTH_SHORT).show();
//            }
////            Toast.makeText(getApplicationContext(), "Add Lables", Toast.LENGTH_SHORT).show();
//        }
//        if (id == R.id.action_addChecklist) {
//            if (is_write)
//                changeFragment(3);
//            //Toast.makeText(getApplicationContext(), "Add Checklist", Toast.LENGTH_SHORT).show();
//        }
//        if (id == R.id.action_addDuedate) {
//            if (is_write)
//                showDueDateDialog("", false);
//            //Toast.makeText(getApplicationContext(), "Add Duedate", Toast.LENGTH_SHORT).show();
//        }
//        if (id == R.id.action_addcomment) {
//            if (is_write) {
//                card_comment.setVisibility(View.VISIBLE);
//                disablecomment = false;
//                invalidateOptionsMenu();
//                itemdata.setComment(true);
//                passDataToAdapterAndSet(itemdata);
//                HashMap<String, Object> mapdata = zboard_history.getJSON_comment(true, itemdata.getObjectId(), itemdata.getData(), zAuth.get());
//                zboard_history.addHistory(MainActivity.BOARD_ID, mapdata, null);
//                Toast.makeText(getApplicationContext(), "Enable comments", Toast.LENGTH_SHORT).show();
//            }
//        }
//        if (id == R.id.action_removecomment) {
//            if (is_write) {
//                card_comment.setVisibility(View.GONE);
//                disablecomment = false;
//                invalidateOptionsMenu();
//                itemdata.setComment(false);
//                passDataToAdapterAndSet(itemdata);
//                HashMap<String, Object> mapdata = zboard_history.getJSON_comment(false, itemdata.getObjectId(), itemdata.getData(), zAuth.get());
//                zboard_history.addHistory(MainActivity.BOARD_ID, mapdata, null);
//                Toast.makeText(getApplicationContext(), "disable comments", Toast.LENGTH_SHORT).show();
//            }
//        }
//        if (id == R.id.action_archive_detailcard) {
//            if (is_write) {
//                String colid = MainActivity.columnsdata.get(pos).get("id").toString();
//                String colname = MainActivity.columnsdata.get(pos).get("title").toString();
//                zboard_archive.addCardInArchive(MainActivity.BOARD_ID, itemdata.getObjectId(), itemdata.getCardJSON(colid, row), new DatabaseReference.CompletionListener() {
//                    @Override
//                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                        Toast.makeText(getApplicationContext(), "Card archived", Toast.LENGTH_SHORT).show();
//                        MainActivity.BOARD_.deleteCardInBoard(MainActivity.BOARD_ID, itemdata.getObjectId(), null);
//                        MainActivity.clickeditem_data = null;
//                        CardDetailActivity.this.finish();
//                    }
//                });
//                HashMap<String, Object> mapdata = zboard_history.getJSON_ArchiveCard(1, colid, colname, MainActivity.BOARD_ID, MainActivity.type, USERID);
//                zboard_history.addHistory(MainActivity.BOARD_ID, mapdata, null);
////                @Override
////                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
////                    Log.i("TAG","after archiving the card:"+databaseError+" refer:"+databaseReference);
////                    MainActivity.BOARD_.deleteCardInBoard(MainActivity.BOARD_ID,itemdata.getObjectId(),null);
////                    MainActivity.clickeditem_data = null;
////                    CardDetailActivity.this.finish();
////
////                }
////            });
//            }
//
//        }

//        if (id == R.id.acion_deletecard) {
//            if (is_write) {
//                MaterialDialog d = new MaterialDialog.Builder(CardDetailActivity.this)
//                        .title("Delete card")
//                        .content("Are you sure you want to delele this card.There is no undo for this action.You can archive this card too instead of delete.")
//                        .positiveText("Delete")
//                        .negativeText("Cancel")
//                        .onNegative(new MaterialDialog.SingleButtonCallback() {
//                            @Override
//                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                dialog.dismiss();
//                            }
//                        })
//                        .onPositive(new MaterialDialog.SingleButtonCallback() {
//                            @Override
//                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                MainActivity.BOARD_.deleteCardInBoard(MainActivity.BOARD_ID, itemdata.getObjectId(), new DatabaseReference.CompletionListener() {
//                                    @Override
//                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                                        Toast.makeText(getApplicationContext(), "Card deleted", Toast.LENGTH_SHORT).show();
//                                        MainActivity.clickeditem_data = null;
//                                        CardDetailActivity.this.finish();
//                                    }
//                                });
//                                dialog.dismiss();
//                            }
//                        })
//                        .show();
//            }
//        }
        return super.onOptionsItemSelected(item);
    }

    private void showDetailSheetMenu() {
        MenuSheetView menuSheetView = new MenuSheetView(CardDetailActivity.this, MenuSheetView.MenuType.GRID, "", new MenuSheetView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (bottomSheetLayout.isSheetShowing()) {
                    bottomSheetLayout.dismissSheet();
                }
                if (item.getItemId() == R.id.action_addAssign) {
                    Log.i("TAG", "item assign:" + itemdata.getAssign());
                    if (is_write) {
                        if (board_memberlist != null && board_memberlist.size() > 0)
                            showAssignDialog(itemdata.getAssign(), board_memberlist);
                        else
                            Toast.makeText(getApplicationContext(), "no members added in board", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                } else if (item.getItemId() == R.id.action_addItems) {
                    if (is_write)
                        changeFragment(2);
                    //showItemDialog(lastBsinessId);
                    Toast.makeText(getApplicationContext(), "Add Items", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (item.getItemId() == R.id.action_addAttach) {
                    if (is_write) {
                        showAttachDialog();
                        Toast.makeText(getApplicationContext(), "Add Attachments", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                } else if (item.getItemId() == R.id.action_addLables) {
                    if (is_write) {
                        if (board_clrlabel.size() > 0)
                            showLabelDialog(itemdata.getColorcodes());
                        else
                            Toast.makeText(getApplicationContext(), "No color lable set in setting", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                } else if (item.getItemId() == R.id.action_addChecklist) {
                    if (is_write)
                        changeFragment(3);
                    return true;
                } else if (item.getItemId() == R.id.action_addDuedate) {
                    if (is_write)
                        showDueDateDialog("", false);
                    return true;
                } else if (item.getItemId() == R.id.action_addcomment) {
                    if (is_write) {
                        card_comment.setVisibility(View.VISIBLE);
                        disablecomment = true;
                        //invalidateOptionsMenu();
                        itemdata.setComment(true);
                        passDataToAdapterAndSet(itemdata);
                        HashMap<String, Object> mapdata = zboard_history.getJSON_comment(true, itemdata.getObjectId(), itemdata.getData(), zAuth.get());
                        zboard_history.addHistory(MainActivity.BOARD_ID, mapdata, null);
                        Toast.makeText(getApplicationContext(), "Enable comments", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                } else if (item.getItemId() == R.id.action_removecomment) {
                    if (is_write) {
                        card_comment.setVisibility(View.GONE);
                        disablecomment = false;
                        //invalidateOptionsMenu();
                        itemdata.setComment(false);
                        passDataToAdapterAndSet(itemdata);
                        HashMap<String, Object> mapdata = zboard_history.getJSON_comment(false, itemdata.getObjectId(), itemdata.getData(), zAuth.get());
                        zboard_history.addHistory(MainActivity.BOARD_ID, mapdata, null);
                        Toast.makeText(getApplicationContext(), "disable comments", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                } else if (item.getItemId() == R.id.action_archive_detailcard) {
                    if (is_write) {
                        MaterialDialog archive = new MaterialDialog.Builder(CardDetailActivity.this)
                                .title("Archive Card").titleColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary))
                                .content("Are you sure you want to archive this card?")
                                .icon(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_archive_cards))
                                .autoDismiss(false)
                                .positiveText("Archive")
                                .negativeText("Cancel")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        final ProgressDialog progressDialog = new ProgressDialog(CardDetailActivity.this);
                                        progressDialog.setMessage("Please wait");
                                        progressDialog.show();
                                        String colid = MainActivity.columnsdata.get(pos).get("id").toString();
                                        String colname = MainActivity.columnsdata.get(pos).get("title").toString();
                                        zboard_archive.addCardInArchive(MainActivity.BOARD_ID, itemdata.getObjectId(), itemdata.getCardJSON(colid, row), new DatabaseReference.CompletionListener() {
                                            @Override
                                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                                progressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Card archived", Toast.LENGTH_SHORT).show();
                                                MainActivity.BOARD_.deleteCardInBoard(MainActivity.BOARD_ID, itemdata.getObjectId(), null);
                                                MainActivity.clickeditem_data = null;
                                                CardDetailActivity.this.finish();
                                            }
                                        });
                                        HashMap<String, Object> mapdata = zboard_history.getJSON_ArchiveCard(1, colid, colname, MainActivity.BOARD_ID, MainActivity.type, USERID);
                                        zboard_history.addHistory(MainActivity.BOARD_ID, mapdata, null);
                                        dialog.dismiss();
                                    }
                                })
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();

                    }
                    return true;
                } else if (item.getItemId() == R.id.acion_deletecard) {
                    if (is_write) {
                        MaterialDialog d = new MaterialDialog.Builder(CardDetailActivity.this)
                                .title("Delete card").titleColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary))
                                .icon(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_deletecard))
                                .content("Are you sure you want to delele this card.There is no undo for this action.You can archive this card too instead of delete.")
                                .positiveText("Delete")
                                .negativeText("Cancel")
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.dismiss();
                                    }
                                })
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        MainActivity.BOARD_.deleteCardInBoard(MainActivity.BOARD_ID, itemdata.getObjectId(), new DatabaseReference.CompletionListener() {
                                            @Override
                                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                                Toast.makeText(getApplicationContext(), "Card deleted", Toast.LENGTH_SHORT).show();
                                                MainActivity mainActivity=new MainActivity();
                                                mainActivity.new updateAllCardData(CardDetailActivity.this).execute();//29March becoz it will change remaing card index if we not calling it will not update firebase new index data
                                                MainActivity.clickeditem_data = null;
                                                CardDetailActivity.this.finish();
                                            }
                                        });
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    }
                    return true;
                }
                return false;
            }
        });
        menuSheetView.inflateMenu(R.menu.menu_carddetail_sheet);
        menuSheetView.setGridItemLayoutRes(R.layout.bottomsheetmenu_row);
        bottomSheetLayout.showWithSheetView(menuSheetView);
        if (disablecomment) {
            menuSheetView.getMenu().findItem(R.id.action_addcomment).setVisible(false);
            menuSheetView.getMenu().findItem(R.id.action_removecomment).setVisible(true);
            menuSheetView.updateMenu();
        } else {
            menuSheetView.getMenu().findItem(R.id.action_addcomment).setVisible(true);
            menuSheetView.getMenu().findItem(R.id.action_removecomment).setVisible(false);
            menuSheetView.updateMenu();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        spin_kit.setVisibility(View.VISIBLE);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_PICTURE) {
                i = 0;
                photos = (List<String>) data.getSerializableExtra(GalleryActivity.PHOTOS);
                final Uri selectedUri = Uri.fromFile(new File(photos.get(0)));
                if (selectedUri != null) {
                    startCropActivity(selectedUri);
                } else {
                    Toast.makeText(CardDetailActivity.this, "Cannot retrieve selected image", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {

                if (photos != null) {
                    if (i == photos.size()) {
                        handleCropResult(data, true);
                        System.out.println("ALL DONE");
                        photos = null;
                    } else {
                        handleCropResult(data, false);
                        System.out.println("i= : " + i);
                        final Uri selectedUri = Uri.fromFile(new File(photos.get((i))));
                        startCropActivity(selectedUri);
                    }
                } else
                    handleCropResult(data, true);

            }
            if (requestCode == FILEMANAGER_REQUEST) {
                Uri fileuri = data.getData();
                final String FILENAME;
                final Long FILESIZE;
                long sizeInMb = 0;
                if (fileuri.toString().contains("%20")) {
                    String uri = fileuri.toString().replace("%20", " ");
                    fileuri = Uri.parse(uri);
                    Log.i("TAG", "file name" + fileuri);
                }
                Log.i("TAG", "file path:" + fileuri + "last path:" + fileuri.getLastPathSegment());
                final Cursor returnCursor = getContentResolver().query(fileuri, null, null, null, null);
                if (returnCursor != null) {
                    final int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                    returnCursor.moveToFirst();
                    FILENAME = returnCursor.getString(nameIndex);
                    FILESIZE = returnCursor.getLong(sizeIndex);
                    Log.i("TAG", "file name is:" + returnCursor.getString(nameIndex));
                    sizeInMb = FILESIZE / (1024 * 1024);
                    Log.i("TAG", "file size in MB:" + sizeInMb);
                    returnCursor.close();
                } else {
                    File file_check = new File(fileuri.getPath());
                    FILENAME = fileuri.getLastPathSegment();
                    FILESIZE = file_check.length();
                    sizeInMb = FILESIZE / (1024 * 1024);
                    Log.i("TAG", "file size in MB else:" + sizeInMb);
                }

                if (sizeInMb < 10) {
                    if (FILENAME.contains("jpg") | FILENAME.contains("png") | FILENAME.contains("jpeg")) {
                        String name = System.currentTimeMillis() / 1000 + "_photo.jpg";
                        destinationFileNamelist.add(name);
                        addImage(fileuri, name);//from file manager if image
                    } else {
                        card_attach.setVisibility(View.VISIBLE);
                        attach_file_view = LayoutInflater.from(CardDetailActivity.this).inflate(R.layout.attach_file_row, null, false);
                        final TextView filenameView = (TextView) attach_file_view.findViewById(R.id.filename_preview);
                        final ImageView delete = (ImageView) attach_file_view.findViewById(R.id.file_preview_delete);
                        delete.setTag(filesCnt);
                        filesCnt++;
                        final String fileName = zAuth.get() + "_" + FILENAME;
                        Toast.makeText(getApplicationContext(), "file is:" + fileuri.getLastPathSegment(), Toast.LENGTH_SHORT).show();
                        zimages.uploadFileBoard(fileuri, fileName).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                spin_kit.setVisibility(View.GONE);
                                filenameView.setText(FILENAME);
                                attchFile.addView(attach_file_view);
                                Log.i("TAG", "file url is:" + taskSnapshot.getDownloadUrl().toString());
                                attachlist.add(taskSnapshot.getDownloadUrl().toString());
                                fileUrlList.add(taskSnapshot.getDownloadUrl().toString());
                                itemdata.setImageAttach(attachlist);
                                passDataToAdapterAndSet(itemdata);
                                HashMap<String, Object> data = zboard_history.getJSON_addAttchment(FILENAME, USERID, itemdata.getObjectId(), itemdata.getData(), "file");
                                zboard_history.addHistory(MainActivity.BOARD_ID, data, null);
                                filesNameList.add(fileName);

                            }
                        });
                        delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(final View v) {
                                spin_kit.setVisibility(View.VISIBLE);
                                String fileremoved = fileUrlList.get((Integer) v.getTag());
                                if (attachlist.contains(fileremoved))
                                    attachlist.remove(fileremoved);
                                itemdata.setImageAttach(attachlist);
                                passDataToAdapterAndSet(itemdata);
                                HashMap<String, Object> data = zboard_history.getJSON_removeAttchment(FILENAME, zAuth.get(), itemdata.getObjectId(), itemdata.getData(), "file");
                                zboard_history.addHistory(MainActivity.BOARD_ID, data, null);
                                Toast.makeText(CardDetailActivity.this, "Title is " + filesNameList.get((Integer) v.getTag()), Toast.LENGTH_SHORT).show();
                                zimages.deleteFileFromBoard(filesNameList.get((Integer) v.getTag()), new OnSuccessListener() {
                                    @Override
                                    public void onSuccess(Object o) {
                                        spin_kit.setVisibility(View.GONE);
                                        attchFile.removeViewAt((Integer) v.getTag());
                                    }
                                });
                            }
                        });
                    }
                } else {
                    Snackbar.make(parent, "File size must be less then 10MB!!!", Snackbar.LENGTH_LONG).show();
                }
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }

        if (resultCode == 0) {
            photos = null;
            spin_kit.setVisibility(View.GONE);
        }


        if (resultCode == RESULT_CANCELED) {
            //photos = null;
            spin_kit.setVisibility(View.GONE);
        }
    }

    private void setUpBusinessData() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        //DatabaseReference myRef = database.getReference("business").child("uRPz0t7TWk");//uRPz0t7TWk is demo static user id
        DatabaseReference myRef = database.getReference("business").child(zAuth.get());
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("TAG", "business data is:" + dataSnapshot);
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    HashMap<String, Object> datalist = (HashMap<String, Object>) data.getValue();
                    businesslist.add(datalist);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void passDataToAdapterAndSet(MyItem item) {
        tv_card_desc.setText(item.getDesc());
        MainActivity.adapterList.get(pos).changeData(itemdata, row);
        MainActivity.adapterList.get(pos).notifyDataSetChanged();
        MainActivity.updateDataInFirebase(itemdata, pos, row);
    }

    private void changeFragment(int pos) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        parent.setVisibility(View.GONE);
        switch (pos) {
            case 1:
                transaction.replace(R.id.fragment_detail, Frag_Description.instantiate(description), "Frag_Description").addToBackStack("Frag_Description").commit();
                toolbarTitle.setText("Description");
                break;
            case 2:
                transaction.replace(R.id.fragment_detail, Frag_business_item.instantiate(businesslist, attach_itemdatalist), "Frag_business_item").addToBackStack("Frag_business_item").commit();
                toolbarTitle.setText("Add Items");
                break;
            case 3:
                transaction.replace(R.id.fragment_detail, Frag_checklist.instantiate(itemdata.getChecklist()), "Frag_checklist").addToBackStack("Frag_checklist").commit();
                toolbarTitle.setText("Add Checklist");
                break;
            default:
                break;
        }

    }

    private void showTitleDialog(final String oldtitle) {
        MaterialDialog title = new MaterialDialog.Builder(CardDetailActivity.this)
                .title("Card title")
                .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                .titleColorRes(R.color.colorPrimary)
                .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                .customView(R.layout.prompt_rename_header, true)
                .positiveText("Ok").positiveColor(ContextCompat.getColor(CardDetailActivity.this, R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(CardDetailActivity.this, R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        final EditText tv_title = (EditText) dialog.findViewById(R.id.tv_list_title);
                        if (tv_title.getText().toString().length() > 0) {
                            itemdata.setData(tv_title.getText().toString().trim());
                            tv_card_title.setText(tv_title.getText().toString().trim());
                            passDataToAdapterAndSet(itemdata);
                        }
                        HashMap<String, Object> titledata = zboard_history.getJSON_ChangeTitle(itemdata, oldtitle, USERID);
                        zboard_history.addHistory(MainActivity.BOARD_ID, titledata, null);
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void showAttachDialog() {
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Select Picture From Following");
        imageRV_adapter = new RVAdapterForImageList(imageUrlList, getApplicationContext());
        image_rv.setAdapter(imageRV_adapter);
        alertDialogBuilder.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Toast.makeText(getApplicationContext(), "You clicked yes button", Toast.LENGTH_LONG).show();
                galleryTask();
            }
        });
        alertDialogBuilder.setNegativeButton("File Manager", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showFileManager();
            }
        });

        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void galleryTask() {
        if (EasyPermissions.hasPermissions(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            // Have permission, do the thing!
            showGallery();
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_galley), RC_GALLERY_PERM, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    public void showGallery() {
        GalleryConfig config = new GalleryConfig.Build()
                .limitPickPhoto(10)
                .singlePhoto(false)
                .hintOfPick("Max 10 allowed")
                .filterMimeTypes(new String[]{})
                .build();
        GalleryActivity.openActivity(CardDetailActivity.this, REQUEST_SELECT_PICTURE, config);
    }

    public void showFileManager() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILEMANAGER_REQUEST);

        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(getApplicationContext(), "Please install a File Manager.", Toast.LENGTH_SHORT).show();
        }
    }

    private void startCropActivity(@NonNull Uri uri) {
        i++;
        String destinationFileName;
        destinationFileName = System.currentTimeMillis() / 1000 + "_photo";
        destinationFileName += ".jpg";
        destinationFileNamelist.add(destinationFileName);
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop.start(this);
    }

    private void handleCropResult(@NonNull final Intent result, final boolean last) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            card_attach.setVisibility(View.VISIBLE);
            addImage(resultUri, null);
            if (last)
                spin_kit.setVisibility(View.GONE);
        } else {
            Toast.makeText(CardDetailActivity.this, "Error occurred, Try again", Toast.LENGTH_SHORT).show();
        }

    }

    //save in firebase
    private void addImage(Uri fileuri, String filename) {
        card_attach.setVisibility(View.VISIBLE);
        if (filename != null) {
            //from filemanager image set name
            zimages.uploadImageBoardWithName(fileuri, filename).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    imageUrlList.add(taskSnapshot.getDownloadUrl().toString());
                    attachlist.add(taskSnapshot.getDownloadUrl().toString());
                    itemdata.setImageAttach(attachlist);
                    passDataToAdapterAndSet(itemdata);
                    HashMap<String, Object> data = zboard_history.getJSON_addAttchment(taskSnapshot.getDownloadUrl().toString(), zAuth.get(), itemdata.getObjectId(), itemdata.getData(), "image");
                    zboard_history.addHistory(MainActivity.BOARD_ID, data, null);
                    Toast.makeText(getApplicationContext(), "Image saved", Toast.LENGTH_SHORT).show();
                    imageRV_adapter.notifyDataSetChanged();
                    spin_kit.setVisibility(View.GONE);
                }
            });
        } else {
            zimages.uploadImageBoard(fileuri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    imageUrlList.add(taskSnapshot.getDownloadUrl().toString());
                    attachlist.add(taskSnapshot.getDownloadUrl().toString());
                    itemdata.setImageAttach(attachlist);
                    passDataToAdapterAndSet(itemdata);
                    HashMap<String, Object> data = zboard_history.getJSON_addAttchment(taskSnapshot.getDownloadUrl().toString(), USERID, itemdata.getObjectId(), itemdata.getData(), "image");
                    zboard_history.addHistory(MainActivity.BOARD_ID, data, null);
                    Toast.makeText(getApplicationContext(), "Image saved", Toast.LENGTH_SHORT).show();
                    imageRV_adapter.notifyDataSetChanged();
                }
            });
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
//            Log.e(TAG, "handleCropError: ", cropError);
            Toast.makeText(CardDetailActivity.this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(CardDetailActivity.this, "Error occurred, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    public static void removeImageFromList(int pos) {
        card_attach.setVisibility(View.GONE);
    }

    public static void deleteImage(final int pos, String str_url) {
        spin_kit.setVisibility(View.VISIBLE);
        String filename = destinationFileNamelist.get(pos);
        Log.i("TAG", "files array is:" + destinationFileNamelist.size());
        Log.i("TAG", "file is:" + filename);
        Log.i("TAG", "Position is:" + pos);

        if (attachlist.contains(str_url))
            attachlist.remove(str_url);
        itemdata.setImageAttach(attachlist);
        passDataToAdapterAndSet(itemdata);
        HashMap<String, Object> data = zboard_history.getJSON_removeAttchment(filename, USERID, itemdata.getObjectId(), itemdata.getData(), "image");
        zboard_history.addHistory(MainActivity.BOARD_ID, data, null);

        zimages.deleteImageFromBoard(filename, new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
                destinationFileNamelist.remove(pos);
                //Log.i("TAG", "Delete pass data to adapter set imageUrlList:" + imageUrlList);
                spin_kit.setVisibility(View.GONE);
            }
        });
    }

    public void showDueDateDialog(String color, final Boolean isChangedate) {
        //Boolean isChangedate = false;

        MaterialDialog promptsView = new MaterialDialog.Builder(CardDetailActivity.this)
                .title("Due Date & Time").titleColor(ContextCompat.getColor(CardDetailActivity.this, R.color.colorPrimary))
                .customView(R.layout.prompt_duedate, true)
                .positiveText("Ok")
                .negativeText("Cancel")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        final Boolean finalIsChangedate = isChangedate;
                        Boolean isOnlyDate = false;
                        TextView tv_date = (TextView) dialog.findViewById(R.id.date_view);
                        TextView tv_time = (TextView) dialog.findViewById(R.id.time_view);
                        if (tv_date.getText().toString().length() > 0) {
                            Long timestamp;
                            if (tv_time.getText().toString().length() > 0) {
                                timestamp = getDateTimeStamp(tv_date.getText().toString().trim(), tv_time.getText().toString().trim());
                            } else {
                                isOnlyDate = true;
                                timestamp = getDateTimeStamp(tv_date.getText().toString().trim(), null);
                            }
                            Toast.makeText(CardDetailActivity.this, "set Time :" + timestamp, Toast.LENGTH_SHORT).show();
                            Log.i("TAG", "timestamp:" + timestamp);
                            card_duedate.setVisibility(View.VISIBLE);

                            tv_card_duedate.setText("");
                            tv_card_duetime.setText("");

                            tv_card_duedate.setText(tv_date.getText().toString());
                            tv_card_duetime.setText(tv_time.getText().toString());
                            HashMap<String, Object> datedata = new HashMap<String, Object>();
                            datedata.put("duedate", timestamp);
                            datedata.put("color", duedatecolor);
                            datedata.put("isOnlyDate", isOnlyDate);
                            itemdata.setdueDate(datedata);
                            passDataToAdapterAndSet(itemdata);
                            if (!finalIsChangedate) {
                                HashMap<String, Object> duedate = zboard_history.getJSON_DuedateInHistory(itemdata, zAuth.get());
                                zboard_history.addHistory(MainActivity.BOARD_ID, duedate, null);
                            } else {
                                HashMap<String, Object> duedate = zboard_history.getJSON_ChangeDuedate(datedata, itemdata.getObjectId(), itemdata.getData()
                                        , USERID);
                                zboard_history.addHistory(MainActivity.BOARD_ID, duedate, null);
                            }
                        }
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
//        final LayoutInflater localView = LayoutInflater.from(CardDetailActivity.this);
//        final View promptsView = localView.inflate(R.layout.prompt_duedate, null);
//
//        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setView(promptsView);
//        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
//                CardDetailActivity.this);
//        alertDialogBuilder.setView(promptsView);
//        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
//        alertDialog.setCanceledOnTouchOutside(false);
//
        final Calendar calender = Calendar.getInstance();
        final TextView tv_date = (TextView) promptsView.findViewById(R.id.date_view);
        final TextView tv_time = (TextView) promptsView.findViewById(R.id.time_view);
        final CheckBox red, green, yellow;
        red = (CheckBox) promptsView.findViewById(R.id.chk_red);
        green = (CheckBox) promptsView.findViewById(R.id.chk_green);
        yellow = (CheckBox) promptsView.findViewById(R.id.chk_yellow);

        red.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (green.isChecked())
                    green.setChecked(false);
                if (yellow.isChecked())
                    yellow.setChecked(false);
                red.setChecked(isChecked);
                if (isChecked)
                    duedatecolor = getString(R.string.red_500);
                else
                    duedatecolor = "";
            }
        });
        green.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (red.isChecked())
                    red.setChecked(false);
                if (yellow.isChecked())
                    yellow.setChecked(false);
                green.setChecked(isChecked);
                if (isChecked)
                    duedatecolor = getString(R.string.green_500);
                else
                    duedatecolor = "";
            }
        });
        yellow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (green.isChecked())
                    green.setChecked(false);
                if (red.isChecked())
                    red.setChecked(false);
                yellow.setChecked(isChecked);
                if (isChecked)
                    duedatecolor = getString(R.string.yellow_500);
                else
                    duedatecolor = "";
            }
        });

        if (tv_card_duedate.getText().length() > 0) {
//            String[] part = tv_card_duedate.getText().toString().split(" ");
            tv_date.setText(tv_card_duedate.getText().toString());
            //isChangedate = true;//to update in history that change duedate to ......
        }
        if (tv_card_duetime.getText().length() > 0) {
            tv_time.setText(tv_card_duetime.getText().toString());
        }
        tv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int date = calender.get(Calendar.DAY_OF_MONTH);
                int month = calender.get(Calendar.MONTH);
                int year = calender.get(Calendar.YEAR);
                DatePickerDialog datepicker = new DatePickerDialog(CardDetailActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Log.i("TAG", "month is :" + MONTH[month]);
                        tv_date.setText(MONTH[month] + "-" + dayOfMonth + "-" + year);
                    }
                }, year, month, date);
                datepicker.show();
                datepicker.setCanceledOnTouchOutside(true);
            }
        });
        tv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = calender.get(Calendar.HOUR_OF_DAY);
                int min = calender.get(Calendar.MINUTE);
                TimePickerDialog timepicker = new TimePickerDialog(CardDetailActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        tv_time.setText(getTime12HourFormate(hourOfDay, minute));
                    }
                }, hour, min, false);
                timepicker.show();
                timepicker.setCanceledOnTouchOutside(true);
            }
        });
        if (color.length() > 0) {
            if (color.equals(getString(R.string.red_500)))
                red.setChecked(true);
            else if (color.equals(getString(R.string.green_500)))
                green.setChecked(true);
            else if (color.equals(getString(R.string.yellow_500)))
                yellow.setChecked(true);
        }

    }

    public String getTime12HourFormate(int hourOfDay, int minute) {
        int hour = hourOfDay;
        int minutes = minute;
        String timeSet = "";
        if (hour > 12) {
            hour -= 12;
            timeSet = "PM";
        } else if (hour == 0) {
            hour += 12;
            timeSet = "AM";
        } else if (hour == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String min = "";
        if (minutes < 10)
            min = "0" + minutes;
        else
            min = String.valueOf(minutes);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hour).append(':')
                .append(min).append(" ").append(timeSet).toString();
        return aTime;
    }

    public Long getDateTimeStamp(String date, String time) {
        java.util.Date d = null;
        SimpleDateFormat formatter;
        if (time != null) {
            formatter = new SimpleDateFormat("MMM-dd-yyyyhh:mm");
            try {
                d = formatter.parse((date + time));
                Log.i("TAG", "date using sql in timestamp:" + (d.getTime()));
                return d.getTime() / 1000;
            } catch (ParseException e) {
                e.printStackTrace();
                Log.i("TAG", "error:" + e);
                return 0L;
            }
        } else {
            formatter = new SimpleDateFormat("MMM-dd-yyyy");
            try {
                d = formatter.parse(date);
                Log.i("TAG", "date using sql in timestamp:" + (d.getTime()));
                return d.getTime() / 1000;
            } catch (ParseException e) {
                e.printStackTrace();
                Log.i("TAG", "error:" + e);
                return 0L;
            }
        }
    }

    public String getDateFromTimeStamp(Long date, Boolean isOnlyDate) {
        if (isOnlyDate) {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd-yyyy");
            java.util.Date resultdate = new java.util.Date(date * 1000);
            Log.i("TAG", "newdate:" + sdf.format(resultdate));
            return sdf.format(resultdate);
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd-yyyy hh:mma");
            java.util.Date resultdate = new java.util.Date(date * 1000);
            Log.i("TAG", "newdate:" + sdf.format(resultdate));
            return sdf.format(resultdate);
        }
    }

    private void showLabelDialog(final List<String> selected_colorlist) {
        Log.i("TAG", "selected colors:" + selected_colorlist);
        final List<String> removed = new ArrayList<>();
        final List<String> added = new ArrayList<>();
        final List<String> TEMP_selectedColorList = new ArrayList<>();
        LayoutInflater localView = LayoutInflater.from(CardDetailActivity.this);
        View promptsView = localView.inflate(R.layout.prompt_label, null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(promptsView);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                CardDetailActivity.this);
        alertDialogBuilder.setView(promptsView);
        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(true);

        final LinearLayout label_list = (LinearLayout) promptsView.findViewById(R.id.label_parent);
        Button ok = (Button) promptsView.findViewById(R.id.btn_ok_label);
        Button cancel = (Button) promptsView.findViewById(R.id.btn_cancel_label);

        for (int i = 0; i < board_clrlabel.size(); i++) {
            View row = LayoutInflater.from(CardDetailActivity.this).inflate(R.layout.prompt_label_row, null, false);
            TextView title = (TextView) row.findViewById(R.id.labeltitle);
            View clrview = (View) row.findViewById(R.id.label_view);
            CheckBox chk = (CheckBox) row.findViewById(R.id.chk_label);
            final int finalI = i;
            chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        if (removed.contains(board_clrlabel.get(finalI).get("color").toString()))
                            removed.remove(board_clrlabel.get(finalI).get("color").toString());
                        if (selected_colorlist != null && !selected_colorlist.contains(board_clrlabel.get(finalI).get("color").toString()))
                            added.add(board_clrlabel.get(finalI).get("color").toString());
                        Log.i("TAG", "color added in list " + added);
                    } else {
                        if (added.contains(board_clrlabel.get(finalI).get("color").toString()))
                            added.remove(board_clrlabel.get(finalI).get("color").toString());
                        if (selected_colorlist != null && selected_colorlist.contains(board_clrlabel.get(finalI).get("color").toString()))
                            removed.add(board_clrlabel.get(finalI).get("color").toString());
                        Log.i("TAG", "color removed in list " + removed);
                    }

                }
            });
            if (board_clrlabel.get(i).get("title") != null | board_clrlabel.get(i).get("title").toString().length() > 0)
                title.setText(board_clrlabel.get(i).get("title").toString());
            if (selected_colorlist != null)
                if (selected_colorlist.contains(board_clrlabel.get(i).get("color").toString()))
                    chk.setChecked(true);
            Drawable mDrawable = ContextCompat.getDrawable(CardDetailActivity.this, R.drawable.viewpink_clr);
            mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor((String) board_clrlabel.get(i).get("color")), PorterDuff.Mode.SRC_IN));
            clrview.setBackground(mDrawable);
            label_list.addView(row);
        }
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = 0;
                Log.i("TAG", "added color" + added);
                Log.i("TAG", "removed color" + removed);
                if (cardlist_parent.getChildCount() > 0)
                    cardlist_parent.removeAllViews();
                if (selected_colorlist != null && selected_colorlist.size() > 0)
                    selected_colorlist.clear();
                for (int i = 0; i < label_list.getChildCount(); i++) {
                    View row = label_list.getChildAt(i);
                    CheckBox chk = (CheckBox) row.findViewById(R.id.chk_label);
                    if (chk.isChecked()) {
                        Toast.makeText(getApplicationContext(), "" + i, Toast.LENGTH_SHORT).show();
                        View view = new View(CardDetailActivity.this);
                        LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(dpToPx(40), dpToPx(30));
                        viewparams.setMargins(5, 0, 5, 0);
                        view.setLayoutParams(viewparams);
                        count++;
                        Drawable mDrawable = ContextCompat.getDrawable(CardDetailActivity.this, R.drawable.viewpink_clr);
                        mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor((String) board_clrlabel.get(i).get("color")), PorterDuff.Mode.SRC_IN));
                        view.setBackground(mDrawable);
                        if (count < 5) {
                            cardlist_parent.addView(view);
                            TEMP_selectedColorList.add(board_clrlabel.get(i).get("color").toString());
                        }
                    }
                }
                if (count > 5) {
                    TEMP_selectedColorList.clear();
                    for (int i = 0; i < board_clrlabel.size(); i++) {
                        View row = label_list.getChildAt(i);
                        CheckBox chk = (CheckBox) row.findViewById(R.id.chk_label);
                        if (added.contains(board_clrlabel.get(i).get("color"))) {
                            chk.setChecked(false);
                        }
                    }
                    added.clear();
                    removed.clear();
                    Log.i("TAG", "if count >5 added color" + added);
                    Log.i("TAG", "if count >5 removed color" + removed);
                    cardlist_parent.removeAllViews();
                    Snackbar.make(card_lables, "You can select any 4 lable", Snackbar.LENGTH_LONG).show();
                } else {
                    itemdata.setColorcodes(TEMP_selectedColorList);
                    passDataToAdapterAndSet(itemdata);
                    Log.i("TAG", "colors item:" + itemdata.getColorcodes());
                    if (selected_colorlist == null && selected_colorlist != TEMP_selectedColorList) {
                        HashMap<String, Object> data = zboard_history.getJSON_addCardLabel(itemdata, null, zAuth.get());
                        zboard_history.addHistory(MainActivity.BOARD_ID, data, null);
                    } else {
                        if (added.size() > 0) {
                            HashMap<String, Object> data = zboard_history.getJSON_addCardLabel(itemdata, added, USERID);
                            zboard_history.addHistory(MainActivity.BOARD_ID, data, null);
                        }
                        if (removed.size() > 0) {
                            HashMap<String, Object> data = zboard_history.getJSON_removeCardLabel(itemdata, removed, zAuth.get());
                            zboard_history.addHistory(MainActivity.BOARD_ID, data, null);
                        }
                    }
                    card_lables.setVisibility(View.VISIBLE);
                    alertDialog.dismiss();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public void setChecklist(List<HashMap<String, Object>> chklist) {
        //checklist_datalist=new ArrayList<>();
        if (checklist_listview.getChildCount() > 0)
            checklist_listview.removeAllViews();
        for (int i = 0; i < chklist.size(); i++) {
            View row = LayoutInflater.from(CardDetailActivity.this).inflate(R.layout.frag_checklist_row, null, false);
            CheckBox chk = (CheckBox) row.findViewById(R.id.chk_row);
            ImageView imgdelete = (ImageView) row.findViewById(R.id.delete_chk);
            View divider = (View) row.findViewById(R.id.chk_divider);
            imgdelete.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
            HashMap<String, Object> data = chklist.get(i);
            Log.i("TAG", "data:" + data);
            final String title = (String) data.get("title");
            Boolean selected = (Boolean) data.get("selected");
            chk.setText(title);
            chk.setChecked(selected);
            final int finalI = i;
            chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        HashMap<String, Object> mapdata = zboard_history.getJSON_checkedChecklist(title, USERID, CardDetailActivity.itemdata.getData(), CardDetailActivity.itemdata.getObjectId(), true);
                        zboard_history.addHistory(MainActivity.BOARD_ID, mapdata, null);
                    } else {
                        HashMap<String, Object> mapdata = zboard_history.getJSON_checkedChecklist(title, USERID, CardDetailActivity.itemdata.getData(), CardDetailActivity.itemdata.getObjectId(), false);
                        zboard_history.addHistory(MainActivity.BOARD_ID, mapdata, null);
                    }

                    HashMap<String, Object> newdata = new HashMap<String, Object>();
                    newdata.put("title", title);
                    newdata.put("selected", isChecked);
                    itemdata.getChecklist().set(finalI, newdata);
                    passDataToAdapterAndSet(itemdata);
                }
            });
            checklist_listview.addView(row);
            //checklist_datalist.add(new MemberPOJO(title,selected));
        }
        //chk_adapter=new MemberAdapter(checklist_datalist,CardDetailActivity.this);
        //checklist_listview.setAdapter(chk_adapter);
        card_chklist.setVisibility(View.VISIBLE);

    }

    private void showAssignDialog(final List<String> assignlist, final List<String> memberlist) {
        final List<MemberPOJO> assignylist = new ArrayList<>();
        final List<String> name = new ArrayList<>();
        final List<String> path = new ArrayList<>();
        final List<String> added = new ArrayList<>();
        final List<String> removed = new ArrayList<>();
        final ListView listview;
        final MemberAdapter adaper;
        MaterialDialog assign = new MaterialDialog.Builder(CardDetailActivity.this)
                .title("Add assign").titleColor(ContextCompat.getColor(CardDetailActivity.this, R.color.colorPrimary))
                .customView(R.layout.prompt_assign, true)
                .positiveText("Add").positiveColor(ContextCompat.getColor(CardDetailActivity.this, R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(CardDetailActivity.this, R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        List<String> temp_assign = new ArrayList<>();
                        if (user_assign.getChildCount() > 0)
                            user_assign.removeAllViews();
                        for (int i = 0; i < assignylist.size(); i++) {
                            if (assignylist.get(i).getIsMember()) {
                                Log.i("TAG", "data:  " + assignylist.get(i).getUsername());
                                temp_assign.add(memberlist.get(i));
                                View row = LayoutInflater.from(CardDetailActivity.this).inflate(R.layout.frag_member_row, null, false);
                                TextView name_ = (TextView) row.findViewById(R.id.user_fullname);
                                ImageView profile = (ImageView) row.findViewById(R.id.user_avatar);
                                name_.setText(assignylist.get(i).getUsername().toString());
                                if (path.get(i) != null && path.get(i).length() > 0)
                                    com.draghv.com.ImageLoader.defaultLoad(profile, assignylist.get(i).getUserprofile(), null);
                                user_assign.addView(row);
                            }
                        }

                        itemdata.setAssign(temp_assign);
                        passDataToAdapterAndSet(itemdata);
                        if(temp_assign.size()>0)
                            card_assign.setVisibility(View.VISIBLE);
                        else
                            card_assign.setVisibility(View.GONE);
                        if (assignlist == null || assignlist.size() == 0) {
                            HashMap<String, Object> assign = zboard_history.getJSON_AssignCardInHistory(itemdata, USERID);
                            zboard_history.addHistory(MainActivity.BOARD_ID, assign, null);
                        } else {
                            if (added.size() > 0) {
//                                HashMap<String,Object> mapdata=zboard_history.getJSON_RemoveAssignCard(added,itemdata.getObjectId(),itemdata.getData(),zAuth.get());
//                                zboard_history.addHistory(MainActivity.BOARD_ID, mapdata, null);
                            }
                            if (removed.size() > 0) {
                                HashMap<String, Object> mapdata = zboard_history.getJSON_RemoveAssignCard(removed, itemdata.getObjectId(), itemdata.getData(), USERID);
                                zboard_history.addHistory(MainActivity.BOARD_ID, mapdata, null);
                            }
//                            Log.i("TAG","assign Added:"+added);
//                            Log.i("TAG","assign Removed:"+removed);
                        }

                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                })
                .show();
        listview = (ListView) assign.findViewById(R.id.assign_parent);
        adaper = new MemberAdapter(assignylist, CardDetailActivity.this);
        listview.setAdapter(adaper);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (assignylist.get(position).getIsMember()) {
                    assignylist.get(position).setIsMember(false);
                    if (added.contains(memberlist.get(position))) {
                        added.remove(memberlist.get(position));
                    }
                    removed.add(memberlist.get(position));
                } else {
                    assignylist.get(position).setIsMember(true);
                    if (removed.contains(memberlist.get(position))) {
                        removed.remove(memberlist.get(position));
                    }
                    added.add(memberlist.get(position));
                }
                adaper.notifyDataSetChanged();
            }
        });
        for (int i = 0; i < memberlist.size(); i++) {
            final String id = memberlist.get(i);
            database.getReference().child("_user").child(id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.i("TAG", "userdata snap:" + dataSnapshot);
                    HashMap<String, Object> map = (HashMap<String, Object>) dataSnapshot.getValue();
                    name.add((String) map.get("name"));
                    String image = map.get("image") != null ? (String) map.get("image") : "";
                    path.add(image);
                    if (assignlist != null && assignlist.size() > 0 && assignlist.contains(id))
                        assignylist.add(new MemberPOJO(map.get("name").toString(), image, true));
                    else
                        assignylist.add(new MemberPOJO(map.get("name").toString(), image, false));
                    adaper.notifyDataSetChanged();
                    Log.i("TAG", "userdata:" + name + "=>" + path);


                    ViewGroup.LayoutParams params = listview.getLayoutParams();
                    params.height = 350;
                    listview.setLayoutParams(params);
                    listview.requestLayout();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }


    @Override
    public void onClick(View v) {

    }
}

