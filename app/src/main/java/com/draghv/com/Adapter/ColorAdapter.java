package com.draghv.com.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.draghv.com.Models.ColorPOJO;
import com.draghv.com.R;

import java.util.List;

/**
 * Created by Payal on 06-Feb-17.
 */
public class ColorAdapter extends BaseAdapter {
    List<ColorPOJO> colorList;
    Context mcontext;

    public ColorAdapter(List<ColorPOJO> colorList, Context mcontext) {
        this.colorList = colorList;
        this.mcontext = mcontext;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater infla = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infla.inflate(R.layout.prompt_color_row, parent, false);
            holder = new ViewHolder();
            holder.btn = convertView.findViewById(R.id.btn_view);
            holder.btn_back = (ImageView)convertView.findViewById(R.id.btn_view_back);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String color = (String) colorList.get(position).getColor();
        Boolean selected=(Boolean)colorList.get(position).getSelected();

        Drawable mDrawable = ContextCompat.getDrawable(mcontext, R.drawable.view_round_back);
        mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_IN));
        holder.btn.setBackground(mDrawable);
        holder.btn.setVisibility(View.VISIBLE);

        if(selected)
            holder.btn_back.setVisibility(View.VISIBLE);
        else
            holder.btn_back.setVisibility(View.GONE);

        return convertView;
    }


    @Override
    public int getCount() {
        return colorList.size();
    }

    @Override
    public Object getItem(int position) {
        return colorList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    public class ViewHolder {
        View btn;
        ImageView btn_back;
    }
}
