package com.draghv.com.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.draghv.com.Fragment.SharedBoardList;
import com.draghv.com.GetBoardList;
import com.draghv.com.GetSharedBoard;
import com.draghv.com.Models.BoardListPOJO;
import com.draghv.com.R;
import com.draghv.com.Zboard;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

/**
 * Created by Payal on 10-Mar-17.
 */
public class SharedBoardListAdapter extends RecyclerView.Adapter<SharedBoardListAdapter.ViewHolder> {

    List<GetBoardList> sharedBoardList;
    Context context;
    Zboard zboard;
    SharedBoardList fragment;

    public SharedBoardListAdapter(List<GetBoardList> sharedBoardList, Context context,SharedBoardList sharedBoardListFrag) {
        this.sharedBoardList = sharedBoardList;
        this.context = context;
        fragment=sharedBoardListFrag;
        zboard = new Zboard();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.boardlist_row,parent,false);
        ViewHolder vh=new ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final GetBoardList data=sharedBoardList.get(position);
        holder.tv_boardName.setText(data.getTitle());
        holder.star.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        if (sharedBoardList != null && sharedBoardList.size() > 0)
            return sharedBoardList.size();
        else
            return 0;
    }

//    @Override
//    public int getCount() {
//        if (sharedBoardList != null && sharedBoardList.size() > 0)
//            return sharedBoardList.size();
//        else
//            return 0;
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return sharedBoardList.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return 0;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//        if(convertView==null){
//            LayoutInflater infla = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = infla.inflate(R.layout.boardlist_row, parent, false);
//            holder = new ViewHolder();
//            holder.tv_boardName=(TextView)convertView.findViewById(R.id.tv_boardname);
//            holder.star=(ImageView)convertView.findViewById(R.id.iv_favorite);
//            holder.sharedwith=(TextView)convertView.findViewById(R.id.tv_shared);
//            convertView.setTag(holder);
//        }else{
//            holder=(ViewHolder)convertView.getTag();
//        }
//        final GetBoardList data=sharedBoardList.get(position);
//        holder.tv_boardName.setText(data.getTitle());
//        holder.star.setVisibility(View.GONE);
//        return convertView;
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_boardName;
        ImageView star;
        TextView sharedwith;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_boardName=(TextView)itemView.findViewById(R.id.tv_boardname);
            star=(ImageView)itemView.findViewById(R.id.iv_favorite);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragment.RecyclerViewClick(getAdapterPosition());
//                    Toast.makeText(v.getContext(),"relative clicked",Toast.LENGTH_SHORT).show();
                }
            });

        }

    }
}
