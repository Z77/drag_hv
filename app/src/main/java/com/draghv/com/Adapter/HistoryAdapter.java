package com.draghv.com.Adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.draghv.com.GetBoardHistory;
import com.draghv.com.MainActivity;
import com.draghv.com.R;
import com.draghv.com.ZobazeUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 16-Feb-17.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    List<GetBoardHistory> dataList;
    Context mcontext;
    FirebaseDatabase database;
    ZobazeUser zobazeUser;

    public HistoryAdapter(List<GetBoardHistory> data, Context mcontext) {
        this.dataList = data;
        this.mcontext = mcontext;
        database = FirebaseDatabase.getInstance();
        zobazeUser=new ZobazeUser();
    }

//    @Override
//    public int getCount() {
//        if (dataList != null && dataList.size() > 0)
//            return dataList.size();
//        else
//            return 0;
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return dataList.get(position);
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(mcontext).inflate(R.layout.activity_history_row,parent,false);
        ViewHolder holder=new ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GetBoardHistory data=dataList.get(position);
        if (data.getAction().equalsIgnoreCase("add card")) {
            setAddCardData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("add assign")) {
            setAssignData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("remove assign")) {
            setRemoveAssignData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("add duedate")) {
            setDueDateData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("change duedate")) {
            setChangeDueDateData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("change title")) {
            setChangeTitleData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("add member")) {
            setAddMemberData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("remove member")) {
            setRemoveMemberData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("add label")) {
            setAddLabelData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("remove label")) {
            setRemoveLabelData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("edit label")) {
            setEditLabelData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("add clabel")) {
            setAddlabelCardData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("remove clabel")) {
            setRemovelabelCardData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("add attchment")) {
            setAddAttchmentData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("remove attchment")) {
            setRemoveAttchmentData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("add checklist")) {
            setAddCheckListData(data, holder, false);
        } else if (data.getAction().equalsIgnoreCase("updated checklist")) {
            setAddCheckListData(data, holder, true);
        } else if (data.getAction().equalsIgnoreCase("checked chklist")) {
            setCheckdedCheckListData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("uncheck chklist")) {
            setUncheckedChecklistData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("delete column")) {
            setDeleteColumnData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("add column")) {
            setAddColumnData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("change columntitle")) {
            setChangeColTitleData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("move card") || data.getAction().equalsIgnoreCase("transfer card")) {
            setMoveCardData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("archive card")) {
            setArchiveCardData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("unarchive card")) {
            setUnarchiveCardData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("copy card") || data.getAction().equalsIgnoreCase("copy card otherboard")) {
            setCopyCardData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("move column")) {
            setMoveColumnData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("drag card")) {
            setCardDragData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("add card desc")) {
            setCardDescData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("update card item") || data.getAction().equalsIgnoreCase("add card item") || data.getAction().equalsIgnoreCase("delete card item")) {
            setCardItemData(data, holder);
        } else if (data.getAction().equalsIgnoreCase("comment")) {
            setCardCommentData(data, holder);
        } else {
            Log.i("TAG", "in action else");
        }

    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        if (dataList != null && dataList.size() > 0)
            return dataList.size();
        else
            return 0;
    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder holder;
//        if (convertView == null) {
//            LayoutInflater infla = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = infla.inflate(R.layout.activity_history_row, parent, false);
//            holder = new ViewHolder();
//            holder.action = (TextView) convertView.findViewById(R.id.action_string);
//            holder.createdat = (TextView) convertView.findViewById(R.id.action_time);
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//        GetBoardHistory data = dataList.get(position);
//        Log.i("TAG", "action data1:" + data.getAction());
//        Log.i("TAG", "action data2:" + data.getUserId());
//        if (data.getAction().equalsIgnoreCase("add card")) {
//            setAddCardData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("add assign")) {
//            setAssignData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("remove assign")) {
//            setRemoveAssignData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("add duedate")) {
//            setDueDateData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("change duedate")) {
//            setChangeDueDateData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("change title")) {
//            setChangeTitleData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("add member")) {
//            setAddMemberData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("remove member")) {
//            setRemoveMemberData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("add label")) {
//            setAddLabelData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("remove label")) {
//            setRemoveLabelData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("edit label")) {
//            setEditLabelData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("add clabel")) {
//            setAddlabelCardData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("remove clabel")) {
//            setRemovelabelCardData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("add attchment")) {
//            setAddAttchmentData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("remove attchment")) {
//            setRemoveAttchmentData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("add checklist")) {
//            setAddCheckListData(data, holder, false);
//        } else if (data.getAction().equalsIgnoreCase("updated checklist")) {
//            setAddCheckListData(data, holder, true);
//        } else if (data.getAction().equalsIgnoreCase("checked chklist")) {
//            setCheckdedCheckListData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("uncheck chklist")) {
//            setUncheckedChecklistData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("delete column")) {
//            setDeleteColumnData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("add column")) {
//            setAddColumnData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("change columntitle")) {
//            setChangeColTitleData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("move card") || data.getAction().equalsIgnoreCase("transfer card")) {
//            setMoveCardData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("archive card")) {
//            setArchiveCardData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("unarchive card")) {
//            setUnarchiveCardData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("copy card") || data.getAction().equalsIgnoreCase("copy card otherboard")) {
//            setCopyCardData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("move column")) {
//            setMoveColumnData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("drag card")) {
//            setCardDragData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("add card desc")) {
//            setCardDescData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("update card item") || data.getAction().equalsIgnoreCase("add card item") || data.getAction().equalsIgnoreCase("delete card item")) {
//            setCardItemData(data, holder);
//        } else if (data.getAction().equalsIgnoreCase("comment")) {
//            setCardCommentData(data, holder);
//        } else {
//            Log.i("TAG", "in action else");
//        }
//        //  || data.getAction().equalsIgnoreCase("update card item")||data.getAction().equalsIgnoreCase("add card desc")) {
//        return convertView;
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView action;
        TextView createdat;

        public ViewHolder(View itemView) {
            super(itemView);
            action=(TextView)itemView.findViewById(R.id.action_string);
            createdat=(TextView)itemView.findViewById(R.id.action_time);
        }
    }

    public String getDateFromTimeStamp(Long date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        //Long init= Long.valueOf(date);
        java.util.Date resultdate;
        if (pattern.equals("MMM dd,hh:mm a")) {
            resultdate = new java.util.Date(date * 1000L);
        } else {
            resultdate = new java.util.Date(date);
        }
        Log.i("TAG", "newdate:" + sdf.format(resultdate));
        return sdf.format(resultdate);
    }

    public void setAddCardData(final GetBoardHistory data, final ViewHolder holder) {
        //database.getReference().child("dashboard").child("e204681a69ff2d54").child(MainActivity.BOARD_ID).child("totalcol").addListenerForSingleValueEvent(new ValueEventListener() {
//        database.getReference().child("dashboard").child(zobazeUser.userObjectId()).child(MainActivity.BOARD_ID).child("totalcol").addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                final List<HashMap<String, Object>> coldata = (List<HashMap<String, Object>>) dataSnapshot.getValue();

                database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String colname = "";
//                        for (int i = 0; i < coldata.size(); i++) {
//                            if (coldata.get(i).get("id").equals(data.getColumnId()))
//                                colname = (String) coldata.get(i).get("title");
//
//                        }
                        colname=data.getColumnTitle();
                        HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                        String username = (String) userdata.get("name");
                        String actionStr = "<b>" + username + "</b>" + " added " + data.getCardTitle() + " to " + colname;
                        if (Build.VERSION.SDK_INT >= 24) {
                            holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                        } else {
                            holder.action.setText(Html.fromHtml(actionStr));
                        }
                        //holder.action.setText(actionStr);
                        holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
    }

    public void setAssignData(final GetBoardHistory data, final ViewHolder holder) {
        final StringBuilder assignyName = new StringBuilder();
        for (int i = 0; i < data.getAssign().size(); i++) {
            final int finalI = i;
            database.getReference().child("_user").child(data.getAssign().get(i)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                    if (finalI != data.getAssign().size() - 1) {
                        assignyName.append(userdata.get("name"));
                        assignyName.append(",");
                    } else {
                        assignyName.append((String) userdata.get("name"));

                        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                                String username = (String) userdata.get("name");
                                String actionStr = "<b>" + username + "</b>" + " assigned " + data.getCardTitle() + " to " + assignyName.toString();
                                //holder.action.setText(actionStr);
                                if (Build.VERSION.SDK_INT >= 24) {
                                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                                } else {
                                    holder.action.setText(Html.fromHtml(actionStr));
                                }
                                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    public void setRemoveAssignData(final GetBoardHistory data,final ViewHolder  holder){
        final StringBuilder assignyName = new StringBuilder();
        for (int i = 0; i < data.getAssign().size(); i++) {
            final int finalI = i;
            database.getReference().child("_user").child(data.getAssign().get(i)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                    if (finalI != data.getAssign().size() - 1) {
                        assignyName.append(userdata.get("name"));
                        assignyName.append(",");
                    } else {
                        assignyName.append((String) userdata.get("name"));

                        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                                String username = (String) userdata.get("name");
                                String actionStr = "<b>" + username + "</b>" + " removed " + assignyName.toString() + "  from " +data.getCardTitle() ;
                                //holder.action.setText(actionStr);
                                if (Build.VERSION.SDK_INT >= 24) {
                                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                                } else {
                                    holder.action.setText(Html.fromHtml(actionStr));
                                }
                                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public void setDueDateData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b>" + " set " + data.getCardTitle() + " to be due " + getDateFromTimeStamp((Long) data.getDuedate().get("duedate"), "MMM dd,hh:mm a");
                //holder.action.setText(actionStr);
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
                Log.i("TAG", "user data:" + userdata);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setChangeDueDateData(final GetBoardHistory data,final ViewHolder holder){
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b>" + " changed the due date of " + data.getCardTitle() + " to " + getDateFromTimeStamp((Long) data.getDuedate().get("duedate"), "MMM dd,hh:mm a");
                //holder.action.setText(actionStr);
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
                Log.i("TAG", "user data:" + userdata);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setChangeTitleData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b>" + " changed title from " + data.getCardoldTitle() + " to " + data.getCardTitle();
                //holder.action.setText(actionStr);
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setAddMemberData(final GetBoardHistory data, final ViewHolder holder) {
        final StringBuilder assignyName = new StringBuilder();
        for (int i = 0; i < data.getMember().size(); i++) {
            final int finalI = i;
            database.getReference().child("_user").child(data.getMember().get(i)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                    if (finalI != data.getMember().size() - 1) {
                        assignyName.append(userdata.get("name"));
                        assignyName.append(",");
                    } else {
                        assignyName.append((String) userdata.get("name"));

                        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                                String username = (String) userdata.get("name");
                                String actionStr = "<b>" + username + "</b>" + " added " + assignyName.toString() + " to this board";
                                //holder.action.setText(actionStr);
                                if (Build.VERSION.SDK_INT >= 24) {
                                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                                } else {
                                    holder.action.setText(Html.fromHtml(actionStr));
                                }
                                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public void setRemoveMemberData(final GetBoardHistory data, final ViewHolder holder) {
        final StringBuilder assignyName = new StringBuilder();
        for (int i = 0; i < data.getMember().size(); i++) {
            final int finalI = i;
            database.getReference().child("_user").child(data.getMember().get(i)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                    if (finalI != data.getMember().size() - 1) {
                        assignyName.append(userdata.get("name"));
                        assignyName.append(",");
                    } else {
                        assignyName.append((String) userdata.get("name"));

                        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                                String username = (String) userdata.get("name");
                                String actionStr = "<b>" + username + "</b>" + " removed " + assignyName.toString() + " from this board";
                                //holder.action.setText(actionStr);
                                if (Build.VERSION.SDK_INT >= 24) {
                                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                                } else {
                                    holder.action.setText(Html.fromHtml(actionStr));
                                }
                                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public void setAddLabelData(final GetBoardHistory data, final ViewHolder holder) {
//        final StringBuilder colors=new StringBuilder();
//        for(int i=0;i<data.getLabel().size();i++){
//            if(i!=data.getLabel().size()-1){
//                colors.append(data.getLabel().get(i));
//                colors.append(", ");
//            }else {
//                colors.append(data.getLabel().get(i));
//            }
//        }
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                //String actionStr = username + " added new color label "+ colors.toString()+" to this board";
                String actionStr = "<b>" + username + "</b>" + " added " + data.getLabel().size() + " new color label to this board";
                //holder.action.setText(actionStr);
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setRemoveLabelData(final GetBoardHistory data, final ViewHolder holder) {
//        final StringBuilder colors=new StringBuilder();
//        for(int i=0;i<data.getLabel().size();i++){
//            if(i!=data.getLabel().size()-1){
//                colors.append(data.getLabel().get(i));
//                colors.append(", ");
//            }else {
//                colors.append(data.getLabel().get(i));
//            }
//        }
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                //String actionStr = username + " removed "+ colors.toString()+" color label from this board";
                String actionStr = "<b>" + username + "</b>" + " removed " + data.getLabel().size() + " color label from this board";
                //holder.action.setText(actionStr);
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setEditLabelData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                //String actionStr = username + " removed "+ colors.toString()+" color label from this board";
                String actionStr = "<b>" + username + "</b>" + " edited " + data.getLabelTitle().size() + " color label to this board";

                //holder.action.setText(actionStr);
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setAddlabelCardData(final GetBoardHistory data, final ViewHolder holder) {

        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b>" + " added " + data.getLabel().size() + " color label to " + data.getCardTitle();
                //holder.action.setText(actionStr);
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setRemovelabelCardData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b>" + " removed " + data.getLabel().size() + " color label from " + data.getCardTitle();
                //holder.action.setText(actionStr);
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setAddAttchmentData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b>" + " added " + data.getAttach_type() + " attachment to " + data.getCardTitle();
                //holder.action.setText(actionStr);
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setRemoveAttchmentData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b>" + " removed " + data.getAttach_type() + " attachment from " + data.getCardTitle();
                //holder.action.setText(actionStr);
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setAddCheckListData(final GetBoardHistory data, final ViewHolder holder, final Boolean isUpdated) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");

                String actionStr;
                if (!isUpdated)
                    actionStr = "<b>" + username + "</b>" + " added checklist to " + data.getCardTitle();
                else
                    actionStr = "<b>" + username + "</b>" + " updated checklist to " + data.getCardTitle();
                //holder.action.setText(actionStr);
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setCheckdedCheckListData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b>" + " completed " + data.getChecked() + " on " + data.getCardTitle();
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                //holder.action.setText(actionStr);
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setUncheckedChecklistData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                //String actionStr = username + " marked incomplete "+data.getChecked()+" on "+ data.getCardTitle();
//                SpannableStringBuilder sb = new SpannableStringBuilder(username);
//                StyleSpan b = new StyleSpan(android.graphics.Typeface.BOLD);
//                sb.setSpan(b, 0, username.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                String actionStr = "<b>" + username + "</b> " + " marked incomplete " + data.getChecked() + " on " + data.getCardTitle();
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setDeleteColumnData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b>" + " deleted " + data.getColumnTitle() + " column from this board.";
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                //holder.action.setText(actionStr);
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setAddColumnData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b>" + " added " + data.getColumnTitle() + " column to this board.";
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                //holder.action.setText(actionStr);
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setChangeColTitleData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b>" + " changed column title from " + data.getColumnTitle() + " to " + data.getColumnoldTitle() + ".";
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                //holder.action.setText(actionStr);
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setMoveCardData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr;
                if (data.getAction().equals("transfer card"))
                    actionStr = "<b>" + username + "</b> " + " transferred " + data.getMoved() + " cards to " + data.getBoardName();
                else
                    actionStr = "<b>" + username + "</b> " + " moved " + data.getMoved() + " cards from " + data.getColumnoldTitle() + " to " + data.getColumnTitle();

                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setArchiveCardData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b> " + " archived " + data.getMoved() + " cards.";
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setUnarchiveCardData(final GetBoardHistory data,final ViewHolder holder){
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b> " + " sent " + data.getCardTitle() + " to the board.";
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setCopyCardData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr;
                if (data.getAction().equals("copy card"))
                    actionStr = "<b>" + username + "</b> " + " copied " + data.getMoved() + " cards from " + data.getColumnoldTitle() + " to " + data.getColumnTitle();
                else
                    actionStr = "<b>" + username + "</b>" + " copied " + data.getMoved() + " cards from list " + data.getColumnoldTitle() + " of board " + data.getBoardName() + ".";
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setMoveColumnData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b> " + " transferred " + data.getColumnTitle() + " to " + data.getBoardName() + ".";
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setCardDragData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b> " + " moved " + data.getCardTitle() + " from " + data.getColumnoldTitle() + " to " + data.getColumnTitle() + ".";
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setCardDescData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr = "<b>" + username + "</b> " + " added description to " + data.getCardTitle() + ".";
                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setCardItemData(final GetBoardHistory data, final ViewHolder holder) {
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr="";
                if (data.getAction().equals("add card item"))
                    actionStr = "<b>" + username + "</b> " + " added " + data.getItemcount() + " items of " + data.getBusinessname() + ".";
                else if (data.getAction().equals("update card item"))
                    actionStr = "<b>" + username + "</b> " + " append " + data.getItemcount() + " items to " + data.getBusinessname() + ".";
                else if (data.getAction().equals("delete card item"))
                    actionStr = "<b>" + username + "</b> " + " deleted " + data.getItemcount() + " items from " + data.getBusinessname() + ".";

                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setCardCommentData(final GetBoardHistory data,final ViewHolder holder){
        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> userdata = (HashMap<String, Object>) dataSnapshot.getValue();
                String username = (String) userdata.get("name");
                String actionStr="";

                if (data.getComment())
                    actionStr = "<b>" + username + "</b> " + " enabled comment to "+ data.getCardTitle();
                else
                    actionStr = "<b>" + username + "</b> " + " disabled comment to "+ data.getCardTitle();

                if (Build.VERSION.SDK_INT >= 24) {
                    holder.action.setText(Html.fromHtml(actionStr, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.action.setText(Html.fromHtml(actionStr));
                }
                holder.createdat.setText(getDateFromTimeStamp(data.getCreatedAt(), "MMM dd,yyyy"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
