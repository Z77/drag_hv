package com.draghv.com.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.draghv.com.Fragment.FragmentRearrangeList;
import com.draghv.com.Helper.ItemTouchHelperCallBack;
import com.draghv.com.R;

import java.util.List;

/**
 * Created by Payal on 05-Jan-17.
 */
public class rearrangeAdapter extends RecyclerView.Adapter<rearrangeAdapter.myViewHolder> {
    List<String> namelist;
    Context mcontext;
    FragmentRearrangeList fragment;
    Boolean is_write;

    public rearrangeAdapter(List<String> namelist, Context mcontext,FragmentRearrangeList fragment,Boolean iswrite) {
        this.namelist = namelist;
        this.mcontext = mcontext;
        this.fragment=fragment;
        this.is_write=iswrite;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myview = LayoutInflater.from(parent.getContext()).inflate(R.layout.rearrange_row, parent, false);
        myViewHolder mvh = new myViewHolder(myview);
        return mvh;
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, final int position) {
        holder.listname.setText(namelist.get(position));
    }

    @Override
    public int getItemCount() {
        return namelist != null ? namelist.size() : 0;
    }

    public class myViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperCallBack {
        TextView listname;
        ImageButton edit, delete;
        CardView card;
        RelativeLayout frame;

        public myViewHolder(View itemView) {
            super(itemView);
            listname = (TextView) itemView.findViewById(R.id.list_name);
            edit = (ImageButton) itemView.findViewById(R.id.edit_name);
            delete = (ImageButton) itemView.findViewById(R.id.delete_name);
            card=(CardView)itemView.findViewById(R.id.rearrane_card);
            frame=(RelativeLayout)itemView.findViewById(R.id.frame_rearrange);
            if(!is_write){
                delete.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
            }
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragment.DeleteHeader(v,getAdapterPosition());
                    //FragmentRearrangeList.DeleteHeader(v,getAdapterPosition());

                }
            });
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragment.EditHeader(v,getAdapterPosition());
                    //FragmentRearrangeList.EditHeader(v,getAdapterPosition());
                }
            });

        }

        @Override
        public void onItemSelected() {
//            card.setElevation(12);
            card.setCardElevation(12);
            frame.setBackground(ContextCompat.getDrawable(itemView.getContext(),R.drawable.selector_row));
            Toast.makeText(itemView.getContext(),"selected",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onItemClear() {
  //          card.setElevation(2);
            card.setCardElevation(2);
            frame.setBackground(null);
            Toast.makeText(itemView.getContext(),"clear",Toast.LENGTH_SHORT).show();
        }
    }


    public void addItem(String newTitle) {
        namelist.add(newTitle);
        notifyDataSetChanged();
    }
}
