package com.draghv.com.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.draghv.com.GetBoardComment;
import com.draghv.com.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 27-Feb-17.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final int WEEKS_MILLIS = 7 * DAY_MILLIS;
    FirebaseDatabase database;

    List<GetBoardComment> commentList;
    Context mcontext;

    public CommentAdapter(List<GetBoardComment> commentList, Context mcontext) {
        this.commentList = commentList;
        this.mcontext = mcontext;
        database=FirebaseDatabase.getInstance();
    }

    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(mcontext).inflate(R.layout.comments_row,parent,false);
        ViewHolder cvh=new ViewHolder(itemView);
        return cvh;
    }

    @Override
    public void onBindViewHolder(final CommentAdapter.ViewHolder holder, int position) {
        GetBoardComment data=commentList.get(position);
        holder.comment.setText(data.getComment());
        holder.time.setText(getTimeAgo(data.getCreatedAt()));
        holder.username.setText(data.getUserId());

        database.getReference().child("_user").child(data.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String,Object> userdata= (HashMap<String, Object>) dataSnapshot.getValue();

                holder.username.setText(userdata.get("name").toString());

                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext).build();
                final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
                loader.init(config);
                final DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .cacheInMemory(true).resetViewBeforeLoading(true)
                        .cacheOnDisk(true)
                        .displayer(new CircleBitmapDisplayer(5)).showImageForEmptyUri(R.drawable.cp_user).build();
                loader.displayImage(userdata.get("image").toString(),holder.img, options);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public int getItemCount() {
        if(commentList!=null&&commentList.size()>0)
            return commentList.size();
        else
            return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;
        public TextView username,comment,time;

        public ViewHolder(View itemView) {
            super(itemView);
            img=(ImageView)itemView.findViewById(R.id.user_profile);
            username=(TextView)itemView.findViewById(R.id.user_name);
            comment=(TextView)itemView.findViewById(R.id.tv_comment);
            time=(TextView)itemView.findViewById(R.id.tv_ctime);
        }
    }

    public static final String getTimeAgo(long time) {
        SimpleDateFormat simpleDateFormat;
        SimpleDateFormat dateFormat;
        DateFormat timeFormat;
        Date dateTimeNow = null;
        String timeFromData;
        String pastDate;
        String sDateTimeNow;

        Date startDate = new Date(time);
        simpleDateFormat = new SimpleDateFormat("dd/M/yyyy HH:mm:ss");
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        timeFormat = new SimpleDateFormat("h:mm aa");

        Date now = new Date();
        sDateTimeNow = simpleDateFormat.format(now);

        try {
            dateTimeNow = simpleDateFormat.parse(sDateTimeNow);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //  date counting is done till todays date
        Date endDate = dateTimeNow;

        //  time difference in milli seconds
        long different = endDate.getTime() - startDate.getTime();

        // TODO: localize
        if (different < MINUTE_MILLIS) {
            return "just now";
        } else if (different < 2 * MINUTE_MILLIS) {
            return "a min ago";
        } else if (different < 50 * MINUTE_MILLIS) {
            return different / MINUTE_MILLIS + " mins ago";
        } else if (different < 90 * MINUTE_MILLIS) {
            return "a hour ago";
        } else if (different < 24 * HOUR_MILLIS) {
            timeFromData = timeFormat.format(startDate);
            return timeFromData;
        } else if (different < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else if (different < 7 * DAY_MILLIS){
            return different / DAY_MILLIS + " days ago";
        } else if (different < 2 * WEEKS_MILLIS){
            return different / WEEKS_MILLIS + " week ago";
        } else if (different < 3.5 * WEEKS_MILLIS){
            return different / WEEKS_MILLIS + " weeks ago";
        } else {
            pastDate = dateFormat.format(startDate);
            return pastDate;
        }
    }
}
