package com.draghv.com.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.draghv.com.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

/**
 * Created by Payal on 26-Nov-16.
 */
public class CDetail_RVItemAdapter extends RecyclerView.Adapter<CDetail_RVItemAdapter.myViewHolder> {
    private List<ItemData> itemDataList;
    private Context context;
    DisplayMetrics displayMetrics;
    int width;


    public class myViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView name;
        ProgressBar pbar;
        public myViewHolder(View itemView) {
            super(itemView);
            image=(ImageView)itemView.findViewById(R.id.item_thumb);
            name=(TextView)itemView.findViewById(R.id.item_name);
            pbar=(ProgressBar)itemView.findViewById(R.id.pbar_item_build);
        }
    }

    public CDetail_RVItemAdapter(List<ItemData> itemDataList, Context context) {
        this.itemDataList = itemDataList;
        this.context = context;
        displayMetrics = context.getResources().getDisplayMetrics();
        width = displayMetrics.widthPixels;
    }

    @Override
    public CDetail_RVItemAdapter.myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.itemsattach_row,parent,false);
        myViewHolder mvh=new myViewHolder(view);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final CDetail_RVItemAdapter.myViewHolder holder, int position) {
        //image loader config
        ImageLoaderConfiguration config=new ImageLoaderConfiguration.Builder(context).build();
        final ImageLoader loader=ImageLoader.getInstance();
        loader.init(config);
        final DisplayImageOptions options=new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .build();

        //int height = displayMetrics.heightPixels;
        int finalwidth=width/3;
        int finalheight=(width/3);
        Log.i("TAG","final width and height=>"+finalheight+" ; "+finalwidth);
        FrameLayout.LayoutParams imagvieparams= (FrameLayout.LayoutParams) holder.image.getLayoutParams();
        imagvieparams.height=finalheight;
        imagvieparams.width=finalwidth;


        holder.image.setLayoutParams(imagvieparams);
//        holder.name.setMinimumWidth(finalwidth - 8);
//        holder.name.setMaxWidth(finalwidth-8);


        if(itemDataList.get(position).getItemImagePath()!=null){
            loader.displayImage(itemDataList.get(position).getItemImagePath(), holder.image, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.pbar.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
        }
        if(itemDataList.get(position).getItemName()!=null){
            holder.name.setSelected(true);
            holder.name.setText(itemDataList.get(position).getItemName());
        }
    }
    public void addItem(ItemData dataObj){
        itemDataList.add(dataObj);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return itemDataList.size();
    }
    public static class ItemData{
        private String itemImagePath;
        private String itemName;

        public ItemData(String itemImagePath, String itemName) {
            this.itemImagePath = itemImagePath;
            this.itemName = itemName;
        }

        public String getItemImagePath() {
            return itemImagePath;
        }

        public void setItemImagePath(String itemImagePath) {
            this.itemImagePath = itemImagePath;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }
    }

}
