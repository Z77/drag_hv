package com.draghv.com.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.draghv.com.Fragment.FragmentLables;
import com.draghv.com.Models.ColorlabelListPOJO;
import com.draghv.com.R;

import java.util.List;

/**
 * Created by Payal on 04-Feb-17.
 */
public class ColorlabelAdapter extends BaseAdapter {
    Context mcontext;
    List<ColorlabelListPOJO> datalist;
    FragmentLables fragmentLables;
    Boolean iswrite;
    public ColorlabelAdapter(Context mcontext, List<ColorlabelListPOJO> datalist,FragmentLables fragment,Boolean is_write) {
        this.mcontext = mcontext;
        this.datalist = datalist;
        this.fragmentLables=fragment;
        this.iswrite=is_write;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater infla = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infla.inflate(R.layout.labels_list_row, parent, false);
            holder = new ViewHolder();
            holder.colorlabel = (TextView) convertView.findViewById(R.id.color_view_title);
            holder.colorview=(View)convertView.findViewById(R.id.color_view);
            holder.edit = (ImageView) convertView.findViewById(R.id.edit_lable);
            holder.delete = (ImageView) convertView.findViewById(R.id.delete_lable);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final ColorlabelListPOJO data = datalist.get(position);
        holder.colorlabel.setText(data.getTitle());
        if(!iswrite){
            holder.edit.setVisibility(View.GONE);
            holder.delete.setVisibility(View.GONE);
        }

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Toast.makeText(v.getContext(),"pos:"+position,Toast.LENGTH_SHORT).show();
                fragmentLables.EditLabel(position,v);

            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Toast.makeText(v.getContext(), "pos:" + position, Toast.LENGTH_SHORT).show();
                fragmentLables.DeleteLabel(position,v);


            }
        });

        Drawable mDrawable = ContextCompat.getDrawable(mcontext, R.drawable.viewpink_clr);
        mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(data.getHexColor()), PorterDuff.Mode.SRC_IN));
        holder.colorview.setBackground(mDrawable);
        return convertView;
    }

    @Override
    public int getCount() {
        if (datalist != null && datalist.size() > 0)
            return datalist.size();
        else
            return 0;

    }

    @Override
    public Object getItem(int position) {
        return datalist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class ViewHolder {
        TextView colorlabel;
        View colorview;
        ImageView edit, delete;
    }

}
