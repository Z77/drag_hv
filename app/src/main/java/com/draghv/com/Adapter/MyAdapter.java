package com.draghv.com.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.draghv.com.Models.MyItem;
import com.draghv.com.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.woxthebox.draglistview.DragItemAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 28-Dec-16.
 */
public class MyAdapter extends DragItemAdapter<MyItem, MyAdapter.ViewHolder> {
    private int mLayoutId; //layout file where we design our item row
    private int mGrabHandleId; //title/header view
    private boolean mDragOnLongPress;
    List<MyItem> myItemList; //our data source
    List<MyItem> myItemListCopy = new ArrayList<>();
    Context mcontext;
    FirebaseDatabase database;
    Calendar c;

    public MyAdapter() {
    }

    public MyAdapter(Context context1, List<MyItem> list, int mLayoutId, int GrabHandleId, boolean DragOnLongPress) {
        this.mLayoutId = mLayoutId;
        this.mGrabHandleId = GrabHandleId;
        this.mDragOnLongPress = DragOnLongPress;
        this.myItemList = list;
        mcontext = context1;
        this.myItemListCopy.addAll(myItemList);
        setHasStableIds(true);
        setItemList(list);
        database = FirebaseDatabase.getInstance();

    }
//    public MyAdapter(Context context1, Map<String,List<MyItem>>list, int mLayoutId, int GrabHandleId, boolean DragOnLongPress) {
//        this.mLayoutId = mLayoutId;
//        this.mGrabHandleId = GrabHandleId;
//        this.mDragOnLongPress = DragOnLongPress;
//        loop....
//        this.myItemList = list;
//        mcontext = context1;
//        this.myItemListCopy.addAll(myItemList);
//        setHasStableIds(true);
//        setItemList(list);
//        database = FirebaseDatabase.getInstance();
//
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        final MyItem data = mItemList.get(position);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        //holder.msg_text.setText(data.getData());
//        if(mItemList.get(position).getimage_attach()>0) {
//            Log.i("DRAGE", "text is in if:"+position+" "+ mItemList.get(position).getData());
//            Log.i("DRAGE", "image is in if:" + position + " " + mItemList.get(position).getimage_attach());
//            holder.msg_image_attach.setImageResource(data.getimage_attach());
//            holder.msg_image_attach.setVisibility(View.VISIBLE);
//        }else {
//            Log.i("DRAGE", "text is in else:"+position+" "+ mItemList.get(position).getData());
//            Log.i("DRAGE", "image is in else:" + position + " " + mItemList.get(position).getimage_attach());
//            holder.msg_image_attach.setImageResource(0);
//            holder.msg_image_attach.setVisibility(View.GONE);
//        }
        //holder.msg_image.setImageResource(R.drawable.cookie);
//        Log.i("TAG","item is in adapter:"+mItemList.get(position).getItemid());

        holder.card_title.setText(mItemList.get(position).getData());

        holder.itemView.setTag(mItemList.get(position).getItemid());

        if (mItemList.get(position).getdueDate() != null) {
            holder.duedate.setVisibility(View.VISIBLE);
            String duedate="";
            //check if only date is available then we must have to show only date.
            if(mItemList.get(position).getdueDate().get("isOnlyDate")!=null) {
                if ((Boolean) mItemList.get(position).getdueDate().get("isOnlyDate"))
                    duedate=getOnlyDateFromTimeStamp((Long) mItemList.get(position).getdueDate().get("duedate"));
                else
                    duedate = getDateFromTimeStamp((Long) mItemList.get(position).getdueDate().get("duedate"));
            }else {
                duedate = getDateFromTimeStamp((Long) mItemList.get(position).getdueDate().get("duedate"));
            }
            String[] due=duedate.split(",");
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM, hh:mma");
            String todayDate =sdf.format(new Date());
            //String todayTimestamp= String.valueOf(getDateTimeStamp(todayDate));
            String[] today=todayDate.split(",");
            c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_YEAR, 1);
            String tomDate= sdf.format(c.getTime());
            String[] tomoro=tomDate.split(",");
//            Log.i("TAG","date due is:"+duedate);
//            Log.i("TAG","date today is:"+todayDate);
//            Log.i("TAG","date tomorrow is:"+tomDate);


            holder.duedate.setText(duedate);
            if(mItemList.get(position).getdueDate().get("color").toString().length()>0)
                holder.duedate.setTextColor(Color.parseColor((String) mItemList.get(position).getdueDate().get("color")));
            else if(mItemList.get(position).getAuto_duedate()){
                if(System.currentTimeMillis()>(Long) mItemList.get(position).getdueDate().get("duedate")){
                    holder.duedate.setTextColor(ContextCompat.getColor(mcontext, R.color.red_500));
                }
                else if(due[0].equals(today[0])||due[0].equals(tomoro[0])) {
                    holder.duedate.setTextColor(ContextCompat.getColor(mcontext, R.color.green_500));
                    Log.i("TAG", "due date is equal to todays date");
                }else {
                    holder.duedate.setTextColor(Color.parseColor("#808080"));
                    Log.i("TAG", "due date is not equal to todays date");
                }

            }


        } else {
            holder.duedate.setVisibility(View.GONE);
        }
//        BitmapDrawable drawable = (BitmapDrawable) mcontext.getResources().getDrawable(R.drawable.profile);
//        if (position == 0)
//            drawable = (BitmapDrawable) mcontext.getResources().getDrawable(R.drawable.profile);
//        if (position == 1)
//            drawable = (BitmapDrawable) mcontext.getResources().getDrawable(R.drawable.profile1);
//        if (position == 2)
//            drawable = (BitmapDrawable) mcontext.getResources().getDrawable(R.drawable.profile2);
//        RoundedBitmapDrawable drawable_ = RoundedBitmapDrawableFactory.create(mcontext.getResources(), drawable.getBitmap());
//        drawable_.setCircular(true);
        ImageLoaderConfiguration config_ = new ImageLoaderConfiguration.Builder(mcontext).build();
        final com.nostra13.universalimageloader.core.ImageLoader loader_ = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        loader_.init(config_);
        final DisplayImageOptions options_ = new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .displayer(new CircleBitmapDisplayer(5)).showImageForEmptyUri(R.drawable.cp_user).build();
        if (mItemList.get(position).getAssign() != null && mItemList.get(position).getAssign().size() > 0) {
            Log.i("TAG","user image path is:pos"+position+" "+mItemList.get(position).getAssign());
            database.getReference().child("_user").child(mItemList.get(position).getAssign().get(0)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    HashMap<String, Object> map = (HashMap<String, Object>) dataSnapshot.getValue();
                    String image = map.get("image") != null ? (String) map.get("image") : "";
                    Log.i("TAG", "image path is:" + image);
                    loader_.displayImage(image, holder.msg_owner, options_);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            holder.msg_owner.setImageDrawable(null);
        }
        //holder.msg_owner.setImageDrawable(drawable_);

        if (data.getImageAttach() != null && data.getItemAttach() != null) {
            holder.attchicon.setVisibility(View.VISIBLE);
            int total_Attcah=(data.getImageAttach().size()+data.getItemAttach().size());
            holder.attchicon.setText(String.valueOf(total_Attcah));
            if (holder.imageItemContainer.getChildCount() > 0)
                holder.imageItemContainer.removeAllViews();
            holder.imageItemContainer.setVisibility(View.VISIBLE); //image item container
//            holder.attchimage.setVisibility(View.VISIBLE); //container_(cnt & imageview)
//            holder.attchitem.setVisibility(View.VISIBLE); //container_(cnt & imageview)
//            holder.image_attach.setVisibility(View.VISIBLE); //visible image view
//            holder.imageattach_cnt.setVisibility(View.VISIBLE); //visible images attach count textview
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext).build();
            final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
            loader.init(config);
            final DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true).resetViewBeforeLoading(true)
                    .cacheOnDisk(true)
                    .build();

            if (data.getItemAttach().size() <= 5) {
                for (int i = 0; i < data.getItemAttach().size(); i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_item_view, null, false);
                    final ImageView item_attach = (ImageView) row.findViewById(R.id.item_image2);
                    TextView item_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt2);
                    String businessid = (String) data.getItemAttach().get(i).get("businessId");
                    String itemid = (String) data.getItemAttach().get(i).get("itemId");
                    DatabaseReference itemRef = database.getReference("b_items").child(businessid).child(itemid);
                    itemRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            HashMap<String, Object> formdetail = (HashMap<String, Object>) dataSnapshot.getValue();
                            loader.displayImage(formdetail.get("image").toString(), item_attach, options);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    //loader.displayImage(data.getImageAttach().get(i), item_attach, options);
                    holder.imageItemContainer.addView(row);
                    item_cnt.setVisibility(View.GONE);
                }
            } else {
                for (int i = 0; i < 5; i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_item_view, null, false);
                    final ImageView item_attach = (ImageView) row.findViewById(R.id.item_image2);
                    TextView item_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt2);
                    String businessid = (String) data.getItemAttach().get(i).get("businessId");
                    String itemid = (String) data.getItemAttach().get(i).get("itemId");
                    DatabaseReference itemRef = database.getReference("b_items").child(businessid).child(itemid);
                    itemRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            HashMap<String, Object> formdetail = (HashMap<String, Object>) dataSnapshot.getValue();
                            loader.displayImage(formdetail.get("image").toString(), item_attach, options);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    holder.imageItemContainer.addView(row);
                    item_cnt.setVisibility(View.GONE);
                }
            }


            if (data.getImageAttach().size() <= 5) {
                for (int i = 0; i < data.getImageAttach().size(); i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_image_view, null, false);
                    ImageView image_attach = (ImageView) row.findViewById(R.id.item_image);
                    TextView img_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt);
                    if (data.getImageAttach().get(i).contains("png") | data.getImageAttach().get(i).contains("jpg") | data.getImageAttach().get(i).contains("jped")) {
                        loader.displayImage(data.getImageAttach().get(i), image_attach, options);
                    }
                    holder.imageItemContainer.addView(row);
                    img_cnt.setVisibility(View.GONE);
                }
            } else {
                for (int i = 0; i < 5; i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_image_view, null, false);
                    ImageView image_attach = (ImageView) row.findViewById(R.id.item_image);
                    TextView img_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt);
                    if (data.getImageAttach().get(i).contains("png") | data.getImageAttach().get(i).contains("jpg") | data.getImageAttach().get(i).contains("jped")) {
                        loader.displayImage(data.getImageAttach().get(i), image_attach, options);
                    }
                    holder.imageItemContainer.addView(row);
                    img_cnt.setVisibility(View.GONE);
                }
            }

            //holder.image_attach.setImageResource(data.getImageAttach().size());
            //holder.image_attach.setImageResource(R.drawable.design);
//            for(int i=0;i<data.getImageAttach().size();i++){
//                if(data.getImageAttach().get(i).contains("png")|data.getImageAttach().get(i).contains("jpg")|data.getImageAttach().get(i).contains("jped")){
//                    loader.displayImage(data.getImageAttach().get(i),holder.image_attach,options);
//                    break;
//                }
//            }

            //counter for image attach
//            if (String.valueOf(data.getImageAttach().size()).length() == 1) {
//                holder.imageattach_cnt.setText("0" + String.valueOf(data.getImageAttach().size()));
//            } else {
//                holder.imageattach_cnt.setText(String.valueOf(data.getImageAttach().size()));
//            }

//            holder.item_attach.setVisibility(View.VISIBLE); //visible item attached image view
//            holder.itemattach_cnt.setVisibility(View.VISIBLE); //visible item attach count text view

            //for

            //


            /*for item attach count*/
            /*if (String.valueOf(data.getItemAttach().size()).length() == 1) {
                holder.itemattach_cnt.setText("0" + String.valueOf(data.getItemAttach().size()));
            } else {
                holder.itemattach_cnt.setText(String.valueOf(data.getItemAttach().size()));
            }*/

        } else {
            holder.attchicon.setVisibility(View.VISIBLE);
            int total_Attch=0;
            if (holder.imageItemContainer.getChildCount() > 0)
                holder.imageItemContainer.removeAllViews();
            if (data.getImageAttach() != null) {
                holder.imageItemContainer.setVisibility(View.VISIBLE);
                total_Attch=data.getImageAttach().size();
//                holder.image_attach.setVisibility(View.VISIBLE);
//                holder.imageattach_cnt.setVisibility(View.VISIBLE);
//                holder.attchimage.setVisibility(View.VISIBLE);
//                holder.attchitem.setVisibility(View.GONE);
//                holder.item_attach.setVisibility(View.GONE);
//                holder.itemattach_cnt.setVisibility(View.GONE);

                //holder.image_attach.setImageResource(data.getImageAttach().get(0));
                //holder.image_attach.setImageResource(R.drawable.design);
                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext).build();
                final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
                loader.init(config);
                final DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .cacheInMemory(true).resetViewBeforeLoading(true)
                        .cacheOnDisk(true)
                        .build();
                /*//holder.image_attach.setImageResource(R.drawable.design);
                for (int i = 0; i < data.getImageAttach().size(); i++) {
                    if (data.getImageAttach().get(i).contains("png") | data.getImageAttach().get(i).contains("jpg") | data.getImageAttach().get(i).contains("jped")) {
                        loader.displayImage(data.getImageAttach().get(i), holder.image_attach, options);
                        break;
                    }
                }*/

                for (int i = 0; i < data.getImageAttach().size(); i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_image_view, null, false);
                    ImageView image_attach = (ImageView) row.findViewById(R.id.item_image);
                    TextView img_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt);
                    if (data.getImageAttach().get(i).contains("png") | data.getImageAttach().get(i).contains("jpg") | data.getImageAttach().get(i).contains("jped")) {
                        loader.displayImage(data.getImageAttach().get(i), image_attach, options);
                    }
                    holder.imageItemContainer.addView(row);
                    img_cnt.setVisibility(View.GONE);
                }


                //for image counter
//                if (String.valueOf(data.getImageAttach().size()).length() == 1) {
//                    holder.imageattach_cnt.setText("0" + String.valueOf(data.getImageAttach().size()));
//                } else {
//                    holder.imageattach_cnt.setText(String.valueOf(data.getImageAttach().size()));
//                }
            }
            if (data.getItemAttach() != null) {
                if (holder.imageItemContainer.getChildCount() > 0)
                    holder.imageItemContainer.removeAllViews();
                holder.imageItemContainer.setVisibility(View.VISIBLE);
                total_Attch=data.getItemAttach().size();
//                holder.item_attach.setVisibility(View.VISIBLE);
//                holder.itemattach_cnt.setVisibility(View.VISIBLE);
//                holder.attchimage.setVisibility(View.GONE);
//                holder.attchitem.setVisibility(View.VISIBLE);
//                holder.image_attach.setVisibility(View.GONE);
//                holder.imageattach_cnt.setVisibility(View.GONE);
                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext).build();
                final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
                loader.init(config);
                final DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .cacheInMemory(true).resetViewBeforeLoading(true)
                        .cacheOnDisk(true)
                        .build();

                for (int i = 0; i < data.getItemAttach().size(); i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_item_view, null, false);
                    final ImageView item_attach = (ImageView) row.findViewById(R.id.item_image2);
                    TextView item_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt2);
                    String businessid = (String) data.getItemAttach().get(i).get("businessId");
                    String itemid = (String) data.getItemAttach().get(i).get("itemId");
                    DatabaseReference itemRef = database.getReference("b_items").child(businessid).child(itemid);
                    itemRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            HashMap<String, Object> formdetail = (HashMap<String, Object>) dataSnapshot.getValue();
                            Log.i("TAG", "data" + formdetail);
                            loader.displayImage(formdetail.get("image").toString(), item_attach, options);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    //  loader.displayImage(data.getImageAttach().get(i), item_attach, options);
                    holder.imageItemContainer.addView(row);
                    item_cnt.setVisibility(View.GONE);
                }



                /*if (String.valueOf(data.getItemAttach().size()).length() == 1) {
                    holder.itemattach_cnt.setText("0" + String.valueOf(data.getItemAttach().size()));
                } else {
                    holder.itemattach_cnt.setText(String.valueOf(data.getItemAttach().size()));
                }*/
            }
            holder.attchicon.setText(String.valueOf(total_Attch));
            if (data.getItemAttach() == null && data.getImageAttach() == null) {
                holder.imageItemContainer.setVisibility(View.GONE);
                holder.attchicon.setVisibility(View.GONE);
            }
        }

        List<String> colorcodes = data.getColorcodes();
        if (holder.colorcode_parent.getChildCount() > 0)
            holder.colorcode_parent.removeAllViews();
        if (colorcodes != null) {
            for (int i = 0; i < colorcodes.size(); i++) {
                View view = new View(mcontext);
                LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(7, WindowManager.LayoutParams.MATCH_PARENT);
                viewparams.setMargins(0, 0, 8, 0);
                view.setLayoutParams(viewparams);
                Drawable mDrawable = ContextCompat.getDrawable(mcontext, R.drawable.viewpink_clr);
                mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(colorcodes.get(i)), PorterDuff.Mode.SRC_IN));
                view.setBackground(mDrawable);
                holder.colorcode_parent.addView(view);
            }
//                holder.Red.setVisibility(View.VISIBLE);
//            if (colorcodes.contains("yellow"))
//                holder.Yellow.setVisibility(View.VISIBLE);
//            if (colorcodes.contains("pink"))
//                holder.Pink.setVisibility(View.VISIBLE);
//            if (colorcodes.contains("green"))
//                holder.Sky.setVisibility(View.VISIBLE);
        }
        if (data.getCommentCnt() != 0L) {
            holder.card_commetcnt.setVisibility(View.VISIBLE);
            holder.card_commetcnt.setText(String.valueOf(data.getCommentCnt()));
        }else{
            holder.card_commetcnt.setVisibility(View.GONE);
        }
        if (data.getAssign()!= null&&data.getAssign().size()>0) {
            holder.person_cnt.setVisibility(View.VISIBLE);
            holder.person_cnt.setText(String.valueOf(data.getAssign().size()));
        }else{
            holder.person_cnt.setVisibility(View.GONE);
        }

    }

    @Override
    public long getItemId(int position) {
        //return super.getItemId(position);
        //return myItemList.get(position).getItemid();
        return mItemList.get(position).getItemid();
    }

    public class ViewHolder extends DragItemAdapter.ViewHolder {
        TextView card_title;
        //FrameLayout attchimage, attchitem;
//        ImageView image_attach;
//        ImageView item_attach;
//        TextView imageattach_cnt;
//        TextView itemattach_cnt;
        TextView duedate;
        ImageView msg_owner;
        TextView card_commetcnt;
        TextView person_cnt;
        LinearLayout imageItemContainer, colorcode_parent;
        TextView attchicon;

        //View Red, Yellow, Pink, Sky;

        public ViewHolder(View itemView) {
            super(itemView, mGrabHandleId, mDragOnLongPress);
            card_title = (TextView) itemView.findViewById(R.id.item_data);
//            image_attach = (ImageView) itemView.findViewById(R.id.item_image);
//            item_attach = (ImageView) itemView.findViewById(R.id.item_image2);
            duedate = (TextView) itemView.findViewById(R.id.tv_createdate);
            msg_owner = (ImageView) itemView.findViewById(R.id.user_profile);
            card_commetcnt = (TextView) itemView.findViewById(R.id.tv_totalmessage);
            person_cnt = (TextView) itemView.findViewById(R.id.tv_totalperson);
            attchicon=(TextView)itemView.findViewById(R.id.attach_icon);
//            imageattach_cnt = (TextView) itemView.findViewById(R.id.tv_totalimage_cnt);
//            itemattach_cnt = (TextView) itemView.findViewById(R.id.tv_totalimage_cnt2);
//            attchimage = (FrameLayout) itemView.findViewById(R.id.attach1);
//            attchitem = (FrameLayout) itemView.findViewById(R.id.attach_item2);
            colorcode_parent = (LinearLayout) itemView.findViewById(R.id.code_container);
//            Red = (View) itemView.findViewById(R.id.barRed);
//            Yellow = (View) itemView.findViewById(R.id.barYellow);
//            Pink = (View) itemView.findViewById(R.id.barPink);
//            Sky = (View) itemView.findViewById(R.id.barSky);
            imageItemContainer = (LinearLayout) itemView.findViewById(R.id.imagecontainer);
        }

        @Override
        public void onItemClicked(View view) {
//            Toast.makeText(view.getContext(), "Item clicked", Toast.LENGTH_SHORT).show();

//            ItemDetail newFragment = new ItemDetail();//not used in switchContent function
//            switchFragment(newFragment, mItemList.get(getAdapterPosition()), getAdapterPosition());
        }

//        //not in use now
//        public void switchFragment(Fragment newFragment, MyItem myItem, int row) {
//            if (mcontext == null)
//                return;
//            if (mcontext instanceof MainActivity) {
//                MainActivity feeds = (MainActivity) mcontext;
//                feeds.switchContent(newFragment, mItemList, myItem, row);
//            }
//            if (mcontext instanceof NewBoard) {
//                NewBoard feeds = (NewBoard) mcontext;
//                feeds.switchContent(newFragment, mItemList, myItem, row);
//            }
//        }

        @Override
        public boolean onItemLongClicked(View view) {
            //Toast.makeText(view.getContext(), "Sorry,You don't have permission to write, contact board admin", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    public void filter(String text) {
        Log.i("TAG","you have serch adapter:"+text);
        Log.i("TAG","myItemListCopy:"+myItemListCopy.size());
        mItemList.clear();
        if (text.isEmpty()) {
            mItemList.addAll(myItemListCopy);
        } else {
            text = text.toLowerCase();
            for (MyItem item : myItemListCopy) {
                Log.i("TAG","myItemListCopy has card:"+item.getData());

                if (item.getData().toLowerCase().contains(text)) {
                    Log.i("TAG","searched item added"+item.getData());
                    mItemList.add(item);
                }
            }
            for(MyItem main:mItemList){
                Log.i("TAG","mItemList has card:"+main.getData());
            }
        }
        notifyDataSetChanged();

    }

    public void setFreshDataInCopy(List<MyItem> myItemListCopyFresh) {
        myItemListCopy.clear();
        myItemListCopy.addAll(myItemListCopyFresh);
        Log.i("TAG", "added new item in copy list size is:" + myItemListCopy.size());
    }

    public void addNewItemInCopyList(MyItem item) {
        myItemListCopy.add(item);
        Log.i("TAG","added new item in copy list:"+item.getData()+"size is:"+myItemListCopy.size());
    }
    public void clearCopyList(){
        myItemListCopy.clear();
    }


    public void changeData(MyItem newitemdata, int position) {
        Log.i("TAG", "pos is in adapter" + position);
        //MyItem item = myItemList.get(position);
        Log.i("TAG", "pos item is:" + newitemdata);
        //item.setData(newitemdata.getData());
        mItemList.set(position, newitemdata);
        myItemList.set(position, newitemdata);
        myItemListCopy.set(position, newitemdata);
        notifyDataSetChanged();
    }

    public String getDateFromTimeStamp(Long date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM, hh:mma");
        java.util.Date resultdate = new java.util.Date(date * 1000);
        return sdf.format(resultdate);
    }
    public String getOnlyDateFromTimeStamp(Long date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM");
        java.util.Date resultdate = new java.util.Date(date * 1000);
        return sdf.format(resultdate);
    }
    public Long getDateTimeStamp(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM, hh:mm");
        java.util.Date d = null;
        try {
            d = formatter.parse(date);
            return d.getTime() / 1000;
//            java.sql.Timestamp timeStampDate = new Timestamp(d.getTime());
//            Log.i("TAG","date using sql in timestamp:"+timeStampDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return 0L;
        }
    }
}
