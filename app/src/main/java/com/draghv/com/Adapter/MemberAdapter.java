package com.draghv.com.Adapter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.draghv.com.Models.MemberPOJO;
import com.draghv.com.R;
import com.nostra13.universalimageloader.core.*;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;

import java.util.List;

/**
 * Created by Payal on 09-Feb-17.
 */
public class MemberAdapter extends BaseAdapter {
    List<MemberPOJO> datalist;
    Context mcontext;

    public MemberAdapter(List<MemberPOJO> datalist, Context mcontext) {
        this.datalist = datalist;
        this.mcontext = mcontext;
    }

    @Override
    public int getCount() {
        if(datalist!=null&&datalist.size()>0)
            return datalist.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {
        return datalist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            LayoutInflater infla = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infla.inflate(R.layout.frag_member_row, parent, false);
            holder = new ViewHolder();
            holder.iv_profile=(ImageView)convertView.findViewById(R.id.user_avatar);
            holder.tv_username=(TextView)convertView.findViewById(R.id.user_fullname);
            holder.chkmember=(ImageView)convertView.findViewById(R.id.isMember);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        final MemberPOJO data=datalist.get(position);
        holder.tv_username.setText(data.getUsername());
        if(data.getIsMember())
            holder.chkmember.setVisibility(View.VISIBLE);
        else
            holder.chkmember.setVisibility(View.GONE);

        ImageLoaderConfiguration config=new ImageLoaderConfiguration.Builder(mcontext).build();
        final com.nostra13.universalimageloader.core.ImageLoader loader= com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        loader.init(config);
        final DisplayImageOptions options=new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .displayer(new CircleBitmapDisplayer(5)).showImageForEmptyUri(R.drawable.cp_user).build();
        if(data.getUserprofile()!=null&&data.getUserprofile().length()>0)
            loader.displayImage(data.getUserprofile(), holder.iv_profile, options);
        else{
            BitmapDrawable default_drawable = (BitmapDrawable) mcontext.getResources().getDrawable(R.drawable.cp_user);
            RoundedBitmapDrawable drawable_ = RoundedBitmapDrawableFactory.create(mcontext.getResources(), default_drawable.getBitmap());
            drawable_.setCircular(true);
            holder.iv_profile.setImageDrawable(drawable_);
        }

        return convertView;
    }

    public class ViewHolder{
        ImageView iv_profile;
        TextView tv_username;
        ImageView chkmember;
    }
}
