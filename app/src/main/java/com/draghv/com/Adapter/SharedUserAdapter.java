package com.draghv.com.Adapter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.draghv.com.MainActivity;
import com.draghv.com.Models.SharedBoardUserPOJO;
import com.draghv.com.R;
import com.draghv.com.Zboard;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;

import java.util.List;

/**
 * Created by Payal on 17-Mar-17.
 */
public class SharedUserAdapter extends BaseAdapter {
    Context context;
    List<SharedBoardUserPOJO> shareduserlistl;
    Zboard zboard;

    public SharedUserAdapter(Context context, List<SharedBoardUserPOJO> shareduserlistl) {
        this.context = context;
        this.shareduserlistl = shareduserlistl;
        zboard=new Zboard();
    }

    @Override
    public int getCount() {
        if(shareduserlistl!=null&&shareduserlistl.size()>0)
            return shareduserlistl.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {
        return shareduserlistl.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Viewholder holder;
        if(convertView==null){
            LayoutInflater infla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infla.inflate(R.layout.shared_user_row, parent, false);
            holder = new Viewholder();
            holder.image=(ImageView)convertView.findViewById(R.id.user_avatar);
            holder.name=(TextView)convertView.findViewById(R.id.user_fullname);
            holder.read=(CheckBox)convertView.findViewById(R.id.parm_read);
            holder.write=(CheckBox)convertView.findViewById(R.id.parm_write);
            convertView.setTag(holder);
        }else{
            holder=(Viewholder)convertView.getTag();
        }
        final SharedBoardUserPOJO data=shareduserlistl.get(position);
        holder.name.setText(data.getUsername());
        holder.read.setChecked(data.getRead());
        holder.write.setChecked(data.getWrite());

        ImageLoaderConfiguration config=new ImageLoaderConfiguration.Builder(context).build();
        final com.nostra13.universalimageloader.core.ImageLoader loader= com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        loader.init(config);
        final DisplayImageOptions options=new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .displayer(new CircleBitmapDisplayer(5)).showImageForEmptyUri(R.drawable.cp_user).build();
        if(data.getUserprofile()!=null&&data.getUserprofile().length()>0)
            loader.displayImage(data.getUserprofile(), holder.image, options);
        else{
            BitmapDrawable default_drawable = (BitmapDrawable) context.getResources().getDrawable(R.drawable.cp_user);
            RoundedBitmapDrawable drawable_ = RoundedBitmapDrawableFactory.create(context.getResources(), default_drawable.getBitmap());
            drawable_.setCircular(true);
            holder.image.setImageDrawable(drawable_);
        }

        holder.read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.read.isChecked()) {
                    holder.write.setEnabled(false);
                    holder.write.setChecked(false);
                    zboard.updateWritePermission(MainActivity.BOARD_ID, MainActivity.ownerId, data.getUserid(), false, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            Toast.makeText(context, "write->read Permission updated", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    holder.write.setEnabled(true);
                }
                zboard.updateReadPermission(MainActivity.BOARD_ID, MainActivity.ownerId, data.getUserid(), holder.read.isChecked(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        Toast.makeText(context, "read Permission updated", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        holder.write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.setWrite(holder.write.isChecked());

                zboard.updateWritePermission(MainActivity.BOARD_ID, MainActivity.ownerId, data.getUserid(), holder.write.isChecked(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        Toast.makeText(context, "write only Permission updated", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
        return convertView;
    }

    private class Viewholder{
        TextView name;
        ImageView image;
        CheckBox read;
        CheckBox write;
    }
}
