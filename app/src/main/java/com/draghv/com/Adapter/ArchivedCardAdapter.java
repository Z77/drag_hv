package com.draghv.com.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.draghv.com.ActivityArchiveCard;
import com.draghv.com.MainActivity;
import com.draghv.com.Models.MyItem;
import com.draghv.com.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 16-Feb-17.
 */
public class ArchivedCardAdapter extends RecyclerView.Adapter<ArchivedCardAdapter.ViewHolder> {
    List<MyItem> dataList;
    Context mcontext;
    FirebaseDatabase database;
    private SparseBooleanArray mSelectedItemsIds;
    List<MyItem> copydatalist = new ArrayList<>();
    List<HashMap<String, Object>> columnData;
    Boolean is_write;
    Calendar c;

    public ArchivedCardAdapter(List<MyItem> datalist, Context mcontext, List<HashMap<String, Object>> columnData, Boolean iswrite) {
        this.dataList = datalist;
        this.mcontext = mcontext;
        database = FirebaseDatabase.getInstance();
        this.copydatalist.addAll(dataList);
        mSelectedItemsIds = new SparseBooleanArray();
        this.columnData = columnData;
        is_write = iswrite;
        c = Calendar.getInstance();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(R.layout.activity_archive_card_row, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (!is_write)
            holder.btn_unarchive.setVisibility(View.GONE);

        holder.btn_unarchive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityArchiveCard) mcontext).unarchive(position);
            }
        });
        MyItem item = dataList.get(position);
        //set title
        if (item.getData() != null)
            holder.card_title.setText(item.getData());
        //set duedate
        if (item.getdueDate() != null) {
            holder.duedate.setVisibility(View.VISIBLE);
            String duedate = "";
            //check if only date is available then we must have to show only date.
            if (item.getdueDate().get("isOnlyDate") != null) {
                if ((Boolean) item.getdueDate().get("isOnlyDate"))
                    duedate = getOnlyDateFromTimeStamp((Long) item.getdueDate().get("duedate"));
                else
                    duedate = getDateFromTimeStamp((Long) item.getdueDate().get("duedate"));
            } else {
                duedate = getDateFromTimeStamp((Long) item.getdueDate().get("duedate"));
            }
            String[] due = duedate.split(",");
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM, hh:mma");
            String todayDate = sdf.format(new Date());
            // String todayTimestamp= String.valueOf(getDateTimeStamp(todayDate));
            String[] today = todayDate.split(",");
            c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_YEAR, 1);
            String tomDate = sdf.format(c.getTime());
            String[] tomoro = tomDate.split(",");
//            Log.i("TAG","date due is:"+duedate);
//            Log.i("TAG","date today is:"+todayDate);
//            Log.i("TAG","date tomorrow is:"+tomDate);


            holder.duedate.setText(duedate);
            if (item.getdueDate().get("color").toString().length() > 0)
                holder.duedate.setTextColor(Color.parseColor((String) item.getdueDate().get("color")));
            else if (item.getAuto_duedate()) {
                if (System.currentTimeMillis() > (Long) item.getdueDate().get("duedate")) {
                    holder.duedate.setTextColor(ContextCompat.getColor(mcontext, R.color.red_500));
                } else if (due[0].equals(today[0]) || due[0].equals(tomoro[0])) {
                    holder.duedate.setTextColor(ContextCompat.getColor(mcontext, R.color.green_500));
                    Log.i("TAG", "due date is equal to todays date");
                } else {
                    holder.duedate.setTextColor(Color.parseColor("#808080"));
                    Log.i("TAG", "due date is not equal to todays date");
                }

            }
        } else {
            holder.duedate.setVisibility(View.GONE);
        }

        String columnname = "Default";
        for (int i = 0; i < columnData.size(); i++) {
            if (columnData.get(i).get("id").equals(item.getColumnid())) {
                columnname = columnData.get(i).get("title").toString();
                break;
            }
        }
        String newcolname = "<b> From:</b>" + columnname;
        holder.from_colname.setText(Html.fromHtml(newcolname));

        //set assign person image
        ImageLoaderConfiguration config_ = new ImageLoaderConfiguration.Builder(mcontext).build();
        final com.nostra13.universalimageloader.core.ImageLoader loader_ = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        loader_.init(config_);
        final DisplayImageOptions options_ = new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .displayer(new CircleBitmapDisplayer(5)).showImageForEmptyUri(R.drawable.cp_user).build();
        if (item.getAssign() != null && item.getAssign().size() > 0) {
            database.getReference().child("_user").child(item.getAssign().get(0)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    HashMap<String, Object> map = (HashMap<String, Object>) dataSnapshot.getValue();
                    String image = map.get("image") != null ? (String) map.get("image") : "";
                    loader_.displayImage(image, holder.msg_owner, options_);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            holder.msg_owner.setImageDrawable(null);
        }


        //set item,image attachment
        if (item.getImageAttach() != null && item.getItemAttach() != null) {
            //chek if both available
            if (holder.imageItemContainer.getChildCount() > 0)
                holder.imageItemContainer.removeAllViews();

            holder.imageItemContainer.setVisibility(View.VISIBLE); //image item container
            holder.attch_icon.setVisibility(View.VISIBLE);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext).build();
            final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
            loader.init(config);
            final DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true).resetViewBeforeLoading(true)
                    .cacheOnDisk(true)
                    .build();

            if (item.getItemAttach().size() <= 5) {
                for (int i = 0; i < item.getItemAttach().size(); i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_item_view, null, false);
                    final ImageView item_attach = (ImageView) row.findViewById(R.id.item_image2);
                    TextView item_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt2);
                    String businessid = (String) item.getItemAttach().get(i).get("businessId");
                    String itemid = (String) item.getItemAttach().get(i).get("itemId");
                    DatabaseReference itemRef = database.getReference("b_items").child(businessid).child(itemid);
                    itemRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            HashMap<String, Object> formdetail = (HashMap<String, Object>) dataSnapshot.getValue();
                            loader.displayImage(formdetail.get("image").toString(), item_attach, options);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    holder.imageItemContainer.addView(row);
                    item_cnt.setVisibility(View.GONE);
                }
            } else {
                for (int i = 0; i < 5; i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_item_view, null, false);
                    final ImageView item_attach = (ImageView) row.findViewById(R.id.item_image2);
                    TextView item_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt2);
                    String businessid = (String) item.getItemAttach().get(i).get("businessId");
                    String itemid = (String) item.getItemAttach().get(i).get("itemId");
                    DatabaseReference itemRef = database.getReference("b_items").child(businessid).child(itemid);
                    itemRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            HashMap<String, Object> formdetail = (HashMap<String, Object>) dataSnapshot.getValue();
                            loader.displayImage(formdetail.get("image").toString(), item_attach, options);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    holder.imageItemContainer.addView(row);
                    item_cnt.setVisibility(View.GONE);
                }
            }

            if (item.getImageAttach().size() <= 5) {
                for (int i = 0; i < item.getImageAttach().size(); i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_image_view, null, false);
                    ImageView image_attach = (ImageView) row.findViewById(R.id.item_image);
                    TextView img_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt);
                    if (item.getImageAttach().get(i).contains("png") | item.getImageAttach().get(i).contains("jpg") | item.getImageAttach().get(i).contains("jped")) {
                        loader.displayImage(item.getImageAttach().get(i), image_attach, options);
                    }
                    holder.imageItemContainer.addView(row);
                    img_cnt.setVisibility(View.GONE);
                }
            } else {
                for (int i = 0; i < 5; i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_image_view, null, false);
                    ImageView image_attach = (ImageView) row.findViewById(R.id.item_image);
                    TextView img_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt);
                    if (item.getImageAttach().get(i).contains("png") | item.getImageAttach().get(i).contains("jpg") | item.getImageAttach().get(i).contains("jped")) {
                        loader.displayImage(item.getImageAttach().get(i), image_attach, options);
                    }
                    holder.imageItemContainer.addView(row);
                    img_cnt.setVisibility(View.GONE);
                }
            }

            //counter for image attach
//            if (String.valueOf(item.getImageAttach().size()).length() == 1) {
//                holder.imageattach_cnt.setText("0" + String.valueOf(item.getImageAttach().size()));
//            } else {
//                holder.imageattach_cnt.setText(String.valueOf(item.getImageAttach().size()));
//            }

            /*for item attach count*/
            /*if (String.valueOf(item.getItemAttach().size()).length() == 1) {
                holder.itemattach_cnt.setText("0" + String.valueOf(item.getItemAttach().size()));
            } else {
                holder.itemattach_cnt.setText(String.valueOf(item.getItemAttach().size()));
            }*/

        } else {

            //else only image available or only item available or none of them
            if (holder.imageItemContainer.getChildCount() > 0)
                holder.imageItemContainer.removeAllViews();

            if (item.getImageAttach() != null) {
                holder.imageItemContainer.setVisibility(View.VISIBLE);
                holder.attch_icon.setVisibility(View.VISIBLE);
                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext).build();
                final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
                loader.init(config);
                final DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .cacheInMemory(true).resetViewBeforeLoading(true)
                        .cacheOnDisk(true)
                        .build();

                for (int i = 0; i < item.getImageAttach().size(); i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_image_view, null, false);
                    ImageView image_attach = (ImageView) row.findViewById(R.id.item_image);
                    TextView img_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt);
                    if (item.getImageAttach().get(i).contains("png") | item.getImageAttach().get(i).contains("jpg") | item.getImageAttach().get(i).contains("jped")) {
                        loader.displayImage(item.getImageAttach().get(i), image_attach, options);
                    }
                    holder.imageItemContainer.addView(row);
                    img_cnt.setVisibility(View.GONE);
                }


                //for image counter
//                if (String.valueOf(item.getImageAttach().size()).length() == 1) {
//                    holder.imageattach_cnt.setText("0" + String.valueOf(item.getImageAttach().size()));
//                } else {
//                    holder.imageattach_cnt.setText(String.valueOf(item.getImageAttach().size()));
//                }
            }
            if (item.getItemAttach() != null) {
                if (holder.imageItemContainer.getChildCount() > 0)
                    holder.imageItemContainer.removeAllViews();

                holder.imageItemContainer.setVisibility(View.VISIBLE);
                holder.attch_icon.setVisibility(View.VISIBLE);
                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext).build();
                final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
                loader.init(config);
                final DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .cacheInMemory(true).resetViewBeforeLoading(true)
                        .cacheOnDisk(true)
                        .build();

                for (int i = 0; i < item.getItemAttach().size(); i++) {
                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_item_view, null, false);
                    final ImageView item_attach = (ImageView) row.findViewById(R.id.item_image2);
                    TextView item_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt2);
                    String businessid = (String) item.getItemAttach().get(i).get("businessId");
                    String itemid = (String) item.getItemAttach().get(i).get("itemId");
                    DatabaseReference itemRef = database.getReference("b_items").child(businessid).child(itemid);
                    itemRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            HashMap<String, Object> formdetail = (HashMap<String, Object>) dataSnapshot.getValue();
                            loader.displayImage(formdetail.get("image").toString(), item_attach, options);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    holder.imageItemContainer.addView(row);
                    item_cnt.setVisibility(View.GONE);
                }
                /*if (String.valueOf(item.getItemAttach().size()).length() == 1) {
                    holder.itemattach_cnt.setText("0" + String.valueOf(item.getItemAttach().size()));
                } else {
                    holder.itemattach_cnt.setText(String.valueOf(item.getItemAttach().size()));
                }*/
            }
            if (item.getItemAttach() == null && item.getImageAttach() == null) {
                holder.imageItemContainer.setVisibility(View.GONE);
                holder.attch_icon.setVisibility(View.GONE);
            }
        }

        //color lable
        List<String> colorcodes = item.getColorcodes();
        if (holder.colorcode_parent.getChildCount() > 0)
            holder.colorcode_parent.removeAllViews();
        if (colorcodes != null) {
            for (int i = 0; i < colorcodes.size(); i++) {
                View view = new View(mcontext);
                LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(4, WindowManager.LayoutParams.MATCH_PARENT);
                viewparams.setMargins(0, 0, 5, 0);
                view.setLayoutParams(viewparams);
                Drawable mDrawable = ContextCompat.getDrawable(mcontext, R.drawable.viewpink_clr);
                mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(colorcodes.get(i)), PorterDuff.Mode.SRC_IN));
                view.setBackground(mDrawable);
                holder.colorcode_parent.addView(view);
            }
        }
        //comment count
        if (item.getCommentCnt() != 0L) {
            holder.card_commetcnt.setVisibility(View.VISIBLE);
            holder.card_commetcnt.setText(String.valueOf(item.getCommentCnt()));
        }else{
            holder.card_commetcnt.setVisibility(View.GONE);
        }
        //share person count
        if (item.getAssign() != null&&item.getAssign().size()>0) {
            holder.person_cnt.setVisibility(View.VISIBLE);
            holder.person_cnt.setText(String.valueOf(item.getAssign().size()));
        }else{
            holder.person_cnt.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        if (dataList != null && dataList.size() > 0)
            return dataList.size();
        else
            return 0;
    }

//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//        if (convertView == null) {
//            LayoutInflater infla = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = infla.inflate(R.layout.activity_archive_card_row, parent, false);
//            holder = new ViewHolder();
//            holder.card_title = (TextView) convertView.findViewById(R.id.item_data);
//            holder.duedate = (TextView) convertView.findViewById(R.id.tv_createdate);
//            holder.msg_owner = (ImageView) convertView.findViewById(R.id.user_profile);
//            holder.card_commetcnt = (TextView) convertView.findViewById(R.id.tv_totalmessage);
//            holder.person_cnt = (TextView) convertView.findViewById(R.id.tv_totalperson);
//            holder.colorcode_parent = (LinearLayout) convertView.findViewById(R.id.code_container);
//            holder.imageItemContainer = (LinearLayout) convertView.findViewById(R.id.imagecontainer);
//            holder.btn_unarchive=(ImageView)convertView.findViewById(R.id.btn_unrchive);
//            holder.from_colname=(TextView)convertView.findViewById(R.id.tv_column_name);
//            holder.attch_icon=(TextView)convertView.findViewById(R.id.attach_icon);
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//        if(!is_write)
//            holder.btn_unarchive.setVisibility(View.GONE);
//        holder.btn_unarchive.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((ActivityArchiveCard)mcontext).unarchive(position);
//            }
//        });
//        MyItem item = dataList.get(position);
//        //set title
//        if(item.getData()!=null)
//            holder.card_title.setText(item.getData());
//        //set duedate
////        if (item.getdueDate() != null) {
////            holder.duedate.setVisibility(View.VISIBLE);
////            holder.duedate.setText(getDateFromTimeStamp((Long) item.getdueDate().get("duedate")));
////        } else {
////            holder.duedate.setVisibility(View.GONE);
////        }
//        if (item.getdueDate() != null) {
//            holder.duedate.setVisibility(View.VISIBLE);
//            String duedate="";
//            //check if only date is available then we must have to show only date.
//            if(item.getdueDate().get("isOnlyDate")!=null) {
//                if ((Boolean) item.getdueDate().get("isOnlyDate"))
//                    duedate=getOnlyDateFromTimeStamp((Long) item.getdueDate().get("duedate"));
//                else
//                    duedate = getDateFromTimeStamp((Long) item.getdueDate().get("duedate"));
//            }else {
//                duedate = getDateFromTimeStamp((Long) item.getdueDate().get("duedate"));
//            }
//            String[] due=duedate.split(",");
//            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM, hh:mma");
//            String todayDate =sdf.format(new Date());
//           // String todayTimestamp= String.valueOf(getDateTimeStamp(todayDate));
//            String[] today=todayDate.split(",");
//            c = Calendar.getInstance();
//            c.add(Calendar.DAY_OF_YEAR, 1);
//            String tomDate= sdf.format(c.getTime());
//            String[] tomoro=tomDate.split(",");
////            Log.i("TAG","date due is:"+duedate);
////            Log.i("TAG","date today is:"+todayDate);
////            Log.i("TAG","date tomorrow is:"+tomDate);
//
//
//            holder.duedate.setText(duedate);
//            if(item.getdueDate().get("color").toString().length()>0)
//                holder.duedate.setTextColor(Color.parseColor((String) item.getdueDate().get("color")));
//            else if(item.getAuto_duedate()){
//                if(System.currentTimeMillis()>(Long) item.getdueDate().get("duedate")){
//                    holder.duedate.setTextColor(ContextCompat.getColor(mcontext, R.color.red_500));
//                }
//                else if(due[0].equals(today[0])||due[0].equals(tomoro[0])) {
//                    holder.duedate.setTextColor(ContextCompat.getColor(mcontext, R.color.green_500));
//                    Log.i("TAG", "due date is equal to todays date");
//                }else {
//                    holder.duedate.setTextColor(Color.parseColor("#808080"));
//                    Log.i("TAG", "due date is not equal to todays date");
//                }
//
//            }
//
//
//        } else {
//            holder.duedate.setVisibility(View.GONE);
//        }
//        String columnname="Default";
//        for(int i=0;i<columnData.size();i++){
//            if(columnData.get(i).get("id").equals(item.getColumnid())) {
//                columnname = columnData.get(i).get("title").toString();
//                break;
//            }
//        }
//        String newcolname="<b> From:</b>"+columnname;
//        holder.from_colname.setText(Html.fromHtml(newcolname));
//        //set assign person image
//        ImageLoaderConfiguration config_ = new ImageLoaderConfiguration.Builder(mcontext).build();
//        final com.nostra13.universalimageloader.core.ImageLoader loader_ = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
//        loader_.init(config_);
//        final DisplayImageOptions options_ = new DisplayImageOptions.Builder()
//                .cacheInMemory(true).resetViewBeforeLoading(true)
//                .cacheOnDisk(true)
//                .displayer(new CircleBitmapDisplayer(5)).showImageForEmptyUri(R.drawable.cp_user).build();
//        if (item.getAssign() != null && item.getAssign().size() > 0) {
//            database.getReference().child("_user").child(item.getAssign().get(0)).addValueEventListener(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    HashMap<String, Object> map = (HashMap<String, Object>) dataSnapshot.getValue();
//                    String image = map.get("image") != null ? (String) map.get("image") : "";
//                    loader_.displayImage(image, holder.msg_owner, options_);
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });
//        } else {
//            holder.msg_owner.setImageDrawable(null);
//        }
//        //set item,image attachment
//        if (item.getImageAttach() != null && item.getItemAttach() != null) {
//            //chek if both available
//            if (holder.imageItemContainer.getChildCount() > 0)
//                holder.imageItemContainer.removeAllViews();
//
//            holder.imageItemContainer.setVisibility(View.VISIBLE); //image item container
//            holder.attch_icon.setVisibility(View.VISIBLE);
//            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext).build();
//            final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
//            loader.init(config);
//            final DisplayImageOptions options = new DisplayImageOptions.Builder()
//                    .cacheInMemory(true).resetViewBeforeLoading(true)
//                    .cacheOnDisk(true)
//                    .build();
//
//            if (item.getItemAttach().size() <= 5) {
//                for (int i = 0; i < item.getItemAttach().size(); i++) {
//                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_item_view, null, false);
//                    final ImageView item_attach = (ImageView) row.findViewById(R.id.item_image2);
//                    TextView item_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt2);
//                    String businessid = (String) item.getItemAttach().get(i).get("businessId");
//                    String itemid = (String) item.getItemAttach().get(i).get("itemId");
//                    DatabaseReference itemRef = database.getReference("b_items").child(businessid).child(itemid);
//                    itemRef.addValueEventListener(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            HashMap<String, Object> formdetail = (HashMap<String, Object>) dataSnapshot.getValue();
//                            loader.displayImage(formdetail.get("image").toString(), item_attach, options);
//                        }
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//
//                        }
//                    });
//                    holder.imageItemContainer.addView(row);
//                    item_cnt.setVisibility(View.GONE);
//                }
//            } else {
//                for (int i = 0; i < 5; i++) {
//                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_item_view, null, false);
//                    final ImageView item_attach = (ImageView) row.findViewById(R.id.item_image2);
//                    TextView item_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt2);
//                    String businessid = (String) item.getItemAttach().get(i).get("businessId");
//                    String itemid = (String) item.getItemAttach().get(i).get("itemId");
//                    DatabaseReference itemRef = database.getReference("b_items").child(businessid).child(itemid);
//                    itemRef.addValueEventListener(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            HashMap<String, Object> formdetail = (HashMap<String, Object>) dataSnapshot.getValue();
//                            loader.displayImage(formdetail.get("image").toString(), item_attach, options);
//                        }
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//
//                        }
//                    });
//                    holder.imageItemContainer.addView(row);
//                    item_cnt.setVisibility(View.GONE);
//                }
//            }
//
//            if (item.getImageAttach().size() <= 5) {
//                for (int i = 0; i < item.getImageAttach().size(); i++) {
//                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_image_view, null, false);
//                    ImageView image_attach = (ImageView) row.findViewById(R.id.item_image);
//                    TextView img_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt);
//                    if (item.getImageAttach().get(i).contains("png") | item.getImageAttach().get(i).contains("jpg") | item.getImageAttach().get(i).contains("jped")) {
//                        loader.displayImage(item.getImageAttach().get(i), image_attach, options);
//                    }
//                    holder.imageItemContainer.addView(row);
//                    img_cnt.setVisibility(View.GONE);
//                }
//            } else {
//                for (int i = 0; i < 5; i++) {
//                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_image_view, null, false);
//                    ImageView image_attach = (ImageView) row.findViewById(R.id.item_image);
//                    TextView img_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt);
//                    if (item.getImageAttach().get(i).contains("png") | item.getImageAttach().get(i).contains("jpg") | item.getImageAttach().get(i).contains("jped")) {
//                        loader.displayImage(item.getImageAttach().get(i), image_attach, options);
//                    }
//                    holder.imageItemContainer.addView(row);
//                    img_cnt.setVisibility(View.GONE);
//                }
//            }
//
//            //counter for image attach
////            if (String.valueOf(item.getImageAttach().size()).length() == 1) {
////                holder.imageattach_cnt.setText("0" + String.valueOf(item.getImageAttach().size()));
////            } else {
////                holder.imageattach_cnt.setText(String.valueOf(item.getImageAttach().size()));
////            }
//
//            /*for item attach count*/
//            /*if (String.valueOf(item.getItemAttach().size()).length() == 1) {
//                holder.itemattach_cnt.setText("0" + String.valueOf(item.getItemAttach().size()));
//            } else {
//                holder.itemattach_cnt.setText(String.valueOf(item.getItemAttach().size()));
//            }*/
//
//        } else {
//
//            //else only image available or only item available or none of them
//            if (holder.imageItemContainer.getChildCount() > 0)
//                holder.imageItemContainer.removeAllViews();
//
//            if (item.getImageAttach() != null) {
//                holder.imageItemContainer.setVisibility(View.VISIBLE);
//                holder.attch_icon.setVisibility(View.VISIBLE);
//                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext).build();
//                final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
//                loader.init(config);
//                final DisplayImageOptions options = new DisplayImageOptions.Builder()
//                        .cacheInMemory(true).resetViewBeforeLoading(true)
//                        .cacheOnDisk(true)
//                        .build();
//
//                for (int i = 0; i < item.getImageAttach().size(); i++) {
//                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_image_view, null, false);
//                    ImageView image_attach = (ImageView) row.findViewById(R.id.item_image);
//                    TextView img_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt);
//                    if (item.getImageAttach().get(i).contains("png") | item.getImageAttach().get(i).contains("jpg") | item.getImageAttach().get(i).contains("jped")) {
//                        loader.displayImage(item.getImageAttach().get(i), image_attach, options);
//                    }
//                    holder.imageItemContainer.addView(row);
//                    img_cnt.setVisibility(View.GONE);
//                }
//
//
//                //for image counter
////                if (String.valueOf(item.getImageAttach().size()).length() == 1) {
////                    holder.imageattach_cnt.setText("0" + String.valueOf(item.getImageAttach().size()));
////                } else {
////                    holder.imageattach_cnt.setText(String.valueOf(item.getImageAttach().size()));
////                }
//            }
//            if (item.getItemAttach() != null) {
//                if (holder.imageItemContainer.getChildCount() > 0)
//                    holder.imageItemContainer.removeAllViews();
//
//                holder.imageItemContainer.setVisibility(View.VISIBLE);
//                holder.attch_icon.setVisibility(View.VISIBLE);
//                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext).build();
//                final com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
//                loader.init(config);
//                final DisplayImageOptions options = new DisplayImageOptions.Builder()
//                        .cacheInMemory(true).resetViewBeforeLoading(true)
//                        .cacheOnDisk(true)
//                        .build();
//
//                for (int i = 0; i < item.getItemAttach().size(); i++) {
//                    View row = LayoutInflater.from(mcontext).inflate(R.layout.attach_item_view, null, false);
//                    final ImageView item_attach = (ImageView) row.findViewById(R.id.item_image2);
//                    TextView item_cnt = (TextView) row.findViewById(R.id.tv_totalimage_cnt2);
//                    String businessid = (String) item.getItemAttach().get(i).get("businessId");
//                    String itemid = (String) item.getItemAttach().get(i).get("itemId");
//                    DatabaseReference itemRef = database.getReference("b_items").child(businessid).child(itemid);
//                    itemRef.addValueEventListener(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            HashMap<String, Object> formdetail = (HashMap<String, Object>) dataSnapshot.getValue();
//                            loader.displayImage(formdetail.get("image").toString(), item_attach, options);
//                        }
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//
//                        }
//                    });
//                    holder.imageItemContainer.addView(row);
//                    item_cnt.setVisibility(View.GONE);
//                }
//                /*if (String.valueOf(item.getItemAttach().size()).length() == 1) {
//                    holder.itemattach_cnt.setText("0" + String.valueOf(item.getItemAttach().size()));
//                } else {
//                    holder.itemattach_cnt.setText(String.valueOf(item.getItemAttach().size()));
//                }*/
//            }
//            if (item.getItemAttach() == null && item.getImageAttach() == null) {
//                holder.imageItemContainer.setVisibility(View.GONE);
//                holder.attch_icon.setVisibility(View.GONE);
//            }
//        }
//        //color lable
//        List<String> colorcodes = item.getColorcodes();
//        if (holder.colorcode_parent.getChildCount() > 0)
//            holder.colorcode_parent.removeAllViews();
//        if (colorcodes != null) {
//            for (int i = 0; i < colorcodes.size(); i++) {
//                View view = new View(mcontext);
//                LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(4, WindowManager.LayoutParams.MATCH_PARENT);
//                viewparams.setMargins(0, 0, 5, 0);
//                view.setLayoutParams(viewparams);
//                Drawable mDrawable = ContextCompat.getDrawable(mcontext, R.drawable.viewpink_clr);
//                mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(colorcodes.get(i)), PorterDuff.Mode.SRC_IN));
//                view.setBackground(mDrawable);
//                holder.colorcode_parent.addView(view);
//            }
//        }
//        //comment count
//        if (item.getCommentCnt() != 0L) {
//            holder.card_commetcnt.setVisibility(View.VISIBLE);
//            holder.card_commetcnt.setText(String.valueOf(item.getCommentCnt()));
//        }else{
//            holder.card_commetcnt.setVisibility(View.GONE);
//        }
//        //share person count
//        if (item.getSharePersoncnt() != 0) {
//            holder.person_cnt.setVisibility(View.VISIBLE);
//            holder.person_cnt.setText(String.valueOf(item.getSharePersoncnt()));
//        }else{
//            holder.person_cnt.setVisibility(View.GONE);
//        }
//
//        return convertView;
//    }
//

//
//    @Override
//    public Object getItem(int position) {
//        return dataList.get(position);
//    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView card_title;
        TextView duedate;
        ImageView msg_owner;
        TextView card_commetcnt;
        TextView person_cnt;
        LinearLayout imageItemContainer, colorcode_parent;
        ImageView btn_unarchive;
        TextView from_colname;
        TextView attch_icon;

        public ViewHolder(View itemView) {
            super(itemView);
            card_title = (TextView) itemView.findViewById(R.id.item_data);
            duedate = (TextView) itemView.findViewById(R.id.tv_createdate);
            msg_owner = (ImageView) itemView.findViewById(R.id.user_profile);
            card_commetcnt = (TextView) itemView.findViewById(R.id.tv_totalmessage);
            person_cnt = (TextView) itemView.findViewById(R.id.tv_totalperson);
            imageItemContainer = (LinearLayout) itemView.findViewById(R.id.imagecontainer);
            colorcode_parent = (LinearLayout) itemView.findViewById(R.id.code_container);
            btn_unarchive = (ImageView) itemView.findViewById(R.id.btn_unrchive);
            from_colname = (TextView) itemView.findViewById(R.id.tv_column_name);
            attch_icon = (TextView) itemView.findViewById(R.id.attach_icon);
        }
    }

    public String getDateFromTimeStamp(Long date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM, hh:mma");
        //Long init= Long.valueOf(date);
        java.util.Date resultdate = new java.util.Date(date * 1000);
        Log.i("TAG", "newdate:" + sdf.format(resultdate));
        return sdf.format(resultdate);
    }

    public String getOnlyDateFromTimeStamp(Long date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM");
        java.util.Date resultdate = new java.util.Date(date * 1000);
        return sdf.format(resultdate);
    }

    public void setFreshDataInCopy(List<MyItem> myItemListCopyFresh) {
        copydatalist.clear();
        copydatalist.addAll(myItemListCopyFresh);
    }

    public void filter(String text) {
        Log.i("TAG", "searched item added: " + text);
        Log.i("TAG", "copy:" + copydatalist);
        dataList.clear();
        if (text.isEmpty()) {
            dataList.addAll(copydatalist);
        } else {
            text = text.toLowerCase();
            for (MyItem item : copydatalist) {
                if (item.getData().toLowerCase().contains(text)) {
                    Log.i("TAG", "searched item added" + item.getData());
                    dataList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }


//    //for selection
//    public void toggleSelection(int position) {
//        Log.i("Toggle selection:",position+"mselected:"+mSelectedItemsIds.get(position));
//        selectView(position, !mSelectedItemsIds.get(position));
//    }
//    public void selectView(int position, boolean value) {
//        if (value)
//            mSelectedItemsIds.put(position, value);
//        else
//            mSelectedItemsIds.delete(position);
//        //notifyDataSetChanged();
//    }
//    public void removeSelection() {
//        mSelectedItemsIds = new SparseBooleanArray();
//        //notifyDataSetChanged();
//    }
//    public int getSelectedCount() {
//        return mSelectedItemsIds.size();
//    }
//
//    public SparseBooleanArray getSelectedIds() {
//        return mSelectedItemsIds;
//    }
}
