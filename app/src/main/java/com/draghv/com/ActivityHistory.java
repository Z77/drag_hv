package com.draghv.com;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.draghv.com.Adapter.HistoryAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Payal on 16-Feb-17.
 */
public class ActivityHistory extends AppCompatActivity {
    TextView toolbarTitle,nodata;
    String boardName;
    Zboard_history zboard_history;
    List<GetBoardHistory> datalist;
    //List<HistoryPOJO> historyPOJOList;
    RecyclerView history_rv;
    HistoryAdapter adapter;
    ValueEventListener eventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        zboard_history = new Zboard_history();
        Bundle b = getIntent().getExtras();
        boardName = (String) b.get("bname");
        datalist = new ArrayList<>();
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("History");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadData();

        nodata=(TextView)findViewById(R.id.nodata);
        history_rv = (RecyclerView) findViewById(R.id.list_history);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        history_rv.setLayoutManager(linearLayoutManager);
        adapter = new HistoryAdapter(datalist, ActivityHistory.this);
        history_rv.setAdapter(adapter);
    }

    public void loadData() {
        Log.i("TAG","main activity board id:"+MainActivity.BOARD_ID);
        eventListener=zboard_history.getBoardHistory(MainActivity.BOARD_ID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("TAG","data snap of history:");
                if(dataSnapshot.getChildrenCount()>0){
                    nodata.setVisibility(View.GONE);
                    if(datalist.size()>0)
                        datalist.clear();
                    Toast.makeText(getApplicationContext(), "history count:" + dataSnapshot.getChildrenCount(), Toast.LENGTH_SHORT).show();
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Log.i("TAG", "history data:=>" + data);
                        GetBoardHistory GBH = data.getValue(GetBoardHistory.class);
                        Log.i("TAG","userid:"+GBH.getUserId());
                        Log.i("TAG","action:"+GBH.getAction());
                        datalist.add(0,GBH);
                        adapter.notifyDataSetChanged();
                    }
                }else{
                    nodata.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("TAG","history data error:=>"+databaseError);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        zboard_history.getBoardHistory(MainActivity.BOARD_ID).removeEventListener(eventListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
