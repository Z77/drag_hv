package com.draghv.com;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 19-Jan-17.
 * used in getboard data & get archived card data
 */
public class GetBoardContent {

    int attachcont;
    List<String> label;
    String objectId;
    String columnId;
    String description;
    List<HashMap<String,Object>> arrItems;
    List<String> attachment;
    List<String> assign;
    int row;
    HashMap<String,Object> duedate;
    Boolean comment;
    Long commentCnt;
    String title;
    List<HashMap<String ,Object>> checklist;

    public int getAttachcont() {
        return attachcont;
    }

    public List<String> getlabel() {
        return label;
    }

    public List<HashMap<String, Object>> getArrItems() {
        return arrItems;
    }

    public List<String> getAttachment() {
        return attachment;
    }

    public Long getCommentCnt() {
        return commentCnt;
    }

    public String getObjectId() {
        return objectId;
    }

    public String getColumnId() {
        return columnId;
    }

    public int getRow() {
        return row;
    }

//    public Long getDuedate() {
//        return duedate;
//    }


    public HashMap<String, Object> getDuedate() {
        return duedate;
    }

    public List<HashMap<String, Object>> getChecklist() {
        return checklist;
    }

    public List<String> getAssign() {
        return assign;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getComment() {
        return comment;
    }







}
