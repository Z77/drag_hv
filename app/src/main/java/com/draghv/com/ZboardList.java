package com.draghv.com;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.ControllableTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Payal on 17-Jan-17.
 */
public class ZboardList {

    DatabaseReference firebase_board;
    DatabaseReference firebase_boarddata;
    Map<String, Object> child;


    private String title;
    private int attachcount;
    private int itemscount;
    private int msgcount;
    private String cardtitle;
    private String status_col;
    private String columnId;
    String cardid;
    private List<String> colorline;
    private String userid;
    private String key;
    private Map<String, Object> boards;
    private Map<String, Object> view;
    List<HashMap<String, Object>> collist;
    List<HashMap<String, Object>> cardlist;
    HashMap<String, Object> cardData;
    private int row, col, totalRow;
    ArrayList<HashMap<String,Object>> totalCol;


    public void setValue(String key, Object value) {
        child.put(key, value);
    }

    public ZboardList() {

        firebase_board = FirebaseDatabase.getInstance().getReference().child("dashboard");
        firebase_boarddata = FirebaseDatabase.getInstance().getReference().child("boardcontent");

//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        child = new HashMap<>();
        boards = new HashMap<>();
        view = new HashMap<>();
        colorline = new ArrayList<>();
        collist = new ArrayList<>();
        cardlist = new ArrayList<>();
        cardData = new HashMap<>();
//        userid = String.valueOf(R.string.userid);
        userid = "1AsCJsa5MC";
    }

    public String save() {
        key = firebase_board.push().getKey();
        Log.i("TAG", "boardid:" + key);

        child.put("createdAt", ServerValue.TIMESTAMP);
        child.put("updatedAt", ServerValue.TIMESTAMP);
        child.put("objectId", key);
        return key;
    }

    //    public void saveCardData(){
//        key=firebase_boarddata.child(key).push().getKey();
//    }
    //called when we adding row in this it will update row in firebase dashboard content
//    public DatabaseReference.CompletionListener upadateColRow(String boardId, DatabaseReference.CompletionListener completionListener) {
//        firebase_board.child(userid).child(boardId).updateChildren(child, completionListener);
//        return completionListener;
//    }
    //called when we adding row in this it will update row in firebase dashboard content
    public DatabaseReference.CompletionListener upadateBoard(String boardId, DatabaseReference.CompletionListener completionListener) {
        firebase_board.child(userid).child(boardId).updateChildren(child, completionListener);
        firebase_board.keepSynced(true);
        return completionListener;
    }

    //called when we press create btn of create board prompt it will add dashboard content not col and row will added right now
    public DatabaseReference.CompletionListener saveboardWithListener(DatabaseReference.CompletionListener completionListener) {
        firebase_board.child(userid).child(key).setValue(child, completionListener);
        firebase_board.keepSynced(true);
        return completionListener;
    }

    //called when we addnew card in board
    public DatabaseReference.CompletionListener saveboarddataWithListener(String boardId, DatabaseReference.CompletionListener completionListener) {
//        HashMap<String,Object> cardmeta=new HashMap<>();
//        cardmeta.put(cardid,cardData);
//        firebase_boarddata.child(key).setValue(cardmeta, completionListener);
        firebase_boarddata.child(boardId).child(cardid).setValue(cardData, completionListener);
        firebase_boarddata.keepSynced(true);
        return completionListener;
    }

    //call when we drag end update all card data
    public DatabaseReference.CompletionListener updateAllCardData(String boardId, HashMap<String, Object> carddataMap, DatabaseReference.CompletionListener completionListener) {
        firebase_boarddata.child(boardId).setValue(carddataMap, completionListener);
        firebase_boarddata.keepSynced(true);
        return completionListener;
    }
    //call when we update any card data mean(when we go in detail screen of card then)
    public DatabaseReference.CompletionListener updateCard(String boardId,String cardId,HashMap<String,Object> cardData,DatabaseReference.CompletionListener completionListener){
        firebase_boarddata.child(boardId).child(cardId).updateChildren(cardData, completionListener);
        firebase_boarddata.keepSynced(true);
        return completionListener;
    }


    public void setTitle(String title) {
        child.put(CONSTANTS.BOARD_TITLE, title);
        this.title = title;
    }


    public void Update(String boardId) {
        child.put(CONSTANTS.UPDATED_AT, ServerValue.TIMESTAMP);
        firebase_board.child(userid).child(boardId).updateChildren(child);
    }

    public void setUserid(String userid) {
        child.put(CONSTANTS.USER_ID, userid);
        this.userid = userid;
    }

    public void setAttachcount(int attachcount) {
        cardData.put(CONSTANTS.ATTACHMENT_COUNT, attachcount);
        this.attachcount = attachcount;
    }

    public void setItemscount(int itemscount) {
        cardData.put(CONSTANTS.ITEMS_COUNT, itemscount);
        this.itemscount = itemscount;
    }

    public void setMsgcount(int msgcount) {
        cardData.put(CONSTANTS.MESSAGE_COUNT, msgcount);
        this.msgcount = msgcount;
    }

    public void setCardtitle(String cardtitle) {
        cardData.put(CONSTANTS.CARD_TITLE, cardtitle);
        this.cardtitle = cardtitle;
    }
    public void setColumnId(String columnId) {
        cardData.put("columnId",columnId);
        this.columnId = columnId;
    }


//    public void setStatus_col(String status_col) {
//        cardData.put(CONSTANTS.COLUMN_NAME, status_col);
//        this.status_col = status_col;
//    }

    public void setCardid(String boardId) {
        Log.i("TAG", "boardid:" + boardId);
        this.cardid = firebase_boarddata.child(boardId).push().getKey();
        cardData.put(CONSTANTS.CARD_ID, cardid);
    }

    public void setColorline(List<String> colorline) {
        cardData.put(CONSTANTS.COLOR_LINES, colorline);
        this.colorline = colorline;
    }

    public void setRow(int row) {
        cardData.put("row", row);
        this.row = row;
    }

    public void setCol(int col) {
        cardData.put("column", col);
        this.col = col;
    }


//    public void setTotalCol(int totalCol) {
//        child.put("totalcol", totalCol);
//        //this.totalCol = totalCol;
//    }
    public void setTotalCol(ArrayList<HashMap<String,Object>> columnList){
        child.put("totalcol",columnList);
        this.totalCol=columnList;
    }
    //not in use now
    public void setTotalRow(int totalRow) {
        child.put("totalrow", totalRow);
        this.totalRow = totalRow;
    }

    public void clearBoardMap() {
        child.clear();
    }
    //    public void setCardlist() {
//
//        this.cardlist = cardlist;
//    }

//    //call 1)//by calling this function we filling cards array and in each of these the card data is available
//    public void setCardData(HashMap<String,Object> cardData) {
//        this.cardData = cardData;
//        cardlist.add(this.cardData);
//    }
//    //call 2)//by calling this function we add columnwise data with cards data so we put cardslist in collist
//    public void setCollist(int col_pos) {
//        HashMap<String,Object> cardslist=new HashMap<>();
//        cardslist.put(CONSTANTS.CARDSLIST_KEY,this.cardlist);
//        collist.set(col_pos,cardslist);
//        view.put("view", collist);
////        this.collist = collist;
//    }
//
//    //call 3)//by calling this fuction we filling board map and inside that "view" key so adding viewdata map and in side "view" column list is there
//    public void setBoards() {
//        HashMap<String,Object> viewdata=new HashMap<>();
//        viewdata.put(CONSTANTS.COLUMNLIST_KEY, collist);
//        child.put("boards",viewdata);
////        this.boards = boards;
//    }
//    public void addCard(int columnPos,HashMap<String,Object> cardData){
//        List<HashMap<String,Object>> cardslist= (List<HashMap<String, Object>>) collist.get(columnPos).get(CONSTANTS.CARDSLIST_KEY);
//        cardslist.add(cardData);
//    }

    public DatabaseReference getROOT() {
        return firebase_board;
    }

    public DatabaseReference getBOARD_ROOT(String boardId) {
        return firebase_board.child(userid).child(boardId);
    }


    public DatabaseReference getBOARD_DATA_ROOT() {
        return firebase_boarddata;
    }

    public DatabaseReference getBOARD_DATA(String boardId) {
        return firebase_boarddata.child(boardId);
    }
}
