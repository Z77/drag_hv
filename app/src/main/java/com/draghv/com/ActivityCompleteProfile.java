package com.draghv.com;

import android.*;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.UploadTask;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Payal on 08-Mar-17.
 */
public class ActivityCompleteProfile extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 101;
    private ProgressBar spin_kit;
    private String mCurrentPhotoPath;
    private static final int RC_CAMERA_PERM = 123;
    private static final int RC_GALLERY_PERM = 124;
    private static final int REQUEST_SELECT_PICTURE = 0x01;
    private static final int REQUEST_SELECT_CAMERA = 0x02;

    Zimages zimages;
    ZobazeUser zobazeUser;
    FirebaseDatabase database;
    GetUsers user;


    TextView cp_charc_count;
    EditText city;
    TextView cp_about;
    EditText name;
    EditText about;
    String gender;
    EditText bday, email, status;
    Button save;
    ImageView profilepic;
    Calendar cal;
    DatePickerDialog.OnDateSetListener date;
    AppCompatCheckBox genm, genf;
    private String userId, imageUrl;
    private boolean fromlogin = false;
    String oldname, oldcity;
    private ScrollView scroll;
    boolean cityClicked = false;
    private boolean callagain = false;
    public double latitude;
    public double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_profile);
        zimages = new Zimages();
        zobazeUser = new ZobazeUser();
        database = FirebaseDatabase.getInstance();
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            userId = bundle.getString("userId");
            fromlogin = bundle.getBoolean("login");
        }
        if (userId == null)
            userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        Log.i("TAG", "in landing activity userid:" + FirebaseAuth.getInstance().getCurrentUser().getUid());

        initUI();

    }

    public void initUI() {
        scroll = (ScrollView) findViewById(R.id.scroll);
        spin_kit = (ProgressBar) findViewById(R.id.spin_kit);
        spin_kit.setVisibility(View.VISIBLE);
        profilepic = (ImageView) findViewById(R.id.profilepic);
        LinearLayout root = (LinearLayout) findViewById(R.id.root);
        //TypeFace.overrideFonts(this, root);
        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open();
            }
        });
        cp_charc_count = (TextView) findViewById(R.id.cp_charc_count);
        cp_about = (EditText) findViewById(R.id.cp_aboutyou);
        cp_about.addTextChangedListener(mTextEditorWatcher);
        save = (Button) findViewById(R.id.save);
        name = (EditText) findViewById(R.id.name);
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 5) {
                    name.setBackgroundColor(0);
                    name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cp_name, 0, 0, 0);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        genm = (AppCompatCheckBox) findViewById(R.id.gen_m);
        genf = (AppCompatCheckBox) findViewById(R.id.gen_f);
        city = (EditText) findViewById(R.id.city);
        email = (EditText) findViewById(R.id.email);
        genm.setChecked(true);


//        city.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!cityClicked) {
//                    cityClicked = true;
//                    findPlace(v);
//                }
//            }
//        });

        city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                city.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cp_name, 0, 0, 0);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        genm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    genf.setChecked(false);
                    gender = "male";
                } else {
                    genf.setChecked(true);
                }

            }
        });
        genf.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    genm.setChecked(false);
                    gender = "female";
                } else {
                    genm.setChecked(true);
                }

            }
        });

        bday = (EditText) findViewById(R.id.dob);
        bday.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    name.setBackgroundColor(0);
                    name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cp_name, 0, 0, 0);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        about = (EditText) findViewById(R.id.cp_aboutyou);
        about.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                about.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        status = (EditText) findViewById(R.id.status);
        status.setText("Hi there i am new to Zobaze");
        //profilepic = (ImageView) findViewById(R.id.profilepic);

        cal = Calendar.getInstance();

        database.getReference().child("_user").child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    user = dataSnapshot.getValue(GetUsers.class);
                    applyUser();
                } else
                    spin_kit.setVisibility(View.VISIBLE);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        database.getReference().child("_user").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    user = dataSnapshot.getValue(GetUsers.class);
                    applyUser();
                } else
                    spin_kit.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        new ZobazeUser().getROOT().child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    user = dataSnapshot.getValue(GetUsers.class);
//                    applyUser();
//                } else
//                    spin_kit.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError firebaseError) {
//
//            }
//        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zobazeUser.setName(name.getText().toString().trim());
                zobazeUser.setGender(gender);
                zobazeUser.setAbout(about.getText().toString().trim());
                zobazeUser.setDob(cal.getTimeInMillis());
                zobazeUser.setEmail(email.getText().toString().trim());
                zobazeUser.setStatus(status.getText().toString().trim());
                if (everythingvalid()) {
                    System.out.println("Valid");
                    userSave();
                }
            }
        });


        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        bday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dp = new DatePickerDialog(ActivityCompleteProfile.this, R.style.DialogTheme, date, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
                dp.getDatePicker().setMaxDate(new Date().getTime());
                dp.show();
            }
        });
//        spin_kit.setVisibility(View.GONE);
//        scroll.setVisibility(View.VISIBLE);
    }

    private boolean everythingvalid() {
        if (name.getText().toString().isEmpty()) {
            name.setBackgroundColor(ContextCompat.getColor(this, R.color.material_red_300));
            scroll.scrollTo(0, name.getTop());
            Toast.makeText(this, "Enter Name", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (bday.getText().toString().isEmpty()) {
            bday.setBackgroundColor(ContextCompat.getColor(this, R.color.material_red_300));
            scroll.scrollTo(0, bday.getTop());
            Toast.makeText(this, "Add your DOB", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (city.getText().toString().isEmpty()) {
            city.setBackgroundColor(ContextCompat.getColor(this, R.color.material_red_300));
            scroll.scrollTo(0, city.getTop());
            Toast.makeText(this, "Enter City", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public void userSave() {
        zobazeUser.setObjectId(userId);
//        zobazeUser.setCallParse(true);
//        if (fromlogin)
//            zobazeUser.setCallParse(true);
//        else {
//            if (oldname.equals(name.getText().toString().trim()) || oldcity.equals(name.getText().toString().trim()))
//                zobazeUser.setCallParse(true);
//        }
        zobazeUser.update(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError firebaseError, DatabaseReference firebase) {
                Intent intent;
                //if (fromlogin) {
                intent = new Intent(getApplicationContext(), ActivityBoardList.class);
                startActivity(intent);
                finish();
//                } else {
//                    Bundle b = new Bundle();
//                    b.putInt("tab", 4);
//                    intent = new Intent(getApplicationContext(), HomeActivity.class);
//                    intent.putExtras(b);
//                    startActivity(intent);
//                    finish();
//                }
            }
        });
    }

    private void applyUser() {
        if (user.getName() != null) {
            oldname = user.getName();
            name.setText(user.getName());
        }

        if (user.getUsername() != null)
            //zobazeUser.setUsername(user.getUsername());
            if (user.getGender() != null) {
                if (user.getGender().equals("male")) {
                    genm.setChecked(true);
                    genf.setChecked(false);
                } else {
                    genm.setChecked(false);
                    genf.setChecked(true);
                }
            }
        if (user.getAbout() != null)
            about.setText(user.getAbout());
        if (user.getImage() != null) {
            //ImageLoader.userProfileLoad(profilepic, user.getImage(), null);
            ImageLoaderConfiguration config_ = new ImageLoaderConfiguration.Builder(ActivityCompleteProfile.this).build();
            final com.nostra13.universalimageloader.core.ImageLoader loader_ = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
            loader_.init(config_);
            final DisplayImageOptions options_ = new DisplayImageOptions.Builder()
                    .cacheInMemory(true).resetViewBeforeLoading(true)
                    .cacheOnDisk(true)
                    //.displayer(new CircleBitmapDisplayer(5))
                    .showImageForEmptyUri(R.drawable.cp_user).build();
            loader_.displayImage(user.getImage(),profilepic,options_);
            imageUrl = user.getImage();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                profilepic.setImageDrawable(getResources().getDrawable(R.drawable.cp_user, getTheme()));
            } else {
                profilepic.setImageDrawable(getResources().getDrawable(R.drawable.cp_user));
            }
        }
        if (user.getDob() != 0) {
            cal.setTimeInMillis(user.getDob());
            updateLabel();
        }
        if (user.getStatus() != null)
            status.setText(user.getStatus());
        if (user.getEmail() != null)
            email.setText(user.getEmail());

        if (user.getCity() != null) {
            city.setText(user.getCity());
            oldcity = user.getCity();
        }
        latitude = user.getLat();
        longitude = user.getLon();
        zobazeUser.setLat(latitude);
        zobazeUser.setLon(longitude);
        spin_kit.setVisibility(View.GONE);
        scroll.setVisibility(View.VISIBLE);
    }

    private void updateLabel() {

        String myFormat = "dd/MMMM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        bday.setText(sdf.format(cal.getTime()));
    }

    private final TextWatcher mTextEditorWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            cp_charc_count.setText(String.valueOf(s.length()) + "/1000");
        }

        public void afterTextChanged(Editable s) {
        }
    };

    public void open() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Select Your Profile Picture From Following");

        alertDialogBuilder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                cameraTask();
            }
        });

        alertDialogBuilder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                galleryTask();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void cameraTask() {
        String[] perms = {android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Have permission, do the thing!
            showCamera();
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_camera),
                    RC_CAMERA_PERM, perms);
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void galleryTask() {
        if (EasyPermissions.hasPermissions(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            // Have permission, do the thing!
            showGallery();
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_galley),
                    RC_GALLERY_PERM, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    public void showGallery() {
        //Answers.getInstance().logCustom(new CustomEvent("Create_Profile_Gallery"));
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, REQUEST_SELECT_PICTURE);
    }

    public void showCamera() {
        //Answers.getInstance().logCustom(new CustomEvent("Create_Profile_Camera"));
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_SELECT_CAMERA);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "POSTFEED_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(selectedUri);
                } else {
                    //Toast.makeText(EditProfileActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            } else if (requestCode == REQUEST_SELECT_CAMERA) {
                final Uri selectedUri = Uri.parse(new File(mCurrentPhotoPath).toString());
                if (selectedUri != null) {
                    startCropActivity(selectedUri);
                } else {
                    //Toast.makeText(EditProfileActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void startCropActivity(@NonNull Uri uri) {
        String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Log.i("TAG", "userid in profile:" + id);
        String destinationFileName;
        if (!id.isEmpty() && id != null)
            destinationFileName = id + "_photo";
        else
            destinationFileName = System.currentTimeMillis() / 1000 + "_photo";
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop.start(this);
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            profilepic.setImageResource(0);
            zimages.uploadProfile(resultUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    imageUrl = taskSnapshot.getDownloadUrl().toString();
                    profilepic.setImageURI(resultUri);
                    ImageLoader.userProfileLoad(profilepic, imageUrl, null);
                    //if (imageUrl != null)
                    //zobazeUser.setImage(imageUrl);

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
        } else {
            Toast.makeText(ActivityCompleteProfile.this, "Error occurred, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("TAG", "handleCropError: ", cropError);
            if (!callagain) {
                final Uri selectedUri = Uri.parse(new File(mCurrentPhotoPath).toString());
                if (selectedUri != null) {
                    startCropActivity(selectedUri);
                }
            } else {
                Toast.makeText(ActivityCompleteProfile.this, cropError.getMessage(), Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(ActivityCompleteProfile.this, "Error occurred, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }
//    public void findPlace(View view) {
//        try {
//            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
//                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
//                    .build();
//            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
//                    .setFilter(typeFilter).build(this);
//            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
//        } catch (GooglePlayServicesRepairableException e) {
//            // TODO: Handle the error.
//        } catch (GooglePlayServicesNotAvailableException e) {
//            // TODO: Handle the error.
//        }
//    }
}
