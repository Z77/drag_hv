package com.draghv.com;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.woxthebox.draglistview.BoardView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Payal on 16-Jan-17.
 */
public class NewBoard extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static long sCreatedItems = 1;
    private BoardView mBoardView;
    private int mColumns;
    private String copylistName;
    ArrayList<String> headerList = new ArrayList<>();
    List<List<MyItem>> grandList = new ArrayList<>();
    public static List<MyAdapter> adapterList = new ArrayList<>();
    List<String> boardList = new ArrayList<>();
    LinearLayout pager_indicator;
    int NUM_PAGES;
    private TextView[] dots;
    AppBarLayout toolbar_cont;
    TextView toolbarTitle;
    List<HashMap<String, Long>> indicatorPosList = new ArrayList<>();
    FrameLayout boardParent;
    int prevIndex = 0;
    SearchView searchView = null;
    String bname;
    public static MyItem clickeditem_data;
    String BOARD_ID;
    static List<GetBoardContent> cardData;
    ZboardList board = new ZboardList();
    int TOTAL_COL = 0;
    int TOTAL_ROW = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

        setContentView(R.layout.activity_main);
        bname = getIntent().getStringExtra("bname");
        BOARD_ID = getIntent().getStringExtra("bid");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(bname);
        setSupportActionBar(toolbar);

        cardData = new ArrayList<>();

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //boardata=new ZboardList();
        boardParent = (FrameLayout) findViewById(R.id.board_parent);
        mBoardView = (BoardView) findViewById(R.id.board_view);
        mBoardView.setSnapToColumnsWhenScrolling(true);
        mBoardView.setSnapToColumnWhenDragging(true);
        mBoardView.setSnapDragItemToTouch(true);
        pager_indicator = (LinearLayout) findViewById(R.id.indicator);
        toolbar_cont = (AppBarLayout) findViewById(R.id.toolbar_container);

        mBoardView.setBoardListener(new BoardView.BoardListener() {
            @Override
            public void onItemDragStarted(int column, int row) {

                pager_indicator.setAlpha(0);
                toolbar_cont.animate().alpha(0).setDuration(650).start();
                pager_indicator.animate().alpha(1).setDuration(500).start();
                Toast.makeText(mBoardView.getContext(), "Start - column: " + column + " row: " + row, Toast.LENGTH_SHORT).show();
                dots[column].setBackground(ContextCompat.getDrawable(NewBoard.this, R.drawable.selected_ract));
                if (indicatorPosList.size() <= 0)
                    for (TextView dot : dots) {
                        HashMap<String, Long> xymap = new HashMap<String, Long>();
                        int[] originalPos = new int[2];
                        dot.getLocationOnScreen(originalPos);
                        //or view.getLocationOnScreen(originalPos)
                        int x = originalPos[0];
                        int y = originalPos[1];
                        xymap.put("x", (long) x);
                        xymap.put("y", (long) y);
                        indicatorPosList.add(xymap);
                    }
            }

            @Override
            public void onItemChangedColumn(int oldColumn, int newColumn) {
                TextView itemCount1 = (TextView) mBoardView.getHeaderView(oldColumn).findViewById(R.id.total_view);
                itemCount1.setText(Integer.toString(mBoardView.getAdapter(oldColumn).getItemCount()));
                TextView itemCount2 = (TextView) mBoardView.getHeaderView(newColumn).findViewById(R.id.total_view);
                itemCount2.setText(Integer.toString(mBoardView.getAdapter(newColumn).getItemCount()));
                dots[oldColumn].setBackground(ContextCompat.getDrawable(NewBoard.this, R.drawable.nonselected_ract));
                dots[newColumn].setBackground(ContextCompat.getDrawable(NewBoard.this, R.drawable.selected_ract));
                //Toast.makeText(mBoardView.getContext(), "You are in tab " + newColumn, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemDragEnded(int fromColumn, int fromRow, int toColumn, int toRow) {
                if (fromColumn != toColumn || fromRow != toRow) {
                    Toast.makeText(mBoardView.getContext(), "End - column: " + toColumn + " row: " + toRow, Toast.LENGTH_SHORT).show();
                }
                if (fromColumn == toColumn && fromRow != toRow) {
                    //mBoardView.getRecyclerView(fromColumn).getAdapter().notifyDataSetChanged();
                    Log.i("DRAGE", "Get recycler view of column:" + fromColumn + "=> ");
                }

                dots[toColumn].setBackground(ContextCompat.getDrawable(NewBoard.this, R.drawable.nonselected_ract));
                toolbar_cont.animate().alpha(1).setDuration(500).start();
                pager_indicator.animate().alpha(0).setDuration(650).start();
                pager_indicator.setAlpha(1);

                board.setCol(toColumn);
                board.setRow(toRow);
                if(cardData.size()>0) {
                    HashMap<String, Object> cardsData = new HashMap<String, Object>();
                    for (int i = 0; i < mBoardView.getColumnCount(); i++) {
                        int row = mBoardView.getAdapter(i).getItemCount();
                        for (int j = 0; j < row; j++) {
                            MyItem card = (MyItem) mBoardView.getAdapter(i).getItemList().get(j);
                            String status = headerList.get(i);
                            //Log.i("TAG","data is:"+card.getData());
                            for (int c = 0; c < cardData.size(); c++) {
                                if (cardData.get(c).getTitle().equals(card.getData())) {
                                    Log.i("TAG", "map added" + card.getData());
                                    HashMap<String, Object> data = new HashMap<String, Object>();
                                    data.put("column", i);
                                    data.put("objectId", cardData.get(c).getObjectId());
                                    data.put("row", j);
                                    data.put("status", status);
                                    data.put("title", cardData.get(c).getTitle());
                                    cardsData.put(cardData.get(c).getObjectId(), data);
                                }
                            }
                        }
                    }
                    board.setTotalRow(getMaxRow());
                    board.upadateBoard(BOARD_ID, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            Toast.makeText(getApplicationContext(), "max is saved", Toast.LENGTH_SHORT).show();
                        }
                    });
                    board.updateAllCardData(BOARD_ID, cardsData, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            Toast.makeText(getApplicationContext(), "data updated in firebase", Toast.LENGTH_SHORT).show();
                            board.getBOARD_DATA(BOARD_ID).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    Log.i("TAG", "data after drag ened" + dataSnapshot);
                                    if (cardData.size() > 0)
                                        cardData.clear();
                                    for (DataSnapshot contentData : dataSnapshot.getChildren()) {
                                        GetBoardContent content = contentData.getValue(GetBoardContent.class);
                                        cardData.add(content);
                                    }
                                 //   isSyncing = false;
                                    Toast.makeText(getApplicationContext(), "updated data taken from firebase", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.i("TAG", "error:" + databaseError.getMessage() + "\n" + databaseError.getDetails());
                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void getXY(Float x, Float y) {
                for (int i = 0; i < indicatorPosList.size(); i++) {
                    if ((x >= indicatorPosList.get(i).get("x") && x <= (indicatorPosList.get(i).get("x") + 30)) && ((y <= indicatorPosList.get(i).get("y") + 20 && y >= indicatorPosList.get(i).get("y") - 70)))
                        if (prevIndex != i) {
                            prevIndex = i;
                            mBoardView.scrollToColumn(i, true);
                            Log.i("TAG", "my x and y is:" + x + " " + y);
                        }

                }
            }
        });
        for (int i = 0; i < 3; i++) {
            ArrayList<MyItem> myItemList = new ArrayList<>();
            List<String> codeList = new ArrayList<>();
            codeList.add("green");
            if (i == 0) {
                addColumn(myItemList, "To do");
            }
            if (i == 1) {
                addColumn(myItemList, "Doing");
            }
            if (i == 2) {
                addColumn(myItemList, "Done");
            }
            TOTAL_COL = headerList.size();

        }
        setPageViewController();
        for (int i = 0; i < mBoardView.getColumnCount(); i++) {
            final int j = i;
            mBoardView.getRecyclerView(i).addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), mBoardView.getRecyclerView(i), new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    clickeditem_data = (MyItem) mBoardView.getAdapter(j).getItemList().get(position);
                    Intent i = new Intent(getApplicationContext(), ItemDetailActivity.class);
                    i.putExtra("col", String.valueOf(j));
                    i.putExtra("row", String.valueOf(position));
                    i.putExtra("from", "new");
                    startActivity(i);
                    Log.i("TAG", clickeditem_data.getData());
                    Toast.makeText(getApplicationContext(), "REcycler item clicked row " + position + " col: " + j, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onLongItemClick(View view, int position) {

                }
            }));
        }
    }

    public int getMaxRow() {
        int maxrow = mBoardView.getAdapter(0).getItemCount();
        Log.i("TAG", "max first is:" + maxrow);
        for (int i = 0; i < mBoardView.getColumnCount(); i++) {
            if (mBoardView.getAdapter(i).getItemCount() > maxrow) {
                maxrow = mBoardView.getAdapter(i).getItemCount();
                Log.i("TAG", "max row is:" + maxrow);
            }
        }
        return maxrow;
    }


    private void setPageViewController() {

        int dotsCount = headerList.size();
        NUM_PAGES = dotsCount;
        Log.i("TAG", "header list size:" + dotsCount);
        dots = new TextView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new TextView(this);
            dots[i].setBackground(ContextCompat.getDrawable(NewBoard.this, R.drawable.nonselected_ract));
            dots[i].setText(String.valueOf(headerList.get(i).charAt(0)) + String.valueOf(headerList.get(i).charAt(0)));
            Log.i("TAG", "header first char is" + headerList.get(i).charAt(0));
            dots[i].setGravity(Gravity.CENTER);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.width = 50;
            params.height = 80;

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }

        //dots[0].setImageDrawable(ContextCompat.getDrawable(MainActivity.this,R.drawable.selected_ract));
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.i("TAG", "searched key word is:" + query);
        for (int i = 0; i < mBoardView.getColumnCount(); i++) {
            adapterList.get(i).filter(query);
            TextView itemCount2 = (TextView) mBoardView.getHeaderView(i).findViewById(R.id.total_view);
            itemCount2.setText(Integer.toString(mBoardView.getAdapter(i).getItemCount()));
            Log.i("TAG", "total cards now is:" + adapterList.get(i).getItemCount());
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.i("TAG", "searching key word is:" + newText);
        for (int i = 0; i < mBoardView.getColumnCount(); i++) {
            adapterList.get(i).filter(newText);
            TextView itemCount2 = (TextView) mBoardView.getHeaderView(i).findViewById(R.id.total_view);
            itemCount2.setText(Integer.toString(mBoardView.getAdapter(i).getItemCount()));
            Log.i("TAG", "total cards now is:" + adapterList.get(i).getItemCount());
        }

        return true;
    }

    public void addColumn(final ArrayList<MyItem> mItemArray, String heading) {
        final List<MyItem> mItemArrays = mItemArray;

        final int column = mColumns;
        grandList.add(mItemArray);
        //grandList.put(column,mItemArray);
        final MyAdapter listAdapter = new MyAdapter(NewBoard.this, mItemArray, R.layout.item_view, R.id.item_layout, true);
        adapterList.add(listAdapter);
        final View header = View.inflate(NewBoard.this, R.layout.header_view, null);
        final View addCard = View.inflate(NewBoard.this, R.layout.add_card_btn, null);

        ((ImageView) header.findViewById(R.id.header_menu)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popup = new PopupMenu(NewBoard.this, v);
                popup.getMenuInflater().inflate(R.menu.menu_mastercard, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String title = item.getTitle().toString();
                        if (title.equals("Move List")) {
                            Toast.makeText(getApplicationContext(), "select " + title, Toast.LENGTH_SHORT).show();
                            return true;
                        } else if (title.equals("Copy")) {
                            final Spinner spinner_list;
                            final MaterialDialog copyDailog = new MaterialDialog.Builder(NewBoard.this)
                                    .title("Copy List")
                                    .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                                    .titleColorRes(R.color.colorPrimary)
                                    .positiveText("Ok").positiveColor(ContextCompat.getColor(NewBoard.this, R.color.colorPrimary))
                                    .negativeText("Cancel").negativeColor(ContextCompat.getColor(NewBoard.this, R.color.colorPrimary))
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            final Spinner spinner_list = (Spinner) dialog.findViewById(R.id.spn_list);
                                            String selected = spinner_list.getSelectedItem().toString();
                                            //String now = ((TextView) header.findViewById(R.id.header_text)).getText().toString();
                                            int newColumnPos = spinner_list.getSelectedItemPosition();
                                            int viewCount = mBoardView.getItemCount(column);
                                            for (int i = 0; i < viewCount; i++) {
                                                //viewCount = mBoardView.getItemCount(column);
                                                Log.i("TAG", "board view column data count:" + mBoardView.getItemCount(column));
                                                MyItem newItem = grandList.get(column).get(i);
                                                newItem.setItemid(sCreatedItems++);
                                                mBoardView.addItem(newColumnPos, i, newItem, false);
                                                //mBoardView.addItem(newColumnPos, 0, newColumnPos, i, false);
                                            }
                                            TextView itemCountnew = (TextView) mBoardView.getHeaderView(newColumnPos).findViewById(R.id.total_view);
                                            itemCountnew.setText(Integer.toString(mBoardView.getAdapter(newColumnPos).getItemCount()));
//                                            final TextInputEditText tv_title = (TextInputEditText) dialog.findViewById(R.id.tv_list_title);
//                                            if (tv_title.getText().toString().length() > 0) {
//                                                copylistName = tv_title.getText().toString();
//                                                //headerList.add(copylistName);
//                                                String old = ((TextView) header.findViewById(R.id.header_text)).getText().toString();
//                                                Log.i("TAG", "erlier name:" + old);
//                                                int position = -1;
//                                                if (headerList.contains(old)) {
//                                                    position = headerList.indexOf(old);
//                                                }
//                                                addColumn((ArrayList<MyItem>) grandList.get(position), copylistName);

                                            Toast.makeText(getApplicationContext(), "Copied successfully", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
//                                            }
                                        }
                                    }).onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
//                                    .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                                    .customView(R.layout.promp_move_all_cards, true)
                                    .show();
                            spinner_list = (Spinner) copyDailog.findViewById(R.id.spn_list);
                            ArrayAdapter<String> a = new ArrayAdapter<String>(NewBoard.this, android.R.layout.simple_spinner_item, headerList);
                            a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner_list.setAdapter(a);
                            return true;
                        } else if (title.equals("Move all cards")) {
                            final Spinner spinner_list;
                            final MaterialDialog moveallcard = new MaterialDialog.Builder(NewBoard.this)
                                    .title("Move All Cards")
                                    .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                                    .titleColorRes(R.color.colorPrimary)
                                    .positiveText("Move").positiveColor(ContextCompat.getColor(NewBoard.this, R.color.colorPrimary))
                                    .negativeText("Cancel").negativeColor(ContextCompat.getColor(NewBoard.this, R.color.colorPrimary))
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            Spinner spinner_list = (Spinner) dialog.findViewById(R.id.spn_list);

                                            String selected = spinner_list.getSelectedItem().toString();
                                            String now = ((TextView) header.findViewById(R.id.header_text)).getText().toString();
                                            if (!selected.equals(now)) {
                                                int newColumnPos = spinner_list.getSelectedItemPosition();
                                                int viewCount = mBoardView.getItemCount(column);
                                                for (int i = 0; i <= viewCount; i++) {
                                                    //viewCount = mBoardView.getItemCount(column);
                                                    Log.i("TAG", "board view column data count:" + mBoardView.getItemCount(column));
                                                    mBoardView.moveItem(column, 0, newColumnPos, i, false);
                                                }
                                                TextView itemCount = (TextView) mBoardView.getHeaderView(column).findViewById(R.id.total_view);
                                                itemCount.setText(Integer.toString(mBoardView.getAdapter(column).getItemCount()));
                                                TextView itemCountnew = (TextView) mBoardView.getHeaderView(newColumnPos).findViewById(R.id.total_view);
                                                itemCountnew.setText(Integer.toString(mBoardView.getAdapter(newColumnPos).getItemCount()));
                                                Log.i("TAG", "after removing size of array" + mItemArrays.size());
                                                dialog.dismiss();
                                            }
                                        }
                                    })
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .customView(R.layout.promp_move_all_cards, true)
                                    .show();
                            spinner_list = (Spinner) moveallcard.findViewById(R.id.spn_list);
                            ArrayAdapter<String> a = new ArrayAdapter<String>(NewBoard.this, android.R.layout.simple_spinner_item, headerList);
                            a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner_list.setAdapter(a);
                            return true;
                        } else if (title.equalsIgnoreCase("Move cards")) {
                            final Spinner boardlist;
                            Spinner columnlist = null;
                            ListView cardslist = null;

                            final List<String> coluITems = new ArrayList<>();
                            final List<String> selected = new ArrayList<>();
                            final List<MyItem> selectedData = new ArrayList<MyItem>();

                            final MaterialDialog movecard = new MaterialDialog.Builder(NewBoard.this)
                                    .title("Move Cards")
                                    .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                                    .titleColorRes(R.color.colorPrimary)
                                    .positiveText("Move").positiveColor(ContextCompat.getColor(NewBoard.this, R.color.colorPrimary))
                                    .negativeText("Cancel").negativeColor(ContextCompat.getColor(NewBoard.this, R.color.colorPrimary))
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            Spinner columnlist = (Spinner) dialog.findViewById(R.id.spn_columnlist);
                                            int newColu = columnlist.getSelectedItemPosition();
                                            Log.i("TAG", "new column is:" + newColu);
                                            int viewCount = mBoardView.getItemCount(column);
                                            //viewCount = mBoardView.getItemCount(column);
                                            Log.i("TAG", "view in column before" + viewCount);

                                            for (int i = 0; i < selected.size(); i++) {
                                                Log.i("TAG", "view in column in loop" + viewCount);
                                                int newindex = mItemArrays.indexOf(selectedData.get(i));
                                                selected.contains(mItemArrays.get(newindex).getData());
                                                {
                                                    MyItem item = mItemArrays.get(newindex);
                                                    mBoardView.removeItem(item);
                                                    mBoardView.addItem(newColu, i, item, false);
                                                }
                                            }
                                            Log.i("TAG", "final array" + mItemArrays.size());
                                            TextView itemCount = (TextView) mBoardView.getHeaderView(column).findViewById(R.id.total_view);
                                            itemCount.setText(Integer.toString(mBoardView.getAdapter(column).getItemCount()));
                                            TextView itemCountnew = (TextView) mBoardView.getHeaderView(newColu).findViewById(R.id.total_view);
                                            itemCountnew.setText(Integer.toString(mBoardView.getAdapter(newColu).getItemCount()));
                                        }
                                    })
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .customView(R.layout.prompt_move_cards, true)
                                    .show();

                            boardlist = (Spinner) movecard.findViewById(R.id.spn_boardlist);
                            columnlist = (Spinner) movecard.findViewById(R.id.spn_columnlist);
                            cardslist = (ListView) movecard.findViewById(R.id.card_list);


                            ArrayAdapter<String> a = new ArrayAdapter<String>(NewBoard.this, android.R.layout.simple_spinner_item, boardList);
                            a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            boardlist.setAdapter(a);
                            ArrayAdapter<String> a2 = new ArrayAdapter<String>(NewBoard.this, android.R.layout.simple_spinner_item, headerList);
                            a2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            columnlist.setAdapter(a2);

                            for (int i = 0; i < mItemArrays.size(); i++) {
                                coluITems.add(mItemArrays.get(i).getData());
                            }
                            cardslist.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                            cardslist.setAdapter(new ArrayAdapter<String>(NewBoard.this, android.R.layout.simple_list_item_checked, coluITems));
                            cardslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    CheckedTextView item = (CheckedTextView) view;
                                    if (item.isChecked()) {
                                        selectedData.add(mItemArrays.get(position));
                                        selected.add(coluITems.get(position));
                                        Log.i("TAG", "item text is:" + coluITems.get(position));
                                    } else {
                                        if (selected.contains(coluITems.get(position)))
                                            selected.remove(coluITems.get(position));
                                        selectedData.remove(position);

                                    }
                                }
                            });
                            return true;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
        ((Button) addCard.findViewById(R.id.btnAddCard)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyboard();
                mBoardView.setEnabled(false);
                final MaterialDialog addCardDailog = new MaterialDialog.Builder(NewBoard.this)
                        .title("Add card")
                        .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                        .titleColorRes(R.color.colorPrimary)
                        .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                        .customView(R.layout.prompt_rename_header, true)
                        .positiveText("Ok").positiveColor(ContextCompat.getColor(NewBoard.this, R.color.colorPrimary))
                        .negativeText("Cancel").negativeColor(ContextCompat.getColor(NewBoard.this, R.color.colorPrimary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                final EditText tv_title = (EditText) dialog.findViewById(R.id.tv_list_title);
                                if (tv_title.getText().toString().length() > 0) {
                                    Calendar c = Calendar.getInstance();
                                    SimpleDateFormat df = new SimpleDateFormat("dd MMM,hh:mma");
                                    String formattedDate = df.format(c.getTime());
                                    String[] datepart = formattedDate.split(" ");
                                    //for  print date like formate of 6th 5th (postfix)
                                    //String newDate=datepart[0]+getDayOfMonthSuffix(Integer.parseInt(datepart[0]))+" "+datepart[1];
                                    //Log.i("TAG","new date is:"+newDate);

                                    List<String> codeList = new ArrayList<>();
                                    codeList.add("red");
                                    codeList.add("sky");
                                    MyItem data = new MyItem(sCreatedItems++,"",tv_title.getText().toString(), R.drawable.design, formattedDate, 1L, 5L, false, false, codeList, 2L, 3L);
                                    mBoardView.addItem(column, mItemArrays.size(), data, false);
                                    mBoardView.scrollToItem(column, mItemArrays.size() - 1, true);
                                    int indexOfAdapter = adapterList.indexOf(listAdapter);
                                    adapterList.get(indexOfAdapter).addNewItemInCopyList(data);
                                    TextView itemCount = (TextView) mBoardView.getHeaderView(column).findViewById(R.id.total_view);
                                    itemCount.setText(Integer.toString(mBoardView.getAdapter(column).getItemCount()));
                                    hidekeyboard(NewBoard.this);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            //hidekeyboard(MainActivity.this);
                                            mBoardView.scrollToItem(column, mItemArrays.size() - 1, true);
                                        }
                                    }, 600);
                                    dialog.dismiss();
                                    mBoardView.setEnabled(true);
//                                    BoardList.zboardList.setCardid(BOARD_ID);
//                                    BoardList.zboardList.setCardtitle(tv_title.getText().toString().trim());
//                                    BoardList.zboardList.setRow(mItemArrays.size() - 1);
//                                    BoardList.zboardList.setCol(column);
//                                    BoardList.zboardList.setStatus_col(headerList.get(column));

                                    int maxrow = mBoardView.getAdapter(0).getItemCount();
                                    Log.i("TAG", "max first is:" + maxrow);
                                    for (int i = 0; i < mBoardView.getColumnCount(); i++) {
                                        if (mBoardView.getAdapter(i).getItemCount() > maxrow) {
                                            maxrow = mBoardView.getAdapter(i).getItemCount();

                                            Log.i("TAG", "max row is:" + maxrow);
                                        }
                                    }
//                                    BoardList.zboardList.clearBoardMap();//bcoz if we don't clear it will overwrite createdAt and updatedAt value //we can do this by creating new object of class
//                                    //BoardList.zboardList.setTotalCol(mColumns);
//                                    //BoardList.zboardList.setTotalCol(headerList);
//                                    BoardList.zboardList.setTotalRow(maxrow);
//                                    BoardList.zboardList.upadateBoard(BOARD_ID, new DatabaseReference.CompletionListener() {
//                                        @Override
//                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                                            Toast.makeText(getApplicationContext(), "max is saved", Toast.LENGTH_SHORT).show();
//                                        }
//                                    });
                                    TOTAL_ROW = maxrow;
                                    TOTAL_COL = mColumns;
//                                    BoardList.zboardList.setCardData();
//                                    BoardList.zboardList.setCollist(column);
//                                    BoardList.zboardList.setBoards();
//                                    BoardList.zboardList.saveboardWithListener(new DatabaseReference.CompletionListener() {
//                                        @Override
//                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                                            Toast.makeText(getApplication(),"Card saved",Toast.LENGTH_SHORT).show();
//                                        }
//                                    });
                                    ZboardList board = new ZboardList();
                                    board.setCardid(BOARD_ID);
                                    board.setCardtitle(tv_title.getText().toString().trim());
                                    board.setRow(mItemArrays.size() - 1);
                                    board.setCol(column);
                                    //board.setStatus_col(headerList.get(column));
                                    board.saveboarddataWithListener(BOARD_ID, new DatabaseReference.CompletionListener() {
                                        @Override
                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                            Toast.makeText(getApplication(), "Card saved", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    getDataFromFirebase(BOARD_ID);
//                                    BoardList.zboardList.saveboarddataWithListener(BOARD_ID,new DatabaseReference.CompletionListener() {
//                                        @Override
//                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                                            Toast.makeText(getApplication(), "Card saved", Toast.LENGTH_SHORT).show();
//                                        }
//                                    });
                                } else {
                                    Toast.makeText(getApplicationContext(), "Card title can not be blank!!!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        hidekeyboard(NewBoard.this);
                                    }
                                }, 300);
                                dialog.dismiss();
                                mBoardView.setEnabled(true);
                            }
                        })
                        .show();

//                Button cancel = (Button) addCardDailog.findViewById(R.id.btn_cancel_rename);
//                Button submit = (Button) addCardDailog.findViewById(R.id.btn_submit_rename);
//                cancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        addCardDailog.dismiss();
//                    }
//                });
//                submit.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                });
//                addCardDailog.show();
            }
        });
        if (copylistName != null) {
            ((TextView) header.findViewById(R.id.header_text)).setText(copylistName);
            copylistName = null;
        } else {
            ((TextView) header.findViewById(R.id.header_text)).setText(heading);
        }
        ((TextView) header.findViewById(R.id.total_view)).setText(String.valueOf(mItemArray.size()));

//        header.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MyItem data=new MyItem(sCreatedItems++,"Call to Alex",0);
//                mBoardView.addItem(column, 0, data, true);
//            }
//        });
        //listAdapter.setItemList(mItemArray);
        String text = ((TextView) header.findViewById(R.id.header_text)).getText().toString();
        Log.i("TAG", "header name is:" + text);
        headerList.add(heading);
        mBoardView.addColumnList(listAdapter, header, addCard, false);

        mColumns++;

    }

    public void getDataFromFirebase(String boardId) {
        board.getBOARD_DATA(boardId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                cardData = new ArrayList<GetBoardContent>();
                for (int i = 0; i < TOTAL_COL; i++) {
                    for (int j = 0; j < TOTAL_ROW; j++) {
                        for (DataSnapshot contentData : dataSnapshot.getChildren()) {
                            GetBoardContent content = contentData.getValue(GetBoardContent.class);
                            if (content.getRow() == j && content.getColumn() == i) {
                                cardData.add(content);
                            }
                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchView.setQueryHint("Enter key word...");
        searchView.setOnQueryTextListener(this);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_rearrange) {
//            Intent i=new Intent(getApplicationContext(),FragmentRearrangeList.class);
//            i.putExtra("namelist",headerList);
//            startActivity(i);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            //transaction.replace(R.id.rearrange_container, FragmentRearrangeList.instantiate(grandList, headerList), "FragmentRearrangeList").addToBackStack("FragmentRearrangeList").commit();
            mBoardView.setVisibility(View.GONE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            //getSupportActionBar().setTitle("Rearrange List");
            toolbarTitle.setText("Rearrange List");
            //grandList.clear();
            return true;
        }

        toolbarTitle.setText(bname);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count != 0) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            mBoardView.setVisibility(View.VISIBLE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            //getSupportActionBar().setTitle("My Dashboard");
            toolbarTitle.setText(bname);
        } else {
            super.onBackPressed();
        }
    }


    //called form fragment  @FragmentRearrangeList (bcoz of header name change or index chnage)
    public void ClearBoardAddColumn(List<List<MyItem>> itemsList, List<String> headers) {
        mBoardView.clearBoard();
        grandList = new ArrayList<>();
        headerList = new ArrayList<>();
        mColumns = 0;
        for (int i = 0; i < itemsList.size(); i++) {
            addColumn((ArrayList<MyItem>) itemsList.get(i), headers.get(i));
        }

    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void hidekeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.RESULT_UNCHANGED_HIDDEN, 0);

/*        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focus = activity.getCurrentFocus();
        if(focus != null)
        imm.hideSoftInputFromWindow(focus.getWindowToken(), 0);*/

/*        InputMethodManager im = (InputMethodManager) getSystemService(activity.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);*/

        /*activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);*/

    }

    //called from adapter named @MyAdapter (to open item detail page(fragment))
    public void switchContent(Fragment fragment, List<MyItem> myItemList, MyItem itemdata, int row) {
        int column = 0;
        for (int i = 0; i < adapterList.size(); i++) {
            if (adapterList.get(i).getItemList() == myItemList) {
                Log.i("TAG", "list has item list" + i);
                column = i;
            }
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        transaction.replace(R.id.rearrange_container, ItemDetail.instantiate(column, row, itemdata), "ItemDetail").addToBackStack("ItemDetail").commit();
        mBoardView.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarTitle.setText(itemdata.getData());

    }
}
