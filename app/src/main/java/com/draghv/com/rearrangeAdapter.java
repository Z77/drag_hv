package com.draghv.com;

import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

/**
 * Created by Payal on 05-Jan-17.
 */
public class rearrangeAdapter extends RecyclerView.Adapter<rearrangeAdapter.myViewHolder> {
    List<String> namelist;

    public rearrangeAdapter(List<String> namelist) {
        this.namelist = namelist;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myview = LayoutInflater.from(parent.getContext()).inflate(R.layout.rearrange_row, parent, false);
        myViewHolder mvh = new myViewHolder(myview);
        return mvh;
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, final int position) {
        holder.listname.setText(namelist.get(position));
    }

    @Override
    public int getItemCount() {
        return namelist != null ? namelist.size() : 0;
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        TextView listname;
        ImageView edit, delete;

        public myViewHolder(View itemView) {
            super(itemView);
            listname = (TextView) itemView.findViewById(R.id.list_name);
            edit = (ImageView) itemView.findViewById(R.id.edit_name);
            delete = (ImageView) itemView.findViewById(R.id.delete_name);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeHeader(getAdapterPosition());
                }
            });
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new MaterialDialog.Builder(v.getContext())
                            .title("Edit column name")
                            .positiveText("Rename")
                            .negativeText("Cacel")
                            .customView(R.layout.prompt_rename_header, true)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                                final EditText tv_title = (EditText) dialog.findViewById(R.id.tv_list_title);
                                                if (tv_title.getText().toString().length() > 0) {
                                                    namelist.set(getAdapterPosition(),tv_title.getText().toString().trim());
                                                    FragmentRearrangeList.columnsData.get(getAdapterPosition()).put("title",tv_title.getText().toString().trim());
                                                    //FragmentRearrangeList.myHeaderList.set(getAdapterPosition(),tv_title.getText().toString());
                                                    notifyDataSetChanged();
                                                }
                                                dialog.dismiss();
                                            }

                                        }

                            )
                            .

                                    onNegative(new MaterialDialog.SingleButtonCallback() {
                                                   @Override
                                                   public void onClick(MaterialDialog dialog, DialogAction which) {
                                                       dialog.dismiss();
                                                   }
                                               }

                                    )
                            .

                                    show();
                }
            });

        }

    }

    public void removeHeader(int position) {
        FragmentRearrangeList.mygrandList.remove(position);
        namelist.remove(position);
        FragmentRearrangeList.columnsData.remove(position);
        notifyItemRemoved(position);
    }
    public void addItem(String newTitle){
        namelist.add(newTitle);
        notifyDataSetChanged();
    }
}
