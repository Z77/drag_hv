package com.draghv.com;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 12-Jan-17.
 */
public class BoardList extends AppCompatActivity {
    List<BoardListPOJO> boardList;
    List<GetBoardList> FB_boardList;
    BoardListAdapter boardListAdapter;
    ListView lv_boardnames;
    FloatingActionButton addBoardfab;
    ZboardList zboardList;
    List<HashMap<String, Object>> boardDataList;
    ArrayList<HashMap<String,Object>> boardColsData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boardlist);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        lv_boardnames = (ListView) findViewById(R.id.boardList);
        addBoardfab = (FloatingActionButton) findViewById(R.id.fabAddBoard);
        boardList = new ArrayList<>();
        boardColsData=new ArrayList<>();

//        boardList.add(new BoardListPOJO("Sales", true));
//        boardList.add(new BoardListPOJO("Press", false));
        loadData();
        boardListAdapter = new BoardListAdapter(BoardList.this, boardList);
        lv_boardnames.setAdapter(boardListAdapter);
        lv_boardnames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("type", FB_boardList.get(position).getTitle());
                i.putExtra("bid", FB_boardList.get(position).getObjectId());
                Log.i("TAG", "header list:" + FB_boardList.get(position).getTotalcol());
                i.putExtra("tcol", String.valueOf(FB_boardList.get(position).getTotalcol().size()));
                i.putExtra("collist", FB_boardList.get(position).getTotalcol());
                i.putExtra("otherdata",boardColsData);
                i.putExtra("trow", String.valueOf(FB_boardList.get(position).getTotalrow()));
                i.putExtra("edit", true);
                startActivity(i);
//                } else {
//                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                    i.putExtra("type", "Press");
//                    startActivity(i);
//                }
            }
        });
        addBoardfab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialDialog createBoard = new MaterialDialog.Builder(BoardList.this)
                        .title("Add board")
                        .customView(R.layout.prompt_rename_header, true)
                        .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                        .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                        .titleColorRes(R.color.colorPrimary)
                        .positiveText("Create").positiveColor(ContextCompat.getColor(BoardList.this, R.color.colorPrimary))
                        .negativeText("Cancel").negativeColor(ContextCompat.getColor(BoardList.this, R.color.colorPrimary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                //TextInputEditText promptTitle = (TextInputEditText) dialog.findViewById(R.id.tv_list_title);
                                EditText promptTitle = (EditText) dialog.findViewById(R.id.tv_list_title);
                                final String name = promptTitle.getText().toString().trim();
                                final String boardid = zboardList.save();
                                zboardList.setTitle(name);
                                zboardList.setUserid(getString(R.string.userid));
                                final ArrayList<HashMap<String, Object>> headernames = (ArrayList<HashMap<String, Object>>) createHeaderlist();
                                zboardList.setTotalCol(headernames);
                                //zboardList.setTotalRow(0);
                                zboardList.saveboardWithListener(new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                        Toast.makeText(getApplicationContext(), "Data saved", Toast.LENGTH_SHORT).show();
                                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                        i.putExtra("type", name);
                                        i.putExtra("bname", name);
                                        i.putExtra("bid", boardid);
                                        i.putExtra("collist", headernames);
                                        i.putExtra("otherdata",boardColsData);
                                        i.putExtra("tcol", String.valueOf(headernames.size()));
                                        i.putExtra("trow", String.valueOf(0));
                                        startActivity(i);
                                    }
                                });


                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                EditText promptTitle = (EditText) createBoard.findViewById(R.id.tv_list_title);
                promptTitle.setHint("Board name");
            }
        });

        // adDataBoard1();

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    public void loadData() {

        zboardList = new ZboardList();
        FB_boardList = new ArrayList<>();
        zboardList.getROOT().child(getString(R.string.userid)).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (boardList.size() > 0) {
                    boardList.clear();
                }
                if(boardColsData.size()>0) {
                    boardColsData.clear();
                }
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    Log.i("TAG", "datasnap:" + dataSnapshot1);
                    GetBoardList board_List = dataSnapshot1.getValue(GetBoardList.class);
                    boardList.add(new BoardListPOJO(board_List.getTitle(), false));
                    boardListAdapter.notifyDataSetChanged();
                    Log.i("TAG", "board data:" + board_List.getTitle());
                    Log.i("TAG", "board header" + board_List.getTotalcol());
                    FB_boardList.add(board_List);
                    HashMap<String,Object> hashMap=new HashMap<String, Object>();
                    hashMap.put("bid",board_List.getObjectId());
                    hashMap.put("name",board_List.getTitle());
                    hashMap.put("coldata",board_List.getTotalcol());
                    boardColsData.add(hashMap);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public HashMap<String, Object> getHashmap(String title) {
        HashMap<String, Object> newHashmap = new HashMap<>();
        newHashmap.put("title", title);
        newHashmap.put("id", createUniqueid());
        return newHashmap;
    }
    public List<HashMap<String,Object>> createHeaderlist(){
        List<HashMap<String,Object>> parent=new ArrayList<>();
        parent.add(getHashmap("To do"));
        parent.add(getHashmap("In progress"));
        parent.add(getHashmap("Done"));
        return parent;
    }

    public void addDataBoard1() {
        boardDataList = new ArrayList<>();
        HashMap<String, Object> subData = new HashMap<>();
        subData.put("boardname", "Sales");
        subData.put("boardteamperson", 5);

        List<HashMap<String, Object>> columnDataList = new ArrayList<>();

        HashMap<String, Object> columndata = new HashMap<>();
        columndata.put("c_status", "Pending enquiry");
        List<HashMap<String, Object>> carddata = new ArrayList<>();
        carddata.add(getCardData("Mr.Kalam-9877223233", 2, 0, 2, 5, "15 Jan,07:50PM", null, true));
        carddata.add(getCardData("Mr.Kola", 1, 0, 2, 5, "15 Jan,07:50PM", null, true));
        carddata.add(getCardData("Mr.Goel", 6, 0, 2, 5, "15 Jan,07:50PM", null, true));
        carddata.add(getCardData("Miss Radhika", 6, 0, 2, 5, "15 Jan,07:50PM", null, true));
        columndata.put("card:", carddata);
        columnDataList.add(columndata);
//                            HashMap<String,Object> data=new HashMap<>();
//                            data.put("card_title","Mr.Kalam-9877223233");
//                            data.put("card_item_attachcnt","2");
//                            data.put("card_img_attachcnt",2);
//                            data.put("card_comment","2");
//                            data.put("card_team",5);
//                            data.put("card_dueDate","15 jan,07:50PM");
//                            data.put("card_color","Red");
//                            data.put("card_assign",true);
//                            carddata.add(data);

//                            HashMap<String,Object> data2=new HashMap<>();
//                            data.put("card_title","Mr.Kalam-9877223233");
//                            data.put("card_item_attachcnt","2");
//                            data.put("card_img_attachcnt",2);
//                            data.put("card_comment","2");
//                            data.put("card_team",5);
//                            data.put("card_dueDate","15 jan,07:50PM");
//                            data.put("card_color","Red");
//                            data.put("card_assign",true);
//                            carddata.add(data2);

//                            HashMap<String,Object> data3=new HashMap<>();
//                            data.put("card_title","Mr.Kalam-9877223233");
//                            data.put("card_item_attachcnt","2");
//                            data.put("card_img_attachcnt",2);
//                            data.put("card_comment","2");
//                            data.put("card_team",5);
//                            data.put("card_dueDate","15 jan,07:50PM");
//                            data.put("card_color","Red");
//                            data.put("card_assign",true);
//                            carddata.add(data3);
//
//                            HashMap<String,Object> data4=new HashMap<>();
//                            data.put("card_title","Mr.Kalam-9877223233");
//                            data.put("card_item_attachcnt","2");
//                            data.put("card_img_attachcnt",2);
//                            data.put("card_comment","2");
//                            data.put("card_team",5);
//                            data.put("card_dueDate","15 jan,07:50PM");
//                            data.put("card_color","Red");
//                            data.put("card_assign",true);
//                            carddata.add(data4);
//
//                            HashMap<String,Object> data5=new HashMap<>();
//                            data.put("card_title","Mr.Kalam-9877223233");
//                            data.put("card_item_attachcnt","2");
//                            data.put("card_img_attachcnt",2);
//                            data.put("card_comment","2");
//                            data.put("card_team",5);
//                            data.put("card_dueDate","15 jan,07:50PM");
//                            data.put("card_color","Red");
//                            data.put("card_assign",true);
//                            carddata.add(data5);

        HashMap<String, Object> columndata2 = new HashMap<>();
        columndata2.put("c_status", "Today Meeting");
        List<HashMap<String, Object>> carddata2 = new ArrayList<>();
        carddata2.add(getCardData("Mr.Kumar-9877223233", 2, 0, 2, 5, "15 Jan,07:50PM", "Green", true));
        carddata2.add(getCardData("Mr.Kishan-97777662233", 1, 0, 2, 5, "12 Jan,07:50PM", "Red", true));
        carddata2.add(getCardData("Ar.Amit-8772626233", 6, 0, 2, 5, "15 Jan,07:50PM", null, true));
        carddata2.add(getCardData("Mr.Manas-9877223233", 2, 0, 2, 5, "15 Jan,07:50PM", "Green", true));
        carddata2.add(getCardData("Mr.John-97777662233", 1, 0, 2, 5, "12 Jan,07:50PM", "Red", true));
        columndata.put("card:", carddata2);
        columnDataList.add(columndata2);

        HashMap<String, Object> columndata3 = new HashMap<>();
        columndata3.put("c_status", "customer hold");
        List<HashMap<String, Object>> carddata3 = new ArrayList<>();
        carddata3.add(getCardData("Mr.Ram-9877223233", 2, 0, 2, 5, "15 Jan,07:50PM", null, true));
        carddata3.add(getCardData("Mr.Krish-9877223233", 1, 0, 2, 5, "16 Jan,07:50PM", null, true));
        carddata3.add(getCardData("Ar.Manu-8772626233", 6, 0, 2, 5, "15 Jan,07:50PM", null, true));
        carddata3.add(getCardData("Mr.Manas-9877223233", 2, 0, 2, 5, "15 Jan,07:50PM", null, true));
        carddata3.add(getCardData("Mr.Heman-97777662233", 2, 0, 2, 5, "15 Jan,07:50PM", null, true));
        columndata.put("card:", carddata3);
        columnDataList.add(columndata3);

        HashMap<String, Object> columndata4 = new HashMap<>();
        columndata4.put("c_status", "Order Closed");
        List<HashMap<String, Object>> carddata4 = new ArrayList<>();
        carddata4.add(getCardData("Mr.karan-9877223233", 2, 0, 2, 5, null, null, true));
        carddata4.add(getCardData("Mr.kumar-97777662233", 1, 0, 2, 5, null, null, true));
        carddata4.add(getCardData("Ar.raman-8772626233", 6, 0, 2, 5, null, null, true));
        carddata4.add(getCardData("Miss.preethi-8454323456", 6, 0, 2, 5, null, null, true));
        columndata.put("card:", carddata4);
        columnDataList.add(columndata4);

        HashMap<String, Object> columndata5 = new HashMap<>();
        columndata5.put("c_status", "Order Lost");
        List<HashMap<String, Object>> carddata5 = new ArrayList<>();
        carddata5.add(getCardData("Mr.rajan-9877223233", 2, 0, 2, 5, null, null, true));
        carddata5.add(getCardData("Mr.kulpreeth-97777662233", 1, 0, 2, 5, null, null, true));
        carddata5.add(getCardData("Ar.Ram-8772626233", 6, 0, 2, 5, null, null, true));
        carddata5.add(getCardData("Miss.Mohini-8454323456", 6, 0, 2, 5, null, null, true));
        columndata.put("card:", carddata5);
        columnDataList.add(columndata5);

        subData.put("column", columnDataList);
        boardDataList.add(subData);
        Log.i("TAG", "josn =>" + boardDataList);
    }

    public HashMap<String, Object> getCardData(String title, int itemAttcnt, int imageAttcnt, int commentcnt, int shareTeam, String dueDate, String color, Boolean isAssign) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("card_title", title);
        data.put("card_item_attachcnt", itemAttcnt);
        data.put("card_img_attachcnt", imageAttcnt);
        data.put("card_comment", commentcnt);
        data.put("card_team", shareTeam);
        data.put("card_dueDate", dueDate);
        data.put("card_color", color);
        data.put("card_assign", isAssign);
        return data;
    }

    //for crateing the boardid
    public static String createUniqueid() {
        String mainString = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String randomString;
        int len = 10;
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < 10; i++) {
            sb.append(mainString.charAt(rnd.nextInt(mainString.length())));
        }
        Log.i("TAG", "Created_unique_id::" + sb);
        return sb.toString();
    }
}
