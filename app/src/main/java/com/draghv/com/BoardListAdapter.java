package com.draghv.com;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Payal on 12-Jan-17.
 */
public class BoardListAdapter extends BaseAdapter {
    Context context;
    List<BoardListPOJO> boardListPOJOs;

    public BoardListAdapter(Context context, List<BoardListPOJO> boardListPOJOs) {
        this.context = context;
        this.boardListPOJOs = boardListPOJOs;
    }

    @Override
    public int getCount() {
        if(boardListPOJOs!=null&&boardListPOJOs.size()>0)
            return boardListPOJOs.size();
        else
            return 0;

    }

    @Override
    public Object getItem(int position) {
        return boardListPOJOs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            LayoutInflater infla = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infla.inflate(R.layout.boardlist_row, parent, false);
            holder = new ViewHolder();
            holder.tv_boardName=(TextView)convertView.findViewById(R.id.tv_boardname);
            holder.star=(ImageView)convertView.findViewById(R.id.iv_favorite);
            convertView.setTag(holder);
        }else{
            holder=(ViewHolder)convertView.getTag();
        }
        final BoardListPOJO data=boardListPOJOs.get(position);
        holder.tv_boardName.setText(data.getBoardname());
        if(data.getFavorite()){
            holder.star.setImageResource(R.drawable.ic_yellowstar_fill);
        }else{
            holder.star.setImageResource(R.drawable.ic_yellowstar_outline);
        }
        holder.star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(data.getFavorite()) {
                    data.setFavorite(false);
                    notifyDataSetChanged();
                }else {
                    data.setFavorite(true);
                    notifyDataSetChanged();
                }
            }
        });

        return convertView;
    }
    public class ViewHolder{
        TextView tv_boardName;
        ImageView star;
    }
}
