package com.draghv.com;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;

/**
 * Created by Payal on 27-Feb-17.
 */
public class GetBoardComment {

    DatabaseReference firebase;
    String userId;
    String comment;
    Long createdAt;
    String objectId;

    public GetBoardComment() {
        firebase = FirebaseDatabase.getInstance().getReference().child("board_comment");
    }

    public DatabaseReference getRoot() {
        return firebase;
    }

    public DatabaseReference getCardCommentRoot(String cardid) {
        return firebase.child(cardid);
    }

    public DatabaseReference.CompletionListener save(String cardid,DatabaseReference.CompletionListener listener){
        HashMap<String ,Object> data=getCommentJSON();
        String key=firebase.child(cardid).push().getKey();
        data.put("objectId", key);
        firebase.child(cardid).child(key).setValue(data,listener);
        return listener;
    }

    public String getUserId() {
        return userId;
    }

    public String getComment() {
        return comment;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public HashMap<String, Object> getCommentJSON() {
        HashMap<String, Object> mapdata = new HashMap<>();
        mapdata.put("userId", this.userId);
        mapdata.put("comment", this.comment);
        mapdata.put("createdAt", ServerValue.TIMESTAMP);
        return mapdata;
    }
}
