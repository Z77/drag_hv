package com.draghv.com;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextPaint;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.draghv.com.Adapter.ArchivedCardAdapter;
import com.draghv.com.Adapter.MyAdapter;
import com.draghv.com.Models.MyItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.tangxiaolv.telegramgallery.TL.Photo;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.Inflater;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Payal on 15-Feb-17.
 */
public class ActivityArchiveCard extends AppCompatActivity implements SearchView.OnQueryTextListener {
    ArchivedCardAdapter adapter;
    List<MyItem> datalist;
    List<GetBoardContent> cardDataList;
    RecyclerView cards_rv;
    String boardname;
    TextView toolbarTitle;
    List<HashMap<String, Object>> columndata;
    TextView tv_nodata;
    SearchView searchView = null;
    HashMap<String, Object> unarchiveData;
    Boolean is_write;

    Zboard_archive zboard_archive;
    Zboard board;
    Zboard_history history;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

        setContentView(R.layout.activity_archivecards);
        Bundle b = getIntent().getExtras();
        boardname = (String) b.get("bname");
        is_write = b.getBoolean("iswrite");

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(boardname);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        columndata = (List<HashMap<String, Object>>) b.get("coldata");

        zboard_archive = new Zboard_archive();
        board = new Zboard();
        history=new Zboard_history();
        cardDataList = new ArrayList<>();
        datalist = new ArrayList<>();
        cards_rv = (RecyclerView) findViewById(R.id.archived_rv);
        RecyclerView.LayoutManager llm = new LinearLayoutManager(ActivityArchiveCard.this);//default it will use vertical orientation
        cards_rv.setLayoutManager(llm);
        adapter = new ArchivedCardAdapter(datalist, ActivityArchiveCard.this, columndata, is_write);
        tv_nodata = (TextView) findViewById(R.id.nodata);
//        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
//        llm.setOrientation(LinearLayoutManager.VERTICAL);
//        cards_rv.setLayoutManager(llm);
        cards_rv.setAdapter(adapter);
        loadData();
//        cards_rv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
//        cards_rv.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
//            @Override
//            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
//                //final int checkedCount = cards_rv.getCheckedItemCount();
//                //mode.setTitle(checkedCount + " Selected");
//                //Toast.makeText(getApplicationContext(),"count:"+cards_rv.getCheckedItemCount(),Toast.LENGTH_SHORT).show();
//                //mode.setTitle(cards_rv.getCheckedItemCount() + " Selected");
//                adapter.toggleSelection(position);
//            }
//
//            @Override
//            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
//                mode.getMenuInflater().inflate(R.menu.menu_done, menu);
//                return true;
//            }
//
//            @Override
//            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
//                return false;
//            }
//
//            @Override
//            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
//                return true;
//            }
//
//            @Override
//            public void onDestroyActionMode(ActionMode mode) {
//                adapter.removeSelection();
//            }
//        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_archive_search, menu);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search2));
        searchView.setQueryHint("Enter key word...");
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        adapter.filter(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.filter(newText);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void loadData() {
        zboard_archive.getBOARD_ARCHIVEDATA(MainActivity.BOARD_ID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (cardDataList.size() > 0)
                    cardDataList.clear();
                if (datalist.size() > 0) {
                    datalist.clear();
                }
                adapter.notifyDataSetChanged();
                if (dataSnapshot.getChildrenCount() > 0) {
                    tv_nodata.setVisibility(View.GONE);
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        GetBoardContent content = data.getValue(GetBoardContent.class);
                        cardDataList.add(content);
                        MyItem card = new MyItem();
                        card.setData(content.getTitle());
                        card.setDesc(content.getDescription());
                        card.setItemAttach(content.getArrItems());
                        card.setImageAttach(content.getAttachment());
                        card.setChecklist(content.getChecklist());
                        card.setAssign(content.getAssign());
                        if (content.getCommentCnt() != null)
                            card.setCommentCnt(content.getCommentCnt());
                        else
                            card.setCommentCnt(0L);
                        card.setObjectId(content.getObjectId());
                        card.setdueDate(content.getDuedate());
                        card.setColorcodes(content.getlabel());
                        card.setComment(content.getComment());
                        card.setColumnid(content.getColumnId());
                        datalist.add(card);
                    }
                    adapter.notifyDataSetChanged();
                    adapter.setFreshDataInCopy(datalist);
                } else {
                    tv_nodata.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

//    public void showDialog(){
//
//        MaterialDialog unarchive=new MaterialDialog.Builder(ActivityArchiveCard.this)
//                .title("Unarchive")
//                .customView(R.layout.prompt_move_cards,true)
//                .positiveText("Unarchive")
//                .negativeText("Cancel")
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(MaterialDialog dialog, DialogAction which) {
//
//                    }
//                })
//                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(MaterialDialog dialog, DialogAction which) {
//
//                    }
//                })
//                .show();
//
//    }

    public void unarchive(final int pos) {
        if (is_write) {
            Boolean isColAvailable = false;
            String columnName = null;
            for (int i = 0; i < columndata.size(); i++) {
                if (cardDataList.get(pos).getColumnId().equals(columndata.get(i).get("id"))) {
                    isColAvailable = true;
                    columnName = (String) columndata.get(i).get("title");
                    unarchiveData = datalist.get(pos).getCardJSON(columndata.get(i).get("id").toString(), cardDataList.get(pos).getRow());
                    break;
                }
            }
            Log.i("TAG", "unarchive data json:" + unarchiveData);
            if (isColAvailable) {
                final MaterialDialog unarchive = new MaterialDialog.Builder(ActivityArchiveCard.this)
                        .title("Unarchive").titleColor(ContextCompat.getColor(ActivityArchiveCard.this, R.color.colorPrimary))
                        .content("This card will be moved in column " + columnName)
                        .positiveText("Unarchive").positiveColor(ContextCompat.getColor(ActivityArchiveCard.this, R.color.colorPrimary))
                        .negativeText("Cacel").negativeColor(ContextCompat.getColor(ActivityArchiveCard.this, R.color.colorPrimary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(final MaterialDialog dialog, DialogAction which) {
                                Log.i("TAG", "col is available so unarchive card:");
                                zboard_archive.deleteCardInArchie(MainActivity.BOARD_ID, cardDataList.get(pos).getObjectId(), new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                        // Toast.makeText(ActivityArchiveCard.this, "Unarchive successfully...", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                board.setCardid(MainActivity.BOARD_ID);
                                unarchiveData.put("objectId", board.cardid);
                                new addCardinBoard().execute(dialog);
                                Log.i("TAG", "unarchive card data:" + unarchiveData);
                                addHistory(MainActivity.ownerId, board.cardid, unarchiveData.get("title").toString(),MainActivity.BOARD_ID);
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();

            } else {
                showMoveDialog(pos);
                Log.i("TAG", "no col available put in another col or board");
            }
        }
//        else{
//            Toast.makeText(getApplicationContext(),"no write permission,contact board admin",Toast.LENGTH_SHORT).show();
//        }
        //Toast.makeText(getApplicationContext(),"Unarchive"+pos,Toast.LENGTH_SHORT).show();
    }

    private class addCardinBoard extends AsyncTask<MaterialDialog, Void, Void> {
        ProgressDialog p = new ProgressDialog(ActivityArchiveCard.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            p.setMessage("Please wait");
            p.setCanceledOnTouchOutside(false);
            p.show();
        }

        @Override
        protected Void doInBackground(final MaterialDialog... params) {
            board.addCardInBoard(MainActivity.BOARD_ID, unarchiveData, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    //Toast.makeText(ActivityArchiveCard.this, "Unarchive successfully...", Toast.LENGTH_SHORT).show();
//                                        Toast.makeText(ActivityArchiveCard.this, "Card added back to board", Toast.LENGTH_SHORT).show();
                    params[0].dismiss();
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (p.isShowing())
                p.dismiss();

            Toast.makeText(ActivityArchiveCard.this, "Unarchive successfully...", Toast.LENGTH_SHORT).show();
        }

        //        board.addCardInBoard(MainActivity.BOARD_ID, unarchiveData, new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                Toast.makeText(ActivityArchiveCard.this, "Unarchive successfully...", Toast.LENGTH_SHORT).show();
////                                        Toast.makeText(ActivityArchiveCard.this, "Card added back to board", Toast.LENGTH_SHORT).show();
//                dialog.dismiss();
//            }
//        });


    }

    public void showMoveDialog(final int pos) {
        Spinner spncol;
        final List<String> collist = new ArrayList<>();
        MaterialDialog move = new MaterialDialog.Builder(ActivityArchiveCard.this)
                .title("Unarchive").titleColor(ContextCompat.getColor(ActivityArchiveCard.this, R.color.colorPrimary))
                .customView(R.layout.prompt_unarchive_anothercol, true)
                .positiveText("Unarchive").positiveColor(ContextCompat.getColor(ActivityArchiveCard.this, R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(ActivityArchiveCard.this, R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        Spinner spncol = (Spinner) dialog.findViewById(R.id.spn_newcol);
                        unarchiveData = datalist.get(pos).getCardJSON(columndata.get(spncol.getSelectedItemPosition()).get("id").toString(), cardDataList.get(pos).getRow());
                        String colid = (String) columndata.get(spncol.getSelectedItemPosition()).get("id");
                        board.setCardid(MainActivity.BOARD_ID);
                        unarchiveData.put("objectId", board.cardid);
                        unarchiveData.put("columnId", colid);
                        board.addCardInBoard(MainActivity.BOARD_ID, unarchiveData, null);
                        zboard_archive.deleteCardInArchie(MainActivity.BOARD_ID, cardDataList.get(pos).getObjectId(), new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                Toast.makeText(ActivityArchiveCard.this, "Unarchive successfully...", Toast.LENGTH_SHORT).show();
                            }
                        });
                        addHistory(MainActivity.ownerId, board.cardid, unarchiveData.get("title").toString(), MainActivity.BOARD_ID);
                        Toast.makeText(getApplicationContext(), "" + spncol.getSelectedItem(), Toast.LENGTH_SHORT).show();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {

                    }
                })
                .show();
        spncol = (Spinner) move.findViewById(R.id.spn_newcol);
        for (int i = 0; i < columndata.size(); i++) {
            collist.add(columndata.get(i).get("title").toString());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ActivityArchiveCard.this, android.R.layout.simple_spinner_dropdown_item, collist);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spncol.setAdapter(adapter);

    }

    private void addHistory(String userid,String cardid,String cardtitle,String boardid){
        HashMap<String,Object> map=history.getJSON_Unarchive(userid,cardid,cardtitle);
        history.addHistory(boardid, map, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                //Toast.makeText(getApplicationContext(), "History added", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

