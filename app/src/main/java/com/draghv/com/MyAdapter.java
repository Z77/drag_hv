package com.draghv.com;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.woxthebox.draglistview.DragItemAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Payal on 28-Dec-16.
 */
public class MyAdapter extends DragItemAdapter<MyItem, MyAdapter.ViewHolder> {
    private int mLayoutId; //layout file where we design our item row
    private int mGrabHandleId; //title/header view
    private boolean mDragOnLongPress;
    List<MyItem> myItemList; //our data source
    List<MyItem> myItemListCopy = new ArrayList<>();
    Context mcontext;

    public MyAdapter() {
    }

    public MyAdapter(Context context1, List<MyItem> list, int mLayoutId, int GrabHandleId, boolean DragOnLongPress) {
        this.mLayoutId = mLayoutId;
        this.mGrabHandleId = GrabHandleId;
        this.mDragOnLongPress = DragOnLongPress;
        this.myItemList = list;
        mcontext = context1;
        this.myItemListCopy.addAll(myItemList);
        setHasStableIds(true);
        setItemList(list);
//        Log.i("DRAGE", "list is argument:" + list);
//        Log.i("DRAGE", "list is variable:" + myItemList);
//        Log.i("DRAGE", "list is view:" + mLayoutId);
//        Log.i("DRAGE", "list is grabhandle id:" + mGrabHandleId);
//        Log.i("DRAGE", "list is drag on long press:" + mDragOnLongPress);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        //ViewHolder viewholder= new ViewHolder(view);
//        Log.i("DRAGE","view is:"+view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        MyItem data = mItemList.get(position);
//        Log.i("TAG","items are:"+data.getData());


        //holder.msg_text.setText(data.getData());
//        if(mItemList.get(position).getimage_attach()>0) {
//            Log.i("DRAGE", "text is in if:"+position+" "+ mItemList.get(position).getData());
//            Log.i("DRAGE", "image is in if:" + position + " " + mItemList.get(position).getimage_attach());
//            holder.msg_image_attach.setImageResource(data.getimage_attach());
//            holder.msg_image_attach.setVisibility(View.VISIBLE);
//        }else {
//            Log.i("DRAGE", "text is in else:"+position+" "+ mItemList.get(position).getData());
//            Log.i("DRAGE", "image is in else:" + position + " " + mItemList.get(position).getimage_attach());
//            holder.msg_image_attach.setImageResource(0);
//            holder.msg_image_attach.setVisibility(View.GONE);
//        }
        //holder.msg_image.setImageResource(R.drawable.cookie);
//        Log.i("TAG","item is in adapter:"+mItemList.get(position).getItemid());
        holder.msg_text.setText(mItemList.get(position).getData());
        holder.itemView.setTag(mItemList.get(position).getItemid());
        if (mItemList.get(position).getCreatedDate() != null) {
            holder.msg_time.setVisibility(View.VISIBLE);
            holder.msg_time.setText(mItemList.get(position).getCreatedDate());
        } else {
            holder.msg_time.setVisibility(View.GONE);
        }
        BitmapDrawable drawable = (BitmapDrawable) mcontext.getResources().getDrawable(R.drawable.profile);
        if (position == 0)
            drawable = (BitmapDrawable) mcontext.getResources().getDrawable(R.drawable.profile);
        if (position == 1)
            drawable = (BitmapDrawable) mcontext.getResources().getDrawable(R.drawable.profile1);
        if (position == 2)
            drawable = (BitmapDrawable) mcontext.getResources().getDrawable(R.drawable.profile2);
        RoundedBitmapDrawable drawable_ = RoundedBitmapDrawableFactory.create(mcontext.getResources(), drawable.getBitmap());
        drawable_.setCircular(true);
        holder.msg_owner.setImageDrawable(drawable_);

        if (data.getAttachimageAvailale() && data.getAttachitemAvailale()) {
            holder.imageItemContainer.setVisibility(View.VISIBLE); //image item container
            holder.attchimage.setVisibility(View.VISIBLE);
            holder.attchitem.setVisibility(View.VISIBLE);
            holder.msg_image_attach.setVisibility(View.VISIBLE); //visible imagecaptue image view
            holder.imageattach_cnt.setVisibility(View.VISIBLE); //visible images attach count textview

            holder.msg_image_attach.setImageResource(data.getimage_attach());
            if (String.valueOf(data.getImageAttachCnt()).length() == 1) {
                holder.imageattach_cnt.setText("0" + String.valueOf(data.getImageAttachCnt()));
            } else {
                holder.imageattach_cnt.setText(String.valueOf(data.getImageAttachCnt()));
            }

            holder.msg_item_attach.setVisibility(View.VISIBLE); //visible item attached image view
            holder.itemattach_cnt.setVisibility(View.VISIBLE); //visible item attach count text view
            if (String.valueOf(data.getItemAttachCnt()).length() == 1) {
                holder.itemattach_cnt.setText("0" + String.valueOf(data.getItemAttachCnt()));
            } else {
                holder.itemattach_cnt.setText(String.valueOf(data.getItemAttachCnt()));
            }

        } else {
            if (data.getAttachimageAvailale()) {
                holder.imageItemContainer.setVisibility(View.VISIBLE);
                holder.msg_image_attach.setVisibility(View.VISIBLE);
                holder.imageattach_cnt.setVisibility(View.VISIBLE);
                holder.attchimage.setVisibility(View.VISIBLE);
                holder.attchitem.setVisibility(View.GONE);
                holder.msg_item_attach.setVisibility(View.GONE);
                holder.itemattach_cnt.setVisibility(View.GONE);

                holder.msg_image_attach.setImageResource(data.getimage_attach());
                if (String.valueOf(data.getImageAttachCnt()).length() == 1) {
                    holder.imageattach_cnt.setText("0" + String.valueOf(data.getImageAttachCnt()));
                } else {
                    holder.imageattach_cnt.setText(String.valueOf(data.getImageAttachCnt()));
                }
            }
            if (data.getAttachitemAvailale()) {
                holder.imageItemContainer.setVisibility(View.VISIBLE);
                holder.msg_item_attach.setVisibility(View.VISIBLE);
                holder.itemattach_cnt.setVisibility(View.VISIBLE);
                holder.attchimage.setVisibility(View.GONE);
                holder.attchitem.setVisibility(View.VISIBLE);
                holder.msg_image_attach.setVisibility(View.GONE);
                holder.imageattach_cnt.setVisibility(View.GONE);

                if (String.valueOf(data.getItemAttachCnt()).length() == 1) {
                    holder.itemattach_cnt.setText("0" + String.valueOf(data.getItemAttachCnt()));
                } else {
                    holder.itemattach_cnt.setText(String.valueOf(data.getItemAttachCnt()));
                }
            }
            if (!data.getAttachitemAvailale() && !data.getAttachimageAvailale()) {
                holder.imageItemContainer.setVisibility(View.GONE);
            }
        }

        List<String> colorcodes = data.getColorcodes();
        if (colorcodes != null) {
            if (colorcodes.contains("red"))
                holder.Red.setVisibility(View.VISIBLE);
            if (colorcodes.contains("yellow"))
                holder.Yellow.setVisibility(View.VISIBLE);
            if (colorcodes.contains("pink"))
                holder.Pink.setVisibility(View.VISIBLE);
            if (colorcodes.contains("green"))
                holder.Sky.setVisibility(View.VISIBLE);
        }
        if (data.getMessagecnt() != null) {
            holder.msg_cnt.setText(String.valueOf(data.getMessagecnt()));
        }
        if (data.getSharePersoncnt() != null) {
            holder.person_cnt.setText(String.valueOf(data.getSharePersoncnt()));
        }

    }

    @Override
    public long getItemId(int position) {
        //return super.getItemId(position);
        //return myItemList.get(position).getItemid();
        return mItemList.get(position).getItemid();
    }

    public class ViewHolder extends DragItemAdapter.ViewHolder {
        TextView msg_text;
        FrameLayout attchimage, attchitem;
        ImageView msg_image_attach;
        ImageView msg_item_attach;
        TextView imageattach_cnt;
        TextView itemattach_cnt;
        TextView msg_time;
        ImageView msg_owner;
        TextView msg_cnt;
        TextView person_cnt;
        RelativeLayout imageItemContainer;

        View Red, Yellow, Pink, Sky;

        public ViewHolder(View itemView) {
            super(itemView, mGrabHandleId, mDragOnLongPress);
            msg_text = (TextView) itemView.findViewById(R.id.item_data);
            msg_image_attach = (ImageView) itemView.findViewById(R.id.item_image);
            msg_item_attach = (ImageView) itemView.findViewById(R.id.item_image2);
            msg_time = (TextView) itemView.findViewById(R.id.tv_createdate);
            msg_owner = (ImageView) itemView.findViewById(R.id.user_profile);
            msg_cnt = (TextView) itemView.findViewById(R.id.tv_totalmessage);
            person_cnt = (TextView) itemView.findViewById(R.id.tv_totalperson);
            imageattach_cnt = (TextView) itemView.findViewById(R.id.tv_totalimage_cnt);
            itemattach_cnt = (TextView) itemView.findViewById(R.id.tv_totalimage_cnt2);
            attchimage = (FrameLayout) itemView.findViewById(R.id.attach1);
            attchitem = (FrameLayout) itemView.findViewById(R.id.attach_item2);
            Red = (View) itemView.findViewById(R.id.barRed);
            Yellow = (View) itemView.findViewById(R.id.barYellow);
            Pink = (View) itemView.findViewById(R.id.barPink);
            Sky = (View) itemView.findViewById(R.id.barSky);
            imageItemContainer = (RelativeLayout) itemView.findViewById(R.id.imagecontainer);
        }

        @Override
        public void onItemClicked(View view) {
//            Toast.makeText(view.getContext(), "Item clicked", Toast.LENGTH_SHORT).show();

//            ItemDetail newFragment = new ItemDetail();//not used in switchContent function
//            switchFragment(newFragment, mItemList.get(getAdapterPosition()), getAdapterPosition());
        }

//        //not in use now
//        public void switchFragment(Fragment newFragment, MyItem myItem, int row) {
//            if (mcontext == null)
//                return;
//            if (mcontext instanceof MainActivity) {
//                MainActivity feeds = (MainActivity) mcontext;
//                feeds.switchContent(newFragment, mItemList, myItem, row);
//            }
//            if (mcontext instanceof NewBoard) {
//                NewBoard feeds = (NewBoard) mcontext;
//                feeds.switchContent(newFragment, mItemList, myItem, row);
//            }
//        }

        @Override
        public boolean onItemLongClicked(View view) {
            Toast.makeText(view.getContext(), "Item long clicked", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    public void filter(String text) {
        mItemList.clear();
        if (text.isEmpty()) {
            mItemList.addAll(myItemListCopy);
        } else {
            text = text.toLowerCase();
            for (MyItem item : myItemListCopy) {
                if (item.getData().toLowerCase().contains(text)) {
//                    Log.i("TAG","searched item added"+item.getData());
                    mItemList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }
    public void setFreshDataInCopy(List<MyItem> myItemListCopyFresh){
        myItemListCopy.clear();
        myItemListCopy.addAll(myItemListCopyFresh);
    }

    public void addNewItemInCopyList(MyItem item) {
        myItemListCopy.add(item);
    }

    public void changeData(String title, int position) {
        Log.i("TAG", "pos is in adapter" + position);
        MyItem item = myItemList.get(position);
        Log.i("TAG", "pos item is:" + item);
        item.setData(title);
        mItemList.set(position, item);
        myItemList.set(position, item);
        myItemListCopy.set(position, item);
        notifyDataSetChanged();
    }
}
