package com.draghv.com;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 16-Feb-17.
 */
public class GetBoardHistory {
    String userId;
    String cardId;
    String cardTitle;
    String columnId;
    String columnTitle;
    String columnoldTitle;
    Long createdAt;
    String cardoldTitle;
    List<String> assign;
    List<String>member;
    List<String> label;
    HashMap<String,Object> duedate;
    List<String> oldTitle;
    List<String> labelTitle;
    String action;
    String attach_type;
    String checked;
    int moved;
    String boardId;
    String boardName;
    String businessname;
    String itemcount;
    Boolean comment;



    String fromColId;
    String toColId;

    public String getUserId() {
        return userId;
    }

    public String getCardId() {
        return cardId;
    }

    public String getCardTitle() {
        return cardTitle;
    }

    public String getColumnId() {
        return columnId;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public List<String> getAssign() {
        return assign;
    }

    public HashMap<String, Object> getDuedate() {
        return duedate;
    }

    public String getAction() {
        return action;
    }

    public String getFromColId() {
        return fromColId;
    }

    public String getToColId() {
        return toColId;
    }

    public List<String> getMember() {
        return member;
    }

    public String getCardoldTitle() {
        return cardoldTitle;
    }

    public List<String> getLabel() {
        return label;
    }

    public List<String> getOldTitle() {
        return oldTitle;
    }

    public List<String> getLabelTitle() {
        return labelTitle;
    }

    public String getAttach_type() {
        return attach_type;
    }

    public String getChecked() {
        return checked;
    }

    public String getColumnTitle() {
        return columnTitle;
    }

    public int getMoved() {
        return moved;
    }

    public String getBoardId() {
        return boardId;
    }

    public String getBoardName() {
        return boardName;
    }

    public String getColumnoldTitle() {
        return columnoldTitle;
    }

    public String getBusinessname() {
        return businessname;
    }

    public String getItemcount() {
        return itemcount;
    }

    public Boolean getComment() {
        return comment;
    }
}
