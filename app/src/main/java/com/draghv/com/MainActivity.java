package com.draghv.com;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.draghv.com.Adapter.MyAdapter;
import com.draghv.com.Fragment.FragmentLables;
import com.draghv.com.Fragment.FragmentMember;
import com.draghv.com.Fragment.FragmentRearrangeList;
import com.draghv.com.Fragment.FragmentShareBoard;
import com.draghv.com.Fragment.MyBoardlist;
import com.draghv.com.Helper.RecyclerItemClickListener;
import com.draghv.com.Models.MyItem;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.woxthebox.draglistview.BoardView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static long sCreatedItems = 1;
    public static String type;
    public static MyItem clickeditem_data;
    public static Zboard BOARD_;
    public static String BOARD_ID;
    static private BoardView mBoardView;
    private int mColumns;
    public static List<MyAdapter> adapterList;
    static ArrayList<GetBoardContent> cardData;
    static ArrayList<HashMap<String, Object>> columnsdata;
    public Boolean fromShare = false, is_write = false;
    public static String ownerId;

    ArrayList<String> headerList, memberlist;
    ArrayList<List<MyItem>> grandList;
    List<String> boardList = new ArrayList<>();
    LinearLayout pager_indicator;
    int NUM_PAGES, prevIndex = 0;
    TextView[] dots;
    List<HashMap<String, Long>> indicatorPosList;

    Zboard_history BOARD_HISTORY;
    ZAuth zAuth;

    Boolean isSyncing = false, IS_EDIT = false;
    ArrayList<HashMap<String, Object>> clrlabelList;
    ArrayList<String> sharedUser = new ArrayList<>();
    HashMap<String, Object> sharedUserData;
    AppBarLayout toolbar_cont;
    TextView toolbarTitle;
    SearchView searchView = null;
    Boolean first = true;//never try to convert it static it cause crashes
    static Boolean isChanged = true;

    List<HashMap<String, Object>> otherboardData, otherboardColor;
    ValueEventListener valueEventListener;
    ValueEventListener colValueListener;
    ValueEventListener sharedWithListener;
    ChildEventListener childListener;
    //String ownerId;
    Zboard_archive zboard_archive;
    ProgressBar progressBar;
    BottomSheetLayout bottomSheetLayout;
    String userid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
        setContentView(R.layout.activity_main);

        type = getIntent().getStringExtra("type");
        BOARD_ID = getIntent().getStringExtra("bid");
        IS_EDIT = getIntent().getBooleanExtra("edit", false);
        //TOTAL_COL = Integer.parseInt(getIntent().getStringExtra("tcol"));
        // TOTAL_ROW = Integer.parseInt(getIntent().getStringExtra("trow"));
        //ownerId = getIntent().getStringExtra("uid");
        //Log.i("TAG", "total row is:" + TOTAL_ROW + " total col is:" + TOTAL_COL);
        Log.i("TAG", "board id is:" + BOARD_ID);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(type);
        setSupportActionBar(toolbar);

        bottomSheetLayout = (BottomSheetLayout) findViewById(R.id.bottomsheet);
        bottomSheetLayout.setPeekOnDismiss(true);

        //toolbarTitle.setText("My Dashboard");
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        BOARD_ = new Zboard();
        BOARD_HISTORY = new Zboard_history();
        zboard_archive = new Zboard_archive();
        zAuth = new ZAuth();
        cardData = new ArrayList<>();
        headerList = new ArrayList<>();
        grandList = new ArrayList<>();
        adapterList = new ArrayList<>();
        indicatorPosList = new ArrayList<>();
        sharedUserData = new HashMap<>();

        userid = zAuth.get();
        if (cardData.size() > 0)
            cardData.clear();

        Bundle b = getIntent().getExtras();
        if (b.get("collist") != null) {
            columnsdata = (ArrayList<HashMap<String, Object>>) b.get("collist");
            Log.i("TAG", "headings data:" + columnsdata);
//            for (int i = 0; i < columnsdata.size(); i++) {
//                TEMP_HEADERLIST.add(columnsdata.get(i).get("title").toString());
//            }
        }

        if (b.get("owner") != null) {
            ownerId = b.getString("owner");
            Log.i("TAG", "owner is not showing own board:" + ownerId);
        } else {
            ownerId = zAuth.get();
            Log.i("TAG", "user is showing own board" + ownerId);
        }
        if (b.get("fromshare") != null) {
            fromShare = b.getBoolean("fromshare");
        }
        if (b.get("otherdata") != null) {
            otherboardData = (List<HashMap<String, Object>>) b.get("otherdata");
        }
        if (b.get("othercolor") != null) {
            otherboardColor = (List<HashMap<String, Object>>) b.get("othercolor");
        }
        if (b.get("clrlbl") != null) {
            clrlabelList = (ArrayList<HashMap<String, Object>>) b.get("clrlbl");
            Log.i("TAG", "color list:" + clrlabelList);
        }
        if (b.get("member") != null) {
            memberlist = (ArrayList<String>) b.get("member");
        }
        if (b.get("sharewith") != null) {
            sharedUserData = (HashMap<String, Object>) b.get("sharewith");
            for (String id : sharedUserData.keySet()) {
                sharedUser.add(id);
            }
            //sharedUser=b.get("sharewith");
            Log.i("TAG", "shared peoples:" + sharedUser);
        }
        if (b.get("write") != null) {
            is_write = b.getBoolean("write");
        }
        Log.i("TAG", "is_write is:" + is_write);

        mBoardView = (BoardView) findViewById(R.id.board_view);
        mBoardView.setSnapToColumnsWhenScrolling(true);
        mBoardView.setSnapToColumnWhenDragging(true);
        mBoardView.setSnapDragItemToTouch(true);
        pager_indicator = (LinearLayout) findViewById(R.id.indicator);
        toolbar_cont = (AppBarLayout) findViewById(R.id.toolbar_container);
        progressBar = (ProgressBar) findViewById(R.id.pbar);

        if (IS_EDIT) {
            Log.i("TAG", "in is edite:");
//            BOARD_.getBOARD_DATA(BOARD_ID).addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//
//                    for (int i = 0; i < TOTAL_COL; i++) {
//                        ArrayList<MyItem> mItemArray = new ArrayList<>();
//                        for (int j = 0; j < TOTAL_ROW; j++) {
//                            for (DataSnapshot contentData : dataSnapshot.getChildren()) {
//                                GetBoardContent content = contentData.getValue(GetBoardContent.class);
//                                Log.i("TAG", "colum id:" + content.getColumnId() + " we get:" + columnsdata.get(i).get("id"));
//                                if (content.getRow() == j && content.getColumnId().equals(columnsdata.get(i).get("id"))) {
//                                    //cardData.add(content);
//                                    Log.i("TAG", "the data is in column " + i + " row " + j + " " + content.getTitle());
//                                    mItemArray.add(new MyItem(sCreatedItems++, content.getObjectId(), content.getTitle(), R.drawable.design, null, 2L, 5L, false, false, null, 0L, 2L));
////                                mItemArray.add(new MyItem(sCreatedItems++, "Mr.Kola-97777662233", R.drawable.design, null, 2L, 5L, false, false, null, 0L, 1L));
//                                } else {
//                                    Log.i("TAG", "data not matched");
//                                }
//                            }
//                        }
//                        addColumn(mItemArray, TEMP_HEADERLIST.get(i));
//                        Log.i("TAG", "the data of new col");
//                    }
//                    setPageViewController();
//                    setClickListner();
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });
            if (grandList.size() > 0)
                grandList.clear();
            for (int i = 0; i < columnsdata.size(); i++) {
                ArrayList<MyItem> mItemArray = new ArrayList<>();
                grandList.add(mItemArray);
                //addColumn(mItemArray, (String) columnsdata.get(i).get("title"));
            }
            Log.i("TAG", "in is edit gradlist size:" + grandList.size());
            /*setPageViewController();
            setClickListner();*/

        } else {
            for (int i = 0; i < 3; i++) {
                ArrayList<MyItem> myItemList = new ArrayList<>();
                grandList.add(myItemList);
//                if (i == 0) {
//                    addColumn(myItemList, "To do");
//                }
//                if (i == 1) {
//                    addColumn(myItemList, "Doing");
//                }
//                if (i == 2) {
//                    addColumn(myItemList, "Done");
//                }
                //TOTAL_COL = headerList.size();
            }
            setPageViewController();
            setClickListner();
        }

        //for columns data
        final Query qer_coldata;
        if (fromShare) {
            qer_coldata = BOARD_.getROOT().child(ownerId).child(BOARD_ID).child("totalcol");
        } else {
            qer_coldata = BOARD_.getROOT().child(userid).child(BOARD_ID).child("totalcol");
        }
        colValueListener = qer_coldata.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("TAG", "board data :" + BOARD_ID + "=>" + dataSnapshot);
                Log.i("TAG", "when column data change ischanged value:" + isChanged);
                List<HashMap<String, Object>> columndata = (List<HashMap<String, Object>>) dataSnapshot.getValue();
                Log.i("TAG", "column data arrived is:" + columndata.size());
                if (isChanged) {

                    if (columnsdata.size() != columndata.size()) {
                        columnsdata = (ArrayList<HashMap<String, Object>>) columndata;
                        mBoardView.clearBoard();
                        Toast.makeText(getApplicationContext(), "size is not same:", Toast.LENGTH_SHORT).show();
                        headerList = new ArrayList<>();
                        adapterList = new ArrayList<>();
                        mColumns = 0;
                        Log.i("TAG", "grand list size:" + grandList.size());
                        Log.i("TAG", "columns data list size:" + columnsdata.size());
                        while (grandList.size() != columnsdata.size()) {
                            List<MyItem> blank = new ArrayList<>();
                            grandList.add(blank);
                        }
                        Log.i("TAG", "after adding grand list size:" + grandList.size());
                        Log.i("TAG", "after adding columns data list size:" + columnsdata.size());
                        //first add columns
                        for (int i = 0; i < columnsdata.size(); i++) {
                            try {
                                Log.i("TAG", "grand list data :" + grandList.get(i));
                                ArrayList<MyItem> myItemList = (ArrayList<MyItem>) grandList.get(i);
                                addColumn(myItemList, (String) columnsdata.get(i).get("title"));
                            } catch (Exception e) {
                                Log.i("TAG", "error when movelist:" + e);
                            }
                        }
                        //and then add that data for search
                        for (int i = 0; i < cardData.size(); i++) {
                            for (int j = 0; j < columnsdata.size(); j++) {
                                adapterList.get(j).clearCopyList();//becoz other wise it showing 2 2 items
                                if (cardData.get(i).getColumnId().equals(columnsdata.get(j).get("id"))) {
                                    Boolean auto_duedate = (Boolean) columnsdata.get(j).get("autoduedate");
                                    int assigns = 0;
                                    Long comment = 0L;
                                    if (cardData.get(i).getCommentCnt() != null)
                                        comment = cardData.get(i).getCommentCnt();

                                    MyItem newitem = new MyItem(sCreatedItems++, cardData.get(i).getObjectId(), cardData.get(i).getTitle(), "", cardData.get(i).getDuedate(), comment, cardData.get(i).getlabel(), cardData.get(i).getAttachment(), cardData.get(i).getArrItems(), cardData.get(i).getChecklist(), cardData.get(i).getComment(), cardData.get(i).getAssign(), auto_duedate);
                                    adapterList.get(j).addNewItemInCopyList(newitem);//for searching fucntionality
                                    TextView itemCount = (TextView) mBoardView.getHeaderView(j).findViewById(R.id.total_view);
                                    itemCount.setText(Integer.toString(mBoardView.getAdapter(j).getItemCount()));
                                }
                            }
                        }
                        //pd.hide();
                    } else {
                        Boolean changed = false;
                        Toast.makeText(getApplicationContext(), "column data is changed", Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < columnsdata.size(); i++) {
//                        if (adapterList != null && adapterList.size() > 0)
//                            adapterList.get(i).notifyDataSetChanged();
                            if (!columnsdata.get(i).equals(columndata.get(i))) {
                                changed = true;
                                Toast.makeText(getApplicationContext(), "column data is changed true", Toast.LENGTH_SHORT).show();
                                break;
                            }
                        }
                        //when only index is changed then or auto due date is changed
                        if (changed) {
//                        ProgressDialog pd = new ProgressDialog(getApplicationContext());
//                        pd.setMessage("Column is swaping...");
//                        pd.show();
//                        pd.setCanceledOnTouchOutside(false);
                            columnsdata = (ArrayList<HashMap<String, Object>>) columndata;
                            mBoardView.clearBoard();
                            grandList = new ArrayList<>();
                            headerList = new ArrayList<>();
                            adapterList = new ArrayList<>();
                            mColumns = 0;
                            for (int i = 0; i < columnsdata.size(); i++) {
                                ArrayList<MyItem> myItemList = new ArrayList<>();
                                grandList.add(myItemList);
                                addColumn(myItemList, (String) columnsdata.get(i).get("title"));
                            }
                            for (int i = 0; i < cardData.size(); i++) {

                                for (int j = 0; j < columnsdata.size(); j++) {
                                    // adapterList.get(j).clearCopyList();//becoz other wise it showing 2 2 items
                                    if (cardData.get(i).getColumnId().equals(columnsdata.get(j).get("id"))) {
                                        Boolean autoduedate = (Boolean) columnsdata.get(j).get("autoduedate");
                                        int assign = 0;
                                        Long comment = 0L;

                                        if (cardData.get(i).getCommentCnt() != null)
                                            comment = cardData.get(i).getCommentCnt();
                                        MyItem newitem = new MyItem(sCreatedItems++, cardData.get(i).getObjectId(), cardData.get(i).getTitle(), cardData.get(i).getDescription(), cardData.get(i).getDuedate(), comment, cardData.get(i).getlabel(), cardData.get(i).getAttachment(), cardData.get(i).getArrItems(), cardData.get(i).getChecklist(), cardData.get(i).getComment(), cardData.get(i).getAssign(), autoduedate);
                                        grandList.get(j).add(newitem);//it will automatic fill boardlist becoz it contains columnwise list
                                        //mBoardView.addItem(j, cardData.get(i).getRow(), newitem, false);
                                        adapterList.get(j).addNewItemInCopyList(newitem);//for searching fucntionality

                                        TextView itemCount = (TextView) mBoardView.getHeaderView(j).findViewById(R.id.total_view);
                                        itemCount.setText(Integer.toString(mBoardView.getAdapter(j).getItemCount()));
                                    }
                                }
                            }
                            Log.i("TAG", "grand list size:" + grandList.size());
                            Log.i("TAG", "adapter list size:" + adapterList.size());
                            for (int i = 0; i < adapterList.size(); i++) {
                                adapterList.get(i).setFreshDataInCopy(grandList.get(i));
                            }
                            setClickListner();
                            setPageViewController();
                            //pd.hide();
                        }
                    }
                } else {
                    isChanged = true;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        for (int i = 0; i < columnsdata.size(); i++) {
            childListener = BOARD_.getShortUserBoard(ownerId, BOARD_ID).child(columnsdata.get(i).get("id").toString()).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    //add card time
                    Log.i("TAG", "datasnap of new list ChildAdded:" + dataSnapshot);
                    BOARD_.getBOARD_DATA(BOARD_ID).child(dataSnapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Log.i("TAG", "datasnap of new card data:: " + dataSnapshot);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    //drag but only for column inside not from one column to another mean it'll change value of "row" only
                    Log.i("TAG", "datasnap of new list ChildChanged:" + dataSnapshot);
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
//        for(int i=0;i<columnsdata.size();i++){
//            Log.i("TAG","adapter is:"+mBoardView.getAdapter(i));
//        }

        Query dataquery = BOARD_.getBOARD_DATA(BOARD_ID);
        Log.i("TAG", "Data query is:" + dataquery);

        valueEventListener = dataquery.orderByChild("row").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                Toast.makeText(MainActivity.this, "New data is arrived", Toast.LENGTH_SHORT).show();

                Log.i("TAG", "datasnapshot when value changed*:" + dataSnapshot);
                Log.i("TAG", "datasnapshot when value changed is chaneds value:" + isChanged);
                if (isChanged) {
                    if (cardData.size() > 0)
                        cardData.clear();

                /*if any card change column so delete from existing column */
                    if (!first) {
                        List<HashMap<String, Object>> mapList = new ArrayList<>();
                        for (int i = 0; i < columnsdata.size(); i++) {
                            //if(mBoardView.getAdapter(i).getItemCount()!=0) {
                            int tot_row = mBoardView.getAdapter(i).getItemCount();
                            for (int j = 0; j < tot_row; j++) {
                                MyItem data = (MyItem) mBoardView.getAdapter(i).getItem(j);
                                for (DataSnapshot Data : dataSnapshot.getChildren()) {
                                    GetBoardContent newData = Data.getValue(GetBoardContent.class);
                                    if (data != null) {
                                        if (data.getObjectId().equals(newData.getObjectId())) {
                                            if (!columnsdata.get(i).get("id").equals(newData.getColumnId())) {
                                                //mBoardView.removeItem(i,j);
                                                HashMap<String, Object> data1 = new HashMap<String, Object>();
                                                data1.put("i", i);
                                                data1.put("j", j);
                                                mapList.add(data1);
                                            }
                                        }
                                    }
                                }
                            }
                            //}
                        }
                        if (mapList != null && mapList.size() > 0) {
                            for (int i = mapList.size() - 1; i >= 0; i--) {
                                int col = (int) mapList.get(i).get("i");
                                int row = (int) mapList.get(i).get("j");
                                MyItem oldData = (MyItem) mBoardView.getAdapter(col).getItem(row);
                                mBoardView.removeItem(oldData);
                                TextView itemCount = (TextView) mBoardView.getHeaderView(col).findViewById(R.id.total_view);
                                itemCount.setText(Integer.toString(mBoardView.getAdapter(col).getItemCount()));
                            }
                        }
//                    for (int i = 0; i < columnsdata.size(); i++) {
//                        int tot_row = mBoardView.getAdapter(i).getItemList().size();
//                        for (int j = 0; j < tot_row; j++) {
//                            MyItem item = (MyItem) mBoardView.getAdapter(i).getItem(j);
//                            for (int k = 0; k < temp.size(); k++) {
//                                GetBoardContent newData = temp.get(k);
//                                if (!newData.getColumnId().equals(columnsdata.get(i).get("id"))) {
//                                    //mBoardView.removeItem(i,j);
//                                    HashMap<String, Object> data = new HashMap<String, Object>();
//                                    data.put("i", i);
//                                    data.put("j", j);
//                                    mapList.add(data);
//                                }
//
//                            }
//                            Log.i("TAG", "old data is:" + item.getData());
////                            for (GetBoardContent newData : temp) {
////                                Log.i("TAG", "data is:" + item.getData() + " arrival data is" + newData.getTitle());
////                                Log.i("TAG", "data id is:" + item.getObjectId() + " arrival data id is" + newData.getObjectId());
////                                if (!item.getObjectId().equals(newData.getObjectId()) && !item.data.equals(newData.getTitle()))
////                                    mBoardView.removeItem(i, j);
////                            }
//                        }
//                    }

                    }

                   /* add item in board for that check if we got col and row at that pos if already any item is there?
                     if yes check if that item object id and new arrived data object id is same if no then item is added by copy,move,moveall option
                    other wise same data so replace it b'coz it may be possible that data has been changed.*/
                    //new AddCardInBoard().execute(dataSnapshot);
//                    ProgressDialog p=new ProgressDialog(MainActivity.this);
//                    p.setMessage("Please wait");
//                    p.show();
//                    grandList=new ArrayList<>();
//                    for (int i = 0; i < columnsdata.size(); i++) {
//                        List<MyItem> blank=new ArrayList<>();
//                        grandList.add(blank);
//                        if(mBoardView.getAdapter(i)!=null)
//                            mBoardView.getAdapter(i).getItemList().clear();
//                    }
//                    if(dataSnapshot.getChildrenCount()>0){
//                        for(DataSnapshot data:dataSnapshot.getChildren()){
//                            GetBoardContent boardcontent = data.getValue(GetBoardContent.class);
//                            for (int i = 0; i < columnsdata.size(); i++) {
//                                if (boardcontent.getColumnId().equals(columnsdata.get(i).get("id"))) {
//                                    Boolean auto_duedate = (Boolean) columnsdata.get(i).get("autoduedate");
//                                    Long comment = 0L;
//                                    if (boardcontent.getCommentCnt() != null)
//                                        comment = boardcontent.getCommentCnt();
//                                    MyItem newitem = new MyItem(sCreatedItems++, boardcontent.getObjectId(), boardcontent.getTitle(), boardcontent.getDescription(), boardcontent.getDuedate(), comment, boardcontent.getlabel(), boardcontent.getAttachment(), boardcontent.getArrItems(), boardcontent.getChecklist(), boardcontent.getComment(), boardcontent.getAssign(), auto_duedate);
//                                    grandList.get(i).add(newitem);
//                                    mBoardView.addItem(i, newitem, false);
//                                    Log.i("TAG","data:"+newitem.getData()+" col:"+i);
//                                }
//                            }
//                        }
//                    }
//                    p.dismiss();

                    if (dataSnapshot.getChildrenCount() > 0) {

                        for (DataSnapshot data : dataSnapshot.getChildren()) {
                            GetBoardContent boardcontent = data.getValue(GetBoardContent.class);
                            cardData.add(boardcontent);
                            MyItem olddata = null;
                            for (int i = 0; i < columnsdata.size(); i++) {
                                if (boardcontent.getColumnId().equals(columnsdata.get(i).get("id"))) {
                                    Boolean auto_duedate = (Boolean) columnsdata.get(i).get("autoduedate");
                                    if (mBoardView.getAdapter(i) != null) {
                                        Log.i("TAG", "has item at colm " + i + "row " + boardcontent.getRow() + "=>" + mBoardView.hasItem(i, boardcontent.getRow()));
//                                        if (mBoardView.hasItem(i, boardcontent.getRow())) {
//                                            MyItem p = (MyItem) mBoardView.getAdapter(i).getItem(boardcontent.getRow());
//                                            Log.i("TAG", "has item that item title is:" + p.getData());
//                                            if(p.getObjectId().equals(boardcontent.getObjectId())){
//                                                Log.i("TAG", "has item that item replace it:" + boardcontent.getTitle());
//                                            }else{
//                                                Log.i("TAG", "has item but deff id item add it:" + boardcontent.getTitle());
//                                            }
//                                        } else {
//                                            Log.i("TAG", "has item no data in row add it" + boardcontent.getTitle());
//                                        }
//                                        int PAyal = 0;
//                                        for (Object exsiting : mBoardView.getAdapter(i).getItemList()) {
//                                            olddata = (MyItem) exsiting;
//                                            if (olddata != null) {
//                                                break;
//                                            }
//                                            PAyal++;
//                                            olddata = (MyItem) mBoardView.getAdapter(i).getItem(boardcontent.getRow());
//                                        }
//                                        Log.i("TAG","old data index:"+PAyal);
                                        olddata = (MyItem) mBoardView.getAdapter(i).getItem(boardcontent.getRow());
                                        // Log.i("TAG","old data title:"+olddata.getData()+" new title:"+boardcontent.getTitle());
                                        // Log.i("TAG","old data objectID:"+olddata.getObjectId()+" new objectId:"+boardcontent.getObjectId());
                                    }
                                    if (olddata != null) {
                                        Long comment = 0L;
                                        if (olddata.getCommentCnt() != null)
                                            comment = olddata.getCommentCnt();

                                        MyItem newitem = new MyItem(sCreatedItems++, boardcontent.getObjectId(), boardcontent.getTitle(), boardcontent.getDescription(), boardcontent.getDuedate(), comment, boardcontent.getlabel(), boardcontent.getAttachment(), boardcontent.getArrItems(), boardcontent.getChecklist(), boardcontent.getComment(), boardcontent.getAssign(), auto_duedate);
//                                        Log.i("TAG", "*old title is:" + olddata.getData() + " id=>" + olddata.getObjectId());
//                                        Log.i("TAG", "*new title is:" + boardcontent.getTitle() + " id=>" + boardcontent.getObjectId());
                                        Log.i("TAG", "object id =>" + olddata.getObjectId() + " new data object id =>" + boardcontent.getObjectId());
//                                        Boolean found = false;
//                                        int index = boardcontent.getRow();
//                                        for (Object exsiting : mBoardView.getAdapter(i).getItemList()) {
//                                            MyItem old = (MyItem) exsiting;
//                                            if (old.getObjectId().equals(boardcontent.getObjectId())) {
//                                                found = true;
//                                                index = mBoardView.getAdapter(i).getItemList().indexOf(old);
//                                                break;
//                                            }
//                                        }
//                                        if (found) {
//
//                                            // Log.i("TAG", "==old item index is:" + index);
//                                            mBoardView.replaceItem(i, boardcontent.getRow(), newitem, false);
//                                            Log.i("TAG", "==replace it bcoz already has that item column " + i + "=>" + boardcontent.getTitle() + " at row from firebase:" + boardcontent.getRow());// + " we get row from adapter" + index);
//                                        } else {
//                                            int row = mBoardView.getAdapter(i).getItemCount();
//                                            // Log.i("TAG", "==board view getapdater item count=>" + row);
//                                            //grandList.get(i).add(newitem);
//                                            //mBoardView.addItem(i, row, newitem, false);
//                                            mBoardView.addItem(i, boardcontent.getRow(), newitem, false);
//                                            adapterList.get(i).addNewItemInCopyList(newitem);//for search
//                                            Log.i("TAG", "==add it bcoz does not contains that item column " + i + "=>" + boardcontent.getTitle() + " at row from firebase:" + boardcontent.getRow());
//                                        }
                                        if (!olddata.getObjectId().equals(boardcontent.getObjectId())) {
//                                            List cardlist=mBoardView.getAdapter(i).getItemList();
//                                            Boolean found = false;
//                                            int index = boardcontent.getRow();
//                                            for(int x=0;x<cardlist.size();x++){
//                                                for(int y=x+1;y<cardlist.size();y++){
//                                                    if(cardlist.get(x).equals(cardlist.get(y))){
//                                                        Log.i("TAG","*item duplicate found index"+y);
//                                                        found = true;
//                                                        index = mBoardView.getAdapter(i).getItemList().indexOf(cardlist.get(y));
//                                                        break;
//                                                    }
//                                                }
//                                            }
                                            Boolean found = false;
                                            int index = boardcontent.getRow();
                                            List cardlist = mBoardView.getAdapter(i).getItemList();
                                            for (int p = 0; p < cardlist.size(); p++) {
                                                MyItem old = (MyItem) cardlist.get(p);
                                                if (old.getObjectId().equals(boardcontent.getObjectId())) {
                                                    found = true;
                                                    index = mBoardView.getAdapter(i).getItemList().indexOf(old);
                                                    Log.i("TAG", "*item removed title:" + old.getData());
                                                    break;
                                                }
                                            }
//                                            for (Object exsiting : mBoardView.getAdapter(i).getItemList()) {
//                                                MyItem old = (MyItem) exsiting;
//                                                if (old.getObjectId().equals(boardcontent.getObjectId())) {
//                                                    found = true;
//                                                    index = mBoardView.getAdapter(i).getItemList().indexOf(old);
//                                                    break;
//                                                }
//                                            }
                                            if (found) {
                                                Log.i("TAG", "*item is removed at index" + index);
                                                mBoardView.getAdapter(i).removeItem(index);
//                                                mBoardView.addItem(i, boardcontent.getRow(), newitem, false);
                                            }
                                            mBoardView.addItem(i, boardcontent.getRow(), newitem, false);
                                            adapterList.get(i).addNewItemInCopyList(newitem);//for search
                                            Log.i("TAG", "*item object id is not same add item in row=>" + boardcontent.getRow() + " title:" + newitem.getData());
                                        } else {
                                            //mBoardView.addItem(i, boardcontent.getRow(), newitem, false);
                                            mBoardView.replaceItem(i, boardcontent.getRow(), newitem, false);
                                            Log.i("TAG", "*item object id is same replace item in row=>" + boardcontent.getRow() + " title:" + newitem.getData());
                                        }
                                        TextView itemCount = (TextView) mBoardView.getHeaderView(i).findViewById(R.id.total_view);
                                        itemCount.setText(Integer.toString(mBoardView.getAdapter(i).getItemCount()));
                                        Log.i("TAG", "==old data is not null");
                                    } else {
                                        //this will execute first time when board is empty or old data is null
                                        int assign = 0;
                                        Long comment = 0L;

                                        if (boardcontent.getCommentCnt() != null)
                                            comment = boardcontent.getCommentCnt();
                                        MyItem newitem = new MyItem(sCreatedItems++, boardcontent.getObjectId(), boardcontent.getTitle(), boardcontent.getDescription(), boardcontent.getDuedate(), comment, boardcontent.getlabel(), boardcontent.getAttachment(), boardcontent.getArrItems(), boardcontent.getChecklist(), boardcontent.getComment(), boardcontent.getAssign(), auto_duedate);
                                        //mBoardView.removeItem(i,boardcontent.getRow());
                                        grandList.get(i).add(newitem);
                                        if (mBoardView.getAdapter(i) != null) {
                                            mBoardView.getAdapter(i).notifyDataSetChanged();
                                        }
                                        //mBoardView.addItem(i, boardcontent.getRow(), newitem, false);//28March for testing put in comment but it works fine becoz grandlist is already integrated with board so no need to add again it will genrate duplicate in board
                                        Log.i("TAG", "==old data is null add=>" + newitem.getData());
                                        //adapterList.get(i).addNewItemInCopyList(newitem);//for searching fucntionality
                                    }
                                    if (!first) {
                                        TextView itemCount = (TextView) mBoardView.getHeaderView(i).findViewById(R.id.total_view);
                                        itemCount.setText(Integer.toString(mBoardView.getAdapter(i).getItemCount()));

                                    }
                                    Log.i("TAG", "*row after adding card in column=> " + i + " total card=>" + mBoardView.getItemCount(i));
                                    Log.i("TAG", "*_________________________________________________________________________________________");

                                    break;
                                }

                            }
                        }
                    }

//                    List<GetBoardContent> newdataaa= cardData.stream().filter(getBoardContent->getBoardContent.getColumnId().contains(columnsdata.get(0).get("title").toString())).collect(Collectors.toList());
//                    Log.i("TAG","new card data of only column first:"+newdataaa);

                    //if board is empty then add columns with arrays
                    if (first) {
                        for (int i = 0; i < columnsdata.size(); i++) {
                            addColumn((ArrayList<MyItem>) grandList.get(i), columnsdata.get(i).get("title").toString());
                        }
                        setPageViewController();
                        setClickListner();
                        for (int i = 0; i < adapterList.size(); i++) {
                            MyAdapter adapter = adapterList.get(i);
                            adapter.setFreshDataInCopy(mBoardView.getAdapter(i).getItemList());
                        }
                    } else {
                        for (int i = 0; i < adapterList.size(); i++) {
                            MyAdapter adapter = adapterList.get(i);
                            adapter.setFreshDataInCopy(mBoardView.getAdapter(i).getItemList());
                        }
                    }

                    for (int i = 0; i < columnsdata.size(); i++) {
                        Log.i("TAG after card added", "column =>" + i);
                        Log.i("TAG after card added", "board col items after card added :=>" + mBoardView.getItemCount(i));
                    }

                    //for deleting perpose if card is deleted from firebase or it deleted by any user
                    if (!first) {
                        //deleting old item from board which is not now in firebase
                        List<HashMap<String, Object>> removeListmap = new ArrayList<>();
                        for (int col = 0; col < columnsdata.size(); col++) {
                            int totrow = mBoardView.getAdapter(col).getItemCount();
                            Log.i("TAG", "board row after archiving in col:" + col + " " + totrow);
                            for (int row = 0; row < totrow; row++) {
                                MyItem oldItem = (MyItem) mBoardView.getAdapter(col).getItem(row);
                                Boolean found = false;
                                for (DataSnapshot newData : dataSnapshot.getChildren()) {
                                    GetBoardContent newdata = newData.getValue(GetBoardContent.class);
                                    //it's wrong.aa khotu 6
//                                    if (newdata.getColumnId().equals(columnsdata.get(col).get("id")) && (newdata.getRow() == row)) {
//                                        found = true;
//                                    }
                                    if (newdata.getColumnId().equals(columnsdata.get(col).get("id")) && oldItem.getObjectId().equals(newdata.getObjectId())) {
                                        found = true;
                                    }
                                }
                                if (!found) {
                                    HashMap<String, Object> listdata = new HashMap<String, Object>();
                                    listdata.put("col", col);
                                    listdata.put("row", row);
                                    removeListmap.add(listdata);
                                }
                            }
                        }
                        Log.i("TAG", "remove data is:" + removeListmap);
                        if (removeListmap.size() > 0) {
                            for (int i = removeListmap.size() - 1; i >= 0; i--) {
                                int col = (int) removeListmap.get(i).get("col");
                                int row = (int) removeListmap.get(i).get("row");
                                MyItem notExistItem = (MyItem) mBoardView.getAdapter(col).getItem(row);
                                //Log.i("TAG", "delted card title is=>" + notExistItem.getData());
                                mBoardView.removeItem(notExistItem);
                                TextView itemCount = (TextView) mBoardView.getHeaderView(col).findViewById(R.id.total_view);
                                itemCount.setText(Integer.toString(mBoardView.getAdapter(col).getItemCount()));
                            }
                        }
                    }

                    for (int i = 0; i < adapterList.size(); i++) {
                        adapterList.get(i).clearCopyList();
                        adapterList.get(i).setFreshDataInCopy(grandList.get(i));
                    }
                    first = false;

                } else {
                    isChanged = true;
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        Query sharedWith;
        if (fromShare)
            sharedWith = BOARD_.getROOT().child(ownerId).child(BOARD_ID).child("sharedwith");
        else
            sharedWith = BOARD_.getROOT().child(userid).child(BOARD_ID).child("sharedwith");
        sharedWithListener = sharedWith.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    if (sharedUser.size() > 0)
                        sharedUser.clear();
                    HashMap<String, Object> hashMap = new HashMap<>();
                    for (DataSnapshot shareddata : dataSnapshot.getChildren()) {
                        sharedUser.add(shareddata.getKey());
                        hashMap.put(shareddata.getKey(), shareddata.getValue());
                        //sharedUserData= (HashMap<String, Object>) shareddata.getValue();
                        Log.i("TAG", "shared user data from listener:" + shareddata.getKey());
                    }
                    Log.i("TAG", "shared user data map:" + hashMap);
                    sharedUserData = hashMap;

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //mBoardView.setCustomDragItem(new MyDragItem(MainActivity.this, R.layout.item_view)); //for showing shadow behind card its optional
        if (!isSyncing) {
            if (is_write) {
                //mBoardView.
                mBoardView.setBoardListener(new BoardView.BoardListener() {
                    String cardtitle;
                    String cardid;

                    @Override
                    public void onItemDragStarted(int column, int row) {

                        pager_indicator.setAlpha(0);
                        toolbar_cont.animate().alpha(0).setDuration(650).start();
                        pager_indicator.animate().alpha(1).setDuration(500).start();

                        dots[column].setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.selected_ract));
                        Toast.makeText(mBoardView.getContext(), "Strt - column: " + column + " row: " + row, Toast.LENGTH_SHORT).show();
                        if (indicatorPosList.size() <= 0) {
                            for (TextView dot : dots) {
                                HashMap<String, Long> xymap = new HashMap<>();

                                int[] originalPos = new int[2];
                                dot.getLocationOnScreen(originalPos);
                                //or view.getLocationOnScreen(originalPos)
                                int x = originalPos[0];
                                int y = originalPos[1];
                                xymap.put("x", (long) x);
                                xymap.put("y", (long) y);
                                indicatorPosList.add(xymap);
                            }
                        }
                        cardtitle = ((MyItem) mBoardView.getAdapter(column).getItem(row)).getData();
                        cardid = ((MyItem) mBoardView.getAdapter(column).getItem(row)).getObjectId();
                    }

                    @Override
                    public void onItemChangedColumn(int oldColumn, int newColumn) {
                        TextView itemCount1 = (TextView) mBoardView.getHeaderView(oldColumn).findViewById(R.id.total_view);
                        String count = Integer.toString(mBoardView.getAdapter(oldColumn).getItemCount());
                        itemCount1.setText(count);
                        TextView itemCount2 = (TextView) mBoardView.getHeaderView(newColumn).findViewById(R.id.total_view);
                        String count2 = Integer.toString(mBoardView.getAdapter(newColumn).getItemCount());
                        itemCount2.setText(count2);
                        dots[oldColumn].setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.nonselected_ract));
                        dots[newColumn].setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.selected_ract));
                    }

                    @Override
                    public void onItemDragEnded(int fromColumn, int fromRow, int toColumn, int toRow) {

                        toolbar_cont.animate().alpha(1).start();
                        pager_indicator.animate().alpha(0).start();
                        pager_indicator.setAlpha(1);
                        dots[toColumn].setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.nonselected_ract));


                        if (fromColumn != toColumn || fromRow != toRow) {
                            //Toast.makeText(mBoardView.getContext(), "End - column: " + toColumn + " row: " + toRow, Toast.LENGTH_SHORT).show();
                            isSyncing = true;
                            isChanged = false;
                            if (CheckInternet()) {
                                if (fromColumn != toColumn) {
                                    Zboard_history zboard_history = new Zboard_history();
                                    HashMap<String, Object> dragCard = zboard_history.getJSON_DragCard(cardtitle, cardid, columnsdata.get(fromColumn).get("title").toString(), columnsdata.get(toColumn).get("title").toString(), columnsdata.get(toColumn).get("id").toString(), userid);
                                    zboard_history.addHistory(BOARD_ID, dragCard, null);
                                } else {
                                    //only row is changed column remains same
                                    MyItem item = (MyItem) mBoardView.getAdapter(fromColumn).getItemList().get(toRow);
                                    BOARD_.updateShort(BOARD_ID, item.getCardJSON(columnsdata.get(fromColumn).get("id").toString(), toRow), null);
                                }

                                ChageDataWhenDRAGE_OR_LISTOPTION(fromColumn, toColumn);
                            } else {
                                Toast.makeText(MainActivity.this, "No internet available", Toast.LENGTH_SHORT).show();
                            }

                            for (int i = 0; i < adapterList.size(); i++) {
                                MyAdapter adapter = adapterList.get(i);
                                adapter.setFreshDataInCopy(mBoardView.getAdapter(i).getItemList());
                            }
                        }
                    }

                    @Override
                    public void getXY(Float x, Float y) {
                        for (int i = 0; i < indicatorPosList.size(); i++) {
                            if ((x >= indicatorPosList.get(i).get("x") && x <= (indicatorPosList.get(i).get("x") + 30)) && ((y <= indicatorPosList.get(i).get("y") + 20 && y >= indicatorPosList.get(i).get("y") - 70)))
                                if (prevIndex != i) {
                                    prevIndex = i;
                                    mBoardView.scrollToColumn(i, true);
                                    Log.i("TAG", "my x and y is:" + x + " " + y);
                                }

                        }
                    }
                });
            } else {
                mBoardView.setDragEnabled(false);
                //Show_Toast(" Sorry,You dont have permission to write\n contact board admin", Toast.LENGTH_LONG);
            }
        } else {
            //Snackbar.make(null, "Wait while sync...", Snackbar.LENGTH_SHORT).show();
            Toast.makeText(getApplicationContext(), "Wait while sync...", Toast.LENGTH_SHORT).show();
        }
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();


    }

    public void Show_Toast(String str, int duration) {
        Toast.makeText(getApplicationContext(), str, duration).show();
    }

    private class AddCardInBoard extends AsyncTask<DataSnapshot, Void, List<List<MyItem>>> {
        ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Data updating,Please wait!!!");
            progressDialog.show();

            grandList = new ArrayList<>();
            for (int i = 0; i < columnsdata.size(); i++) {
                List<MyItem> blank = new ArrayList<>();
                grandList.add(blank);
                if (mBoardView.getAdapter(i) != null)
                    mBoardView.getAdapter(i).getItemList().clear();
            }
        }

        @Override
        protected List<List<MyItem>> doInBackground(DataSnapshot... params) {

            if (params[0].getChildrenCount() > 0) {
                for (DataSnapshot data : params[0].getChildren()) {
                    GetBoardContent boardcontent = data.getValue(GetBoardContent.class);
                    for (int i = 0; i < columnsdata.size(); i++) {
                        if (boardcontent.getColumnId().equals(columnsdata.get(i).get("id"))) {
                            Boolean auto_duedate = (Boolean) columnsdata.get(i).get("autoduedate");
                            Long comment = 0L;
                            if (boardcontent.getCommentCnt() != null)
                                comment = boardcontent.getCommentCnt();
                            MyItem newitem = new MyItem(sCreatedItems++, boardcontent.getObjectId(), boardcontent.getTitle(), boardcontent.getDescription(), boardcontent.getDuedate(), comment, boardcontent.getlabel(), boardcontent.getAttachment(), boardcontent.getArrItems(), boardcontent.getChecklist(), boardcontent.getComment(), boardcontent.getAssign(), auto_duedate);
                            grandList.get(i).add(newitem);
                            //mBoardView.addItem(i,newitem,false);
                            Log.i("TAG", "data:" + newitem.getData() + " col:" + i);
                        }
                    }
                }
            }

            return grandList;
        }

        @Override
        protected void onPostExecute(List<List<MyItem>> newlist) {
            //super.onPostExecute(newlist);
            Log.i("TAG", "size of col1 list:" + newlist.get(0).size());
            Log.i("TAG", "size of col2 list:" + newlist.get(1).size());

            for (int i = 0; i < newlist.size(); i++) {
                int total = newlist.get(i).size();
                //List<MyItem> items=newlist.get(i);
                for (int j = 0; j < newlist.get(i).size(); j++) {
                    if (total == j) {
                        break;
                    } else {
                        MyItem data = newlist.get(i).get(j);
                        mBoardView.addItem(i, data, false);
                    }
                    //Log.i("new data","new arrived data col:"+i+" row:"+j+" title:"+data.getData());
                }
                Log.i("new data", "************************************");
            }

            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    public void setClickListner() {
        for (int i = 0; i < mBoardView.getColumnCount(); i++) {
            final int j = i;
            mBoardView.getRecyclerView(i).addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), mBoardView.getRecyclerView(i), new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    clickeditem_data = (MyItem) mBoardView.getAdapter(j).getItemList().get(position);
                    Intent i = new Intent(getApplicationContext(), CardDetailActivity.class);
                    i.putExtra("col", String.valueOf(j));
                    i.putExtra("row", String.valueOf(position));
                    i.putExtra("from", "created");
                    i.putExtra("clr", clrlabelList);
                    i.putExtra("member", sharedUser);
                    i.putExtra("iswrite", is_write);
                    startActivity(i);
                    Toast.makeText(getApplicationContext(), "REcycler item clicked col " + j + " row: " + position, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onLongItemClick(View view, int position) {

                }
            }));
        }
    }

    //used in detail screen and update only that card
    public static void updateDataInFirebase(MyItem newItemdata, final int col, final int row) {
        String columnId = columnsdata.get(col).get("id").toString();
//        Log.i("TAG", "board view col id and name is:" + columnsdata.get(col).get("id") + " name:" + columnsdata.get(col).get("title"));
//        Log.i("TAG", "board row is:" + row);
//        for (int i = 0; i < mBoardView.getItemCount(); i++) {
//            Log.i("TAG", "card column is:" + cardData.get(i).getColumnId() + " col:" + columnsdata.get(col).get("id"));
//            Log.i("TAG", "card column row is:" + cardData.get(i).getRow() + " row is:" + row);
//            if (cardData.get(i).getColumnId().equals(columnsdata.get(col).get("id")) && cardData.get(i).getRow() == row) {
//                //cardId = cardData.get(i).getObjectId();
//                columnId = cardData.get(i).getColumnId();
//            }
//        }
        Zboard board = new Zboard();
        Log.i("TAG", "col total item:" + mBoardView.getAdapter(col).getItemCount() + "col index is:" + col);
        adapterList.get(col).notifyDataSetChanged();
        //for (int i = 0; i < mBoardView.getAdapter(col).getItemCount(); i++) {
        MyItem newItem = (MyItem) mBoardView.getAdapter(col).getItem(row);
        Log.i("TAG", "col data:" + newItem.getData());
        Log.i("TAG", "json is:" + newItem.getCardJSON(columnId, row));
        isChanged = false;
        board.updateCard(BOARD_ID, newItem.getObjectId(), newItem.getCardJSON(columnId, row), new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                Log.i("TAG", "card updated in firebase col=>" + col + " row=>" + row);
            }
        });
        //}
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.i("TAG", "key word is searched:" + query);
        for (int i = 0; i < mBoardView.getColumnCount(); i++) {
            adapterList.get(i).filter(query);
            Log.i("TAG", "total cards now is:" + adapterList.get(i).getItemCount());
            TextView itemCount1 = (TextView) mBoardView.getHeaderView(i).findViewById(R.id.total_view);
            itemCount1.setText(Integer.toString(mBoardView.getAdapter(i).getItemCount()));
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.i("TAG", "key word is searching:" + newText);
        for (int i = 0; i < mBoardView.getColumnCount(); i++) {
            adapterList.get(i).filter(newText);
            Log.i("TAG", "total cards now is:" + adapterList.get(i).getItemCount());
            TextView itemCount1 = (TextView) mBoardView.getHeaderView(i).findViewById(R.id.total_view);
            itemCount1.setText(Integer.toString(mBoardView.getAdapter(i).getItemCount()));
        }


        return true;
    }


    public void addColumn(final ArrayList<MyItem> mItemArray, String heading) {
        final List<MyItem> mItemArrays = mItemArray;
        final int column = mColumns;

        //grandList.add(mItemArray);
        final MyAdapter listAdapter = new MyAdapter(MainActivity.this, mItemArray, R.layout.item_view, R.id.item_layout, true);
        adapterList.add(listAdapter);
        for (int i = 0; i < mItemArrays.size(); i++) {
            listAdapter.addNewItemInCopyList(mItemArrays.get(i));
        }
        final View header = View.inflate(MainActivity.this, R.layout.header_view, null);
        final View addCard = View.inflate(MainActivity.this, R.layout.add_card_btn, null);
        final View blank = View.inflate(MainActivity.this, R.layout.blank_footer, null);

        if (!is_write)
            ((ImageView) header.findViewById(R.id.header_menu)).setVisibility(View.GONE);

        ((ImageView) header.findViewById(R.id.header_menu)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenuSheet(column, mItemArrays);
            }
        });
        ((Button) addCard.findViewById(R.id.btnAddCard)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_write) {
                    //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    showKeyboard();
                    mBoardView.setEnabled(false);
                    final MaterialDialog addCardDailog = new MaterialDialog.Builder(MainActivity.this)
                            .title("Add card")
                            .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                            .titleColorRes(R.color.colorPrimary)
                            .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                            .customView(R.layout.prompt_rename_header, true)
                            .positiveText("Ok").positiveColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                            .negativeText("Cancel").negativeColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                    final EditText tv_title = (EditText) dialog.findViewById(R.id.tv_list_title);
                                    if (tv_title.getText().toString().length() > 0) {
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat("dd MMM,hh:mma");
                                        String formattedDate = df.format(c.getTime());
                                        //String[] datepart = formattedDate.split(" ");
                                        //for  print date like formate of 6th 5th (postfix)
                                        //String newDate=datepart[0]+getDayOfMonthSuffix(Integer.parseInt(datepart[0]))+" "+datepart[1];
                                        //Log.i("TAG","new date is:"+newDate);
                                        String cardTitle = tv_title.getText().toString().trim();
                                        List<String> codeList = new ArrayList<>();
                                        codeList.add("red");
                                        codeList.add("sky");
                                        Log.i("TAG", "column is in add card:" + column);
                                        BOARD_.setCardid(BOARD_ID);
                                        Log.i("TAG", "card id is:" + BOARD_.cardid);
                                        isChanged = false;
                                        MyItem data = new MyItem(sCreatedItems++, BOARD_.cardid, cardTitle, "", null, 0L, null, null, null, null, null, null, false);
                                        mBoardView.addItem(column, mItemArrays.size(), data, false);
                                        mBoardView.scrollToItem(column, mItemArrays.size(), true);
                                        int indexOfAdapter = adapterList.indexOf(listAdapter);
                                        adapterList.get(indexOfAdapter).addNewItemInCopyList(data);//for searching fucntionality
                                        TextView itemCount = (TextView) mBoardView.getHeaderView(column).findViewById(R.id.total_view);
                                        itemCount.setText(Integer.toString(mBoardView.getAdapter(column).getItemCount()));
                                        hidekeyboard(MainActivity.this);
                                        //mBoardView.scrollToItem(column, mItemArrays.size() - 1, true);
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                //hidekeyboard(MainActivity.this);
                                                mBoardView.scrollToItem(column, mItemArrays.size() - 1, true);
                                            }
                                        }, 600);
                                        AddCardHistory(data, columnsdata.get(column).get("id").toString(), columnsdata.get(column).get("title").toString());
                                        dialog.dismiss();
                                        mBoardView.setEnabled(true);
                                        //BOARD_.setCardtitle(tv_title.getText().toString().trim());
                                        //BOARD_.setRow(mItemArrays.size());
                                        //BOARD_.setColumnId((String) columnsdata.get(column).get("id"));
                                        //BOARD_.setTotalCol((ArrayList<HashMap<String, Object>>) columnsdata);

                                        BOARD_.addCardInBoard(BOARD_ID, data.getCardJSON(columnsdata.get(column).get("id").toString(), mItemArrays.size() - 1), new DatabaseReference.CompletionListener() {
                                            @Override
                                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                                Toast.makeText(getApplication(), "Card saved", Toast.LENGTH_SHORT).show();

                                            }
                                        });

                                    } else {
                                        Toast.makeText(getApplicationContext(), "Card title can not be blank!!!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            hidekeyboard(MainActivity.this);
                                        }
                                    }, 300);
                                    dialog.dismiss();
                                    mBoardView.setEnabled(true);
                                }
                            })
                            .show();
                }
//                else {
//                    Toast.makeText(getApplicationContext(), "Sorry you don't have permission to write\n contact board admin!!!", Toast.LENGTH_LONG).show();
//                }
            }
        });

        ((TextView) header.findViewById(R.id.header_text)).setText(heading);

        ((TextView) header.findViewById(R.id.total_view)).setText(String.valueOf(mItemArray.size()));
//        header.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MyItem data=new MyItem(sCreatedItems++,"Call to Alex",0);
//                mBoardView.addItem(column, 0, data, true);
//            }
//        });
        //listAdapter.setItemList(mItemArray);
        String text = ((TextView) header.findViewById(R.id.header_text)).getText().toString();
        headerList.add(heading);
        if (is_write)
            mBoardView.addColumnList(listAdapter, header, addCard, false);
        else
            mBoardView.addColumnList(listAdapter, header, blank, false);

        mColumns++;


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchView.setQueryHint("Enter key word...");
        searchView.setOnQueryTextListener(this);
        return true;
    }

    private void changeFragment(int pos) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        mBoardView.setVisibility(View.GONE);
        switch (pos) {
            case 1:
                Log.i("TAG", "go to rearrange screen:" + headerList + " grand:" + grandList);
                transaction.replace(R.id.rearrange_container, FragmentRearrangeList.instantiate(grandList, headerList, columnsdata, (ArrayList<HashMap<String, Object>>) otherboardData), "FragmentRearrangeList").addToBackStack("FragmentRearrangeList").commit();
                toolbarTitle.setText("Rearrange List");
                break;
            case 2:
                transaction.replace(R.id.rearrange_container, FragmentLables.instantiate(clrlabelList), "FragmentLables").addToBackStack("FragmentLables").commit();
                toolbarTitle.setText("Edit Lables");
                break;
            case 3:
                transaction.replace(R.id.rearrange_container, FragmentMember.instantiate(memberlist), "FragmentMember").addToBackStack("FragmentMember").commit();
                toolbarTitle.setText("Add member");
                break;
            case 4:
                transaction.replace(R.id.rearrange_container, FragmentShareBoard.instantiate(sharedUser, sharedUserData), "FragmentShareBoard").addToBackStack("FragmentMember").commit();
                toolbarTitle.setText("Shared with");
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.menu_icon) {
            showMainSheetMenu();
            return true;
        }
//        if (id == R.id.action_rearrange) {
//            changeFragment(1);
//            return true;
//        }
//        if (id == R.id.actioncardlables) {
//            changeFragment(2);
//            return true;
//        }
//        if (id == R.id.actionmember) {
//            changeFragment(3);
//            return true;
//        }
//        if (id == R.id.actionhistory) {
//            //showMenuSheet(0);
//            Intent i = new Intent(getApplicationContext(), ActivityHistory.class);
//            i.putExtra("bname", type);
//            startActivity(i);
//            return true;
//        }
//        if (id == R.id.actionarchivecard) {
//            Intent i = new Intent(getApplicationContext(), ActivityArchiveCard.class);
//            i.putExtra("bname", type);
//            i.putExtra("coldata", columnsdata);
//            startActivity(i);
//            return true;
//        }
//        if (id == R.id.actionshareboard) {
//            changeFragment(4);
//        }
        if (id == android.R.id.home) {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            if (count != 0) {
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mBoardView.setVisibility(View.VISIBLE);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                toolbarTitle.setText(type);
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            if (fromShare) {
                BOARD_.getBOARD_DATA_ROOT().child(ownerId).child(BOARD_ID).removeEventListener(valueEventListener);
                BOARD_.getROOT().child(ownerId).child(BOARD_ID).child("totalcol").removeEventListener(colValueListener);
                BOARD_.getROOT().child(ownerId).child(BOARD_ID).child("sharedwith").removeEventListener(sharedWithListener);
            } else {
                BOARD_.getBOARD_DATA(BOARD_ID).removeEventListener(valueEventListener);
                BOARD_.getBOARD_ROOT(BOARD_ID).child("totalcol").removeEventListener(colValueListener);
                BOARD_.getROOT().child(userid).child(BOARD_ID).child("sharedwith").removeEventListener(sharedWithListener);
            }
            isChanged = true;
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            mBoardView.setVisibility(View.VISIBLE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            toolbarTitle.setText(type);
        }
    }

    String getDayOfMonthSuffix(final int n) {
//        if(n >= 1 && n <= 31){
//            return "";
//        }
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    //called form fragment  @FragmentRearrangeList
    public void ClearBoardAddColumn(List<List<MyItem>> itemsList, List<String> headers) {
        mBoardView.clearBoard();
        grandList = new ArrayList<>();
        headerList = new ArrayList<>();//fill when addCOlumn
        adapterList = new ArrayList<>();//fill when add column
        mColumns = 0;
        for (int i = 0; i < itemsList.size(); i++) {
            grandList.add(itemsList.get(i));
            addColumn((ArrayList<MyItem>) itemsList.get(i), headers.get(i));
        }
        for (int i = 0; i < adapterList.size(); i++) {
            adapterList.get(i).setFreshDataInCopy(itemsList.get(i));
        }

        setPageViewController();
    }

    //when we rearrange column then it called
    public void changeDataInFirebase(ArrayList<HashMap<String, Object>> colsData) {
        columnsdata = colsData;
        //BOARD_.setTotalCol((ArrayList<HashMap<String, Object>>) columnsdata);
        isChanged = false;//becoz in owner mobile show two card in searching time so no need to add column again in them phone.
        if (fromShare) {
            BOARD_.upadateCOL_Board(ownerId, BOARD_ID, columnsdata, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    Toast.makeText(getApplicationContext(), "Col list saved", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            BOARD_.upadateCOL_Board(userid, BOARD_ID, columnsdata, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    Toast.makeText(getApplicationContext(), "Col list saved", Toast.LENGTH_SHORT).show();
                }
            });
        }

        setClickListner();
        setPageViewController();
    }

    public HashMap<String, Object> createCardHashmap(int col, int row) {
        MyItem data = (MyItem) mBoardView.getAdapter(col).getItem(row);
        HashMap<String, Object> map = new HashMap<>();
        map.put("columnId", columnsdata.get(col).get("id"));
        map.put("objectId", data.getObjectId());
        map.put("row", row);
        map.put("title", data.getData());
        map.put("description", data.getDesc());
        if (data.getItemAttach() != null)
            map.put("arrItems", data.getItemAttach());
        if (data.getdueDate() != null)
            map.put("duedate", data.getdueDate());
        if (data.getColorcodes() != null)
            map.put("label", data.getColorcodes());
        if (data.getChecklist() != null)
            map.put("checklist", data.getChecklist());
        if (data.getComment() != null)
            map.put("comment", data.getComment());
        if (data.getAssign() != null)
            map.put("assign", data.getAssign());

        return map;
    }

    public void ChageDataWhenDRAGE_OR_LISTOPTION(int fromcol, int newcol) {
        new updateAllCardData(MainActivity.this).execute();
//        HashMap<String,Object > TEMP_cardData=new HashMap<>();
//        int r=mBoardView.getItemCount(fromcol);
//        for(int i=0;i<r;i++){
//            //HashMap<String,Object > TEMP_cardData=new HashMap<>();
//            MyItem item= (MyItem) mBoardView.getAdapter(fromcol).getItem(i);
//            TEMP_cardData.put(item.getObjectId(), createCardHashmap(fromcol, i));
//            //BOARD_.updateCard(BOARD_ID, item.getObjectId(), createCardHashmap(fromcol, i),null);
//        }
//        int r1=mBoardView.getItemCount(newcol);
//        for(int i=0;i<r1;i++){
//            //HashMap<String,Object > TEMP_cardData=new HashMap<>();
//            MyItem item= (MyItem) mBoardView.getAdapter(fromcol).getItem(i);
//            TEMP_cardData.put(item.getObjectId(), createCardHashmap(fromcol, i));
//            //BOARD_.updateCard(BOARD_ID, item.getObjectId(),createCardHashmap(fromcol, i),null);
//        }
//        BOARD_.updateAllCardData(BOARD_ID, TEMP_cardData, new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//            }
//        });


    }

    public class updateAllCardData extends AsyncTask<Void, Void, Void> {
        Context mcontext;
        public updateAllCardData(Context context){
            this.mcontext=context;
        }
        ProgressDialog p;
        HashMap<String, Object> cardsData = new HashMap<>();

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
            p = new ProgressDialog(mcontext);
            p.setMessage("Please wait");
            p.show();

//for updating data in firebase that's y taking data column wise and updating that data
            for (int i = 0; i < mBoardView.getColumnCount(); i++) {
                int row = mBoardView.getAdapter(i).getItemCount();
                Log.i("TAG", "board column vise data:" + row);
                for (int j = 0; j < row; j++) {
                    MyItem card = (MyItem) mBoardView.getAdapter(i).getItemList().get(j);
                    HashMap<String, Object> cd = card.getCardJSON(columnsdata.get(i).get("id").toString(), j);
                    Log.i("TAG", "car data:" + cd);
                    cardsData.put(card.getObjectId(), cd);
                }
            }
            Log.i("TAG", "pre execute of update:");
        }

        @Override
        protected Void doInBackground(Void... params) {

            //it will update cards data that we have gathered above by loops
            BOARD_.updateAllCardData(BOARD_ID, cardsData, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                    Toast.makeText(getApplicationContext(), "data updated in firebase", Toast.LENGTH_SHORT).show();
                }
            });
            Log.i("TAG", "do in background of update:");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.i("TAG", "post execute of update:");
            if (p.isShowing())
                p.dismiss();
            Toast.makeText(mcontext, "data updated in firebase", Toast.LENGTH_SHORT).show();//29March change getApplicationContext() to mcontext
        }
    }

    //for upper rectangular indicator
    private void setPageViewController() {
        int dotsCount = headerList.size();
        NUM_PAGES = dotsCount;
        dots = new TextView[dotsCount];
        if (pager_indicator.getChildCount() > 0)
            pager_indicator.removeAllViews();
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new TextView(this);
            dots[i].setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.nonselected_ract));
            dots[i].setText(String.valueOf(headerList.get(i).toString().trim().charAt(0)) + String.valueOf(headerList.get(i).toString().trim().charAt(0)));
            dots[i].setGravity(Gravity.CENTER);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.width = 60;
            params.height = 100;
            params.setMargins(4, 0, 4, 0);
            pager_indicator.addView(dots[i], params);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void hidekeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.RESULT_UNCHANGED_HIDDEN, 0);
    }

    public Boolean CheckInternet() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public void checkForColor(int board, List<String> sourceTitle) {
        String id = (String) otherboardData.get(board).get("bid");
        Log.i("TAG", "b id:" + id);
        if (clrlabelList != null && clrlabelList.size() > 0) {
            List<HashMap<String, Object>> temp = new ArrayList<>();
            for (int i = 0; i < clrlabelList.size(); i++) {
                if (sourceTitle.contains(clrlabelList.get(i).get("color")))
                    temp.add(clrlabelList.get(i));
            }
            List<HashMap<String, Object>> label = (List<HashMap<String, Object>>) otherboardColor.get(board).get("labeldata");
            for (int i = 0; i < temp.size(); i++) {
                String color = (String) temp.get(i).get("color");
                String title = (String) temp.get(i).get("title");
                Boolean change = false;
                for (int j = 0; j < label.size(); j++) {
                    String s_color = (String) label.get(j).get("color");
                    String s_title = (String) label.get(j).get("title");
                    if (color.equals(s_color)) {
                        Log.i("TAG", "contains:" + title.contains(s_title));
                        Log.i("TAG", "title:" + title + " s_title:" + s_title);
                    } else {
                        change = true;
                        break;
                    }
                }
                if (change) {
                    Log.i("TAG", "=>add color");
                    label.add(temp.get(i));
                    BOARD_.getBOARD_ROOT(id).child("label").setValue(label);
                }
            }


        }
    }

    public void moveList(final int column) {
        final Spinner board_list;
        final MaterialDialog moveList = new MaterialDialog.Builder(MainActivity.this)
                .title("Move list")
                .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                .titleColorRes(R.color.colorPrimary)
                .positiveText("Ok").positiveColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        final Spinner board_list = (Spinner) dialog.findViewById(R.id.spn_boardlist);
                        if (!board_list.getSelectedItem().equals(type)) {
                            List<String> cardOldid = new ArrayList<String>();
                            String newboardID = (String) otherboardData.get(board_list.getSelectedItemPosition()).get("bid");
                            List<HashMap<String, Object>> coldata = (List<HashMap<String, Object>>) otherboardData.get(board_list.getSelectedItemPosition()).get("coldata");

                            //adding new data in that board totalcol list
                            HashMap<String, Object> newcoldata = new HashMap<String, Object>();
                            String newColId = MyBoardlist.createUniqueid();
                            newcoldata.put("id", newColId);
                            newcoldata.put("title", headerList.get(column));
                            newcoldata.put("autoduedate", columnsdata.get(column).get("autoduedate"));
                            coldata.add(newcoldata);
                            //particular that user boards list board id

                            if (fromShare) {
                                BOARD_.getROOT().child(ownerId).child(newboardID).child("totalcol").setValue(coldata).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(Task<Void> task) {
                                        Toast.makeText(getApplicationContext(), "new column added", Toast.LENGTH_SHORT);
                                    }
                                });
                            } else {
                                BOARD_.getBOARD_ROOT(newboardID).child("totalcol").setValue(coldata).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(Task<Void> task) {
                                        Toast.makeText(getApplicationContext(), "new column added", Toast.LENGTH_SHORT);
                                    }
                                });
                            }

                            int viewcount = mBoardView.getAdapter(column).getItemList().size();
                            if (viewcount > 0) {
                                //now remove that card from current board and add in new board so first add and then remove
                                for (int i = 0; i < viewcount; i++) {
                                    MyItem newItem = grandList.get(column).get(i);
                                    cardOldid.add(newItem.getObjectId());
                                    BOARD_.setCardid(newboardID);
                                    newItem.setObjectId(BOARD_.cardid);
                                    BOARD_.addCardInBoard(newboardID, newItem.getCardJSON(newColId, i), new DatabaseReference.CompletionListener() {
                                        @Override
                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                            //Toast.makeText(getApplication(), "Card saved", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                isChanged = false;
                                for (int i = 0; i < cardOldid.size(); i++) {
                                    BOARD_.deleteCardInBoard(BOARD_ID, cardOldid.get(i), null);
                                    //BOARD_.getBOARD_DATA(BOARD_ID).child(cardOldid.get(i)).removeValue();
                                    Log.i("TAG", "removing form that board");
                                    mBoardView.removeItem(column, 0);//0 bcoz after removing as per "i" so in list last no any card at that index
                                }
                            }
                            //history
                            MoveList(columnsdata.get(column).get("id").toString(), columnsdata.get(column).get("title").toString(), newboardID, board_list.getSelectedItem().toString());
                            //now remove thar col from this board detail
                            columnsdata.remove(column);
                            mBoardView.removeColumn(column);
                            headerList.remove(column);//bcoz after movelist in to other board and right away going to rearrange it showing removed column name so it's necessary to remove from this (only header's name) array
                            grandList.remove(column);//bcoz after removing that card in board no data in that index so remove that index data(obviously it's blank)
                            changeDataInFirebase(columnsdata);
                        }
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .customView(R.layout.prompt_move_list, true)
                .show();
        board_list = (Spinner) moveList.findViewById(R.id.spn_boardlist);

        if (boardList.size() > 0)
            boardList.clear();
        for (int i = 0; i < otherboardData.size(); i++) {
            boardList.add((String) otherboardData.get(i).get("name"));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, boardList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        board_list.setAdapter(adapter);
        board_list.setSelection(boardList.indexOf(type));
    }

    public void moveAllcard(final int column) {
        final Spinner spinner_list;
        final Spinner board_list;
        final MaterialDialog moveallcard = new MaterialDialog.Builder(MainActivity.this)
                .title("Move All Cards")
                .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                .titleColorRes(R.color.colorPrimary)
                .positiveText("Move").positiveColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        Spinner spinner_boardlist = (Spinner) dialog.findViewById(R.id.spn_boardlist);
                        Spinner spinner_list = (Spinner) dialog.findViewById(R.id.spn_list);

                        if (spinner_boardlist.getSelectedItem().equals(type)) {
                            String selected = spinner_list.getSelectedItem().toString();//current column
                            View header = mBoardView.getHeaderView(column);
                            String now = ((TextView) header.findViewById(R.id.header_text)).getText().toString();//selected column
                            if (!selected.equals(now)) {
                                int newColumnPos = spinner_list.getSelectedItemPosition();
                                int viewCount = mBoardView.getItemCount(column);
                                AddMoveCardHistory(mBoardView.getItemCount(column), columnsdata.get(column).get("id").toString(), now, columnsdata.get(newColumnPos).get("title").toString(), BOARD_ID, type, true);
                                for (int i = 0; i <= viewCount; i++) {
                                    //viewCount = mBoardView.getItemCount(column);
                                    Log.i("TAG", "board view column data count:" + mBoardView.getItemCount(column));
                                    mBoardView.moveItem(column, 0, newColumnPos, i, false);
                                }
                                TextView itemCount = (TextView) mBoardView.getHeaderView(column).findViewById(R.id.total_view);
                                itemCount.setText(Integer.toString(mBoardView.getAdapter(column).getItemCount()));
                                TextView itemCountnew = (TextView) mBoardView.getHeaderView(newColumnPos).findViewById(R.id.total_view);
                                itemCountnew.setText(Integer.toString(mBoardView.getAdapter(newColumnPos).getItemCount()));
                                dialog.dismiss();
                                isChanged = false;
                                ChageDataWhenDRAGE_OR_LISTOPTION(column, newColumnPos);
                            } else {
                                Toast.makeText(getApplicationContext(), "not moved", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            String newboardID = (String) otherboardData.get(spinner_boardlist.getSelectedItemPosition()).get("bid");
                            int newColumnPos = spinner_list.getSelectedItemPosition();
                            Log.i("TAG", "column index" + newColumnPos);
                            List<HashMap<String, Object>> coldata = (List<HashMap<String, Object>>) otherboardData.get(spinner_boardlist.getSelectedItemPosition()).get("coldata");
                            String newColumnId = (String) coldata.get(newColumnPos).get("id");
                            String fromcol_name = (String) columnsdata.get(column).get("title");

                            Log.i("TAG", "col id is:" + newColumnId);
                            int viewCount = mBoardView.getItemCount(column);
                            List<String> cardOldid = new ArrayList<String>();
                            for (int i = 0; i < viewCount; i++) {
                                //viewCount = mBoardView.getItemCount(column);
                                Log.i("TAG", "board view column data count:" + mBoardView.getItemCount(column));
                                MyItem newItem = (MyItem) mBoardView.getAdapter(column).getItem(i);
                                cardOldid.add(newItem.getObjectId());
                                BOARD_.setCardid(newboardID);
                                newItem.setObjectId(BOARD_.cardid);
                                //BOARD_.setCardtitle(newItem.getData().trim());
                                //BOARD_.setRow(0);
                                //BOARD_.setColumnId(newColumnId);
                                BOARD_.addCardInBoard(newboardID, newItem.getCardJSON(newColumnId, 0), new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    }
                                });
                            }
                            //history
                            AddMoveCardHistory(viewCount, newColumnId, fromcol_name, coldata.get(newColumnPos).get("title").toString(), newboardID, (String) spinner_boardlist.getSelectedItem(), false);

                            for (int i = 0; i < cardOldid.size(); i++) {
                                mBoardView.removeItem(column, 0);
                                BOARD_.getBOARD_DATA(BOARD_ID).child(cardOldid.get(i)).removeValue();
                            }
                            isChanged = false;
                            TextView itemCount = (TextView) mBoardView.getHeaderView(column).findViewById(R.id.total_view);
                            itemCount.setText(Integer.toString(mBoardView.getAdapter(column).getItemCount()));
                            dialog.dismiss();
                            ChageDataWhenDRAGE_OR_LISTOPTION(column, column);
                        }

                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .customView(R.layout.promp_move_all_cards, true)
                .show();
        board_list = (Spinner) moveallcard.findViewById(R.id.spn_boardlist);
        spinner_list = (Spinner) moveallcard.findViewById(R.id.spn_list);

        if (boardList.size() > 0)
            boardList.clear();

        for (int i = 0; i < otherboardData.size(); i++) {
            boardList.add((String) otherboardData.get(i).get("name"));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, boardList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        board_list.setAdapter(adapter);
        board_list.setSelection(boardList.indexOf(type));

        List<String> newcoldata = new ArrayList<>();
        for (int i = 0; i < columnsdata.size(); i++) {
            if (!columnsdata.get(i).get("title").toString().equals(columnsdata.get(column).get("title")))
                newcoldata.add(columnsdata.get(i).get("title").toString());
        }
        Log.i("TAG", "new col data:" + newcoldata);

        ArrayAdapter<String> a = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, newcoldata);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_list.setAdapter(a);

        board_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                List<String> newcollist = new ArrayList<>();
                List<HashMap<String, Object>> coldata = (List<HashMap<String, Object>>) otherboardData.get(position).get("coldata");
                if (!otherboardData.get(position).get("bid").equals(BOARD_ID)) {
                    for (int i = 0; i < coldata.size(); i++) {
                        newcollist.add((String) coldata.get(i).get("title"));
                    }
                } else {
                    newcollist.addAll(headerList);
                }
                ArrayAdapter<String> a = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, newcollist);
                a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_list.setAdapter(a);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void moveCard(final int column, final List<MyItem> mItemArrays) {
        final Spinner spinner_list;
        final Spinner board_list;
        ListView cardslist = null;

        final List<String> coluItems = new ArrayList<>();
        //final List<String> selected = new ArrayList<>();
        final List<MyItem> selectedData = new ArrayList<>();

        final MaterialDialog movecard = new MaterialDialog.Builder(MainActivity.this)
                .title("Move Cards")
                .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                .titleColorRes(R.color.colorPrimary)
                .positiveText("Move").positiveColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        Spinner boardlist = (Spinner) dialog.findViewById(R.id.spn_boardlist);
                        Spinner columnlist = (Spinner) dialog.findViewById(R.id.spn_columnlist);
                        if (boardlist.getSelectedItem().equals(type)) {
                            //same board but column change
                            if (!columnlist.getSelectedItem().equals(columnsdata.get(column).get("title"))) {
                                int newColu = columnlist.getSelectedItemPosition();
                                Log.i("TAG", "new column is:" + newColu);
                                //history
//                                                                      int count,          String newcolumnid,                            String fromcolname,                         String colname,String boardid,String boardname,Boolean sameBoard
                                AddMoveCardHistory(selectedData.size(), columnsdata.get(newColu).get("id").toString(), columnsdata.get(column).get("title").toString(), columnsdata.get(newColu).get("title").toString(), BOARD_ID, type, true);

                                for (int i = 0; i < selectedData.size(); i++) {
                                    int newindex = mItemArrays.indexOf(selectedData.get(i));
                                    MyItem item = mItemArrays.get(newindex);
                                    Log.i("TAG", "has color:" + item.getColorcodes());
                                    if (item.getColorcodes() != null && item.getColorcodes().size() > 0) {
                                        checkForColor(boardlist.getSelectedItemPosition(), item.getColorcodes());
                                    }
                                    mBoardView.removeItem(item);
                                    mBoardView.addItem(newColu, i, item, false);
                                }
                                TextView itemCount = (TextView) mBoardView.getHeaderView(column).findViewById(R.id.total_view);
                                itemCount.setText(Integer.toString(mBoardView.getAdapter(column).getItemCount()));
                                TextView itemCountnew = (TextView) mBoardView.getHeaderView(newColu).findViewById(R.id.total_view);
                                itemCountnew.setText(Integer.toString(mBoardView.getAdapter(newColu).getItemCount()));
                                Log.i("TAG", "final array" + mItemArrays.size());
                                isChanged = false;
                                ChageDataWhenDRAGE_OR_LISTOPTION(column, newColu);
                            }
                        } else {
                            String newboardID = (String) otherboardData.get(boardlist.getSelectedItemPosition()).get("bid");
                            int newColumnPos = columnlist.getSelectedItemPosition();
                            List<HashMap<String, Object>> coldata = (List<HashMap<String, Object>>) otherboardData.get(boardlist.getSelectedItemPosition()).get("coldata");
                            String newColumnId = (String) coldata.get(newColumnPos).get("id");

                            List<String> cardOldid = new ArrayList<>();
                            for (int i = 0; i < selectedData.size(); i++) {
                                int newindex = mItemArrays.indexOf(selectedData.get(i));
                                MyItem item = mItemArrays.get(newindex);
                                if (item.getColorcodes() != null && item.getColorcodes().size() > 0) {
                                    checkForColor(boardlist.getSelectedItemPosition(), item.getColorcodes());
                                }
                                cardOldid.add(item.getObjectId());
                                BOARD_.setCardid(newboardID);//it will create push id on given board id;
                                item.setObjectId(BOARD_.cardid);
                                BOARD_.addCardInBoard(newboardID, item.getCardJSON(newColumnId, 0), new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    }
                                });
                            }
                            for (int i = 0; i < cardOldid.size(); i++) {
                                mBoardView.removeItem(selectedData.get(i));
                                BOARD_.getBOARD_DATA(BOARD_ID).child(cardOldid.get(i)).removeValue();
                            }

                            //history
                            AddMoveCardHistory(selectedData.size(), newColumnId, columnsdata.get(column).get("title").toString(), columnsdata.get(newColumnPos).get("title").toString(), newboardID, (String) boardlist.getSelectedItem(), false);

                            TextView itemCount = (TextView) mBoardView.getHeaderView(column).findViewById(R.id.total_view);
                            itemCount.setText(Integer.toString(mBoardView.getAdapter(column).getItemCount()));

                            isChanged = false;
                            ChageDataWhenDRAGE_OR_LISTOPTION(column, column);
                        }
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .customView(R.layout.prompt_move_cards, true)
                .show();

        board_list = (Spinner) movecard.findViewById(R.id.spn_boardlist);
        spinner_list = (Spinner) movecard.findViewById(R.id.spn_columnlist);
        cardslist = (ListView) movecard.findViewById(R.id.card_list);
        Log.i("TAG", "board list size:" + boardList);
        if (boardList.size() > 0)
            boardList.clear();

        for (int i = 0; i < otherboardData.size(); i++) {
            boardList.add((String) otherboardData.get(i).get("name"));
        }

        ArrayAdapter<String> a = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, boardList);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        board_list.setAdapter(a);
        board_list.setSelection(boardList.indexOf(type));

        final List<String> newcollist = new ArrayList<>();
        for (int i = 0; i < columnsdata.size(); i++) {
            newcollist.add(columnsdata.get(i).get("id").toString());
        }

        ArrayAdapter<String> a2 = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, newcollist);
        a2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_list.setAdapter(a2);

        board_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                List<String> newcollist = new ArrayList<>();
                if (!otherboardData.get(position).get("bid").equals(BOARD_ID)) {
                    List<HashMap<String, Object>> coldata = (List<HashMap<String, Object>>) otherboardData.get(position).get("coldata");
                    for (int i = 0; i < coldata.size(); i++) {
                        newcollist.add((String) coldata.get(i).get("title"));
                    }
                } else {
                    newcollist.addAll(headerList);
                }
                ArrayAdapter<String> a = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, newcollist);
                a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_list.setAdapter(a);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        for (int i = 0; i < mItemArrays.size(); i++) {
            coluItems.add(mItemArrays.get(i).getData());
        }
        cardslist.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        cardslist.setAdapter(new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_checked, coluItems));
        cardslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckedTextView item = (CheckedTextView) view;
                if (item.isChecked()) {
                    selectedData.add(mItemArrays.get(position));
                } else {
                    if (selectedData.contains(mItemArrays.get(position))) {
                        selectedData.remove(mItemArrays.get(position));
                    }
                }
            }
        });
    }

    public void copy(final int column) {
        final Spinner spinner_list, board_list;
        final MaterialDialog copyDailog = new MaterialDialog.Builder(MainActivity.this)
                .title("Copy List")
                .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                .titleColorRes(R.color.colorPrimary)
                .positiveText("Ok").positiveColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {

                        final Spinner board_list = (Spinner) dialog.findViewById(R.id.spn_boardlist);
                        final Spinner spinner_list = (Spinner) dialog.findViewById(R.id.spn_list);
                        if (board_list.getSelectedItem().equals(type)) {
                            Log.i("TAG", "board is same");//check for if selectd col is not equal to current col
                            Log.i("TAG", "selected current index:" + column);
                            Log.i("TAG", "selected current col name:" + spinner_list.getSelectedItem());
                            if (!columnsdata.get(column).get("title").equals(spinner_list.getSelectedItem())) {

                                final int newColumnPos = spinner_list.getSelectedItemPosition();
                                Log.i("TAG", "selected col index:" + newColumnPos);
                                Log.i("TAG", "selected col name:" + spinner_list.getSelectedItem());
                                int viewCount = mBoardView.getItemCount(column);
                                String sourcecol = columnsdata.get(column).get("title").toString();
                                String destcol = (String) spinner_list.getSelectedItem();
                                String destcolid = (String) columnsdata.get(newColumnPos).get("id");
                                AddCopyHistory(viewCount, sourcecol, destcol, destcolid, BOARD_ID);
                                isChanged = false;

                                HashMap<String, Object> newdatamap = new HashMap<String, Object>();
                                for (int i = 0; i < viewCount; i++) {
                                    MyItem newItem = grandList.get(column).get(i);
                                    newItem.setItemid(sCreatedItems++);
                                    BOARD_.setCardid(BOARD_ID);
                                    newItem.setObjectId(BOARD_.cardid);
                                    int count = mBoardView.getAdapter(newColumnPos).getItemCount();
                                    mBoardView.addItem(newColumnPos, count, newItem, false);
                                    newdatamap.put(newItem.getObjectId(), newItem.getCardJSON(columnsdata.get(newColumnPos).get("id").toString(), count));
//                                                        if(i==viewCount-1) {
//                                                            BOARD_.addCardInBoard(BOARD_ID, newItem.getCardJSON(columnsdata.get(newColumnPos).get("id").toString(), count), new DatabaseReference.CompletionListener() {
//                                                                @Override
//                                                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                                                                    prd.dismiss();
//                                                                }
//                                                            });
//                                                        }else{
//                                                            BOARD_.addCardInBoard(BOARD_ID, newItem.getCardJSON(columnsdata.get(newColumnPos).get("id").toString(), count), new DatabaseReference.CompletionListener() {
//                                                                @Override
//                                                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//
//                                                                }
//                                                            });
//                                                        }

                                }
                                new CopyCards().execute(newdatamap, null, null);
                                TextView itemCountnew = (TextView) mBoardView.getHeaderView(newColumnPos).findViewById(R.id.total_view);
                                itemCountnew.setText(Integer.toString(mBoardView.getItemCount(newColumnPos)));

                                TextView itemCountold = (TextView) mBoardView.getHeaderView(column).findViewById(R.id.total_view);
                                itemCountold.setText(Integer.toString(mBoardView.getItemCount(column)));

                                Log.i("TAG", "copied data:" + newdatamap);
                                Toast.makeText(getApplicationContext(), "Copied successfully", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.i("TAG", "board is changed");
                            String newboardID = (String) otherboardData.get(board_list.getSelectedItemPosition()).get("bid");
                            int newColumnPos = spinner_list.getSelectedItemPosition();
                            List<HashMap<String, Object>> coldata = (List<HashMap<String, Object>>) otherboardData.get(board_list.getSelectedItemPosition()).get("coldata");
                            String newColumnId = (String) coldata.get(newColumnPos).get("id");
                            int viewCount = mBoardView.getItemCount(column);

                            String sourcecol = columnsdata.get(column).get("title").toString();
                            String destcol = (String) spinner_list.getSelectedItem();
                            AddCopyHistory(viewCount, sourcecol, destcol, newColumnId, newboardID);

                            for (int i = 0; i < viewCount; i++) {
                                //viewCount = mBoardView.getItemCount(column);
                                Log.i("TAG", "board view column data count:" + mBoardView.getItemCount(column));
                                MyItem newItem = grandList.get(column).get(i);
                                BOARD_.setCardid(newboardID);
                                newItem.setObjectId(BOARD_.cardid);
                                //BOARD_.setCardtitle(newItem.getData().trim());
                                //BOARD_.setRow(0);
                                //BOARD_.setColumnId(newColumnId);
                                BOARD_.addCardInBoard(newboardID, newItem.getCardJSON(newColumnId, 0), null);
                            }
                            Toast.makeText(getApplicationContext(), "Copied successfully", Toast.LENGTH_SHORT).show();
                            ;
                            //TextView itemCountnew = (TextView) mBoardView.getHeaderView(newColumnPos).findViewById(R.id.total_view);
                            //itemCountnew.setText(Integer.toString(mBoardView.getAdapter(newColumnPos).getItemCount()));

                        }

                        //String now = ((TextView) header.findViewById(R.id.header_text)).getText().toString();


//                                            final TextInputEditText tv_title = (TextInputEditText) dialog.findViewById(R.id.tv_list_title);
//                                            if (tv_title.getText().toString().length() > 0) {
//                                                copylistName = tv_title.getText().toString();
//                                                //headerList.add(copylistName);
//                                                String old = ((TextView) header.findViewById(R.id.header_text)).getText().toString();
//                                                Log.i("TAG", "erlier name:" + old);
//                                                int position = -1;
//                                                if (headerList.contains(old)) {
//                                                    position = headerList.indexOf(old);
//                                                }
//                                                addColumn((ArrayList<MyItem>) grandList.get(position), copylistName);

                        dialog.dismiss();
                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
//                                    .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                .customView(R.layout.promp_move_all_cards, true)
                .show();
        board_list = (Spinner) copyDailog.findViewById(R.id.spn_boardlist);
        Log.i("TAG", "other board data list size:" + otherboardData.size());
        if (boardList.size() > 0)
            boardList.clear();
        for (int i = 0; i < otherboardData.size(); i++) {
            boardList.add((String) otherboardData.get(i).get("name"));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, boardList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        board_list.setAdapter(adapter);
        board_list.setSelection(boardList.indexOf(type));

        spinner_list = (Spinner) copyDailog.findViewById(R.id.spn_list);
        final List<String> changed = new ArrayList<>();
        for (int i = 0; i < columnsdata.size(); i++) {
            //if (!columnsdata.get(i).get("title").equals(columnsdata.get(column).get("title")))
            changed.add(columnsdata.get(i).get("title").toString());
        }
        Log.i("TAG", "after col added data:" + changed);
        ArrayAdapter<String> a = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, changed);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_list.setAdapter(a);

        board_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                List<String> newcollist = new ArrayList<>();
                List<HashMap<String, Object>> coldata = (List<HashMap<String, Object>>) otherboardData.get(position).get("coldata");
                for (int i = 0; i < coldata.size(); i++) {
                    if (otherboardData.get(position).get("bid").equals(BOARD_ID)) {
                        newcollist.addAll(changed);
                        break;
                    } else {
                        newcollist.add((String) coldata.get(i).get("title"));
                    }
                }
                ArrayAdapter<String> a = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, newcollist);
                a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_list.setAdapter(a);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void archiveAllCard(final int column, final List<MyItem> mItemArrays) {
        MaterialDialog archiveCard = new MaterialDialog.Builder(MainActivity.this)
                .title("Archive Card").titleColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .content("Are you sure you want to archive these " + mItemArrays.size() + " cards?")
                .positiveText("Archive").positiveColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {

                        List<String> oldid = new ArrayList<String>();
                        String colid = columnsdata.get(column).get("id").toString();
                        AddArchHistory(mItemArrays.size(), colid, columnsdata.get(column).get("title").toString());
                        for (int i = 0; i < mItemArrays.size(); i++) {
                            MyItem item = mItemArrays.get(i);
                            oldid.add(item.getObjectId());
                            zboard_archive.addCardInArchive(BOARD_ID, item.getObjectId(), item.getCardJSON(colid, 0), new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                }
                            });
                        }

                        for (int i = 0; i < oldid.size(); i++) {
                            mBoardView.removeItem(column, 0);
                            BOARD_.getBOARD_DATA(BOARD_ID).child(oldid.get(i)).removeValue();
                        }
                        isChanged = false;
//                        for (int k = 0; k < oldid.size(); k++) {
//                            //MyItem deleteItem = oldid.get(k);
//                            //Log.i("TAG", "archive id:" + deleteItem.getObjectId());
//                            BOARD_.getBOARD_DATA(BOARD_ID).child(oldid.get(k)).removeValue();
//                        }

                        for (int i = 0; i < adapterList.size(); i++) {
                            MyAdapter adapter = adapterList.get(i);
                            adapter.setFreshDataInCopy(mBoardView.getAdapter(i).getItemList());
                        }
                        TextView itemCount = (TextView) mBoardView.getHeaderView(column).findViewById(R.id.total_view);
                        itemCount.setText(Integer.toString(mBoardView.getAdapter(column).getItemCount()));
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void archiveCard(final int column, final List<MyItem> mItemArrays) {
        ListView cardlist;
        Spinner boardlist, collist;
        final List<String> coluItems = new ArrayList<>();
        final List<MyItem> selectedData = new ArrayList<>();
        MaterialDialog archivecards = new MaterialDialog.Builder(MainActivity.this)
                .title("Archive Cards").titleColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .customView(R.layout.prompt_move_cards, true)
                .positiveText("Archive").positiveColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .negativeText("Cancel").negativeColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                    isChanged = false;
                                    String colid = columnsdata.get(column).get("id").toString();
                                    AddArchHistory(selectedData.size(), colid, columnsdata.get(column).get("title").toString());
                                    for (int i = 0; i < selectedData.size(); i++) {

                                        int index = mItemArrays.indexOf(selectedData.get(i));
                                        MyItem item = selectedData.get(i);
                                        //by MB
//                                        item.getCardJSON(colid, mBoardView.getItemCount(column)
                                        zboard_archive.addCardInArchive(BOARD_ID, item.getObjectId(), item.getCardJSON(colid, 0), new DatabaseReference.CompletionListener() {
                                            @Override
                                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                            }
                                        });
                                    }
                                    for (int k = 0; k < selectedData.size(); k++) {
                                        MyItem deleteItem = selectedData.get(k);
                                        Log.i("TAG", "archive id:" + deleteItem.getObjectId());
                                        BOARD_.getBOARD_DATA(BOARD_ID).child(deleteItem.getObjectId()).removeValue();
                                        mBoardView.removeItem(deleteItem);
                                    }
                                    for (int i = 0; i < adapterList.size(); i++) {
                                        MyAdapter adapter = adapterList.get(i);
                                        adapter.setFreshDataInCopy(mBoardView.getAdapter(i).getItemList());
                                    }
                                    ChageDataWhenDRAGE_OR_LISTOPTION(column, column);//29March becoz it will change remaining card index if we not calling,firebase does not know now change new indexing data
//                        for (int i = 0; i < columnsdata.size(); i++) {
//                            Log.i("TAG", "after archiving column =>" + i);
//                            Log.i("TAG", "after archiving board col items after card added :=>" + mBoardView.getItemCount(i));
//                            Log.i("TAG", "after archiving board view col total card:=>" + mBoardView.getAdapter(i).getItemCount());
//                            Log.i("TAG", "after archiving Grand list size :=>" + grandList.get(i).size());
//                        }
                                    //ChageDataWhenDRAGE_OR_LISTOPTION(column,column);
                                    dialog.dismiss();
                                }
                            }

                )
                .

                        onNegative(new MaterialDialog.SingleButtonCallback() {
                                       @Override
                                       public void onClick(MaterialDialog dialog, DialogAction which) {
                                           dialog.dismiss();
                                       }
                                   }

                        )
                .

                        show();

        cardlist = (ListView) archivecards.findViewById(R.id.card_list);
        boardlist = (Spinner) archivecards.findViewById(R.id.spn_boardlist);
        collist = (Spinner) archivecards.findViewById(R.id.spn_columnlist);
        boardlist.setVisibility(View.GONE);
        collist.setVisibility(View.GONE);
        for (
                int i = 0;
                i < mItemArrays.size(); i++)

        {
            coluItems.add(mItemArrays.get(i).getData());
        }

        cardlist.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        cardlist.setAdapter(new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_checked, coluItems));
        cardlist.setOnItemClickListener(new AdapterView.OnItemClickListener()

                                        {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position,
                                                                    long id) {
                                                CheckedTextView item = (CheckedTextView) view;
                                                if (item.isChecked()) {
                                                    selectedData.add(mItemArrays.get(position));
                                                } else {
                                                    if (selectedData.contains(mItemArrays.get(position))) {
                                                        selectedData.remove(mItemArrays.get(position));
                                                    }
                                                }
                                            }
                                        }

        );
    }


    public void AddCardHistory(MyItem data, String columnid, String columntitle) {
        HashMap<String, Object> mapdata = BOARD_HISTORY.getJSON_AddCardInHistory(data, zAuth.get(), columnid, columntitle);
        BOARD_HISTORY.addHistory(BOARD_ID, mapdata, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                Toast.makeText(getApplicationContext(), "History added", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void Addmember(List<String> newmemberlist) {
        memberlist = (ArrayList<String>) newmemberlist;
    }

    public void addSharedUser(List<String> userlist, HashMap<String, Object> newsharedData) {
        sharedUser = (ArrayList<String>) userlist;
        sharedUserData = newsharedData;
        Log.i("TAG", "updated shared :" + sharedUserData);
    }

    public void AddMoveCardHistory(int count, String newcolumnid, String fromcolname, String colname, String boardid, String boardname, Boolean sameBoard) {
        HashMap<String, Object> mapdata = BOARD_HISTORY.
                getJSON_MoveCard(count, zAuth.get(), newcolumnid, colname, fromcolname, boardid, boardname, sameBoard);
//        int count,String userid,String colid,String colname,String oldcolname,String bid,String bname,Boolean sameBoard){
        BOARD_HISTORY.addHistory(BOARD_ID, mapdata, null);
    }

    public void AddArchHistory(int count, String colid, String colname) {
        HashMap<String, Object> mapdata = BOARD_HISTORY.getJSON_ArchiveCard(count, colid, colname, BOARD_ID, type, zAuth.get());
        BOARD_HISTORY.addHistory(BOARD_ID, mapdata, null);
    }

    //int count,String source_colname,String dest_colname,String dest_colid,String userid
    public void AddCopyHistory(int count, String source_colname, String dest_colname, String dest_colid, String dest_boarid) {
        HashMap<String, Object> mapdata;
        if (dest_boarid.equals(BOARD_ID))
            mapdata = BOARD_HISTORY.getJSON_CopyCard(count, source_colname, dest_colname, dest_colid, zAuth.get(), true, type);
        else
            mapdata = BOARD_HISTORY.getJSON_CopyCard(count, source_colname, dest_colname, dest_colid, zAuth.get(), false, type);

        BOARD_HISTORY.addHistory(dest_boarid, mapdata, null);
    }

    public void MoveList(String colid, String colname, String frombid, String frombname) {
        HashMap<String, Object> mapdata = BOARD_HISTORY.getJSON_MoveList(colid, colname, frombid, frombname, zAuth.get());
        BOARD_HISTORY.addHistory(MainActivity.BOARD_ID, mapdata, null);
    }

    private class CopyCards extends AsyncTask<Object, Void, Void> {
        ProgressDialog prd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            prd = new ProgressDialog(MainActivity.this);
            prd.setMessage("Please Wait...");
            prd.setCancelable(false);
            prd.show();
        }

        @Override
        protected Void doInBackground(Object... params) {
            HashMap<String, Object> newdatamap = (HashMap<String, Object>) params[0];
            BOARD_.updateAllCardData(BOARD_ID, newdatamap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    prd.dismiss();

                }
            });
            return null;
        }
    }

    private void showMenuSheet(final int column, final List<MyItem> myItemList) {
        MenuSheetView menuSheetView =
                new MenuSheetView(MainActivity.this, MenuSheetView.MenuType.GRID, "", new MenuSheetView.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(MainActivity.this, item.getTitle(), Toast.LENGTH_SHORT).show();
                        if (bottomSheetLayout.isSheetShowing()) {
                            bottomSheetLayout.dismissSheet();
                        }
                        if (item.getItemId() == R.id.action_movelist) {
                            if (is_write)
                                moveList(column);
//                            else{
//                                Show_Toast("No write permission,Contact board admin",Toast.LENGTH_LONG);
//                            }
                        } else if (item.getItemId() == R.id.action_copy) {
                            if (is_write)
                                copy(column);

                        } else if (item.getItemId() == R.id.action_moveall_cards) {
                            if (is_write)
                                moveAllcard(column);

                        } else if (item.getItemId() == R.id.action_movefew_card) {
                            if (is_write)
                                moveCard(column, myItemList);

                        } else if (item.getItemId() == R.id.action_archive_card) {
                            if (is_write)
                                archiveAllCard(column, myItemList);

                        } else if (item.getItemId() == R.id.action_archivefew_card) {
                            if (is_write)
                                archiveCard(column, myItemList);

                        } else if (item.getItemId() == R.id.action_settings) {
                            if (is_write) {
                                Intent i = new Intent(getApplicationContext(), ActivitySettings.class);
                                i.putExtra("pos", columnsdata);
                                i.putExtra("owner", ownerId);
                                i.putExtra("userid", userid);
                                i.putExtra("fromshare", fromShare);
                                startActivity(i);
                            }
                        }
//                        if (item.getItemId() == R.id.reopen) {
//                            showMenuSheet(menuType == MenuSheetView.MenuType.LIST ? MenuSheetView.MenuType.GRID : MenuSheetView.MenuType.LIST);
//                        }
                        return true;
                    }
                });
        menuSheetView.inflateMenu(R.menu.menu_mastercard_sheet);
        menuSheetView.setGridItemLayoutRes(R.layout.bottomsheetmenu_row);
        bottomSheetLayout.showWithSheetView(menuSheetView);
    }

    private void showMainSheetMenu() {
        MenuSheetView menuSheetView =
                new MenuSheetView(MainActivity.this, MenuSheetView.MenuType.GRID, "", new MenuSheetView.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(MainActivity.this, item.getTitle(), Toast.LENGTH_SHORT).show();
                        if (bottomSheetLayout.isSheetShowing()) {
                            bottomSheetLayout.dismissSheet();
                        }
                        if (item.getItemId() == R.id.action_rearrange) {
                            changeFragment(1);
                            return true;
                        } else if (item.getItemId() == R.id.actionhistory) {
                            Intent i = new Intent(getApplicationContext(), ActivityHistory.class);
                            i.putExtra("bname", type);
                            startActivity(i);
                        } else if (item.getItemId() == R.id.actionboardsetting) {
                            Toast.makeText(MainActivity.this, item.getTitle(), Toast.LENGTH_SHORT).show();
                        } else if (item.getItemId() == R.id.actionshareboard) {
                            changeFragment(4);
                        } else if (item.getItemId() == R.id.actionmember) {
                            changeFragment(3);
                        } else if (item.getItemId() == R.id.actionarchivecard) {
                            Intent i = new Intent(getApplicationContext(), ActivityArchiveCard.class);
                            i.putExtra("bname", type);
                            i.putExtra("coldata", columnsdata);
                            i.putExtra("iswrite", is_write);
                            startActivity(i);
                        } else if (item.getItemId() == R.id.actioncardlables) {
                            changeFragment(2);
                        }
                        return true;
                    }
                });
        menuSheetView.inflateMenu(R.menu.menu_main_sheet);
        menuSheetView.setGridItemLayoutRes(R.layout.bottomsheetmenu_row);
        bottomSheetLayout.showWithSheetView(menuSheetView);
    }

}


