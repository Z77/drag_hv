package com.draghv.com;

/**
 * Created by Payal on 18-Jan-17.
 */
public class CONSTANTS {
    public static String UPDATED_AT="updatedAt";
    public static String BOARD_TITLE="title";
    public static String USER_ID="userId";
    public static String ATTACHMENT_COUNT="attachcount";
    public static String ITEMS_COUNT="itemscount";
    public static String MESSAGE_COUNT="msgcount";
    public static String CARD_TITLE="title";
    public static String CARD_ID="objectId";
    public static String COLOR_LABELS="label";

    public static String CARDSLIST_KEY="cards";
    public static String COLUMNLIST_KEY="view";

}
