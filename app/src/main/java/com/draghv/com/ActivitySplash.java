package com.draghv.com;

import android.*;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsAuthConfig;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import okhttp3.internal.http.StatusLine;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Payal on 07-Mar-17.
 */
public class ActivitySplash extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

//    private static final int RC_SMS_PERM = 122;
    private static final String TWITTER_KEY = "XMguyH8ef1RUf6CCd28hW7BKy";
    private static final String TWITTER_SECRET = "bZJ1cm3OGQWOzrpozNLvwfqnWHkJxCxJtd3i2vDeKbozRrnWV3";
//    private static final String GET_TOKEN = "https://shrouded-beyond-80676.herokuapp.com/web/";
//    private static final String TAG = "FromSplash=>";
//    private FirebaseAuth mAuth;
//    private FirebaseAuth.AuthStateListener mAuthListener;
//    Boolean newUser = false;
//    String token = null;
//    Button start;
    private ProgressBar spin_kit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Crashlytics(), new TwitterCore(authConfig), new Digits());
//
        Parse.enableLocalDatastore(getApplicationContext());
        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId("q3kt6n4tqlakzg8uwl6y62xa86q30erj4i42evcj")
                .clientKey("hd92hrkri02s8fivl3xf1z4y83e20lrrbqc4ziex")
                .server("https://pg-app-2f5vwbdjanbljer03euwcq4h8zjx2d.scalabl.cloud/1/")
                .build());
//
//        mAuth = FirebaseAuth.getInstance();
        spin_kit = (ProgressBar) findViewById(R.id.spin_kit);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new start().execute();
            }
        }, 1500);

//        mAuthListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser user = firebaseAuth.getCurrentUser();
//                if (user != null) {
//                    // User is signed in
//                    //start.setVisibility(View.VISIBLE); //bcoz at that time i have not wrriten Async class start
//                    Log.i("TAG", "Firebase current user id:" + FirebaseAuth.getInstance().getCurrentUser().getUid());
//                    Log.i("TAG", "user object id parse===:" + ParseUser.getCurrentUser().getObjectId());
//                    Log.i("TAG", "onAuthStateChanged:signed_in:" + user.getUid());
//                } else {
//                    // User is signed out
//                    Log.i("TAG", "onAuthStateChanged:signed_out");
//                }
//            }
//        };

//        start = (Button) findViewById(R.id.start);
//        start.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                smsTask();
//            }
//        });

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
////                finish();
//                //twitter digit for getting otp
//                new start().execute();
//                //smsTask();
//            }
//        }, 1500);
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        mAuth.addAuthStateListener(mAuthListener);
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        if (mAuthListener != null) {
//            mAuth.removeAuthStateListener(mAuthListener);
//        }
//    }

//    @AfterPermissionGranted(RC_SMS_PERM)
//    private void smsTask() {
//        String[] perms = {android.Manifest.permission.RECEIVE_SMS, android.Manifest.permission.READ_SMS};
//        if (EasyPermissions.hasPermissions(this, perms)) {
//            // Have permission, do the thing!
//            getStarted();
//        } else {
//            // Ask for one permission
//            getStarted();
//            EasyPermissions.requestPermissions(this, getString(R.string.rationale_sms),
//                    RC_SMS_PERM, perms);
//        }
//    }

//    private void doLogin(final String phone) {
//        //spin_kit.setVisibility(View.VISIBLE);
//        HashMap<String, Object> params = new HashMap<String, Object>();
//        params.put("phoneNumber", phone);
//        ParseCloud.callFunctionInBackground("logInExpress", params, new FunctionCallback<String>() {
//            public void done(String response, ParseException e) {
//                if (e == null) {
//                    String[] separator = response.split(";");
//                    if (separator[1].equals("1"))
//                        newUser = true;
//                    ParseUser.becomeInBackground(separator[0], new LogInCallback() {
//                        @Override
//                        public void done(ParseUser parseUser, ParseException e) {
//                            if (e == null) {
//                                Log.i("TAG", "user id:" + parseUser.getObjectId());
//
//                                startSignIn(parseUser, phone);
//                            } else {
//                                Log.d("Cloud Response", "Exception: " + e);
//                                Toast.makeText(getApplicationContext(),
//                                        "Something went wrong.  Please try again." + e,
//                                        Toast.LENGTH_LONG).show();
//                            }
//                        }
//                    });
//                } else {
//                    Toast.makeText(getApplicationContext(),
//                            "Something went wrong.  Please try again." + e,
//                            Toast.LENGTH_LONG).show();
//                    if (Digits.getSessionManager() != null)
//                        Digits.getSessionManager().clearActiveSession();
//                    getStarted();
//                }
//            }
//        });
//    }

//    public void getStarted() {
//        start.setVisibility(View.VISIBLE);
//        DigitsAuthConfig.Builder digitsAuthConfigBuilder = new DigitsAuthConfig.Builder().withAuthCallBack(new AuthCallback() {
//            @Override
//            public void success(DigitsSession session, String phoneNumber) {
//
//                System.out.println(phoneNumber);
//                Log.i("TAG", "phone number is :" + phoneNumber);
//                Toast.makeText(ActivitySplash.this,
//                        "Authentication Successful for " + phoneNumber, Toast.LENGTH_SHORT).show();
//
//                doLogin(phoneNumber.substring(1, phoneNumber.length()));
//
//            }
//
//            @Override
//            public void failure(DigitsException error) {
//                Toast.makeText(ActivitySplash.this, error.getMessage(),
//                        Toast.LENGTH_SHORT).show();
//                Log.i("TAG", "Digits error :" + error);
//                start.setVisibility(View.GONE);
//            }
//        }).withThemeResId(R.style.Digits_AppTheme);
//        Digits.authenticate(digitsAuthConfigBuilder.build());
//    }

//    private void startSignIn(final ParseUser user, String phonenumber) {
//        if (user.getString(getString(R.string.firebaseToken)) != null || !user.getString(getString(R.string.firebaseToken)).isEmpty()) {
//            //api call for get token
//            Log.i("TAG", "start sign in:" + user.getObjectId());
//            new GetTocken().execute(phonenumber, user.getObjectId());
////            mAuth.signInWithCustomToken(token)
////                    .addOnCompleteListener(ActivitySplash.this, new OnCompleteListener<AuthResult>() {
////                        @Override
////                        public void onComplete(@NonNull final Task<AuthResult> task) {
////                            Log.i(TAG, "TAsk msg" + String.valueOf(task.isSuccessful()));
////                            if (!task.isSuccessful()) {
////                                Log.w(TAG, "signInWithCustomToken", task.getException());
////                                Toast.makeText(ActivitySplash.this, "Authentication failed.",
////                                        Toast.LENGTH_SHORT).show();
////                            } else {
////                                Toast.makeText(getApplicationContext(), "move to profile screen" + user.getObjectId(), Toast.LENGTH_SHORT).show();
////                                Log.i(TAG, "user id is:" + user.getObjectId());
//////                                zobazeUser = new ZobazeUser();
//////                                zobazeUser.setProvider(getString(R.string.custom));
//////                                zobazeUser.setUsername(user.getUsername());
//////                                OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
//////                                    @Override
//////                                    public void idsAvailable(String userId, String registrationId) {
//////                                        zobazeUser.setDevicetype("android");
//////                                        zobazeUser.setPlayerId(userId);
//////                                        zobazeUser.setConnection(getResources().getString(R.string.online));
//////                                        zobazeUser.setObjectId(task.getResult().getUser().getUid());
//////                                        if (newUser)
//////                                            zobazeUser.save();
//////                                        else {
//////                                            zobazeUser.update();
//////                                        }
//////                                        Bundle bundle = new Bundle();
//////                                        Intent i = new Intent(getApplicationContext(), CompleteProfileActivity.class);
//////                                        bundle.putString(getString(R.string.userid), user.getObjectId());
//////                                        bundle.putBoolean("login", true);
//////                                        i.putExtras(bundle);
//////                                        startActivity(i);
//////                                        finish();
//////                                        Log.d(TAG, "signInWithCustomToken:onComplete:" + task.isSuccessful());
//////                                        spin_kit.setVisibility(View.GONE);
//////                                    }
//////                                });
////                            }
////
////                        }
////                    });
//        } else {
//            Toast.makeText(getApplicationContext(), "Something went wrong.  Please try again.", Toast.LENGTH_LONG).show();
//            if (Digits.getSessionManager() != null)
//                Digits.getSessionManager().clearActiveSession();
//            if (ParseUser.getCurrentUser() != null)
//                ParseUser.getCurrentUser().logOut();
//            getStarted();
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        // Handle negative button on click listener
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Let's show a toast
                Toast.makeText(getApplicationContext(), "Settings dialog canceled", Toast.LENGTH_SHORT).show();
            }
        };
    }

//    private class GetTocken extends AsyncTask<String, Void, String> {
//        ProgressDialog progressDialog;
//        String userid;
//
//        @Override
//        protected void onPreExecute() {
//            //super.onPreExecute();
//            Context context = getApplicationContext();
//            // if(!((Activity)context).isFinishing()) {
//            progressDialog = new ProgressDialog(ActivitySplash.this);
//            progressDialog.setMessage("Please wait");
//            progressDialog.setCanceledOnTouchOutside(false);
//            progressDialog.show();
//            //}
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            JSONParser jsonParser = new JSONParser();
//            String data = jsonParser.getJSONFromUrl((GET_TOKEN + params[1]));
//            try {
//                JSONObject object = new JSONObject(data);
//                token = object.getString("key");
//                userid = params[1];
//                Log.i("TAG", "ur tocken is:" + token);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            return token;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            if (progressDialog.isShowing())
//                progressDialog.dismiss();
//            if (result != null && !result.isEmpty()) {
//                mAuth.signInWithCustomToken(result)
//                        .addOnCompleteListener(ActivitySplash.this, new OnCompleteListener<AuthResult>() {
//                            @Override
//                            public void onComplete(@NonNull final Task<AuthResult> task) {
//                                Log.i("TAG", "TAsk msg" + String.valueOf(task.isSuccessful()));
//                                if (!task.isSuccessful()) {
//                                    Log.w("TAG", "signInWithCustomToken", task.getException());
//                                    Toast.makeText(ActivitySplash.this, "Authentication failed.",
//                                            Toast.LENGTH_SHORT).show();
//                                } else {
//                                    Log.i("TAG", "user object id:" + task.getResult().getUser().getUid());
//                                    Bundle bundle = new Bundle();
//                                    Intent i = new Intent(getApplicationContext(), ActivityCompleteProfile.class);
//                                    bundle.putString("userId", userid);
//                                    bundle.putBoolean("login", true);
//                                    i.putExtras(bundle);
//                                    startActivity(i);
//                                    finish();
//                                }
//
//                            }
//                        });
//            } else {
//                Toast.makeText(ActivitySplash.this, "Somethig went wrong.Please try again later.", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }

    public class JSONParser {
        InputStream iStream = null;
        JSONArray jarray = null;
        String json = "";

        public JSONParser() {
        }

        public String getJSONFromUrl(String url) {

            //String final_url = url.replaceAll(" ", "%20");
            String newurl = url;
            Log.i("FINA URL", newurl);
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(newurl);
            try {
                HttpResponse httpresponse = httpclient.execute(httpget);
                HttpEntity httpentity = httpresponse.getEntity();
                return EntityUtils.toString(httpentity);
                //return "";
            } catch (IOException e) {
                e.printStackTrace();
                return "error";
            }
        }
    }

    class start extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... holder) {
            // TODO Auto-generated method stub
            if (FirebaseAuth.getInstance().getCurrentUser() != null && ParseUser.getCurrentUser() != null ) {
//                OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
//                    @Override
//                    public void idsAvailable(String userId, String registrationId) {
//                        zobazeUser = new ZobazeUser();
//                        zobazeUser.getCurrentUserRoot().child("playerId").setValue(userId);
//                        zobazeUser.getCurrentUserRoot().child("a_version").setValue(BuildConfig.VERSION_CODE);
//                        zobazeUser.getCurrentUserRoot().keepSynced(true);
//                    }
//                });
                callHome();
            } else {
                callLanding();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    public void callHome() {
        Intent i = new Intent(getApplicationContext(), ActivityBoardList.class);
        startActivity(i);
        this.finish();
    }

    public void callLanding() {
        //intro screen view pager
        Intent i=new Intent(getApplicationContext(),ActivityLanding.class);
        startActivity(i);
        this.finish();

//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                start.setVisibility(View.VISIBLE);
//                //stuff that updates ui
//            }
//        });

    }
}
