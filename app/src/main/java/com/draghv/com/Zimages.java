package com.draghv.com;

import android.net.Uri;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

/**
 * Created by karthik on 22/07/16.
 */
public class Zimages {
    private StorageReference photoRef;
    private StorageReference mStorageRef;
    private StorageReference storageRef;

    public Zimages() {
        mStorageRef = FirebaseStorage.getInstance().getReference();
         storageRef= FirebaseStorage.getInstance().getReferenceFromUrl("gs://webtest-249d0.appspot.com");
    }

    public UploadTask uploadProfile(Uri fileUri) {
        photoRef = mStorageRef.child("profile").child(fileUri.getLastPathSegment());
       return photoRef.putFile(fileUri);
    }

    public UploadTask uploadThumnail(byte[] fileUri, String name) {
        photoRef = mStorageRef.child("thumb").child(name);
        return photoRef.putBytes(fileUri);
    }

    public UploadTask uploadImage(Uri fileUri) {
        photoRef = mStorageRef.child("image").child(fileUri.getLastPathSegment());
        return photoRef.putFile(fileUri);
    }

    public UploadTask uploadImageTicket(Uri fileUri) {
        photoRef = mStorageRef.child("image_ticket").child(fileUri.getLastPathSegment());
        return photoRef.putFile(fileUri);
    }
    public UploadTask uploadImageBoard(Uri fileUri) {
        photoRef = mStorageRef.child("image_board").child(fileUri.getLastPathSegment());
        return photoRef.putFile(fileUri);
    }
    public UploadTask uploadImageBoardWithName(Uri fileUri,String name) {
        photoRef = mStorageRef.child("image_board").child(name);
        return photoRef.putFile(fileUri);
    }
    public OnSuccessListener deleteImageFromBoard(String fileName,OnSuccessListener successListener){
        StorageReference desertRef = storageRef.child("image_board/" + fileName);
        desertRef.delete().addOnSuccessListener(successListener);
        return successListener;
    }
    public UploadTask uploadFileBoard(Uri fileUri,String fileName){
        photoRef=mStorageRef.child("file_board").child(fileName);
        return photoRef.putFile(fileUri);
    }
    public OnSuccessListener deleteFileFromBoard(String fileName,OnSuccessListener successListener){
        StorageReference desertRef = storageRef.child("file_board/" + fileName);
        desertRef.delete().addOnSuccessListener(successListener);
        return successListener;
    }


}
