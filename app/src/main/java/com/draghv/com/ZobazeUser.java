package com.draghv.com;


import com.draghv.com.Helper.URLs;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by karthik on 29/01/16.
 */
public class ZobazeUser {
    private DatabaseReference small;
    DatabaseReference firebase;
    String username;
    String name;
    String image;
    String location;
    String status;
    String about;
    String gender;
    Map<String, Object> child;
    Map<String, Object> mini;
    List<DataSnapshot> snapshot;
    long updatedAt;
    String objectId;
    long viewcount;
    //ZAuth auth;
    String provider;
    long dob;
    String connection;
    String email;
    String playerId;
    String devicetype;
    String city;
    double lat;
    double lon;
    ParseQuery<ParseUser> query;
    ParseUser parseUser;
    FirebaseUser user;
    boolean callParse = false;
    String USERID;


    public void setValue(String key, Object value) {
        child.put(key, value);
    }

    public ZobazeUser() {
        firebase = FirebaseDatabase.getInstance().getReference().child(URLs.MASTER_URL_USER);
        small = FirebaseDatabase.getInstance().getReference().child(URLs.SHORT);
        child = new HashMap<>();
        mini = new HashMap<>();
        USERID=ParseUser.getCurrentUser().getObjectId();
        //auth = new ZAuth();
        query = ParseUser.getQuery();
    }


    public void save() {
        child.put("createdAt", ServerValue.TIMESTAMP);
        child.put("updatedAt", ServerValue.TIMESTAMP);
        firebase.child(objectId).setValue(child);
        small.child(URLs.USER).child(objectId).setValue(mini);
    }

    public DatabaseReference getCurrentUserRoot() {

        return firebase.child(USERID);
    }

    public DatabaseReference getCurrentUserRootSmall() {

        return small.child(URLs.USER).child(USERID);
    }

    public String userObjectId() {
        return USERID;
    }

    public DatabaseReference.CompletionListener save(DatabaseReference.CompletionListener listener) {
        firebase.push().setValue(child, listener);
        return listener;
    }

    public DatabaseReference.CompletionListener update(DatabaseReference.CompletionListener listener) {
        child.put("updatedAt", ServerValue.TIMESTAMP);
        firebase.child(objectId).updateChildren(child, listener);
        small.child(URLs.USER).child(objectId).updateChildren(mini);
        //callParse();
        return listener;
    }

    public DatabaseReference getROOT() {
        return firebase;
    }

    public DatabaseReference getROOTSMALL() {
        return small.child(URLs.USER);
    }


    public void update() {
        child.put("updatedAt", ServerValue.TIMESTAMP);
        firebase.child(objectId).updateChildren(child);
        small.child(URLs.USER).child(objectId).updateChildren(mini);
//        callParse();
    }

    public void setUsername(String username) {
        mini.put("phone", username);
        child.put("username", username);
        this.username = username;
    }

    public void setName(String name) {
        mini.put("name", name);
        child.put("name", name);
        this.name = name;
    }


    public void setImage(String image) {
        mini.put("image", image);
        child.put("image", image);
    }

    public void setLocation(String location) {
        child.put("location", location);
    }

    public void setStatus(String status) {
        mini.put("status", status);
        child.put("status", status);
    }

    public void setAbout(String about) {
        child.put("about", about);
    }

    public void setGender(String gender) {
        child.put("gender", gender);
    }

    public void setObjectId(String objectId) {
        child.put("objectId", objectId);
        this.objectId = objectId;
    }

    public void setViewcount(long viewcount) {
        child.put("viewcount", viewcount);
    }

    public void setDob(long dob) {
        child.put("dob", dob);
    }

    public void setProvider(String provider) {
        child.put("provider", provider);
    }

    public void setConnection(String connection) {
        child.put("connection", connection);
    }

    public void setEmail(String email) {
        child.put("email", email);
        this.email = email;
    }


    public void setLon(double lon) {
        child.put("lon", lon);
        this.lon = lon;
    }

    public void setLat(double lat) {
        child.put("lat", lat);
        this.lat = lat;
    }

    public void setCity(String city) {
        child.put("city", city);
        this.city = city;
    }

    public void setDevicetype(String devicetype) {
        child.put("devicetype", devicetype);
        this.devicetype = devicetype;
    }

    public void setPlayerId(String playerId) {
        child.put("playerId", playerId);
        this.playerId = playerId;
    }

//    public void callParse() {
//        query.whereEqualTo("username", username);
//        query.findInBackground(new FindCallback<ParseUser>() {
//            @Override
//            public void done(List<ParseUser> objects, ParseException e) {
//                parseUser = objects.get(0);
//                parseUser.put("name", name);
//                parseUser.put("location", new ParseGeoPoint(lat, lon));
//                parseUser.saveEventually();
//            }
//        });
//    }

    public void setCallParse(boolean callParse) {
        this.callParse = callParse;
    }
}
