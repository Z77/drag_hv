package com.draghv.com;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Payal on 20-Feb-17.
 */
public class ActivitySettings extends AppCompatActivity{
    RelativeLayout duedate;
    CheckBox duedate_chk;
    int col_index;
    List<HashMap<String,Object>> columndata;
    String ownerid,userid;
    Boolean isFromShare=false;
    Zboard zboard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
        setContentView(R.layout.activity_settings);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        zboard=new Zboard();
        duedate=(RelativeLayout)findViewById(R.id.duedate_Parent);
        duedate_chk=(CheckBox)findViewById(R.id.chk_Duedate);

        //columndata=MainActivity.columnsdata;

        Bundle b=getIntent().getExtras();
        if(b.get("pos")!=null){
            columndata= (List<HashMap<String, Object>>) b.get("pos");
        }
        if(b.get("fromshare")!=null){
            isFromShare=b.getBoolean("fromshare");
        }
        if(b.get("owner")!=null){
            ownerid=b.getString("owner");
        }
        if(b.get("userid")!=null){
            userid=b.getString("userid");
        }
        Boolean isduedateCheck= (Boolean) columndata.get(col_index).get("autoduedate");
        duedate_chk.setChecked(isduedateCheck);

        duedate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(duedate_chk.isChecked()) {
                    duedate_chk.setChecked(false);
                    columndata.get(col_index).put("autoduedate", false);
                    changeDataInFirebase();

                }else {
                    duedate_chk.setChecked(true);
                    columndata.get(col_index).put("autoduedate", true);
                    changeDataInFirebase();
                }
            }
        });
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void changeDataInFirebase() {
        //BOARD_.setTotalCol((ArrayList<HashMap<String, Object>>) columnsdata);
        if(isFromShare){
            zboard.upadateCOL_Board(ownerid,MainActivity.BOARD_ID, columndata, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    Toast.makeText(getApplicationContext(), "due date auto is saved", Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            zboard.upadateCOL_Board(userid,MainActivity.BOARD_ID, columndata, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    Toast.makeText(getApplicationContext(), "due date auto is saved", Toast.LENGTH_SHORT).show();
                }
            });
        }

        MainActivity.adapterList.get(col_index).notifyDataSetChanged();
    }
}
