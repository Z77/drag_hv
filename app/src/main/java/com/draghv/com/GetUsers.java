package com.draghv.com;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by karthik on 30/01/16.
 */
@IgnoreExtraProperties
public class GetUsers {
    String username;
    String name;
    String image;
    String city;
    String status;
    String about;
    String gender;
    long updatedAt;
    String objectId;
    long viewcount;
    String provider;
    long dob;
    String connection;
    String email;
    String playerId;
    long followingcount;
    long count;
    long followercount;
    long followingbusiness;
    int businessCount;
     double lat;
     double lon;


    public GetUsers() {
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getEmail() {
        return email;
    }

    public String getConnection() {
        return connection;
    }

    public long getDob() {
        return dob;
    }

    public String getProvider() {
        return provider;
    }

    public long getViewcount() {
        return viewcount;
    }

    public String getObjectId() {
        return objectId;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public String getGender() {
        return gender;
    }

    public String getAbout() {
        return about;
    }

    public String getStatus() {
        return status;
    }

    public String getCity() {
        return city;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }


    public long getCount() {
        return count;
    }

    public long getFollowingcount() {
        return followingcount;
    }

    public long getFollowercount() {
        return followercount;
    }

    public long getFollowingbusiness() {
        return followingbusiness;
    }

    public int getBusinessCount() {
        return businessCount;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
