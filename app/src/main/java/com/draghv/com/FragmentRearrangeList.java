package com.draghv.com;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 05-Jan-17.
 */
public class FragmentRearrangeList extends Fragment implements CallbackItemTouch{
    RecyclerView listrv;
    rearrangeAdapter list_adapter;
    static ArrayList<String> myHeaderList;
    static List<List<MyItem>> mygrandList;
    static ArrayList<HashMap<String,Object>> columnsData;
    View rootView;
    Button newcol;
    public static Fragment instantiate(List<List<MyItem>> myGrandList,ArrayList<String> headerList,ArrayList<HashMap<String,Object>> colsData) {
        FragmentRearrangeList rearrangeList=new FragmentRearrangeList();
        myHeaderList=headerList;
        mygrandList=myGrandList;
        columnsData=colsData;
        return rearrangeList;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.fragment_rearrange_list,container,false);
        listrv=(RecyclerView)rootView.findViewById(R.id.listname_RV);
        newcol=(Button)rootView.findViewById(R.id.addnewcol);

        LinearLayoutManager llm=new LinearLayoutManager(getActivity().getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listrv.setLayoutManager(llm);

        ItemTouchHelper.Callback callback = new MyItemTouchHelperCallback(this);// create MyItemTouchHelperCallback
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback); // Create ItemTouchHelper and pass with parameter the MyItemTouchHelperCallback
        touchHelper.attachToRecyclerView(listrv); // Attach ItemTouchHelper to RecyclerView

        newcol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialDialog createBoard = new MaterialDialog.Builder(getActivity())
                        .title("Add new column")
                        .customView(R.layout.prompt_rename_header, true)
                        .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                        .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                        .titleColorRes(R.color.colorPrimary)
                        .positiveText("Add").positiveColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary))
                        .negativeText("Cancel").negativeColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                EditText promptTitle=(EditText)dialog.findViewById(R.id.tv_list_title);
                                String name=promptTitle.getText().toString().trim();
                                //list_adapter.addItem(name);
                                myHeaderList.add(name);
                                HashMap<String,Object> colsdata=new HashMap<String, Object>();
                                colsdata.put("title",name);
                                colsdata.put("id",BoardList.createUniqueid());
                                columnsData.add(colsdata);
                                List<MyItem> newitemList=new ArrayList<MyItem>();
                                mygrandList.add(newitemList);

                                list_adapter.notifyDataSetChanged();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                EditText promptTitle=(EditText)createBoard.findViewById(R.id.tv_list_title);
                promptTitle.setHint("column name");
            }
        });
        displaylist(myHeaderList, mygrandList);

        return rootView;
    }

    private void displaylist(ArrayList<String> myHeaderList,List<List<MyItem>> mylist) {
        list_adapter=new rearrangeAdapter(myHeaderList);
        listrv.setAdapter(list_adapter);
        Log.i("TAG", "grand listitems display list:" + mylist);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        //super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_rearrange);
        MenuItem id=menu.findItem(R.id.main_menu_group);
        //id.setVisible(false);
        menu.setGroupVisible(R.id.main_menu_group,false);
        //item.setVisible(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_rearrange_save, menu);
        //super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId=item.getItemId();
        if(itemId==android.R.id.home){
            getActivity().onBackPressed();
        }
        if(itemId==R.id.done){
            try {
                ((MainActivity) getActivity()).ClearBoardAddColumn(mygrandList, myHeaderList);
                ((MainActivity)getActivity()).changeDataInFirebase(columnsData);
            }catch (ClassCastException e){
                ((NewBoard)getActivity()).ClearBoardAddColumn(mygrandList,myHeaderList);
            }
            getActivity().onBackPressed();
            Toast.makeText(getActivity().getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void itemTouchOnMove(int oldPosition, int newPosition) {
        Collections.swap(myHeaderList, oldPosition, newPosition);
        Collections.swap(columnsData,oldPosition,newPosition);
        list_adapter.notifyItemMoved(oldPosition, newPosition); //notify changes in adapter
        Collections.swap(mygrandList, oldPosition, newPosition);
    }
}

