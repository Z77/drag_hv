package com.draghv.com;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Payal on 16-Jan-17.
 */
public class ItemDetail extends Fragment {
    static int pos;
    static int row;
    static MyItem itemdata;
    Button settitle;
    EditText title;
    View rootView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_itemdetail, container, false);
        title=(EditText)rootView.findViewById(R.id.item_title_D);
        settitle=(Button)rootView.findViewById(R.id.btn_settitle);
        title.setText(itemdata.getData());
        settitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String new_title;
                if(title.getText().toString().length()>0)
                    new_title=title.getText().toString();
                else
                    new_title=itemdata.getData();
                try {
                    ((MainActivity) getActivity()).adapterList.get(pos).changeData(new_title, row);
                } catch (ClassCastException e) {
                    ((NewBoard) getActivity()).adapterList.get(pos).changeData(new_title, row);
                }
                getActivity().onBackPressed();
            }
        });
        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Log.i("TAG", "pos col:" + pos);
            Log.i("TAG", "pos row:" + row);
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public static Fragment instantiate(int position, int row_, MyItem itemData) {
        ItemDetail itemDetail = new ItemDetail();
        pos = position;
        row = row_;
        itemdata=itemData;
        Log.i("TAG", "positin is" + position);
        return itemDetail;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.setGroupVisible(R.id.main_menu_group,false);
        //super.onPrepareOptionsMenu(menu);
    }
}
