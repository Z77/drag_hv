package com.draghv.com;

import java.util.List;

/**
 * Created by Payal on 28-Dec-16.
 */
public class MyItem {

    String data;
    int image_attach;
    long itemid;
    String objectId;
    String createdDate;
    Long messagecnt;
    Long sharePersoncnt;
    Boolean attachimageAvailale;
    Boolean attachitemAvailale;
    List<String> colorcodes;
    Long imageAttachCnt;
    Long itemAttachCnt;


    public MyItem(long itemid,
                  String objectId,
                  String data,
                  int image,
                  String createdDate,
                  Long messagecnt,
                  Long sharePersoncnt,
                  Boolean attachimageAvailale,
                  Boolean attachitemAvailale,
                  List<String> colorcodes,
                  Long imageAttachCnt,
                  Long itemAttachCnt) {
        this.data = data;
        this.objectId=objectId;
        this.image_attach = image;
        this.itemid=itemid;
        this.createdDate=createdDate;
        this.messagecnt = messagecnt;
        this.sharePersoncnt = sharePersoncnt;
        this.attachimageAvailale = attachimageAvailale;
        this.attachitemAvailale = attachitemAvailale;
        this.colorcodes = colorcodes;
        this.imageAttachCnt = imageAttachCnt;
        this.itemAttachCnt = itemAttachCnt;
    }

    public long getItemid() {
        return itemid;
    }

    public void setItemid(long itemid) {
        this.itemid = itemid;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getimage_attach() {
        return image_attach;
    }

    public void setimage_attach(int image_attach) {
        this.image_attach = image_attach;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Long getMessagecnt() {
        return messagecnt;
    }

    public void setMessagecnt(Long messagecnt) {
        this.messagecnt = messagecnt;
    }

    public Long getSharePersoncnt() {
        return sharePersoncnt;
    }

    public void setSharePersoncnt(Long sharePersoncnt) {
        this.sharePersoncnt = sharePersoncnt;
    }

    public Boolean getAttachimageAvailale() {
        return attachimageAvailale;
    }

    public void setAttachimageAvailale(Boolean attachimageAvailale) {
        this.attachimageAvailale = attachimageAvailale;
    }

    public Boolean getAttachitemAvailale() {
        return attachitemAvailale;
    }

    public void setAttachitemAvailale(Boolean attachitemAvailale) {
        this.attachitemAvailale = attachitemAvailale;
    }

    public List<String> getColorcodes() {
        return colorcodes;
    }

    public void setColorcodes(List<String> colorcodes) {
        this.colorcodes = colorcodes;
    }

    public Long getImageAttachCnt() {
        return imageAttachCnt;
    }

    public void setImageAttachCnt(Long imageAttachCnt) {
        this.imageAttachCnt = imageAttachCnt;
    }

    public Long getItemAttachCnt() {
        return itemAttachCnt;
    }

    public void setItemAttachCnt(Long itemAttachCnt) {
        this.itemAttachCnt = itemAttachCnt;
    }
}
