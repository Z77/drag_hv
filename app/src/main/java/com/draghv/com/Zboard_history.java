package com.draghv.com;

import android.util.Log;

import com.draghv.com.Models.MyItem;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.sql.Struct;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 17-Feb-17.
 */
public class Zboard_history {
    DatabaseReference firebase_history;

    public Zboard_history() {
        firebase_history = FirebaseDatabase.getInstance().getReference().child("board_history");
    }

    public DatabaseReference getBoardHistoryROOT() {
        return firebase_history;
    }

    public DatabaseReference getBoardHistory(String boardId) {
        return firebase_history.child(boardId);
    }

    public DatabaseReference.CompletionListener addHistory(String boardId, HashMap<String, Object> carddata, DatabaseReference.CompletionListener completionListener) {
        String key = firebase_history.child(boardId).push().getKey();
        firebase_history.child(boardId).child(key).setValue(carddata);
        return completionListener;
    }

    public HashMap<String, Object> getJSON_AddCardInHistory(MyItem item, String userid, String columnid,String columnTitle) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "add card");
        data.put("userId", userid);
        data.put("cardId", item.getObjectId());
        data.put("cardTitle", item.getData());
        data.put("columnId", columnid);
        data.put("columnTitle",columnTitle);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_AssignCardInHistory(MyItem item, String userid) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "add assign");
        data.put("userId", userid);
        data.put("cardId", item.getObjectId());
        data.put("cardTitle", item.getData());
        data.put("assign", item.getAssign());
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_RemoveAssignCard(List<String> userlist,String cardid,String cardtitle, String userid) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "remove assign");
        data.put("userId", userid);
        data.put("cardId",cardid);
        data.put("cardTitle", cardtitle);
        data.put("assign", userlist);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_DuedateInHistory(MyItem item, String userid) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "add duedate");
        data.put("userId", userid);
        data.put("cardId", item.getObjectId());
        data.put("cardTitle", item.getData());
        data.put("duedate", item.getdueDate());
        data.put("createdAt", ServerValue.TIMESTAMP);
        Log.i("TAG", "duedate:" + data);
        return data;

    }

    public HashMap<String, Object> getJSON_ChangeTitle(MyItem item, String oldtile, String userid) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "change title");
        data.put("userId", userid);
        data.put("cardId", item.getObjectId());
        data.put("cardTitle", item.getData());
        data.put("cardoldTitle", oldtile);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_Addmember(List<String> memberlist, String userid, String boardid, String bname) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "add member");
        data.put("userId", userid);
        data.put("member", memberlist);
        data.put("boardId", boardid);
        data.put("boardName", bname);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_Removemember(List<String> memberlist, String userid, String bid, String bname) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "remove member");
        data.put("userId", userid);
        data.put("member", memberlist);
        data.put("boardId", bid);
        data.put("boardName", bname);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_Addlabel(List<String> label, String userid, String bid, String bname) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "add label");
        data.put("userId", userid);
        data.put("label", label);
        data.put("boardId", bid);
        data.put("boardName", bname);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_Removelabel(List<String> label, String userid, String bid, String bname) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "remove label");
        data.put("userId", userid);
        data.put("label", label);
        data.put("boardId", bid);
        data.put("boardName", bname);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_Editlabel(List<String> label, List<String> oldtitle, List<String> newtitle, String userid, String bid, String bname) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "edit label");
        data.put("userId", userid);
        data.put("label", label);
        data.put("oldTitle", oldtitle);
        data.put("labelTitle", newtitle);
        data.put("boardId", bid);
        data.put("boardName", bname);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_addCardLabel(MyItem item, List<String> added_, String userid) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "add clabel");
        data.put("userId", userid);
        if (added_ != null)
            data.put("label", added_);
        else
            data.put("label", item.getColorcodes());
        data.put("cardId", item.getObjectId());
        data.put("cardTitle", item.getData());
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_removeCardLabel(MyItem item, List<String> removed, String userid) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "remove clabel");
        data.put("userId", userid);
        data.put("label", removed);
        data.put("cardId", item.getObjectId());
        data.put("cardTitle", item.getData());
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_addAttchment(String list, String userid, String cid, String ctitle, String attchtype) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "add attchment");
        data.put("cardId", cid);
        data.put("cardTitle", ctitle);
        data.put("userId", userid);
        data.put("attach_type", attchtype);
        data.put("attach", list);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String, Object> getJSON_removeAttchment(String list, String userid, String cid, String ctitle, String attchtype) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("action", "remove attchment");
        data.put("cardId", cid);
        data.put("cardTitle", ctitle);
        data.put("userId", userid);
        data.put("attach_type", attchtype);
        data.put("attach", list);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String,Object> getJSON_addChecklist(MyItem item,String userid,String added,List<HashMap<String,Object>> update){
        HashMap<String, Object> data = new HashMap<>();
        if(added.equals("added"))
            data.put("action", "add checklist");
        else
            data.put("action", "updated checklist");
        data.put("cardId", item.getObjectId());
        data.put("cardTitle", item.getData());
        data.put("userId", userid);
        if(update!=null)
            data.put("chkarray",update);
        else
            data.put("chkarray",item.getChecklist());
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String,Object> getJSON_checkedChecklist(String title,String userid,String cardtitle,String cardid,Boolean isChecked){
        HashMap<String, Object> data = new HashMap<>();
        if(isChecked)
            data.put("action","checked chklist");
        else
            data.put("action","uncheck chklist");
        data.put("cardId",cardid);
        data.put("cardTitle",cardtitle);
        data.put("userId", userid);
        data.put("checked",title);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String,Object>getJSON_DeleteColumn(String col_name,String col_id,String userid){
        HashMap<String, Object> data = new HashMap<>();
        data.put("action","delete column");
        data.put("userId", userid);
        data.put("columnId",col_id);
        data.put("columnTitle",col_name);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }
    public HashMap<String,Object>getJSON_AddColumn(String col_name,String col_id,String userid){
        HashMap<String, Object> data = new HashMap<>();
        data.put("action","add column");
        data.put("userId", userid);
        data.put("columnId",col_id);
        data.put("columnTitle",col_name);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }
    public HashMap<String ,Object>getJSON_EditColumn(String col_id,String oldname,String newname,String userid){
        HashMap<String, Object> data = new HashMap<>();
        data.put("action","change columntitle");
        data.put("userId", userid);
        data.put("columnId",col_id);
        data.put("columnTitle",oldname);
        data.put("columnoldTitle",newname);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }

    public HashMap<String,Object>getJSON_MoveCard(int count,String userid,String colid,String colname,String oldcolname,String bid,String bname,Boolean sameBoard){
        HashMap<String, Object> data = new HashMap<>();
        if(sameBoard)
            data.put("action","move card");
        else
            data.put("action","transfer card");
        data.put("userId", userid);
        data.put("columnId",colid);//new columnid
        data.put("columnTitle",colname);
        data.put("columnoldTitle",oldcolname);
        data.put("moved",count);
        data.put("boardId", bid);
        data.put("boardName", bname);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }
    public HashMap<String,Object> getJSON_ArchiveCard(int count,String colid,String colname,String bid,String bname,String userid){
        HashMap<String, Object> data = new HashMap<>();
        data.put("action","archive card");
        data.put("userId",userid);
        data.put("columnId",colid);
        data.put("columnTitle",colname);
        data.put("moved",count);
        data.put("boardId", bid);
        data.put("boardName", bname);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }
    public HashMap<String,Object> getJSON_CopyCard(int count,String source_colname,String dest_colname,String dest_colid,String userid,Boolean sameboard,String source_bname){
        HashMap<String, Object> data = new HashMap<>();
        if(sameboard)
            data.put("action","copy card");
        else
            data.put("action","copy card otherboard");

        data.put("userId",userid);
        data.put("columnId",dest_colid);
        data.put("columnTitle",dest_colname);
        data.put("columnoldTitle",source_colname);
        data.put("moved",count);
        data.put("boardName", source_bname);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }
    public HashMap<String,Object> getJSON_MoveList(String colid,String colname,String destbid,String destbname,String userid){
        HashMap<String, Object> data = new HashMap<>();
        data.put("action","move column");
        data.put("userId",userid);
        data.put("columnId",colid);
        data.put("columnTitle",colname);
        data.put("boardId",destbid);
        data.put("boardName", destbname);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }
    public HashMap<String,Object> getJSON_DragCard(String cardtitle,String cardid,String fromcolname,String tocolname,String tocolid,String userid){
        HashMap<String, Object> data = new HashMap<>();
        data.put("action","drag card");
        data.put("userId",userid);
        data.put("cardTitle",cardtitle);
        data.put("cardId",cardid);
        data.put("columnId",tocolid);
        data.put("columnTitle",tocolname);
        data.put("columnoldTitle",fromcolname);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }
    public HashMap<String,Object> getJSON_CardDesc(String cardid,String cardtitle,String userid){
        HashMap<String, Object> data = new HashMap<>();
        data.put("action","add card desc");
        data.put("userId",userid);
        data.put("cardTitle",cardtitle);
        data.put("cardId",cardid);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }
    public HashMap<String,Object> getJSON_CardItem(String count,String businessname,String cardid,String cardname,String userid,String action){
        HashMap<String, Object> data = new HashMap<>();
        if(action.equalsIgnoreCase("add"))
            data.put("action","add card item");
        else if(action.equalsIgnoreCase("update"))
            data.put("action","update card item");
        else
            data.put("action","delete card item");
        data.put("userId",userid);
        data.put("cardId",cardid);
        data.put("cardTitle",cardname);
        data.put("businessname",businessname);
        data.put("itemcount",count);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }
    public HashMap<String,Object> getJSON_ChangeDuedate(HashMap<String,Object> datemap,String cardid,String cardtitle,String userid){
        HashMap<String, Object> data = new HashMap<>();
        data.put("action","change duedate");
        data.put("userId",userid);
        data.put("cardId",cardid);
        data.put("cardTitle",cardtitle);
        data.put("duedate",datemap);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }
    public HashMap<String,Object> getJSON_comment(Boolean enable,String cardid,String cardtitle,String userid){
        HashMap<String, Object> data = new HashMap<>();
        data.put("action","comment");
        data.put("userId",userid);
        data.put("cardId",cardid);
        data.put("cardTitle",cardtitle);
        data.put("comment",enable);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }
    public HashMap<String,Object> getJSON_Unarchive(String userid,String cardid,String cardtitle){
        HashMap<String, Object> data = new HashMap<>();
        data.put("action","unarchive card");
        data.put("userId",userid);
        data.put("cardId",cardid);
        data.put("cardTitle",cardtitle);
        data.put("createdAt", ServerValue.TIMESTAMP);
        return data;
    }



}
