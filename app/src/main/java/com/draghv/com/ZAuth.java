package com.draghv.com;

import com.draghv.com.Helper.URLs;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.parse.ParseUser;


/**
 * Created by karthik on 21/02/16.
 */
public class ZAuth {
    FirebaseUser user;
    DatabaseReference firebase;
    public ZAuth() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        firebase = FirebaseDatabase.getInstance().getReference().child(URLs.MASTER_URL_USER).child(user.getUid());
        firebase.keepSynced(true);
    }
    public String get(){
        return user.getUid();
    }
//    public String get(){
//        return ParseUser.getCurrentUser().getObjectId();
//    }
    public DatabaseReference getUserImage(){
        return firebase.child("image");
    }

    public DatabaseReference getUserName(){
        return firebase.child("name");
    }

    public DatabaseReference getPlayerId(){
        return firebase.child("playerId");
    }

    public DatabaseReference getRootOther(){
        return FirebaseDatabase.getInstance().getReference().child(URLs.MASTER_URL_USER);
    }

    public DatabaseReference getRoot(){
        return firebase;
    }
}
