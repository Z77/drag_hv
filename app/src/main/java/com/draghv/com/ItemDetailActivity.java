package com.draghv.com;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Payal on 16-Jan-17.
 */
public class ItemDetailActivity extends AppCompatActivity {
//    Button settitle;
//    EditText title;

    static int pos;
    static int row;
    static MyItem itemdata;
    Toolbar toolbar;
    String from, lastBsinessId;
    FloatingActionButton addmore;
    TextView tv_card_title;
    TextView tv_card_desc;
    CardView card_title, card_desc, card_assign, card_items, card_attach, card_lables;
    List<HashMap<String, Object>> businesslist = new ArrayList<>();
    List<String> businessTitleList = new ArrayList<>();
    LinearLayout itemlist;
    List<String> businessIdList, categoryIdList, itemIdList;
    String lastbusinessId;


    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);
        setContentView(R.layout.activity_itemdetail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        pos = Integer.parseInt(getIntent().getStringExtra("col"));
        row = Integer.parseInt(getIntent().getStringExtra("row"));
        from = getIntent().getStringExtra("from");
        itemdata = MainActivity.clickeditem_data;
        toolbarTitle.setText(itemdata.getData());
        if (itemdata == null)
            itemdata = NewBoard.clickeditem_data;

        Log.i("TAG", "data:" + itemdata);

        card_title = (CardView) findViewById(R.id.card_title);
        card_desc = (CardView) findViewById(R.id.card_desc);
        card_assign = (CardView) findViewById(R.id.card_assign);
        card_items = (CardView) findViewById(R.id.card_item);
        card_attach = (CardView) findViewById(R.id.card_attach);
        card_lables = (CardView) findViewById(R.id.card_lable);

        tv_card_title = (TextView) findViewById(R.id.tv_card_title);
        tv_card_desc = (TextView) findViewById(R.id.tv_card_description);
        addmore = (FloatingActionButton) findViewById(R.id.fab_card_detail);
        tv_card_title.setText(itemdata.getData());

        card_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog title = new MaterialDialog.Builder(ItemDetailActivity.this)
                        .title("Card title")
                        .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                        .titleColorRes(R.color.colorPrimary)
                        .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                        .customView(R.layout.prompt_rename_header, true)
                        .positiveText("Ok").positiveColor(ContextCompat.getColor(ItemDetailActivity.this, R.color.colorPrimary))
                        .negativeText("Cancel").negativeColor(ContextCompat.getColor(ItemDetailActivity.this, R.color.colorPrimary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                final EditText tv_title = (EditText) dialog.findViewById(R.id.tv_list_title);
                                if (tv_title.getText().toString().length() > 0) {
                                    tv_card_title.setText(tv_title.getText().toString().trim());
                                    MainActivity.adapterList.get(pos).changeData(tv_title.getText().toString().trim(), row);
                                    MainActivity.updateDataInFirebase(tv_title.getText().toString().trim(), pos, row);
                                }
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
        card_desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog title = new MaterialDialog.Builder(ItemDetailActivity.this)
                        .title("Card description")
                        .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
                        .titleColorRes(R.color.colorPrimary)
                        .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                        .customView(R.layout.prompt_rename_header, true)
                        .positiveText("Ok").positiveColor(ContextCompat.getColor(ItemDetailActivity.this, R.color.colorPrimary))
                        .negativeText("Cancel").negativeColor(ContextCompat.getColor(ItemDetailActivity.this, R.color.colorPrimary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                final EditText tv_title = (EditText) dialog.findViewById(R.id.tv_list_title);

                                if (tv_title.getText().toString().length() > 0) {
                                    tv_card_desc.setText(tv_title.getText().toString().trim());
                                }
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        //get all business data of user
        setUpBusinessData();


//        settitle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String new_title;
//                if (title.getText().toString().length() > 0)
//                    new_title = title.getText().toString();
//                else
//                    new_title = itemdata.getData();
//
//                Log.i("TAG", "adapter size:" + MainActivity.adapterList.size());
//                if(from.equalsIgnoreCase("created")) {
//                    MainActivity.adapterList.get(pos).changeData(new_title, row);
//
//                    MainActivity.updateDataInFirebase(new_title, pos, row);
//                }else {
//                    NewBoard.adapterList.get(pos).changeData(new_title, row);
//                }
//
//                finish();
//                MainActivity.clickeditem_data = null;
//                NewBoard.clickeditem_data = null;
//                Log.i("TAG", "pos col:" + pos);
//                Log.i("TAG", "pos row:" + row);
//            }
//        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_card_detail, menu);
        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Log.i("TAG", "pos col:" + pos);
            Log.i("TAG", "pos row:" + row);
            MainActivity.clickeditem_data = null;
            this.finish();
        }
        if (id == R.id.action_addAssign) {
            Toast.makeText(getApplicationContext(), "Add assign", Toast.LENGTH_SHORT).show();
        }
        if (id == R.id.action_addItems) {
            showItemDialog(lastBsinessId);
            Toast.makeText(getApplicationContext(), "Add Items", Toast.LENGTH_SHORT).show();
        }
        if (id == R.id.action_addAttach) {
            card_attach.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "Add Attachments", Toast.LENGTH_SHORT).show();
        }
        if (id == R.id.action_addLables) {
            Toast.makeText(getApplicationContext(), "Add Lables", Toast.LENGTH_SHORT).show();
        }
        if (id == R.id.action_addChecklist) {
            Toast.makeText(getApplicationContext(), "Add Checklist", Toast.LENGTH_SHORT).show();
        }
        if (id == R.id.action_addDuedate) {
            Toast.makeText(getApplicationContext(), "Add Duedate", Toast.LENGTH_SHORT).show();
        }
        if (id == R.id.action_addcomment) {
            Toast.makeText(getApplicationContext(), "Enable comments", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpBusinessData() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("business").child("uRPz0t7TWk");//uRPz0t7TWk is demo static user id
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("TAG", "business data is:" + dataSnapshot);
                businessTitleList.add("---Select business---");
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    HashMap<String, Object> datalist = (HashMap<String, Object>) data.getValue();
                    businesslist.add(datalist);
                    businessTitleList.add((String) datalist.get("title"));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showItemDialog(final String lastBsiness_Id) {

        if (businessTitleList.size() > 0) {
            LayoutInflater localView = LayoutInflater.from(ItemDetailActivity.this);
            View promptsView = localView.inflate(R.layout.activity_add_item, null);

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(promptsView);
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                    ItemDetailActivity.this);
            alertDialogBuilder.setView(promptsView);
            final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.setCanceledOnTouchOutside(false);

            final Spinner spnBusiness = (Spinner) promptsView.findViewById(R.id.spn_business);
            final Button btnsubmit = (Button) promptsView.findViewById(R.id.btn_ok_item);
            Button btncancel = (Button) promptsView.findViewById(R.id.btn_cancel_item);

            btnsubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (spnBusiness.getSelectedItemPosition() > 0) {
                        lastBsinessId = (String) businesslist.get(spnBusiness.getSelectedItemPosition() - 1).get("objectId");
                        Log.i("TAG", "u selected bid is posoi>0:" + lastBsiness_Id);
                    }

                    Log.i("TAG", "u selected bid is:" + lastBsiness_Id);
                    card_items.setVisibility(View.VISIBLE);
                    alertDialog.dismiss();
                }
            });
            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();

//            final MaterialDialog item_Selection = new MaterialDialog.Builder(ItemDetailActivity.this)
//                    .title("Select Item")
//                    .customView(R.layout.activity_add_item, true)
//                    .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
//                    .typeface("OpenSans-Semibold.ttf", "OpenSans-Regular.ttf")
//                    .titleColorRes(R.color.colorPrimary)
//                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                        @Override
//                        public void onClick(MaterialDialog dialog, DialogAction which) {
//                            card_items.setVisibility(View.VISIBLE);
//                        }
//                    })
//                    .onNegative(new MaterialDialog.SingleButtonCallback() {
//                        @Override
//                        public void onClick(MaterialDialog dialog, DialogAction which) {
//
//                        }
//                    })
//                    .show();

            itemlist = (LinearLayout) promptsView.findViewById(R.id.parent_items);
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, businessTitleList); //selected item will look like a spinner set from XML
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnBusiness.setAdapter(spinnerArrayAdapter);
            if (lastBsiness_Id != null) {
                int i = 0;
                for (HashMap<String, Object> businessData : businesslist) {
                    if (businessData.get("objectId").equals(lastBsiness_Id)) {
                        Log.i("TAG", "last selected id:" + lastBsiness_Id);
                        spnBusiness.setSelection(i + 1);
                        break;
                    }
                    i++;

                }
            }
            spnBusiness.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position > 0) {
                        itemlist.setVisibility(View.VISIBLE);
                        String businessId = (String) businesslist.get(spnBusiness.getSelectedItemPosition() - 1).get("objectId");
                        addItems(businessId);
                    } else {
                        itemlist.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } else {
            Toast.makeText(getApplicationContext(), "business is loading \nplease try again or later", Toast.LENGTH_SHORT).show();
        }
    }

    private void addItems(String BusinessId) {
        itemlist.removeAllViews();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("b_items").child(BusinessId.trim());

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        final ImageLoader loader = ImageLoader.getInstance();
        loader.init(config);
        final DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .build();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> itemdetail = (HashMap<String, Object>) dataSnapshot.getValue();
                businessIdList = new ArrayList<String>();
                categoryIdList = new ArrayList<String>();
                itemIdList = new ArrayList<String>();
                if (dataSnapshot.getValue() != null) {
                    for (Object objlist : itemdetail.values()) {
                        HashMap<String, Object> datamap = (HashMap<String, Object>) objlist;
                        View row = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.ticket_builder_items_select_row, null, false);
                        final ImageView thumb = (ImageView) row.findViewById(R.id.iv_imgthumbnail);
                        final ProgressBar progress = (ProgressBar) row.findViewById(R.id.pbar);
                        TextView title = (TextView) row.findViewById(R.id.item_name);
                        CheckBox chk = (CheckBox) row.findViewById(R.id.chk_item);

                        title.setText(datamap.get("title").toString());
                        final String img_url = datamap.get("image").toString();//.replace("image", "thumb");
                        businessIdList.add(datamap.get("businessId").toString());
                        if (datamap.containsKey("categoryId")) {
                            categoryIdList.add(datamap.get("categoryId").toString());
                        } else {
                            categoryIdList.add("");
                        }
                        itemIdList.add(datamap.get("objectId").toString());

                        try {
                            loader.displayImage(img_url, thumb, options, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    Log.i("TAG", "image loading started");
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    Log.i("TAG", "image loading failed");
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    progress.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                    Log.i("TAG", "image loading candelled");
                                }
                            });
                        } catch (Exception e) {
                            Log.i("TAG", "image loading catch");
                            Toast.makeText(getApplicationContext(), "TicketItemDetail image loading prob!", Toast.LENGTH_SHORT).show();
                        }
                        itemlist.addView(row);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No items,Please select another business", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    //    @Override
//    public void onPrepareOptionsMenu(Menu menu) {
//        menu.setGroupVisible(R.id.main_menu_group,false);
//        //super.onPrepareOptionsMenu(menu);
//    }

}
