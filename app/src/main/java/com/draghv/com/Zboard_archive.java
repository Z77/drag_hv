package com.draghv.com;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.Struct;
import java.util.HashMap;

/**
 * Created by Payal on 15-Feb-17.
 */
public class Zboard_archive {
    DatabaseReference firebaseboard_archive;

    public Zboard_archive() {
        firebaseboard_archive= FirebaseDatabase.getInstance().getReference().child("board_archive");
    }
    public DatabaseReference.CompletionListener addCardInArchive(String boardId,String cardid,HashMap<String,Object> cardMap,DatabaseReference.CompletionListener completionListener){
        firebaseboard_archive.child(boardId).child(cardid).setValue(cardMap,completionListener);
        return completionListener;
    }
    public DatabaseReference getBOARD_ARCHIVE_ROOT(){
        return  firebaseboard_archive;
    }

    public DatabaseReference getBOARD_ARCHIVEDATA(String boardId){
        return firebaseboard_archive.child(boardId);
    }
    public DatabaseReference.CompletionListener deleteCardInArchie(String boardId,String cardId,DatabaseReference.CompletionListener completionListener){
        firebaseboard_archive.child(boardId).child(cardId).removeValue(completionListener);
        return completionListener;
    }
}
