package com.draghv.com;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by karthik on 22/07/16.
 */
public class ImageLoader {

    private static final DisplayImageOptions.Builder DEFAULT_DISPLAY_IMAGE_OPTIONS_BUIDLER = new DisplayImageOptions.Builder()
            .imageScaleType(ImageScaleType.EXACTLY)
            .displayer(new FadeInBitmapDisplayer(300, true, false, false))
//            .showImageForEmptyUri(R.drawable.noimagefound)
//            .showImageOnLoading(R.drawable.default_image)
//            .showImageOnFail(R.drawable.default_image)
            .cacheOnDisk(true)
            .cacheInMemory(true).bitmapConfig(Bitmap.Config.RGB_565);

    private static final DisplayImageOptions.Builder DEFAULT_DISPLAY_IMAGE_OPTIONS_BUIDLER_WITHOUT_LOADING = new DisplayImageOptions.Builder()
            .imageScaleType(ImageScaleType.EXACTLY)
            .displayer(new FadeInBitmapDisplayer(300, true, false, false))
                    //.showImageForEmptyUri(R.drawable.noimagefound)
            .cacheInMemory(true).bitmapConfig(Bitmap.Config.RGB_565);


    private static final DisplayImageOptions.Builder DEFAULT_DISPLAY_IMAGE_OPTIONS_BUIDLER_USER = new DisplayImageOptions.Builder()
            .imageScaleType(ImageScaleType.EXACTLY)
            .displayer(new FadeInBitmapDisplayer(300, true, false, false))
//            .showImageForEmptyUri(R.drawable.cp_user)
//            .showImageOnLoading(R.drawable.default_image)
//            .showImageOnFail(R.drawable.cp_user)
            .cacheOnDisk(true)
            .cacheInMemory(true).bitmapConfig(Bitmap.Config.RGB_565);

    private static final DisplayImageOptions DEFAULT_DISPLAY_IMAGE_OPTIONS = DEFAULT_DISPLAY_IMAGE_OPTIONS_BUIDLER
            .build();
    private static final DisplayImageOptions DEFAULT_DISPLAY_IMAGE_OPTIONS_WITHOUT_LOADING = DEFAULT_DISPLAY_IMAGE_OPTIONS_BUIDLER_WITHOUT_LOADING
            .build();
    private static final DisplayImageOptions ROUND_DISPLAY_IMAGE_OPTIONS = DEFAULT_DISPLAY_IMAGE_OPTIONS_BUIDLER
            .displayer(new RoundedBitmapDisplayer(5)).build();

//    private static final DisplayImageOptions ROUND_DISPLAY_IMAGE_OPTIONS_USER = DEFAULT_DISPLAY_IMAGE_OPTIONS_BUIDLER_USER
//            .displayer(new RoundedBitmapDisplayer(5)).showImageForEmptyUri(R.drawable.cp_user).build();
    private static final DisplayImageOptions CIRCLE_DISPLAY_IMAGE_OPTIONS = DEFAULT_DISPLAY_IMAGE_OPTIONS_BUIDLER
            .displayer(new CircleBitmapDisplayer(5)).build();


    public static void defaultLoad(ImageView view, String path, ImageLoadingListener listener) {
        com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        //loader.init(ImageLoaderConfiguration.createDefault(context.getApplicationContext()));
        try {
            loader.displayImage(path, view, DEFAULT_DISPLAY_IMAGE_OPTIONS, listener);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            loader.clearMemoryCache();
        }
    }


    public static void defaultLoadWithoutLoading(ImageView view, String path, ImageLoadingListener listener) {
        com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        //loader.init(ImageLoaderConfiguration.createDefault(context.getApplicationContext()));
        try {
            loader.displayImage(path, view, DEFAULT_DISPLAY_IMAGE_OPTIONS_WITHOUT_LOADING, listener);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            loader.clearMemoryCache();
        }
    }


    public static void CircleLoad(ImageView view, String path, ImageLoadingListener listener) {
        com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        try {
            loader.displayImage(path, view, CIRCLE_DISPLAY_IMAGE_OPTIONS, listener);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            loader.clearMemoryCache();
        }
    }

    public static void userProfileLoad(ImageView view, String path, ImageLoadingListener listener) {
        com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        try {
            //loader.displayImage(path, view, ROUND_DISPLAY_IMAGE_OPTIONS_USER, listener);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            loader.clearMemoryCache();
        }
    }

    public static void RoundLoad(ImageView view, String path, ImageLoadingListener listener) {
        com.nostra13.universalimageloader.core.ImageLoader loader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();

        try {
            loader.displayImage(path, view, ROUND_DISPLAY_IMAGE_OPTIONS, listener);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            loader.clearMemoryCache();
        }
    }
}
